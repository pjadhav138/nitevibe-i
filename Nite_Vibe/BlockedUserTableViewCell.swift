//
//  BlockedUserTableViewCell.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 11/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
//protocol SwiftyTableViewCellDelegate : NSObjectProtocol {
//
//    func swiftyTableViewCellDidTapHeart(_ sender: BlockedUserTableViewCell)
//}

class BlockedUserTableViewCell: UITableViewCell {

    @IBOutlet weak var unblockUserView: UIView!
    @IBOutlet weak var btn_Unblock: UIButton!
    @IBOutlet weak var usersLastMsg: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    var blockeduserId = String()
    //weak var delegate: SwiftyTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        unblockUserView.isHidden = true
        userImage.layer.borderWidth = 1
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.blue.cgColor
        userImage.layer.cornerRadius = self.userImage.frame.size.width/2 //This will change with corners of image and height/2 will make this circle shape
       // userImage.layer.cornerRadius = self.userImage.frame.height/2
        userImage.clipsToBounds = true
        
    }
    @IBAction func btnUnblockAction(_ sender: Any) {
        
        unblockUserView.isHidden = false
    }
    
    @IBAction func unblockUser_Btn(_ sender: Any) {

        unblockUser()
        //delegate?.swiftyTableViewCellDidTapHeart(self)
        

    }
    
    func unblockUser(){
        
        var respo = Int()
        
        URLCache.shared.removeAllCachedResponses()
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("BlockedUserId : \(blockeduserId)")
        
        let URL_ClubString = "http://www.nitevibe.in/webservices/block.php?user_id=\(User_Id!)&block_id=\(blockeduserId)&mode=1"
        
        print("Notification String\(URL_ClubString)")
        do{
            
        let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: escapeUrl!)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON{
            
            
            (response) in
            
            print("alamofire Request: \(String(describing: response.request))")
            
            print(" Notification response :\(String(describing: response.request))")
            
            guard response.result.isSuccess  else {
                
                print("test1")
                
                print("Block 1 ")
                print(" response :\(response)")
                
                print("Error with response: \(String(describing: response.result.error))")
                
                return
                
            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                print("test 2")
                print("response :\(response)")
                
                print("Error with dictionary: \(String(describing: response.result.error))")
                
                return
                
            }
            
            print("dict message:\(dict["message"]!)")
            
            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" Event dict data : \(dictData)")
            
            print("message\(dict)")
            print("dict respo: \(dict["response"]!)")
            print("dict message:\(dict["message"]!)")
            respo = dict["response"]! as! Int
            let responseMessage = dict["message"]
            
            
            print(type(of:responseMessage))
            
            
            if(respo == 201){
                print("Message from Server: \(String(describing: responseMessage))")
                
                let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(alertAction)
                
               // present(alert, animated: true, completion: nil)
                
                
            }else if(respo == 200){
                
                self.unblockUserView.isHidden = true
                
                let alert = UIAlertController(title: ":)", message: responseMessage as! String, preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(alertAction)
                //self.fetchswiftyChatData()
                //present(alert, animated: true, completion: nil)
                
                print("Message from server success")
                
                }
            
            }
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
