//
//  ClubEventListCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 15/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ClubEventListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var clubEventListImageView: UIImageView!
    
    @IBOutlet weak var feventFavouriteButtonTapped: UIButton!
    
    @IBOutlet weak var eventShareButtonTapped: UIButton!
    
    @IBOutlet weak var eventNameLbl: UILabel!
    
    @IBOutlet weak var peopleAtEventLbl: UILabel!
    
    @IBOutlet weak var eventDateLbl: UILabel!
    
    @IBOutlet weak var costOfLbl: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var femaleCountLabel: UILabel!
    @IBOutlet weak var maleCountLabel: UILabel!
    
    
    @IBOutlet weak var femaleImageView: UIImageView!
    
    
    @IBOutlet weak var maleImageView: UIImageView!
    
    
    
    @IBOutlet weak var rupeeImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        femaleCountLabel.isHidden = true
        maleCountLabel.isHidden = true
        eventDateLbl.layer.cornerRadius = 4
        eventDateLbl.clipsToBounds = true
        
    }
    
}
