//
//  CustomTextField.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 04/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        styleTextField()
    }
    private func styleTextField(){
        
        let neon = UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)
        
        let border = CALayer()
        let width = CGFloat(1.5)
        
        border.borderColor = neon.cgColor
        
        
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width + 30, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
}
