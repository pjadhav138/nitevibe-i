//
//  ParentTableFirstCellCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 21/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ParentTableFirstCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
}
