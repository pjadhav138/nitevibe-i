//
//  BlockedUsersViewController.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 11/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher



class BlockedUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbl_BlockedUser: UITableView!
    var username = String()
    var userimage = String()
    var userblockid = String()
    var usernameArray = [String]()
    var userimagearray = [String]()
    var useridarray = [String]()
    //var blockFlagArray = [String]()
    var blockID = String()
    var setblockTimmer = Timer.self
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbl_BlockedUser.delegate = self
        self.tbl_BlockedUser.dataSource = self
        
        getBlockedUsers()
        self.tbl_BlockedUser.reloadData()
        self.setblockTimmer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.getBlockedUsers), userInfo: nil, repeats: true)
       
    }
    
    
    @objc func getBlockedUsers(){
        usernameArray.removeAll()
        useridarray.removeAll()
        userimagearray.removeAll()
        URLCache.shared.removeAllCachedResponses()
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let URL_ClubString = "http://www.nitevibe.in/webservices/block_user_list.php?user_id=\(User_Id!)"
        
        print("Notification String\(URL_ClubString)")
        
        let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: escapeUrl!)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON{
            
            
            (response) in
            
            print("alamofire Request: \(String(describing: response.request))")
            
            print(" Notification response :\(String(describing: response.request))")
            
            guard response.result.isSuccess  else {
                
                print("test1")
                
                print("Block 1 ")
                print(" response :\(response)")
                
                print("Error with response: \(String(describing: response.result.error))")
                
                return
                
            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                print("test 2")
                print("response :\(response)")
                
                print("Error with dictionary: \(String(describing: response.result.error))")
                
                return
                
            }
            
            print("dict message:\(dict["message"]!)")
            
            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" Event dict data : \(dictData)")
            
            
            for data in dictData{
                
                self.userblockid = data["block_id"] as! String
                print("Blocked User Id :\(self.userblockid)")
                self.useridarray.append(self.userblockid)
                print("\(self.useridarray)")
                
                self.username =  data["name"] as! String
                print("Blocked User Name :\(self.username)")
                self.usernameArray.append(self.username)
                print("\(self.usernameArray)")
                
                self.userimage = data["img"] as! String
                print("Blocked User Image :\(self.userimage)")
                self.userimagearray.append(self.userimage)
                print("\(self.userimagearray)")
                
            }
            
            self.tbl_BlockedUser.reloadData()
            
        }
        
        
    }
//    func swiftyTableViewCellDidTapHeart(_ sender: BlockedUserTableViewCell) {
//        guard let tappedIndexPath = tbl_BlockedUser.indexPath(for: sender) else { return }
//        print("Heart", sender, tappedIndexPath)
//
//        // "Love" this item
//        // items[tappedIndexPath.row].love()
//        self.getBlockedUsers()
//
//    }
    
    @IBAction func back_btnAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
        let settingVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        present(settingVC, animated: true, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return useridarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl_BlockedUser.dequeueReusableCell(withIdentifier:"blockCell") as! BlockedUserTableViewCell
       // cell.userImage.layer.cornerRadius = cell.userImage.frame.height/2
        print(" User Name assignd to table :\(username)")
        cell.userName.text = usernameArray[indexPath.row]
        print(" User Name assignd to table :\(userblockid )")
        cell.blockeduserId = useridarray[indexPath.row]
        blockID = useridarray[indexPath.row]
        print("BID : \(blockID)")
        let userimages = userimagearray[indexPath.row]
        print("User Image String \(userimages)")
        let imageURL = URL(string: userimages)
        let escapeUrl = userimages.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        cell.userImage.kf.setImage(with: URL(string: escapeUrl!))
       // cell.btn_Unblock.addTarget(self, action:#selector(unblockUser), for:.touchUpInside)
       // cell.btn_Unblock.addTarget(self, action: #selector(BlockedUsersViewController.unblockUser(_:)), for: .touchUpInside)
        cell.btn_Unblock.tag = indexPath.row
        print("selected button tag \(cell.btn_Unblock.tag)")

        return cell
    }
   
//    @objc func unblockUser(_ sender: Any?){
//
//        var respo = Int()
//
//        URLCache.shared.removeAllCachedResponses()
//
//
//        let getUserData :UserDefaults = UserDefaults.standard
//        let User_Id = getUserData.value(forKey: "userid")
//        print("userid :\(String(describing: User_Id))")
//
//        print("BlockedUserId : \(blockID)")
//
//        let URL_ClubString = "http://www.nitevibe.in/webservices/block.php?user_id=\(User_Id!)&block_id=\(blockID)&mode=1"
//
//        print("Notification String\(URL_ClubString)")
//        do{
//
//            let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//            let url = NSURL(string: escapeUrl!)
//            var urlRequest = URLRequest(url: url! as URL)
//            urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
//
//            Alamofire.request(urlRequest).validate().responseJSON{
//
//
//                (response) in
//
//                print("alamofire Request: \(String(describing: response.request))")
//
//                print(" Notification response :\(String(describing: response.request))")
//
//                guard response.result.isSuccess  else {
//
//                    print("test1")
//
//                    print("Block 1 ")
//                    print(" response :\(response)")
//
//                    print("Error with response: \(String(describing: response.result.error))")
//
//                    return
//
//                }
//                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
//                    print("test 2")
//                    print("response :\(response)")
//
//                    print("Error with dictionary: \(String(describing: response.result.error))")
//
//                    return
//
//                }
//
//                print("dict message:\(dict["message"]!)")
//
//                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
//                    print("test 3")
//                    print("Error with dictionary data: \(String(describing: response.result.error))")
//                    return
//                }
//                print(" Event dict data : \(dictData)")
//
//                print("message\(dict)")
//                print("dict respo: \(dict["response"]!)")
//                print("dict message:\(dict["message"]!)")
//                respo = dict["response"]! as! Int
//                let responseMessage = dict["message"]
//
//
//                print(type(of:responseMessage))
//
//
//                if(respo == 201){
//                    print("Message from Server: \(String(describing: responseMessage))")
//
//                }else if(respo == 200){
//
//                    print("Message from server success")
////                    self.getBlockedUsers()
//
//
//                }
//
//            }
//        }catch {
//
//            // TODO: find a better way to handle error
//            print(error)
//
//        }
//
//    }
    
    

    
}
