//
//  ForgotPass_1VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 30/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
enum ForgotPasswordDataType:Int{
    case Other = 0
    case Number = 1
    case Email = 2
}
class ForgotPass_1VC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var txt_EmailMob: UITextField!
    
    @IBOutlet weak var checkButtonOutlet: UIButton!
     var usernametext = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txt_EmailMob.delegate = self
        
        let image = UIImage(named:"SubmitBtnDisable") as! UIImage
        checkButtonOutlet.setBackgroundImage(image, for: UIControlState.normal)
        checkButtonOutlet.isEnabled = false
       
        
        // Do any additional setup after loading the view.
    }

    @IBAction func textFieldEditingDidChange(_ sender: Any) {
        
        print("TextFieldDidChanged:\(String(describing: txt_EmailMob.text))")
        var dataType = checkDataType(text: txt_EmailMob.text!)
        
        print("DataType = \(dataType)")
        
        if dataType == ForgotPasswordDataType.Email{
            print("true")
            
            let image = UIImage(named:"SubmitBtnEnable") as! UIImage
            checkButtonOutlet.setBackgroundImage(image, for: UIControlState.normal)
            checkButtonOutlet.isEnabled = true
        }else if dataType == ForgotPasswordDataType.Number{
            print("true")
            let image = UIImage(named:"SubmitBtnEnable") as! UIImage
            checkButtonOutlet.setBackgroundImage(image, for: UIControlState.normal)
            checkButtonOutlet.isEnabled = true
           
        }else if dataType == ForgotPasswordDataType.Other{
            print("false")
            let image = UIImage(named:"SubmitBtnDisable") as! UIImage
            checkButtonOutlet.setBackgroundImage(image, for: UIControlState.normal)
            checkButtonOutlet.isEnabled = false
            
            //imageViewCheck.image = UIImage(named:"SubmitBtnDisable")
            // imageViewCheck.image = UIImage(named:"SubmitBtnDisable")
        }
}
    @IBAction func backBtn_action(_ sender: Any) {
        let LogIn_1_VC:LogIn_1_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
        
        present(LogIn_1_VC, animated: true, completion: nil)
    }
    
    @IBAction func submitBtn_action(_ sender: Any) {
        self.sendOtp()
        let ForgotPass_VC:ForgotPass_2VC = storyboard?.instantiateViewController(withIdentifier: "ForgotPass_2VC") as! ForgotPass_2VC
        present(ForgotPass_VC, animated: true, completion: nil)
        let setUsername:UserDefaults = UserDefaults.standard
        
        setUsername.removeObject(forKey: "username")
        print("Username:\(txt_EmailMob.text!)")
        setUsername.set(txt_EmailMob.text, forKey: "username")
        
        
    }
        
      
        
        
        func isValidEmail(email: String) -> Bool {
            
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: txt_EmailMob.text!)
            return result
        }
        
        func isValidPhone(phone: String) -> Bool {
            
            let PHONE_REGEX = "^[7-9][0-9]{9}$"
            
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
            let result =  phoneTest.evaluate(with: txt_EmailMob.text!)
            return result
            
        }
        
        func checkDataType(text:String)->ForgotPasswordDataType{
            
            //if let intVal = Int(text){
            if isValidPhone(phone: text){
                
                return ForgotPasswordDataType.Number
            }else if isValidEmail(email: text){
                return ForgotPasswordDataType.Email
                
            }else{
                
                return ForgotPasswordDataType.Other
            }
            
            
            
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
        }
    
    func sendOtp(){
        var respo = Int()
        usernametext = txt_EmailMob.text!
        let URL_String = "http://www.nitevibe.in/webservices/otp.php?username=\(usernametext)"
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    
                    print("Valid otp response : ",responseMessage!)
                    
                    let setOtpRespnse:UserDefaults = UserDefaults.standard
                    
                    setOtpRespnse.set(responseMessage!, forKey: "forgetpasswordotp")
                    //let textfieldOtp = txt_OTP.text!
                    
                    //if textfieldOtp == responseMessage
                    let alert = UIAlertController(title: ":(", message: responseMessage as? String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 408){
                    
                    
                    let alert = UIAlertController(title: "Yes", message:"Somthing went wrong" , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }
    
   

}
