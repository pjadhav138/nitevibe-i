//
//  DisplayProfileModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 26/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DisplayProfileModel{
    
    var profileImage = String()
    var profileImageStack = [profileImageStackModel]()
    var userName = String()
    var userGender = String()
    var userAge = String()
    var userAgeStatus = String()
    var userLoaction = String()
    var userLoactionSatus = String()
    var userOccupation = String()
    var userPrefAge = String()
    var userPrefGender = String()
    var userBio = String()
    var userDrink = Array<String>()
    var userDrinkStatus = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let profilePhoto = json["profile"] as? String{
            
            self.profileImage = profilePhoto
            
        }
        if let user_name = json["name"] as? String{
            
            self.userName = user_name
            
            
            
        }
        if let user_gender = json["gender"] as? String{
            
            self.userGender = user_gender
            
        }
        if let user_age = json["age"] as? String{
            
            self.userAge = user_age
            
        }
        if let user_age_status = json["age_status"] as? String{
            
            self.userAgeStatus = user_age_status
            
        }
        if let user_location = json["loc"] as? String{
            
            self.userLoaction = user_location
            
        }
        if let user_loaction_status = json["loc_status"] as? String{
            
            self.userLoactionSatus = user_loaction_status
        }
        
        if let user_occupation = json["occupation"] as? String{
            self.userOccupation = user_occupation
        }
        
        if let user_pref_age = json["pref_age"] as? String{
            
            self.userPrefAge = user_pref_age
        }
        
        if let user_pref_gender = json["pref_gender"] as? String{
            self.userPrefGender = user_pref_gender
        }
        
        if let user_bio = json["bio"] as? String{
            
            self.userBio = user_bio
        }
        
        if let user_drink = json["drink"] as? String{
            self.userDrink = user_drink.components(separatedBy: ",")
        }
        
        if let user_drink_status = json["drink_status"] as? String{
            
            self.userDrinkStatus = user_drink_status
        }
        
        
        if let stackImagesData = json["photos"] as? [[String:Any]]{
            
            for stackImages in stackImagesData{
                
                let images = profileImageStackModel(json: stackImages)
                
                self.profileImageStack.append(images)
                
            }
        }
        
    }
    
}


struct profileImageStackModel{
    
    var images = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let stackImage = json["img"] as? String{
            
            self.images = stackImage
        }
    }
    
    
}
