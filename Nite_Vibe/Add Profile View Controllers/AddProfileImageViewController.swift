//
//  AddProfileImageViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 26/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit


class AddProfileImageViewController: UIViewController {
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = false
    var allowResizing: Bool = true
    var allowMoving: Bool = false
    var minimumSize: CGSize = CGSize(width: 60, height: 60)
    var localImage = UIImage()
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var croppingParametersView: UIView!
    @IBOutlet weak var minimumSizeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("print imaage size \(String(describing: imageView.image))")
        
        self.imageView.contentMode = .scaleAspectFit
    }
    
    @IBAction func openCamera(_ sender: Any) {
        let cameraViewController = CameraViewController(croppingParameters: croppingParameters, allowsLibraryAccess: libraryEnabled) { [weak self] image, asset in
            self?.imageView.image = image
            self?.localImage = image!
            self?.dismiss(animated: true, completion: nil)
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
    @IBAction func openLibrary(_ sender: Any) {
        //4)library change.
        let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingParameters) { [weak self] image, asset in
            self?.imageView.image = image
            
            
            self?.localImage = image!
            print("original size \(image?.size)")
            self?.dismiss(animated: true, completion: nil)
        }
        
        present(libraryViewController, animated: true, completion: nil)
    }
    
    @IBAction func libraryChanged(_ sender: Any) {
        libraryEnabled = !libraryEnabled
    }
    
    @IBAction func croppingChanged(_ sender: UISwitch) {
        
        //1st Step Cropping Enable
        croppingEnabled = sender.isOn
        //2nd Show croppingParametersView
        croppingParametersView.isHidden = !sender.isOn
    }
    
    @IBAction func resizingChanged(_ sender: UISwitch) {
        //3.Crop Setting
        //3.a) enable resizable
        allowResizing = sender.isOn
    }
    
    @IBAction func movingChanged(_ sender: UISwitch) {
        
        //3.b) enable moving parameter
        allowMoving = sender.isOn
    }
    
    @IBAction func minimumSizeChanged(_ sender: UISlider) {
        //3.c) Minimum size change
        let newValue = sender.value
        minimumSize = CGSize(width: CGFloat(newValue), height: CGFloat(newValue))
        minimumSizeLabel.text = "Minimum size: \(newValue.rounded())"
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let CreateProfileVC  = storyboard.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
        
        CreateProfileVC.selectedPhoto = localImage
        self.present(CreateProfileVC, animated: true, completion: nil)
        
        
    }
    
}
