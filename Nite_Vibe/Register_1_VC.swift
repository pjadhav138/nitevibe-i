//
//  Register_1_VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 29/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore
enum UsernameDataType:Int{
    case Other = 0
    case Number = 1
    case Email = 2
}
class Register_1_VC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var txt_EmailMob: UITextField!
    @IBOutlet weak var txt_OTP: UITextField!
    @IBOutlet weak var lbl_OTPTime: UILabel!
    
    @IBOutlet weak var sendOtpOutlet: UIButton!
    @IBOutlet weak var enterPasswordTextField: UITextField!
    
    @IBOutlet weak var enterOtpLabel: UILabel!
    @IBOutlet weak var createAccountOutlet: UIButton!
    @IBOutlet weak var reenterPasswordTextField: UITextField!
    
    @IBOutlet weak var resendOtpbuttonOutlet: UIButton!
    var username = String()
    var userEnterPasswordString = String()
    var userReEnterPasswordString = String()
  
   var seconds = 60
    var secondsForResend = 60
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txt_EmailMob.delegate = self
        self.txt_OTP.delegate = self
        self.enterPasswordTextField.delegate = self
        self.reenterPasswordTextField.delegate = self
        
        sendOtpOutlet.isEnabled = false
        lbl_OTPTime.isHidden = true
        createAccountOutlet.layer.cornerRadius = 10
        createAccountOutlet.clipsToBounds = true
        
        txt_OTP.isHidden = true
       // enterOtpLabel.isHidden = true
        resendOtpbuttonOutlet.isHidden  = true
        txt_EmailMob.attributedPlaceholder = NSAttributedString(string: "Enter your mobile /email ",attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)])
        txt_OTP.attributedPlaceholder = NSAttributedString(string: "Enter your otp",attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)])
        
        enterPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Enter your password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)])
        
        reenterPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Re-enter your password",attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)])
    }
    
    @IBAction func textFieldEditingDidChanged(_ sender: Any) {
        print("TextFieldDidChanged:\(String(describing: txt_EmailMob.text))")
        var dataType = checkDataType(text: txt_EmailMob.text!)
        
        
        print("DataType = \(dataType)")
        
        
        if dataType == UsernameDataType.Email{
            print("true")
            
            sendOtpOutlet.isEnabled = true
            //let setUserDefault
            username = txt_EmailMob.text!
            
            
        }
        
        else if dataType == UsernameDataType.Number{
            print("true")
             sendOtpOutlet.isEnabled = true
            
            username = txt_EmailMob.text!
        }
        else if dataType == UsernameDataType.Other{
            
            print("false")
              sendOtpOutlet.isEnabled = false
            
            //let alert = UIAlertController(title: "Oops", message: "Please enter valid email or mobile number", preferredStyle: .alert)
            202
            

            //let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
           // alert.addAction(alertAction)
            
            //self.present(alert, animated: true, completion: nil)
        }
    }
    func sendOtp(){
        
        
        
        var respo = Int()
        username = txt_EmailMob.text!
        let URL_String = "http://www.nitevibe.in/webservices/otp.php?username=\(username)"
       
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    
                    print("Valid otp response : ",responseMessage!)
                    
                    let setOtpRespnse:UserDefaults = UserDefaults.standard
                    
                    setOtpRespnse.set(responseMessage!, forKey: "signupotp")
                    //let textfieldOtp = txt_OTP.text!
                    
                    //if textfieldOtp == responseMessage
//                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
//
//                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                    alert.addAction(alertAction)
//
//                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 408){
                    
                    
                    let alert = UIAlertController(title: "Yes", message:"Somthing went wrong" , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
        
        
    }

    

    
    @IBAction func sendOTPBtn_action(_ sender: Any) {
        if(txt_EmailMob.text == ""){
            
            let alert = UIAlertController(title: "", message: "Enter Email or Mobile Number", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            sendOtpOutlet.isHidden = true
            txt_OTP.isHidden = false
           // enterOtpLabel.isHidden = false
            
            
            resendOtpbuttonOutlet.isHidden = false
            self.sendOtp()
            
            print("Send Successfully")
            print("Timer for 1 minutes")
            lbl_OTPTime.isHidden = false
            self.startTimer()
//            _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(UIMenuController.update), userInfo: nil, repeats: true)
           
        }
    }
  
    func startTimer(){
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Register_1_VC.Counter), userInfo: nil, repeats: true)
    }
    
    @objc func Counter(){
        seconds -= 1
        lbl_OTPTime.text = ("OTP Valid for 30 Seconds:\(String(seconds))")
        
        print("Print counter : \(String(seconds))")
        
        if (seconds == 0){
            
            timer.invalidate()
            lbl_OTPTime.text = "Please Resend OTP"
            
        }
        
    }
    
    
    
    
    
    
    @IBAction func resendOTPBtn_action(_ sender: Any) {
        self.sendOtp()
        print("ReSend Successfully")
        print("Timer for 1 minutes")
        lbl_OTPTime.isHidden = false
        self.startResendTimer()
    }
   
    
    func startResendTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Register_1_VC.CounterResend), userInfo: nil, repeats: true)
    }
    
    @objc func CounterResend(){
        secondsForResend -= 1
        lbl_OTPTime.text = ("OTP Valid for 30 Seconds:\(String(secondsForResend))")
        
        print("Print counter : \(String(secondsForResend))")
        
        if secondsForResend == 0{
            
            timer.invalidate()
            lbl_OTPTime.text = "Please Resend OTP"
            
        }
        
    }
    
    @IBAction func backBtn_action(_ sender: Any) {
        let LogIn1VC:LogIn_1_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
        present(LogIn1VC, animated: true, completion: nil)
    }
   
    
    
    @IBAction func createAccBtn_action(_ sender: Any) {
        var respo = Int()
    
        var  otpTextField = String()
            otpTextField = txt_OTP.text!
    
        let getResponseOtp:UserDefaults = UserDefaults.standard
        //(String(describing:getResponseOtp.value(forKey: "signupotp")))
        //Handle NSFCNumber error by using (String(describing:getResponseOtp.value(forKey: "signupotp")))
        let responseOtpMessage = (String(describing:getResponseOtp.value(forKey: "signupotp")!))
        
        print("textfield otp: \(otpTextField)")
        print("response otp:\(responseOtpMessage)")
        
        if otpTextField == responseOtpMessage{
            
            userEnterPasswordString = enterPasswordTextField.text!
                        userReEnterPasswordString = reenterPasswordTextField.text!
            
                        if(userEnterPasswordString == userReEnterPasswordString){
            
                            userReEnterPasswordString = reenterPasswordTextField.text!
            
                            let URL_String = "http://www.nitevibe.in/webservices/register.php?username=\(txt_EmailMob.text!)&password=\(userReEnterPasswordString)&mode=Normal&provider=&photo=&name=&gender="
                            //let USER_URL = "https://jsonplaceholder.typicode.com/posts/1"
            
                            //Alamofire.request(URL_String, method: .get).responseJSON { response in
                            //       print("Success: \(response.result.isSuccess)")
                            //        print("Response String: \(response.result.value)")
                            //}
                            Alamofire.request(URL_String, method: .get).responseJSON{
            
                                (response) in
            
                                if self.txt_EmailMob.text == "" {
            
                                    let alert = UIAlertController(title: "Oops!", message: "Please Enter Email/Mobile No.", preferredStyle: .alert)
            
                                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
                                    alert.addAction(alertAction)
            
                                    self.present(alert, animated: true, completion: nil)
            
                                }else{
            
            
                                    print(" response :\(String(describing: response.request))")
            
                                    guard response.result.isSuccess  else {
            
                                        print("Block 1 ")
                                        print(" response :\(response)")
            
                                        print("Error with response: \(String(describing: response.result.error))")
            
                                        return
            
                                    }
                                    guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
            
                                        print(" response :\(response)")
            
                                        print("Error with dictionary: \(String(describing: response.result.error))")
            
                                        return
            
                                    }
                                    print("message\(dict)")
                                    print("dict respo: \(dict["response"]!)")
                                    print("dict message:\(dict["message"]!)")
            
            
                                    respo = dict["response"]! as! Int
                                    let responseMessage = dict["message"]
            
            
                                    print(type(of:responseMessage))
            
                                    if (respo == 201){
            
                                        let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
            
                                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
                                        alert.addAction(alertAction)
            
                                        self.present(alert, animated: true, completion: nil)
            
                                    }
                                    else if(respo == 202){
            
                                        let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
            
                                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
                                        alert.addAction(alertAction)
            
                                        self.present(alert, animated: true, completion: nil)
            
                                    }else if(respo == 200){
            
                                        let setUserId = UserDefaults.standard
            
                                        setUserId.set(String(describing: responseMessage!), forKey: "userid")
            
                                        let alert = UIAlertController(title: "Yes", message:"Sign Up Successfully" , preferredStyle: .alert)
            
                                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
                                        alert.addAction(alertAction)
                                        
                                        self.present(alert, animated: true, completion: nil)
                                     
                                        
                                    }
                                    
                                    
                                }
            
                            }
                            let LoginVC:LogIn_1_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
                            LoginVC.selectedUserName = "\(txt_EmailMob.text!)"
                            present(LoginVC, animated: true, completion: nil)
                         
                        }else{
                            let alert = UIAlertController(title: "Oops!", message: "Password not matched.Re-Enter Password", preferredStyle: .alert)
            
                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
                            alert.addAction(alertAction)
            
                            self.present(alert, animated: true, completion: nil)
            
                        }
            
        }else{
            
            let alert = UIAlertController(title: "Oops!", message: "Please enter valid otp", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    
        
//        if usernameTextField   == responseOtpMessage{
//            userEnterPasswordString = enterPasswordTextField.text!
//            userReEnterPasswordString = reenterPasswordTextField.text!
//
//            if(userEnterPasswordString == userReEnterPasswordString){
//
//                userReEnterPasswordString = reenterPasswordTextField.text!
//
//                let URL_String = "http://www.nitevibe.in/webservices/register.php?username=\(txt_EmailMob.text!)&password=\(userReEnterPasswordString)&mode=Normal&provider=&photo=&name=&gender="
//                //let USER_URL = "https://jsonplaceholder.typicode.com/posts/1"
//
//
//
//                //Alamofire.request(URL_String, method: .get).responseJSON { response in
//                //       print("Success: \(response.result.isSuccess)")
//                //        print("Response String: \(response.result.value)")
//                //}
//                Alamofire.request(URL_String, method: .get).responseJSON{
//
//                    (response) in
//
//                    if self.txt_EmailMob.text == "" {
//
//                        let alert = UIAlertController(title: "Oops!", message: "Please Enter Email/Mobile No.", preferredStyle: .alert)
//
//                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                        alert.addAction(alertAction)
//
//                        self.present(alert, animated: true, completion: nil)
//
//                    }else{
//
//
//                        print(" response :\(String(describing: response.request))")
//
//                        guard response.result.isSuccess  else {
//
//                            print("Block 1 ")
//                            print(" response :\(response)")
//
//                            print("Error with response: \(String(describing: response.result.error))")
//
//                            return
//
//                        }
//                        guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
//
//                            print(" response :\(response)")
//
//                            print("Error with dictionary: \(String(describing: response.result.error))")
//
//                            return
//
//                        }
//                        print("message\(dict)")
//                        print("dict respo: \(dict["response"]!)")
//                        print("dict message:\(dict["message"]!)")
//
//
//                        respo = dict["response"]! as! Int
//                        let responseMessage = dict["message"]
//
//
//                        print(type(of:responseMessage))
//
//                        if (respo == 201){
//
//                            let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
//
//                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                            alert.addAction(alertAction)
//
//                            self.present(alert, animated: true, completion: nil)
//
//                        }
//                        else if(respo == 202){
//
//                            let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
//
//                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                            alert.addAction(alertAction)
//
//                            self.present(alert, animated: true, completion: nil)
//
//                        }else if(respo == 200){
//
//                            let setUserId = UserDefaults.standard
//
//                            setUserId.set(String(describing: responseMessage!), forKey: "userid")
//
//                            let alert = UIAlertController(title: "Yes", message:"Sign Up Successfully" , preferredStyle: .alert)
//
//                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                            alert.addAction(alertAction)
//
//                            self.present(alert, animated: true, completion: nil)
//                        }
//                    }
//
//                }
//
//
//            }else{
//                let alert = UIAlertController(title: "Oops!", message: "Password not matched.Re-Enter Password", preferredStyle: .alert)
//
//                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                alert.addAction(alertAction)
//
//                self.present(alert, animated: true, completion: nil)
//
//            }
//
//        }else{
//
//            print("otp  mismatch")
//
//        }
        
       
}
    
    
    func isValidEmail(email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: txt_EmailMob.text!)
        return result
    }
    
    func isValidPhone(phone: String) -> Bool {
        
        let PHONE_REGEX = "^[7-9][0-9]{9}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: txt_EmailMob.text!)
        return result
        
    }
    func checkDataType(text:String)->UsernameDataType{
        
        //if let intVal = Int(text){
        if isValidPhone(phone: text){
            
            return UsernameDataType.Number
        }else if isValidEmail(email: text){
            return UsernameDataType.Email
            
        }else{
            
            return UsernameDataType.Other
        }
        
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

