//
//  SettingsViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 05/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire
import UserNotifications
var muteNotificationAfter = ""
let setMuteSwitch = UserDefaults.standard
let setSNOLSSwitch = UserDefaults.standard
let setAutoDLOCSwitch = UserDefaults.standard
let setNewMatchSwitch = UserDefaults.standard
let setNewMsgSwitch = UserDefaults.standard
let setCheckInNotificationSwitch = UserDefaults.standard
class SettingsViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    var from_facebooklogin = String()
    var reason = String()
    var muteNotificationFirstTime = "muteNotification Empty on First Launch"
    var setclubTimmer = Timer.self
    
    @IBOutlet weak var autoDetectingLOCOutlet: UISwitch!
    @IBOutlet weak var showNotificationonLCOutlet: UISwitch!
    @IBOutlet weak var muteNotificationOutlet: UISwitch!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var nearByView: UIView!
    
    @IBOutlet weak var newMatchOutlet: UISwitch!
    @IBOutlet weak var newMsgOutlet: UISwitch!
    @IBOutlet weak var checkInNotificationOutlet: UISwitch!
    
    @IBOutlet weak var tellAFriendView: UIImageView!
    @IBOutlet weak var supportForwardBtnImageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //Thread.sleep(forTimeInterval: 5)
        logoutBtn.layer.masksToBounds = true
        logoutBtn.layer.cornerRadius = 8
        self.nearByView.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tellFriendAction))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(supportButtonTap))
        
        
        supportForwardBtnImageView.addGestureRecognizer(tap)
        tellAFriendView.addGestureRecognizer(tap2)
        self.setclubTimmer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.checkInOverlay), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.checkInOverlay()
    }
    
    @objc func checkInOverlay()
    {

        let muteNotificationLaunchedBefore = UserDefaults.standard.bool(forKey: "muteNotificationLaunchedBefore")

        if muteNotificationLaunchedBefore {

            CheckInLaunch = "muteNotification Has Set Before"
            self.muteNotification()
            print("muteNotificationBeforeLaunchStatus : \(CheckInLaunch)")

        }
        else{

            muteNotificationAfter = "muteNotification Empty on First Launch"
            print("muteNotificationFirstLaunchStatus : \(muteNotificationAfter)")
            self.muteNotification()
            UserDefaults.standard.set(true, forKey: "muteNotificationLaunchedBefore")

        }

    }
    
    func muteNotification(){

        print("Print muteNotification Launch Status : \(muteNotificationFirstTime)")
        print("Print muteNotification userDefault Launch Status : \(muteNotificationAfter)")

        if muteNotificationFirstTime == muteNotificationAfter{

            print("Mute Notification")

            //let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
            if setMuteSwitch.object(forKey: "setMuteSwitch") == nil {

                muteNotificationOutlet.isOn = false
                setMuteSwitch.set(false, forKey: "setMuteSwitch")
                setMuteSwitch.synchronize()

            }else{

                let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
                print("muteDefaultValue : \(muteSwitchState)")
                if muteSwitchState == false{
                    muteNotificationOutlet.isOn = false
                }else if muteSwitchState == true{
                    muteNotificationOutlet.isOn = true
                }

            }


            if setSNOLSSwitch.object(forKey: "setSNOLSSwitch") == nil {

                showNotificationonLCOutlet.isOn = true
                setSNOLSSwitch.set(true, forKey: "setSNOLSSwitch")
                setSNOLSSwitch.synchronize()

            }else{

                let showNotificationState = setSNOLSSwitch.bool(forKey: "setSNOLSSwitch")
                print("showNotificationDefaultValue : \(showNotificationState)")
                if showNotificationState == false{
                    showNotificationonLCOutlet.isOn = false
                }else if showNotificationState == true{
                    showNotificationonLCOutlet.isOn = true
                }

            }


            if setNewMsgSwitch.object(forKey: "setNewMsgSwitch") == nil {

                newMsgOutlet.isOn = true
                setNewMsgSwitch.set(true, forKey: "setNewMsgSwitch")
                setNewMsgSwitch.synchronize()

            }else{

                let showNewMsgState = setNewMsgSwitch.bool(forKey: "setNewMsgSwitch")
                print("showNewMsgDefaultValue : \(showNewMsgState)")
                if showNewMsgState == false{
                    newMsgOutlet.isOn = false
                }else if showNewMsgState == true{
                    newMsgOutlet.isOn = true
                }

            }


            if setNewMatchSwitch.object(forKey: "setNewMatchSwitch") == nil {

                newMatchOutlet.isOn = true
                setNewMatchSwitch.set(true, forKey: "setNewMatchSwitch")
                setNewMatchSwitch.synchronize()

            }else{

                let showNewMatchState = setNewMatchSwitch.bool(forKey: "setNewMatchSwitch")
                print("showNewMatchDefaultValue : \(showNewMatchState)")
                if showNewMatchState == false{
                    newMatchOutlet.isOn = false
                }else if showNewMatchState == true{
                    newMatchOutlet.isOn = true
                }

            }


            if setAutoDLOCSwitch.object(forKey: "setAutoDLOCSwitch") == nil {

                autoDetectingLOCOutlet.isOn = true
                setAutoDLOCSwitch.set(true, forKey: "setAutoDLOCSwitch")
                setAutoDLOCSwitch.synchronize()

            }else{

                let autoDetectLOCState = setAutoDLOCSwitch.bool(forKey: "setAutoDLOCSwitch")
                print("autoDetectLOCDefaultValue : \(autoDetectLOCState)")
                if autoDetectLOCState == false{
                    autoDetectingLOCOutlet.isOn = false
                }else if autoDetectLOCState == true{
                    autoDetectingLOCOutlet.isOn = true
                }

            }


            if setCheckInNotificationSwitch.object(forKey: "setCheckInNotificationSwitch") == nil {

                checkInNotificationOutlet.isOn = true
                setCheckInNotificationSwitch.set(true, forKey: "setCheckInNotificationSwitch")
                setCheckInNotificationSwitch.synchronize()

            }else{

                let checkinNotificationState = setCheckInNotificationSwitch.bool(forKey: "setCheckInNotificationSwitch")
                print("checkInNotificationDefaultValue : \(checkinNotificationState)")
                if checkinNotificationState == false{
                    checkInNotificationOutlet.isOn = false
                }else if checkinNotificationState == true{
                    checkInNotificationOutlet.isOn = true
                }

            }



        }
        else if muteNotificationFirstTime != muteNotificationAfter{

            let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
            print("muteDefaultValue : \(muteSwitchState)")
            if muteSwitchState == false{
                muteNotificationOutlet.isOn = false
            }else if muteSwitchState == true{
                muteNotificationOutlet.isOn = true
            }


            let showNotificationState = setSNOLSSwitch.bool(forKey: "setSNOLSSwitch")
            print("showNotificationDefaultValue : \(showNotificationState)")
            if showNotificationState == false{
                showNotificationonLCOutlet.isOn = false
            }else if showNotificationState == true{
                showNotificationonLCOutlet.isOn = true
            }


            let showNewMsgState = setNewMsgSwitch.bool(forKey: "setNewMsgSwitch")
            print("showNewMsgDefaultValue : \(showNewMsgState)")
            if showNewMsgState == false{
                newMsgOutlet.isOn = false
            }else if showNewMsgState == true{
                newMsgOutlet.isOn = true
            }

            let showNewMatchState = setNewMatchSwitch.bool(forKey: "setNewMatchSwitch")
            print("showNewMatchDefaultValue : \(showNewMatchState)")
            if showNewMatchState == false{
                newMatchOutlet.isOn = false
            }else if showNewMatchState == true{
                newMatchOutlet.isOn = true
            }

            let autoDetectLOCState = setAutoDLOCSwitch.bool(forKey: "setAutoDLOCSwitch")
            print("autoDetectLOCDefaultValue : \(autoDetectLOCState)")
            if autoDetectLOCState == false{
                autoDetectingLOCOutlet.isOn = false
            }else if autoDetectLOCState == true{
                autoDetectingLOCOutlet.isOn = true
            }

            let checkinNotificationState = setCheckInNotificationSwitch.bool(forKey: "setCheckInNotificationSwitch")
            print("checkInNotificationDefaultValue : \(checkinNotificationState)")
            if checkinNotificationState == false{
                checkInNotificationOutlet.isOn = false
            }else if checkinNotificationState == true{
                checkInNotificationOutlet.isOn = true
            }

        }

    }

    @IBAction func autoDetectLocationAction(_ sender: UISwitch) {
        
        let autoDetectLOCState = setAutoDLOCSwitch.bool(forKey: "setAutoDLOCSwitch")
        
        if autoDetectLOCState == true{
            
            autoDetectingLOCOutlet.isOn = false
            setAutoDLOCSwitch.removeObject(forKey: "setAutoDLOCSwitch")
            setAutoDLOCSwitch.set(false, forKey: "setAutoDLOCSwitch")
            setAutoDLOCSwitch.synchronize()
            print("autoDetectLOCState 1 : \(autoDetectLOCState)")
            
            
        }else{
            
            autoDetectingLOCOutlet.isOn = true
            setAutoDLOCSwitch.removeObject(forKey: "setAutoDLOCSwitch")
            setAutoDLOCSwitch.set(true, forKey: "setAutoDLOCSwitch")
            setAutoDLOCSwitch.synchronize()
            print("autoDetectLOCState 2 : \(autoDetectLOCState)")
            
        }
    }
    @IBAction func showNotificationonLSAction(_ sender: UISwitch) {
        
        let showNotificationState = setSNOLSSwitch.bool(forKey: "setSNOLSSwitch")
        
        if showNotificationState == true{
            
            showNotificationonLCOutlet.isOn = false
            setSNOLSSwitch.removeObject(forKey: "setSNOLSSwitch")
            setSNOLSSwitch.set(false, forKey: "setSNOLSSwitch")
            setSNOLSSwitch.synchronize()
            print("showNotificationState 1 : \(showNotificationState)")
            
        }else{
           
            showNotificationonLCOutlet.isOn = true
            setSNOLSSwitch.removeObject(forKey: "setSNOLSSwitch")
            setSNOLSSwitch.set(true, forKey: "setSNOLSSwitch")
            setSNOLSSwitch.synchronize()
            print("showNotificationState 2 : \(showNotificationState)")
        }
    }
    
    @IBAction func muteNotificatonAction(_ sender: UISwitch) {
       
        let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
        var muteNotificationValue = 1
        if muteSwitchState == false{
            
            muteNotificationOutlet.isOn = true
            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
            setMuteSwitch.set(true, forKey: "setMuteSwitch")
            setMuteSwitch.synchronize()
            muteNotificationValue = 1
            print("muteDefaultValue 1 : \(muteSwitchState)")
            UIApplication.shared.registerForRemoteNotifications()

            
        }else if muteSwitchState == true{
            
            muteNotificationOutlet.isOn = false
            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
            setMuteSwitch.set(false, forKey: "setMuteSwitch")
            setMuteSwitch.synchronize()
            muteNotificationValue = 2
            print("muteDefaultValue 2 : \(muteSwitchState)")
            UIApplication.shared.unregisterForRemoteNotifications()

        }
        
        
          let URL_String = "http://nitevibe.in/webservices/block_globle_notification.php?id=\(muteNotificationValue)"
            print(" Url String: \(URL_String)")

            do {
                var urlRequest = try URLRequest(url: URL_String, method:.get)
                urlRequest.cachePolicy = .reloadIgnoringCacheData
                
                Alamofire.request(urlRequest).responseJSON{
                    response in
                    
                    print(" response :\(String(describing: response.request))")
                    print(" response :\(String(describing: response.request))")
                    
                    guard response.result.isSuccess  else {
                        print("Block 1 ")
                        print(" response :\(response)")
                        print("Error with response: \(String(describing: response.result.error))")
                        
                        return
                        
                    }
                    guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                        
                        print(" response :\(response)")
                        
                        print("Error with dictionary: \(String(describing: response.result.error))")
                        
                        return
                        
                    }
                    
                    print("dict respo \(dict["response"]!)")
                    //response 200 : Login Success
                    if(dict["response"] as! Int == 200){
                        
                        guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                            
                            print("Error with dictionary data: \(String(describing: response.result.error))")
                            
                            return
                        }
                        
                        print(" dict data\(dictData)")
                        
                        let notificationStatus = dict["status"] as? String;                        print(notificationStatus as Any)
                        if notificationStatus == "active" {
                            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
                            setMuteSwitch.set(true, forKey: "setMuteSwitch")
                            setMuteSwitch.synchronize()
                        }
                        else if notificationStatus == "inactive"{
                            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
                            setMuteSwitch.set(false, forKey: "setMuteSwitch")
                            setMuteSwitch.synchronize()
                        }
                    }
                    else if(dict["response"] as! Int == 408){
                        
                        let alert = UIAlertController(title: "", message:"Somthing went wrong", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
            } catch {
                // TODO: find a better way to handle error
                print(error)
                
            }
        

        
    }
    
    @objc func tellFriendAction(){
        
        let storyboard =  UIStoryboard(name: "Swipe", bundle: nil)
        
        let tellVC =  storyboard.instantiateViewController(withIdentifier: "TellAFriendViewController") as! TellAFriendViewController
        
        present(tellVC, animated: true, completion: nil)
        
    }
    
    @IBAction func tellAfriendButtonTapped(_ sender: Any) {
        
        let urlString = "https://itunes.apple.com/us/app/nitevibe/id1444854034?ls=1&mt=8"
        
        let linkToShare = [urlString]
        
        let activityController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
        
        self.present(activityController, animated: true, completion: nil)
    }
    
    @objc func supportButtonTap(){
        
        let storyboard =  UIStoryboard(name: "Swipe", bundle: nil)
        
        let supportVC =  storyboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        
        present(supportVC, animated: true, completion: nil)
        
    }
    @IBAction func btn_LogOut_Action(_ sender: Any) {
        
        
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)

        FBSDKLoginManager().logOut()
        
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        
        let UserGuide:UserGuideWithFBLogIn = storyboard.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
        self.present(UserGuide, animated: true, completion: nil)
        
        
        //Check Normal login -(without facebook)
        //Check user already login or not
        let defaults = UserDefaults.standard
        
        let loginFlag = defaults.value(forKey: "loginflag") as? String
        defaults.removeObject(forKey: "ovelayflag")
        defaults.set("0", forKey: "ovelayflag")
        
        if loginFlag == "1"{
            
            
            defaults.removeObject(forKey: "loginflag")
            defaults.set("0", forKey: "loginflag")
            //defaults.set("0", forKey: "ovelayflag")
            let UserGuide:UserGuideWithFBLogIn = storyboard.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
            self.present(UserGuide, animated: true, completion: nil)
            
        }
       

    }
    
    @IBAction func deleteAccountButtonTapped(_ sender: Any) {
        
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        
        let showAlertVC:CustomAlertViewController = storyboard.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        showAlertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        self.addChildViewController(showAlertVC)
        self.view.addSubview(showAlertVC.view)
        

    }

    @IBAction func newMsgAction(_ sender: UISwitch) {
        
        let newMsgState = setNewMsgSwitch.bool(forKey: "setNewMsgSwitch")
        
        if newMsgState == true{
            
            newMsgOutlet.isOn = false
            setNewMsgSwitch.removeObject(forKey: "setNewMsgSwitch")
            setNewMsgSwitch.set(false, forKey: "setNewMsgSwitch")
            setAutoDLOCSwitch.synchronize()
            print("newMsgState 1 : \(newMsgState)")
            
            
        }else{
            
            newMsgOutlet.isOn = true
            setNewMsgSwitch.removeObject(forKey: "setNewMsgSwitch")
            setNewMsgSwitch.set(true, forKey: "setNewMsgSwitch")
            setNewMsgSwitch.synchronize()
            print("newMsgState 2 : \(newMsgState)")
            
        }
        
        
        
    }
    
    @IBAction func newMatchAction(_ sender: UISwitch) {
        
        let newMatchState = setNewMatchSwitch.bool(forKey: "setNewMatchSwitch")
        
        if newMatchState == true{
            
            newMatchOutlet.isOn = false
            setNewMatchSwitch.removeObject(forKey: "setNewMatchSwitch")
            setNewMatchSwitch.set(false, forKey: "setNewMatchSwitch")
            setNewMatchSwitch.synchronize()
            print("newMatchState 1 : \(newMatchState)")
            
            
        }else{
            
            newMatchOutlet.isOn = true
            setNewMatchSwitch.removeObject(forKey: "setNewMatchSwitch")
            setNewMatchSwitch.set(true, forKey: "setNewMatchSwitch")
            setNewMatchSwitch.synchronize()
            print("newMatchState 2 : \(newMatchState)")
            
        }
        
        
    }
    
    @IBAction func checkInNotificationAction(_ sender: UISwitch) {
        
        let checkInNotificationState = setCheckInNotificationSwitch.bool(forKey: "setCheckInNotificationSwitch")
        
        if checkInNotificationState == true{
            
            checkInNotificationOutlet.isOn = false
            setCheckInNotificationSwitch.removeObject(forKey: "setCheckInNotificationSwitch")
            setCheckInNotificationSwitch.set(false, forKey: "setCheckInNotificationSwitch")
            setCheckInNotificationSwitch.synchronize()
            print("checkInNotificationState 1 : \(checkInNotificationState)")
            
            
        }else{
            
            checkInNotificationOutlet.isOn = true
            setCheckInNotificationSwitch.removeObject(forKey: "setCheckInNotificationSwitch")
            setCheckInNotificationSwitch.set(true, forKey: "setCheckInNotificationSwitch")
            setCheckInNotificationSwitch.synchronize()
            print("checkInNotificationState 2 : \(checkInNotificationState)")
            
        }
        
        
        
    }
    
    @IBAction func backBtn_action(_ sender: Any) {
        
        let storyBoard  = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyBoard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        self.present(BaseController, animated: true, completion: nil)
    }
    
    
    @IBAction func helpButtonTapped(_ sender: Any) {
        
        
        let storyBoard  = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        
        let faqVC =  storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        faqVC.selectedButtonFlag = "0"
        faqVC.selectedVCFlag = "1"
        present(faqVC, animated: true, completion: nil)
    }
    
    
    @IBAction func faqForwardButtonTapped(_ sender: Any) {
        
        let storyBoard  = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        
        let faqVC =  storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        faqVC.selectedButtonFlag = "0"
        faqVC.selectedVCFlag = "1"
        present(faqVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func legalButtonTapped(_ sender: Any) {
        
        let storyBoard  = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let faqVC =  storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        faqVC.selectedButtonFlag = "1"
        faqVC.selectedVCFlag = "1"
        present(faqVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func legalForwardButtonTapped(_ sender: Any) {
        
        let storyBoard  = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let faqVC =  storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        faqVC.selectedButtonFlag = "1"
        faqVC.selectedVCFlag = "1"
        present(faqVC, animated: true, completion: nil)
    }
    
    
    @IBAction func supportButtonTapped(_ sender: Any) {
        
        let storyboard =  UIStoryboard(name: "Swipe", bundle: nil)
        
        let supportVC =  storyboard.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        
        present(supportVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func blockedUserAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
        let blockedUserVC = storyboard.instantiateViewController(withIdentifier: "BlockedUsersViewController") as! BlockedUsersViewController
        present(blockedUserVC, animated: true, completion: nil)
    }
    

}
