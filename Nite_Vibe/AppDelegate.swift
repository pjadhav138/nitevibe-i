//
//  AppDelegate.swift
//  Nite_Vibe
//
//  Created by Appmonks on 21/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//
//let IS_OS_8_OR_LATER = Float(UIDevice.current.systemVersion)! >= 8.0
import UIKit

//var ClubListLaunchScroll = ""
//var ClubListLaunchCreateProfile = ""
//
//var EventDetailsGoingLaunch = ""
//var EventDetailsRatioLaunch = ""
var settingNotificationLaunch = ""

import FirebaseCore
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
import CoreLocation
import GoogleMaps
import GooglePlaces

var userIDCommingFromNotification = ""
var NotificationTitleCommingFromNotification = ""
var FCMTOKEN = ""



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate,CLLocationManagerDelegate,UINavigationBarDelegate {
    // CLLocationManagerDelegate,
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var fromTerminated = false
    
    var instanceIDTokenMessage = ""
    var locationManager = CLLocationManager()
    var clubID_value = String()
    var eventID_value = String()
    var notificationText = String()
    var notificationTitle = String()
    var Navigation = UINavigationBar()
    var AppDelegateLocationLatitude = Double()
    var AppDelegateLocationLongitude = Double()
    var timer = Timer()
    var supervibedMe = String()
    var settingNotificationLaunchBefore = "muteNotification Empty on First Launch"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        fromTerminated = false
        if CLLocationManager.locationServicesEnabled(){
            fromTerminated = true
            //locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.activityType = .otherNavigation
            locationManager.startUpdatingLocation()
        }
        
        
        
        GMSServices.provideAPIKey("AIzaSyCPactPzQBMg4yGO9azSLrXNHB1yNLjjuQ")
        GMSPlacesClient.provideAPIKey("AIzaSyCPactPzQBMg4yGO9azSLrXNHB1yNLjjuQ")
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //print(" print access  \(FBSDKAccessToken.current())")
        application.applicationIconBadgeNumber = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 15.0, execute: {
            
            FirebaseApp.configure()
            Messaging.messaging().delegate = self
            
            UIApplication.shared.applicationIconBadgeNumber = 0
            
            if #available(iOS 10.0, *)
            {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
            }
            else
            {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            
            application.registerForRemoteNotifications()
            
        })
        
        
        // [END register_for_notifications]
        
        if launchOptions != nil {
            // opened from a push notification when the app is closed
            let userInfo = launchOptions?[.remoteNotification] as? [AnyHashable : Any]
            if userInfo != nil {
                if let aKey = userInfo?["aps"] {
                    print("userInfo->\(aKey)")
                }
            }
        } else {
            // opened app without a push notification.
        }
        
        return true
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        FBSDKAppEvents.activateApp()
    }
    
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        locationManager.startUpdatingLocation()
        Messaging.messaging().shouldEstablishDirectChannel = false
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        locationManager.startUpdatingLocation()
        
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        locationManager.startUpdatingLocation()
        
        AppDelegateLocationLatitude = (locationManager.location?.coordinate.latitude)!
        AppDelegateLocationLongitude = (locationManager.location?.coordinate.longitude)!
        
        let locationUserDefaults = UserDefaults.standard
        locationUserDefaults.set(AppDelegateLocationLatitude, forKey: "AppDelegateLocationLatitude")
        locationUserDefaults.set(AppDelegateLocationLongitude, forKey: "AppDelegateLocationLongitude")
        
        let locationLatitude = locationUserDefaults.value(forKey: "AppDelegateLocationLatitude")
        let locationLongitude = locationUserDefaults.value(forKey: "AppDelegateLocationLongitude")
        
        print("UserLocationLatitude : \(locationLatitude!)")
        print("UserLocationLatitude : \(locationLongitude!)")
        
        //locationManager.stopUpdatingLocation()
        
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Got token data! \(tokenString)")

        UserDefaults.standard.set(tokenString, forKey: "device_token")
        UserDefaults.standard.synchronize()
        NSLog("Device Token : %@", tokenString)
        Messaging.messaging().apnsToken = deviceToken
    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        if application.applicationState == .background {
            // opened from a push notification when the app is on background
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_BackGroundMode->\(aKey)")
            }
            
        }else if application.applicationState == .inactive {
            // opened from a push notification when the app was on background
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_InactiveMode->\(aKey)")
            }
        } else if application.applicationState == .active {
            // a push notification when the app is running. So that you can display an alert and push in any view
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_ActiveMode->\(aKey)")
            }
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("-------------------------Notificartonlk \(response.notification.request.content.userInfo)")
        var userInfo = NSDictionary()
        userInfo = response.notification.request.content.userInfo as NSDictionary
        print("DidRecieveserInfo : \(userInfo)")
        
        print("UserInfo : \(userInfo)")
        if let clubValue = userInfo["gcm.notification.value"] {
            print("clubValue : \(clubValue)")
            clubID_value = clubValue as! String
        }
        if let eventValue = userInfo["gcm.notification.event"] {
            print("eventValue : \(eventValue)")
            eventID_value = eventValue as! String
        }
        if let supervibeMe = userInfo["gcm.notification.userid"]{
            print("supervibeMe : \(supervibeMe)")
            supervibedMe = supervibeMe as! String
        }
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["body"] as? NSString {
                    print("Message : \(message)")
                    notificationText = message as String
                }
                if let messageTitle = alert["title"] as? NSString {
                    print("MessageTitle : \(messageTitle)")
                    notificationTitle = messageTitle as String
                }
            }
            if let sound = aps["sound"] as? String {
                
                print("Sound : \(sound)")
                
            }
            if let badge = aps["badge"] as? Int {
                
                print("Badge : \(badge)")
                
            }
            
        }
        
        if let messageID = userInfo[gcmMessageIDKey]
        {
            print("Message ID: \(messageID)")
        }
        
        if notificationTitle == "Check-In"{
            print("NotifyTitle: \(notificationTitle)")
            let storyboard: UIStoryboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
            let clubDetailVC = storyboard.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
            print("ClubValue: \(String(describing: clubID_value))")
            clubDetailVC.selectedClubId = clubID_value
            let navigationController = UINavigationController.init(rootViewController: clubDetailVC)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            navigationController.isNavigationBarHidden = true
            self.window?.makeKeyAndVisible()
            
        }else if(notificationTitle == "Match Profile"){
            
            print("NotifyTitle: \(notificationTitle)")
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
            BaseController.selectedIndex = 2
            let navigationController = UINavigationController.init(rootViewController: BaseController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            navigationController.isNavigationBarHidden = true
            self.window?.makeKeyAndVisible()
            
            
        }else if(notificationTitle == "New Message"){
            
            print("NotifyTitle: \(notificationTitle)")
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

            let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
            BaseController.selectedIndex = 2
            let navigationController = UINavigationController.init(rootViewController: BaseController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            navigationController.isNavigationBarHidden = true
            self.window?.makeKeyAndVisible()
            
        }else if(notificationTitle == "Supervibe"){
            
            print("NotifyTitle: \(notificationTitle)")
            let storyboard: UIStoryboard = UIStoryboard(name: "DrinkPreference", bundle: nil)

            let SupervibeViewController:SuperVibedViewController = storyboard.instantiateViewController(withIdentifier: "SuperVibedViewController") as! SuperVibedViewController
            print("SUPERVIBEID : \(supervibedMe)")
            SupervibeViewController.whoSupervibeMeID = supervibedMe
            print("SUPERVIBEEVENT : \(eventID_value)")
            SupervibeViewController.eventId = eventID_value
            let navigationController = UINavigationController.init(rootViewController: SupervibeViewController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            navigationController.isNavigationBarHidden = true
            self.window?.makeKeyAndVisible()


        }else{
            
            print("Notification from Admin")
            
        }
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        

        let userInfo = notification.request.content.userInfo
        print("UserInfi1 : \(String(describing: userInfo))")
        
        
        print("----------------------Notification In willPresent : \(notification.request.content.userInfo)")
        print("DidRecieveserInfo : \(userInfo)")
        
        print("UserInfo : \(userInfo)")
        if let clubValue = userInfo["gcm.notification.value"] {
            print("clubValue : \(clubValue)")
            clubID_value = clubValue as! String
        }
        if let eventValue = userInfo["gcm.notification.event"] {
            print("eventValue : \(eventValue)")
            eventID_value = eventValue as! String
        }
        if let supervibeMe = userInfo["gcm.notification.userid"]{
            print("supervibeMe : \(supervibeMe)")
            supervibedMe = supervibeMe as! String
        }
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["body"] as? NSString {
                    print("Message : \(message)")
                    notificationText = message as String
                }
                if let messageTitle = alert["title"] as? NSString {
                    print("MessageTitle : \(messageTitle)")
                    notificationTitle = messageTitle as String
                }
            }
            if let sound = aps["sound"] as? String {
                
                print("Sound : \(sound)")
                
            }
            if let badge = aps["badge"] as? Int {
                
                print("Badge : \(badge)")
                
            }
            
        }
        
        if let messageID = userInfo[gcmMessageIDKey]
        {
            print("Message ID: \(messageID)")
        }
        
        

        if setMuteSwitch.object(forKey: "setMuteSwitch") == nil
        {
            print("Enter in mute UD")
            completionHandler([.sound,.alert])
        }else{

            if(notificationTitle == "New Message"){
                
                let getUserData :UserDefaults = UserDefaults.standard
                let ReceiverUser_Id = getUserData.value(forKey: "receiver_user_id")
                print("User_Id from app delegate: dert \(ReceiverUser_Id)")
                let uid = ReceiverUser_Id as! String
                print("User_Id from app delegate: \(uid)")
                //let User_Location = getUserData.value(forKey: "selectedlocationcity")
                print("userid of default app delegate :\(String(describing: ReceiverUser_Id))")
                
                if (setNewMsgSwitch.object(forKey: "setNewMsgSwitch") == nil) {
                    
                    if(uid == supervibedMe && setMuteSwitch.object(forKey: "setMuteSwitch") == nil){
                        
                        print("Dont Show Notification when nill")
                        
                    }else if(uid != supervibedMe && setMuteSwitch.object(forKey: "setMuteSwitch") == nil){
                        completionHandler([.sound,.alert])

                        print("Show Notification with alert and sound")
                        
                    }else if(uid != supervibedMe && setMuteSwitch.object(forKey: "setMuteSwitch") != nil){
                        
                        print("Show Notification with sound only")
                        completionHandler(.sound)
                        
                    }
                    
                }else{
                    
                    print("Enter in mute UD else part")
                    let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
                    print("muteDefaultValue 1 : \(muteSwitchState)")
                    let showNewMsgState = setNewMsgSwitch.bool(forKey: "setNewMsgSwitch")
                    print("showNewMsgDefaultValue 22: \(showNewMsgState)")
                    
                    
                    if(showNewMsgState == false && muteSwitchState == false){
                        
                        if(uid == supervibedMe ){
                            
                            print("Dont Show Notification")
                            
                        }else{
                            
                            print("Dont Show Notification when false and false")
                            
                        }
                        
                    }else if(showNewMsgState == false && muteSwitchState == true){
                        
                        if(uid == supervibedMe ){
                            
                            print("Dont Show Notification")
                            
                        }else{
                            
                            print("Dont Show Notification when false and true")
                            
                        }
                        
                    }else if(showNewMsgState == true && muteSwitchState == true){
                        
                        if(uid == supervibedMe ){
                            
                            print("Dont Show Notification")
                            
                        }else{
                            
                            print("Show Notification when true and true with alert")
                            completionHandler(.alert)
                            
                        }
                        
                        
                    }else if(showNewMsgState == true && muteSwitchState == false){
                        
                        if(uid == supervibedMe ){
                            
                            print("Dont Show Notification")
                            
                        }else{
                            
                            print("Show Notification when true and false with alert and sound")
                            completionHandler([.sound,.alert])
                            
                        }
                        
                    }
                    
                }
                
            }
            

            
            
            if(notificationTitle == "Match Profile"){
                
                if (setNewMatchSwitch.object(forKey: "setNewMatchSwitch") == nil && setMuteSwitch.object(forKey: "setMuteSwitch") == nil){
                    
                     completionHandler([.sound,.alert])
                    print("show match notification when nill")
                    
                   
                    
                }else{
                    
                    print("new match if set")
                    let showNewMatchState = setNewMatchSwitch.bool(forKey: "setNewMatchSwitch")
                    print("showNewMatchDefaultValue : \(showNewMatchState)")
                    let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
                    print("muteDefaultValue 1 : \(muteSwitchState)")
                    
                    if(showNewMatchState == false && muteSwitchState == false){
                        
                        print("Dont show match notification when false and false")
                        
                    }else if(showNewMatchState == true && muteSwitchState == false){
                        
                        print("show match notification when true and false with alert and sound")
                        completionHandler([.sound,.alert])
                        
                    }else if(showNewMatchState == true && muteSwitchState == true){
                        
                        print("show match notification when true and true with alert only")
                        completionHandler(.alert)
                        
                    }else if(showNewMatchState == false && muteSwitchState == true){
                        
                        print("Dont show match notification when false and true")
                        
                    }
                    
                }
            }
            
            
            if(notificationTitle == "Check-In"){
                
                if (setCheckInNotificationSwitch.object(forKey: "setCheckInNotificationSwitch") == nil && setMuteSwitch.object(forKey: "setMuteSwitch") == nil){
                    
                    completionHandler([.sound,.alert])
                    
                }else{
                    
                    let checkinNotificationState = setCheckInNotificationSwitch.bool(forKey: "setCheckInNotificationSwitch")
                    print("checkInNotificationDefaultValue : \(checkinNotificationState)")
                    let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
                    print("muteDefaultValue 1 : \(muteSwitchState)")
                    
                    if (checkinNotificationState == false && muteSwitchState == false){
                        
                        print("Dont show notification")
                        
                    }else if (checkinNotificationState == true && muteSwitchState == false){
                        
                        completionHandler([.sound,.alert])
                        
                    }else if (checkinNotificationState == true && muteSwitchState == true){
                        
                        completionHandler(.alert)
                        
                    }else if (checkinNotificationState == false && muteSwitchState == true){
                        
                        print("Dont show notification")
                        
                    }
                    
                }
            }
            
            


        }
        

        
        
        
        
        if setAutoDLOCSwitch.object(forKey: "setAutoDLOCSwitch") == nil {
            
            locationManager.startUpdatingLocation()
            
        }else{
            
            let autoDetectLOCState = setAutoDLOCSwitch.bool(forKey: "setAutoDLOCSwitch")
            print("autoDetectLOCDefaultValue : \(autoDetectLOCState)")
            
            if autoDetectLOCState == false{
               
                locationManager.stopUpdatingLocation()
                
            }else if autoDetectLOCState == true{
               
                locationManager.startUpdatingLocation()
                
            }
            
        }
        
        if setSNOLSSwitch.object(forKey: "setSNOLSSwitch") == nil {
            
           // completionHandler([.alert,.sound])
            
            
        }else{
            
            let showNotificationState = setSNOLSSwitch.bool(forKey: "setSNOLSSwitch")
            print("showNotificationDefaultValue : \(showNotificationState)")
            if showNotificationState == false{
                
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
               // completionHandler(.sound)
                
            }else if showNotificationState == true{
                
               // completionHandler([.alert,.sound])
                
            }
            
        }
        

        
 
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void){
        

        //Handle the notification
            completionHandler(
                [UNNotificationPresentationOptions.alert,
                 UNNotificationPresentationOptions.sound,
                 UNNotificationPresentationOptions.badge])
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        
        if application.applicationState == .background {
            // opened from a push notification when the app is on background
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_BackGroundMode->\(aKey)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationReceived"), object: nil)
            }
            // Print full message.
            print(userInfo)
        }else if application.applicationState == .inactive {
            // opened from a push notification when the app was on background
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_InactiveMode->\(aKey)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationReceived"), object: nil)
            }
        }else if application.applicationState == .active {
            // a push notification when the app is running. So that you can display an alert and push in any view
            if let aKey = userInfo["aps"] {
                print("userInfoWhenIn_ActiveMode->\(aKey)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationReceived"), object: nil)
            }
        }
        
        // Print full message.
        print(userInfo)
        //completionHandler(UIBackgroundFetchResultNoData);
        //NotificationHandler.sharedInstance().handleNotificationWhenActiveState(userInfo:userInfo)
        
        
    }
    
    //    @objc func callTimer(){
    //
    //        FirebaseApp.configure()
    //        Messaging.messaging().delegate = self
    //        UIApplication.shared.applicationIconBadgeNumber = 0
    //
    //
    //        if #available(iOS 10.0, *) {
    //            // For iOS 10 display notification (sent via APNS)
    //            UNUserNotificationCenter.current().delegate = self
    //
    //            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
    //            UNUserNotificationCenter.current().requestAuthorization(
    //                options: authOptions,
    //                completionHandler: {_, _ in })
    //        } else {
    //            let settings: UIUserNotificationSettings =
    //                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
    //           // application.registerUserNotificationSettings(settings)
    //        }
    //
    //       // application.registerForRemoteNotifications()
    //
    //    }
    
    
    
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let userdefaults = UserDefaults.standard
        userdefaults.set(fcmToken, forKey: "FCMToken")
        userdefaults.synchronize()
        // updateFCMToken_WS(fcmToken: fcmToken)
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.instanceIDTokenMessage  = "Remote InstanceID token: \(result.token)"
            }
        }
        
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    
        
        print("Firebase registration token 1: \(fcmToken)")
        let userdefaults = UserDefaults.standard
        userdefaults.set(fcmToken, forKey: "FCMToken")
        userdefaults.synchronize()
        updateFCMToken_WS(fcmToken: fcmToken)
        NSLog("FCM Token : %@", fcmToken)
    }
    
    func updateFCMToken_WS(fcmToken : String){
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "gcm_reg_id")
        guard let id = UserDefaults.standard.value(forKey: "userid") as? String else {
            return
        }
        print("Firebase registration token 2: \(fcmToken)")
        var userDetails = [String : String]()
        let userdefaults = UserDefaults.standard
        userdefaults.set(fcmToken, forKey: "FCMToken")
        userdefaults.synchronize()
        /* capacity: 1 */
        userDetails["gcm_reg_id"] = fcmToken
        
        
    }
    
    
}




