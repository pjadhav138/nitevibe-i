//
//  HomePageCollectionViewCell_VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 04/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit


class HomePageCollectionViewCell_VC: UICollectionViewCell {
    
    @IBOutlet weak var CollectionCellView: UIView!
    @IBOutlet weak var ClubImage: UIImageView!
    @IBOutlet weak var lbl_EventDate: UILabel!
    @IBOutlet weak var lbl_ClubName: UILabel!
    @IBOutlet weak var lbl_PeopleGoing: UILabel!
    @IBOutlet weak var ProgressBarRatio: UIProgressView!
    @IBOutlet weak var btn_ShareEvent: UIButton!
    @IBOutlet weak var btn_FavouritEvent: UIButton!
    @IBAction func btn_action_ShareEvent(_ sender: Any) {
    }
    @IBAction func btn_action_FavouriteEvent(_ sender: Any) {
    }
}
