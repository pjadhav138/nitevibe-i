//
//  UserProfileViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 03/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var superviewHeight: NSLayoutConstraint!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var drinkCollHeight: NSLayoutConstraint!
    @IBOutlet weak var drinkHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var aboutMeHeight: NSLayoutConstraint!
    @IBOutlet weak var drinkCollectionView: UICollectionView!
    @IBOutlet weak var lbl_DrinkPreference: UILabel!
    @IBOutlet weak var aboutYou_TextView: UITextView!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var userLocHeight: NSLayoutConstraint!
    @IBOutlet weak var userGoingEvent_collectionView: UICollectionView!
    @IBOutlet weak var lbl_userLocation: UILabel!
    @IBOutlet weak var lbl_userWorkedAs: UILabel!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var pageControll_outlet: UIPageControl!
    @IBOutlet weak var userProfilePhotos: UIImageView!
    var namevalue: String!
    var userName = String()
    var userWork = String()
    var userId = String()
    var userLocation = String()
    var userPic = String()
    var userAge = String()
    var UserData = [UserDataModel]()
    // var userProfilePicData = [userPhotoDataModel]()
    var urlString = ""
    var UserCount = Int()
    var userProfilePicsArray = [String]()
    var userEventArray = [String]()
    var getUserAge = String()
    var getUserAgeStatus = String()
    var getUserLocation = String()
    var getUserLocationStatus = String()
    var getUserDrinks = String()
    var getUserDrinksStatus = String()
    var getUserBiodata = String()
    var ageofUser = ""
    var pointsArr = [String]()
    var userEventIdArray = [String]()
    var userEventClubIdArray = [String]()
    
    @IBOutlet weak var lbl_events: UILabel!
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var eventIndex = 0
    let userDefault = UserDefaults.standard
    var imageArray = [Data]()
    var userImages = String()
    var userProfileImageArray = [String]()
    
    var selectedVCFlag = ""
    var selectedClubId = ""
    var selectedEventId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        drinkCollectionView.delegate = self
        drinkCollectionView.dataSource = self
        userGoingEvent_collectionView.delegate = self
        userGoingEvent_collectionView.dataSource = self
        
        getData()
        
        getEventData()
        print("UserName : \(userName)")
        print("UserWork : \(userWork)")
        print("UserId : \(userId)")
        print("UserLocation : \(userLocation)")
        print("UserPic : \(userPic)")
        print("UserAge : \(userAge)")
        
        if userPic.isEmpty {
            
            print("Do nothing")
        }else{
            
            let imageURL = URL(string: userPic)
            let imageData = try! Data(contentsOf: imageURL! as URL)
            print("\(imageData)")
            userProfilePhotos.image = UIImage(data: imageData)
        }
        
        self.superView.sizeToFit()
        contentView.sizeToFit()
        lbl_userName.sizeToFit()
        lbl_userLocation.sizeToFit()
        lbl_userWorkedAs.sizeToFit()
        aboutYou_TextView.sizeToFit()
        pageControll_outlet.sizeToFit()
        //self.aboutMeHeight?.constant = self.aboutYou_TextView.heightAnchor
        //lbl_userName.text = "\(userName) \(userAge)"
        //lbl_userWorkedAs.text = userWork
        let fixedWidth = aboutYou_TextView.frame.size.width
        let newSize = aboutYou_TextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        aboutYou_TextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        aboutYou_TextView.isScrollEnabled = false
      
        drinkCollectionView.reloadData()
        userGoingEvent_collectionView.reloadData()
        
        self.userProfilePhotos.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapUserPhoto))
        userProfilePhotos.addGestureRecognizer(tapGesture)
        //scrollview.resizeUserProfileScrollViewContentSize()
        
        
    }
    
    
    
    @IBAction func homeBtn_action(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        //self.navigationController?.pushViewController(HomeVC, animated: true)
        present(HomeVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func back_btnAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func getData(){
        
        
        print("userid : \(userId)")
        urlString = "http://www.nitevibe.in/webservices/display_profile?user_id=\(userId)"
        print("userDataUrl: \(urlString)")
        
        do{
            
            var urlRequest = try URLRequest(url: urlString, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print(" dict data event detail \(dictData)")
                
                
                
                for data in dictData{
                    
                    
                    
                    let userName = data["name"] as! String
                    self.getUserAge = data["age"] as! String
                    print("UserAge : \(self.getUserAge)")
                    self.getUserAgeStatus = data["age_status"] as! String
                    print("GetUserAgeStatus : \(self.getUserAgeStatus)")
                    
                    if self.getUserAgeStatus == "Hide"{
                        
                        self.lbl_userName.text = "\(userName)"
                        
                    }else if self.getUserAgeStatus == "Show"{
                        
                        self.lbl_userName.text = "\(self.userName) \(self.getUserAge)"
                    }
                    
                    self.getUserLocation = data["loc"] as! String
                    print("GetUserLocation : \(self.getUserLocation)")
                    
                    self.getUserLocationStatus = data["loc_status"] as! String
                    print("GetUserLocationStatus : \(self.getUserLocationStatus)")
                    
                    if self.getUserLocationStatus == "Hide"{
                        
                       // self.lbl_userLocation.text = ""
                        self.lbl_userLocation.isHidden = true
                        self.userLocHeight.constant = 0
                        
                    }else if self.getUserLocationStatus == "Show"{
                        
                        self.userLocHeight.constant = 21
                        self.lbl_userLocation.isHidden = false
                        self.lbl_userLocation.text = self.getUserLocation
                        
                    }
                    
                    self.getUserDrinks = data["drink"] as! String
                    print("GetUserDrinks : \(self.getUserDrinks)")
                    self.pointsArr = self.getUserDrinks.components(separatedBy: ",")
                    print("GetUserDrinkspointsArr : \(self.pointsArr)")
                    self.drinkCollectionView.reloadData()
                    
                    
                    
                    self.getUserDrinksStatus = data["drink_status"] as! String
                    print("GetUserDrinksStatus : \(self.getUserDrinksStatus)")
                    
                    if self.getUserDrinksStatus == "Hide"{
                        
                        //self.lbl_DrinkPreference.text = ""
                        self.lbl_DrinkPreference.isHidden = true
                        self.drinkHeight.constant = 0
                        self.drinkCollectionView.isHidden = true
                        self.drinkCollHeight.constant = 0
                        self.scrollview.resizeScrollViewContentSize()
                        
                    }else if self.getUserDrinksStatus == "Show"{
                        
                        self.lbl_DrinkPreference.isHidden = false
                        self.drinkHeight.constant = 21
                        self.drinkCollHeight.constant = 59
                        self.lbl_DrinkPreference.text = "Drink Preference"
                        self.drinkCollectionView.isHidden = false
                        self.scrollview.resizeScrollViewContentSize()
                    }
                    
                    
                    self.getUserBiodata = data["bio"] as! String
                    print("getUserBiodata : \(self.getUserBiodata)")
                    self.aboutYou_TextView.text = self.getUserBiodata
                    
                    let userOccupation = data["occupation"] as! String
                    
                    self.lbl_userWorkedAs.text = userOccupation
                    
                    guard let dictImage = data["photos"] as? [Dictionary <String,AnyObject>] else {
                        print("test 3")
                        print("Error with dictionary data: \(String(describing: response.result.error))")
                        return
                    }
                    
                    
                    for imgData in dictImage{
                        
                        let userImages = imgData["img"] as! String
                        print("image Data \(userImages)")
                        self.userProfilePicsArray.append(userImages)
                        
                        
                    }
                   // self.userDefault.set(self.userProfilePicsArray, forKey: "userProfilePicsArray")
                }
                
                
                print("PictureArray : \(self.userProfilePicsArray)")
                print("GetUserDrinkspointsArr1 : \(self.pointsArr)")
                
            }
            
        }catch{
            print("error")
            
        }
        
        
    }
    
    func getEventData(){
        
        
        let urlString = "http://www.nitevibe.in/webservices/view_profile.php?user_id=\(userId)"
        
        print("url event_detail:-\(urlString)")
        do{
            
            var urlRequest = try URLRequest(url: urlString, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                

                print(" dict data event detail \(dictData)")
                
                for data in dictData{
                    //fetch eventdata
                    guard let dictEvents = data["event"] as? [Dictionary <String,AnyObject>] else {
                        print("test 3")
                        print("Error with dictionary data: \(String(describing: response.result.error))")
                        return
                    }
                    self.userEventIdArray.removeAll()
                    self.userEventArray.removeAll()
                    self.userEventClubIdArray.removeAll()
                    for eventData in dictEvents{
                        
                        let eventsGoing = eventData["event_name"] as! String
                        print("event Data : \(eventsGoing)")
                        self.userEventArray.append(eventsGoing)
                        print("event Array Data : \(self.userEventArray)")
                        self.userGoingEvent_collectionView.reloadData()
                        if self.userEventArray.isEmpty{
                            self.lbl_events.isHidden = true
                            self.userGoingEvent_collectionView.isHidden = true
                            
                        }else{
                            
                            self.lbl_events.isHidden = false
                            self.userGoingEvent_collectionView.isHidden = false
                            let defaults = UserDefaults.standard
                            
                            defaults.set(self.userEventArray, forKey: "userEventArray")
                        }
                        
                        let eventId = eventData["event_id"] as! String
                        self.userEventIdArray.append(eventId)
                        let clubId = eventData["event_club"] as! String
                        self.userEventClubIdArray.append(clubId)
                    }
                    
                    guard let dictImages = data["photos"] as? [Dictionary <String,AnyObject>] else {
                        print("test 3")
                        print("Error with dictionary data: \(String(describing: response.result.error))")
                        return
                    }
                    
                    for imagedata in dictImages{
                        
                     let image =  imagedata["img"] as! String
                     self.userProfileImageArray.append(image)
                        
                    self.pageControll_outlet.numberOfPages = self.userProfileImageArray.count

                        
                    }
                    
                    
                    
                    

                    print("print image string array :\(self.userProfileImageArray)")
                    
                    self.userDefault.set(self.userProfileImageArray, forKey: "userProfilePicsArray")
                    
                }
           
            }
           
        }catch{
            print("error")
            
        }
        
    }
    
    
    
    
    
    
    @objc func tapUserPhoto(){
        
        self.loadImages()
        
    }
    
    func loadImages(){
        
        let defaults = UserDefaults.standard
        
        let userImageArray = defaults.stringArray(forKey: "userProfilePicsArray") ?? [String]()
        print("defaultsArray : \(defaults.stringArray(forKey: "userProfilePicsArray") ?? [String]())")
        print("imagesCount : \(userImageArray.count)")
        print("imageArray : \(userImageArray)")
        self.imageArray.removeAll()
        if userImageArray.count == 0 {
            
            print("Do Nothing")
            
        }else {
        
            for i in 0..<userImageArray.count{
            
            let imageURL = URL(string: userImageArray[i])
            let imageData = try! Data(contentsOf: imageURL! as URL)
            print("imageData : \(imageData)")
            self.imageArray.append(imageData)
            
        }
        print("count inside the load action \(self.imageArray.count)")
       
        print("Event Index Before : \(self.eventIndex)")
        self.eventIndex = (self.eventIndex >= self.imageArray.count - 1) ? 0 : self.eventIndex + 1
        print("Event Index After : \(self.eventIndex)")
        
        if self.imageArray[eventIndex].count == 0{
           
            print("Do Nothing")
            
        }else{
        
            self.userProfilePhotos.image = UIImage(data: self.imageArray[eventIndex])
       
            print("imagecount: \(self.imageArray.count)")
            
        }
        print("print images userProfilePhotos : \(String(describing: self.userProfilePhotos.image))")
       // self.pageControll_outlet.currentPage =
        self.pageControll_outlet.currentPage = self.eventIndex
        
        //print("Event Index \(self.eventIndex)")
        //self.eventIndex = (self.eventIndex >= self.imageArray.count - 1) ? 0 : self.eventIndex + 1
      
//        for i in 0..<self.imageArray.count{
//        self.eventIndex = (self.eventIndex >= self.imageArray.count - 1) ? 0 : self.eventIndex + 1
//        self.userProfilePhotos.image = UIImage(data: self.imageArray[i])
//            print("imagecount :\(self.imageArray.count)")
        }
    }
    
    
    
    @IBAction func pageControll_Action(_ sender: UIPageControl) {
        
    self.pageControll_outlet.numberOfPages = self.imageArray.count
        
        print("count inside the page controller action \(self.imageArray.count)")
        
    
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == drinkCollectionView{
            return pointsArr.count
        }
        else{
            
            return userEventArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == drinkCollectionView{
            
            let cell:Drink_CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "drinkCell", for: indexPath) as! Drink_CollectionViewCell
            
            
            cell.lbl_userSelectedDrinks.text = self.pointsArr[indexPath.row]
            
            
            
            
            return cell
        }else{
            
            let cell:userGoingEvent_CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GoingEvent_Cell", for: indexPath) as! userGoingEvent_CollectionViewCell
            
            cell.lbl_eventName.text = self.userEventArray[indexPath.row]
            
            return cell
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == userGoingEvent_collectionView{
            
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            
            let eventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
            
            eventDetailVC.selectedEventId = userEventIdArray[indexPath.row]
            eventDetailVC.selectedClubId =  userEventClubIdArray[indexPath.row]
            
            //print("Selected club Id \(eventDetailVC.selectedClubId)")
            //print("Selected event ID : \(eventIdArray[indexPath.row])")
            
            present(eventDetailVC, animated: true, completion: nil)
            
        }else{
            
            print("do nothing ")
        }
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let defaults = UserDefaults.standard
//
//        let userImageArray = defaults.stringArray(forKey: "userProfilePicsArray") ?? [String]()
//        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
//        if let indexPath = userProfilePhotos.indexPathForItem(at: center) {
//            self.pageController.currentPage = indexPath.row
//        }
//    }
    
}

extension UIScrollView {
    
    func resizeUserProfileScrollViewContentSize() {
        
        var contentRect = CGRect.zero
        
        for view in self.subviews {
            
            contentRect = contentRect.union(view.frame)
            
        }
        
        self.contentSize = contentRect.size
        
    }
    
}
