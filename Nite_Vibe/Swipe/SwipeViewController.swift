//
//  SwipeViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 20/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//
let  MAX_BUFFER_SIZE = 3;
let  SEPERATOR_DISTANCE = 0;
let  TOPYAXIS = 75;
import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import NVActivityIndicatorView
import CTShowcase
var SwipeLaunch = ""


class SwipeViewController: UIViewController, TinderCardDelegate{
    
    
    
    @IBOutlet weak var overlayOutlet: UIImageView!
    
    @IBOutlet weak var noSwipeLeftButton: UIButton!
    @IBOutlet weak var noSwipeLeftImage: UIImageView!
    
    
    @IBOutlet weak var findEventNearMe: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var btn_supervibe: UIButton!
    @IBOutlet weak var btn_dislike: UIButton!
    @IBOutlet weak var viewTinderBackGround: UIView!
    
    @IBOutlet weak var viewActions: UIView!
    @IBOutlet weak var viewActionHeightConstrain: NSLayoutConstraint!
    
   
    //var SuperVibeArray:[UIImage] = [UIImage(named: "supervibe0")!,UIImage(named: "supervibe1")!,UIImage(named: "supervibe2")!,UIImage(named: "supervibe3")!]
    
    
    //var supervibeCount = Int()
    
    var SuperVibeArray:[UIImage] = [UIImage(named: "supervibe3")!,UIImage(named: "supervibe2")!,UIImage(named: "supervibe1")!,UIImage(named: "supervibe0")!]
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var BackBtnView: UIView!
    var btn_back: UIButton!
    var backImg: UIImage!
    
    var HomeBtnView: UIView!
    var home_btn: UIButton!
    var homeImg: UIImage!
    
    var RewindBtnView = UIView()
    var rewind_btn = UIButton()
    var rewindImg: UIImage!
    
    var ForwardLikeBtnView = UIView()
    var ForwardLike_btn = UIButton()
    var ForwardLikeImg: UIImage!
    
    var uID = String()
    var uName = String()
    var uLoc = String()
    var uAge = String()
    var uWorkAs = String()
    var uPhoto = String()
    //var backFromUserProfileUserId = String()
    
    var event_name = String()
    
    var urlString = ""
    var category = String()
   // var selectedEventId = ""
    var matchUserId = [String]()
    var newCardCategory = [String]()
    
    
    
    var currentIndex = 0
    var isMakeUndo = false
    var currentLostCard : TinderCard!
    var currentLoadedCardsArray = [TinderCard]()
    var allCardsArray = [TinderCard]()
    var tCArd : TinderCard!
    
    var cardValueArray = [String]()
    var userIdArray = [String]()
    var userNameArray = [String]()
    var userAgeArray = [String]()
    var userLocationArray = [String]()
    var userOccupationArray = [String]()
    var userProfile = [String]()
    var userViewCategory = String()
    var viewMoreCategory = String()
    
//    var whoSupervibeMeID = ""
//    var eventId = ""
    var WhoSupervibeData = [SupervibeModel]()
    var comingFrom = ""

    var selectedGoingFlag = ""
    var selectedClubIdFromSwipe = ""
    var selectedEventIdFromSwipe = ""
    
//    var userImageArray = [String]()
//    var imageDataInfo = [String]()
    
    var matchData = [userPhotoMatchModel]()
    var swipeData = [SwipeCardModel]()
    var goingSwipeData = [UserGoingSwipeModel]()
    var whoSupervibeData = [SupervibeModel]()
    var swipeCount = Int()
    var matchCount = Int()
    var selectedCkeckInEventId = ""
    var likeCategory = ""
    var supervibe = [Int]()
    var responseMessage = Int()
    var supervibeCount = Int()
    var cardBG = UIImage()
    var uidfromtinder = String()
    var catfromtinder = String()
    
    var superLikeCountFlag = Int()
    
    var selectedViewMoreUserId = ""
    
    var OverlaySwipe = "Swipe First Time Launch"
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var looadingImageView: UIImageView!
    
    @IBOutlet weak var loadinImageViewAppIcon: UIImageView!
    
    
    var activityIndicatorView : NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("EVID 0 :\(selectedEventIdFromSwipe)")
        //let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
       // print("selected user event id \(String(describing: selecteEventId))")
       // self.TinderCardDelegate.prepareScreen(self.navigationController!)
       // self.TinderCard.delegate = self
        //let tab = tCArd
        //tab?.delegate = self
       // self.cardBG = UIImage(named: "2swipe_blur_bg")!
     
        //self.btn_supervibe.isHidden = true

        self.noSwipeLeftImage.isHidden = true
       // self.viewTinderBackGround.isHidden = true
        self.noSwipeLeftButton.isHidden = true
        self.findEventNearMe.isHidden = true
        self.btn_dislike.isHidden = true
        self.btn_supervibe.isHidden = true
        self.likeBtn.isHidden = true
        //self.RewindBtnView.isHidden = true
        //self.ForwardLike_btn.isHidden = true
        //self.ForwardLikeBtnView.isHidden = true
        //self.rewind_btn.isHidden = true
        //self.ForwardLike_btn.isHidden = true
        //self.rewind_btn.isHidden = false
        self.screenSize = UIScreen.main.bounds
       
        self.screenWidth = self.screenSize.width
        self.screenHeight = self.screenSize.height
        view.addSubview(loadingView)
        
        //searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        //searchTextField.text = "Thane"
        
        loadingView.addSubview(looadingImageView)
        looadingImageView.addSubview(loadinImageViewAppIcon)
        
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: w / 2, y: h / 2, width: 200, height: 21),type: NVActivityIndicatorType(rawValue: 17))
        let animationTypeLabel = UILabel(frame: CGRect(x: w / 2, y: h / 2, width: 200, height: 21))
        
        activityIndicatorView.center = CGPoint(x: w / 2, y: h / 2)
        
        //activityIndicatorView.backgroundColor = UIColor.green
        animationTypeLabel.center = CGPoint(x: w / 2, y: h / 2)
        
        animationTypeLabel.sizeToFit()
        animationTypeLabel.textColor = UIColor.red
        //animationTypeLabel.backgroundColor =  UIColor.green
        
        activityIndicatorView.padding = 25
        
        self.looadingImageView.addSubview(activityIndicatorView)
        self.looadingImageView.addSubview(animationTypeLabel)
        activityIndicatorView.startAnimating()
        
        view.layoutIfNeeded()
        
//        if comingFrom == "1"{
//
//            self.getWhoSupervibedData()
//
//        }
        
        
        if selectedGoingFlag == "3"{
           
            self.swipeFromViewMore()
            
            
            
        }else{
            
            getGoingData()
            
        }
        
        addComponents()
        
       
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.swipeOverlay()
    }
    
    
    func swipeOverlay()
    {
        
        let swipeLaunchedBefore = UserDefaults.standard.bool(forKey: "swipeLaunchedBefore")
        
        if swipeLaunchedBefore {
            
            SwipeLaunch = "Swipe Has Launched Before"
            print("SwipeBeforeLaunchStatus : \(SwipeLaunch)")
            
        }
        else{
            
            SwipeLaunch = "Swipe First Time Launch"
            print("SwipeFirstLaunchStatus : \(SwipeLaunch)")
            self.SwipeOverLay()
            UserDefaults.standard.set(true, forKey: "swipeLaunchedBefore")
            
        }
        
    }
    
    func SwipeOverLay(){
        
        print("Print EventDetailsRatio Launch Status : \(OverlaySwipe)")
        print("Print UserDefaultsRatio Launch Status : \(SwipeLaunch)")
        
        if OverlaySwipe == SwipeLaunch{
            
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "Swipe1.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.clear
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            showcase.setup(for: self.overlayOutlet, offset: CGPoint.zero, margin: 0)
            showcase.show()
        }
        else{
            
        }
        
    }
    
//    func getWhoSupervibedData(){
//
//        //need to pass remaining data in webservice like user_id category event id
//
//
//        //            var currentUserId = String()
//        //            var currentUserPhoto = String()
//        //            var matchUserId = String()
//        //            var matchUserPhoto = String()
//
//        //            var respo = Int()
//
//        let getUserData :UserDefaults = UserDefaults.standard
//        let User_Id = getUserData.value(forKey: "userid")
//        print("userid :\(String(describing: User_Id))")
//
//
//        let URL_String = "http://www.nitevibe.in/webservices/who_supervibe.php?login_userid=\(User_Id!)&user_id=\(whoSupervibeMeID)&event_id=\(eventId)"
//
//        print("UserID : \(User_Id!)")
//        print("WhoSupervibedID : \(whoSupervibeMeID)")
//        print("EventID : \(eventId)")
//
//
//        let url = NSURL(string: URL_String)
//        var urlRequest =  URLRequest(url: url! as URL)
//        urlRequest.cachePolicy = .reloadIgnoringCacheData
//
//        Alamofire.request(urlRequest).responseJSON{
//
//            (response) in
//            if let value = response.data {
//                do {
//                    let json = try JSON(data: value)
//                    if let dictionnary = json.dictionaryObject {
//
//                        print("Dict : \(dictionnary.count)")
//                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
//
//                            // print("print swipe data  \(messageArray)")
//                            print("print count: \(messageArray.count)")
//
//                            if messageArray.count == 0{
//
//                                print("no data found")
//                                                                self.activityIndicatorView.isHidden = true
//                                                                self.noSwipeLeftImage.isHidden = false
//                                                                self.viewTinderBackGround.isHidden = false
//                                                                self.noSwipeLeftButton.isHidden = false
//                                                                self.btn_dislike.isHidden = true
//                                                                self.btn_supervibe.isHidden = true
//                                                                self.likeBtn.isHidden = true
//                                                                self.btn_back.isHidden = false
//
//                                                                self.rewind_btn.isHidden = true
//                                                                self.ForwardLike_btn.isHidden = true
//                                                                self.loadingView.isHidden = true
//                                                                self.loadinImageViewAppIcon.isHidden = true
//                                                                self.home_btn.isHidden = false
//                                                                self.findEventNearMe.isHidden = false
//
//
//                                                                self.findEventNearMe.addTarget(self, action: #selector(self.findEventNearMeAction), for: .touchUpInside)
//
//                            }else{
//                                                                self.noSwipeLeftImage.isHidden = true
//                                                                self.viewTinderBackGround.isHidden = false
//                                                                self.noSwipeLeftButton.isHidden = true
//                                                                self.findEventNearMe.isHidden = true
//                                                                self.btn_dislike.isHidden = false
//                                                                self.btn_supervibe.isHidden = false
//                                                                self.likeBtn.isHidden = false
//                                //self.loadingView.isHidden = true
//                                //self.loadinImageViewAppIcon.isHidden = true
//                                                               // self.rewind_btn.isHidden = false
//                                                               // self.ForwardLike_btn.isHidden = false
//                                                                self.btn_back.isHidden = false
//                                                                self.home_btn.isHidden = false
//
//                                                                //self.loadingView.removeFromSuperview()
//                               // self.WhoSupervibeData.removeAll()
//                                for arr in messageArray {
//
//                                    self.WhoSupervibeData.append(SupervibeModel(json: arr))
//
//                                    print("get swipe data \(self.WhoSupervibeData.count)")
//                                    self.supervibeCount = self.WhoSupervibeData.count
//
//
//                                    for i in 0..<self.WhoSupervibeData.count{
//
//
//                                        print("print whoSupervibe name : \(self.WhoSupervibeData[i].userName)")
//                                       // self.lbl_Name.text = self.WhoSupervibeData[i].userName
//
//                                        print("print whoSupervibe images : \(self.WhoSupervibeData[i].userProfilePicUrl)")
//
//
//                                        print("print whoSupervibe age : \(self.WhoSupervibeData[i].userAge)")
//
//                                        print("print whoSupervibe occupation : \(self.WhoSupervibeData[i].userOccupation)")
//
//                                        //self.lbl_Occupation.text = self.WhoSupervibeData[i].userOccupation
//
//                                        print("print whoSupervibe location : \(self.WhoSupervibeData[i].userLocation)")
//
//                                        //                                    if self.WhoSupervibeData[i].userLocation == ""{
//                                        //
//                                        //                                       // self.locationHeight.constant = 0
//                                        //
//                                        //                                    }else{
//                                        //
//                                        //                                    //self.locationHeight.constant = 30
//                                        //self.lbl_Location.text = self.WhoSupervibeData[i].userLocation
//                                        //}
//
//
//
//                                        print("print whoSupervibe goingEvent : \(self.WhoSupervibeData[i].supervibeForEvent)")
//
//                                        print("print supervibe count :\(self.WhoSupervibeData[i].superVibeCount)")
//
//
//                                        if self.WhoSupervibeData[i].superVibeCount == 0{
//
//
//                                                                                        self.btn_supervibe.setImage(UIImage(named: "supervibe3")!, for: .normal)
//
//
//                                        }else if self.WhoSupervibeData[i].superVibeCount == 1{
//                                                                                        self.btn_supervibe.setImage(UIImage(named: "supervibe2")!, for: .normal)
//                                        }else if self.WhoSupervibeData[i].superVibeCount == 2{
//                                                                                        self.btn_supervibe.setImage(UIImage(named: "supervibe1")!, for: .normal)
//
//                                        }else if self.WhoSupervibeData[i].superVibeCount == 3{
//                                                                                        self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)
//
//                                                                                        self.btn_supervibe.isEnabled = false
//
//                                        }
//
//
//                                    }
//                                   self.loadCardforSupervibeNotification()
//
//                                }
//
//                            }
//                           // self.loadCardforSupervibeNotification()
//                        }
//                    }
//
//                }catch {
//                    print("cannot convert to Json")
//                }
//            }
//        }
//
//
//    }
    
//    func loadCardforSupervibeNotification(){
//        
//        self.userIdArray.removeAll()
//        self.userNameArray.removeAll()
//        self.userOccupationArray.removeAll()
//        self.userAgeArray.removeAll()
//        self.userProfile.removeAll()
//        self.supervibe.removeAll()
//        self.userLocationArray.removeAll()
//        self.cardValueArray.removeAll()
////        for i in 0..<self.whoSupervibeData.count{
////
////            //print("user going count \(self.goingSwipeData[0].userGoingArray.count)")
////            print("user Swiped count \(self.WhoSupervibeData.count)")
//
////            for i in 0..self.whoSupervibeData.count{
//
//                print("user going view more name \(self.whoSupervibeData[0].userName)")
//                self.userNameArray.append(self.whoSupervibeData[0].userName)
//                print("user going view more id\(self.whoSupervibeData[0].user_id)")
//                self.userIdArray.append(self.whoSupervibeData[0].user_id)
//                print("user going view more user profile pic \(self.whoSupervibeData[0].userProfilePicUrl)")
//                // self.userProfile.append(self.goingSwipeData[0].userGoingArray[i].userProfilePic)
//                print("user going view more age \(self.whoSupervibeData[0].userAge)")
//                self.userAgeArray.append(self.whoSupervibeData[0].userAge)
//                print("user going view more location \(self.whoSupervibeData[0].userLocation)")
//                self.userLocationArray.append(self.whoSupervibeData[0].userLocation)
//                print("user going view more occupation \(self.whoSupervibeData[0].userOccupation)")
//                self.userOccupationArray.append(self.whoSupervibeData[0].userOccupation)
//
//                //print("user going view more like category \(self.whoSupervibeData[0].userLikeCategory)")
//
//               // self.supervibe.append(Int(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)!)
//
//                print("user going view more profile images \(self.whoSupervibeData[0].userProfilePicUrl)")
//
//                self.cardValueArray.append(self.WhoSupervibeData[0].userProfilePicUrl)
// //           }
////
////
////
// //       }
//
//        let userDefault = UserDefaults.standard
//        userDefault.set(userIdArray, forKey: "userIdArray")
//        
//        print("UserId :\(userIdArray)")
//        
//        print("cardcount : \(self.cardValueArray.count)")
//        
//        if self.cardValueArray.count >= 0{
//            
//            print("cardcount : \(self.cardValueArray.count)")
//            
//            let num_currentLoadedCardsArrayCap = (self.cardValueArray.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : self.cardValueArray.count
//            
//            
//            
////            for i in 0..<self.WhoSupervibeData.count{
////
////                // self.goingSwipeData[1].userGoingArray.count
////
////                //print("user going count \(self.goingSwipeData[0].userGoingArray.count)")
////                print("user supervibe count : \(self.WhoSupervibeData.count)")
//            
//            for i in 0..<self.WhoSupervibeData.count{
//                
//                print("user SVibed count :\(self.WhoSupervibeData.count)")
//                print("print i : \(i)")
//                
//                let newCard = self.createDraggableViewWithData(at: i,userProfilePic:self.WhoSupervibeData[0].userProfilePicUrl,name:self.WhoSupervibeData[0].userName,age:self.WhoSupervibeData[0].userAge,loc:self.WhoSupervibeData[0].userLocation,occupation:self.WhoSupervibeData[0].userOccupation,userId:self.WhoSupervibeData[0].user_id)
//                
//                self.allCardsArray.append(newCard)
//                print("card array : \(num_currentLoadedCardsArrayCap)")
//                if i < num_currentLoadedCardsArrayCap {
//                    self.currentLoadedCardsArray.append(newCard)
//                }
//                
//            }
//
//       //     }
//            for (i,_) in self.currentLoadedCardsArray.enumerated() {
//                if i > 0 {
////                    self.viewTinderBackGround.insertSubview(self.currentLoadedCardsArray[i], belowSubview: self.currentLoadedCardsArray[i - 1])
// //               }
//  //              else{
//                    self.viewTinderBackGround.addSubview(self.currentLoadedCardsArray[i])
//                }
//                
//               // self.currentIndex += 1
//            }
//            
//            self.animateCardAfterSwiping()
//            self.perform(#selector(self.createDummyCard), with: nil, afterDelay: 1.0)
//            
//        }
//        
//        self.loadinImageViewAppIcon.isHidden = true
//        
//        self.activityIndicatorView.isHidden = true
//        
//        
//    }
        
    
    
    
    func getGoingData() {
        URLCache.shared.removeAllCachedResponses()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        
        
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey:"selectedEventIdFromSwipe")
        print("EVID 1 :\(selectedEventIdFromSwipe)")
        userDefault.removeObject(forKey:"selectedCkeckInEventId")
        userDefault.removeObject(forKey:"selectedgoingflag")
        userDefault.set(selectedEventIdFromSwipe, forKey: "selectedEventIdFromSwipe")
        //print("event id 1: \(selectedEventIdFromSwipe)")
        print("EVID 2 :\(selectedEventIdFromSwipe)")
        
        userDefault.set(selectedCkeckInEventId, forKey: "selectedCkeckInEventId")
        print("event id 2: \(selectedCkeckInEventId)")
        
        
        userDefault.set(selectedGoingFlag, forKey: "selectedgoingflag")
        
        print("event id 3: \(selectedGoingFlag)")
        
        if selectedGoingFlag == "0"{
            
            //call whos check in swipe card api
            urlString = "http://www.nitevibe.in/webservices/checkin_swipe_card.php?user_id=\(User_Id!)&event_id=\(selectedCkeckInEventId)"
            print("checkInUrl: \(urlString)")
            
            
            
        }else if selectedGoingFlag == "1"{
            print("EVID 3 :\(selectedEventIdFromSwipe)")
            //call whos going swipe card api
            urlString = "http://www.nitevibe.in/webservices/going_swipe_card.php?user_id=\(User_Id!)&event_id=\(selectedEventIdFromSwipe)"
            print("goingInUrl: \(urlString)")
            
            
        }
//        else if selectedGoingFlag == "2"{
//
////            //call userGoingSwipe api
////            urlString = "http://www.nitevibe.in/webservices/going_swipe_card.php?user_id=\(User_Id!)&event_id=\(selectedEventIdFromSwipe)"
////            print("goingInUrl: \(urlString)")
//
//            self.swipeFromViewMore()
//
//
//        }
        
        
       // Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
        //let url = NSURL(string: urlString)
        //var urlRequest = URLRequest(url: url! as URL)
        //urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let url = NSURL(string: urlString)
        var urlRequest =  URLRequest(url: url! as URL)
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).responseJSON{
            
            (response) in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        
                        print("Dict : \(dictionnary.count)")
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            // print("print swipe data  \(messageArray)")
                            print("print count: \(messageArray.count)")
                            
                            if messageArray.count == 0{
                                
                                print("no data found")
                                self.activityIndicatorView.isHidden = true
                                self.noSwipeLeftImage.isHidden = false
                                self.viewTinderBackGround.isHidden = false
                                self.noSwipeLeftButton.isHidden = false
                                self.btn_dislike.isHidden = true
                                self.btn_supervibe.isHidden = true
                                self.likeBtn.isHidden = true
                                self.btn_back.isHidden = false
                                
                                self.rewind_btn.isHidden = true
                                self.ForwardLike_btn.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadinImageViewAppIcon.isHidden = true
                                self.home_btn.isHidden = false
                                self.findEventNearMe.isHidden = false
                                
                                
                                self.findEventNearMe.addTarget(self, action: #selector(self.findEventNearMeAction), for: .touchUpInside)
                                
                            }else{
                                self.noSwipeLeftImage.isHidden = true
                                self.viewTinderBackGround.isHidden = false
                                self.noSwipeLeftButton.isHidden = true
                                self.findEventNearMe.isHidden = true
                                self.btn_dislike.isHidden = false
                                self.btn_supervibe.isHidden = false
                                self.likeBtn.isHidden = false
                                self.rewind_btn.isHidden = false
                                self.ForwardLike_btn.isHidden = false
                                self.btn_back.isHidden = false
                                self.home_btn.isHidden = false
                                
                                self.loadingView.removeFromSuperview()
                                self.swipeData.removeAll()
                                for arr in messageArray {
                                    
                                    self.swipeData.append(SwipeCardModel(json: arr))
                                    print("get swipe data \(self.swipeData.count)")
                                    self.swipeCount = self.swipeData.count
                                    
                                    
                                    for i in 0..<self.swipeData.count{
                                        
                                        print("print swipe user id : \(self.swipeData[i].user_id)")
                                        print("print user images : \(self.swipeData[i].userPhotoData)")
                                        print("print supervibe count :\(self.swipeData[i].superVibeCount)")
                                        
                                        
                                        if self.swipeData[i].superVibeCount == 0{
                                            
                                            
                                            self.btn_supervibe.setImage(UIImage(named: "supervibe3")!, for: .normal)
                                           
                                            
                                        }else if self.swipeData[i].superVibeCount == 1{
                                            self.btn_supervibe.setImage(UIImage(named: "supervibe2")!, for: .normal)
                                        }else if self.swipeData[i].superVibeCount == 2{
                                            self.btn_supervibe.setImage(UIImage(named: "supervibe1")!, for: .normal)
                                            
                                        }else if self.swipeData[i].superVibeCount == 3{
                                            self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)
                                            
                                            self.btn_supervibe.isEnabled = false
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                            self.loadCardsforGoing()
                        }
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
        }
        
        
    }
    
    
    
    
    
    
    func swipeFromViewMore(){
        
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey:"selectedEventIdFromSwipe")
        print("EVID 1 :\(selectedEventIdFromSwipe)")
        userDefault.removeObject(forKey:"selectedCkeckInEventId")
        userDefault.removeObject(forKey:"selectedgoingflag")
        userDefault.set(selectedEventIdFromSwipe, forKey: "selectedEventIdFromSwipe")
        //print("event id 1: \(selectedEventIdFromSwipe)")
        print("EVID 2 :\(selectedEventIdFromSwipe)")
        
        userDefault.set(selectedCkeckInEventId, forKey: "selectedCkeckInEventId")
        print("event id 2: \(selectedCkeckInEventId)")
        
        
        userDefault.set(selectedGoingFlag, forKey: "selectedgoingflag")
        
        print("event id 3: \(selectedGoingFlag)")
        print("EVID 4 :\(selectedEventIdFromSwipe)")
        URLCache.shared.removeAllCachedResponses()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        urlString = "http://www.nitevibe.in/webservices/user_going_swipe.php?event_id=\(selectedEventIdFromSwipe)&user_id=\(User_Id!)&selected_id=\(selectedViewMoreUserId)&club_id=\(selectedClubIdFromSwipe)&visited="
        print("goingInUrl: \(urlString)")
        
        
        
        let url = NSURL(string: urlString)
        var urlRequest =  URLRequest(url: url! as URL)
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).responseJSON{
            
            (response) in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        
                        print("Dict : \(dictionnary.count)")
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            // print("print swipe data  \(messageArray)")
                            print("print count: \(messageArray.count)")
                            
                            if messageArray.count == 0{
                                
                                print("no data found")
                                self.activityIndicatorView.isHidden = true
                                self.noSwipeLeftImage.isHidden = false
                                self.viewTinderBackGround.isHidden = false
                                self.noSwipeLeftButton.isHidden = false
                                self.btn_dislike.isHidden = true
                                self.btn_supervibe.isHidden = true
                                self.likeBtn.isHidden = true
                                self.btn_back.isHidden = false
                                
                                self.rewind_btn.isHidden = true
                                self.ForwardLike_btn.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadinImageViewAppIcon.isHidden = true
                                self.home_btn.isHidden = false
                                
                            }else{
                                self.noSwipeLeftImage.isHidden = true
                                self.viewTinderBackGround.isHidden = false
                                self.noSwipeLeftButton.isHidden = true
                                self.btn_dislike.isHidden = false
                                self.btn_supervibe.isHidden = false
                                self.likeBtn.isHidden = false
                                self.rewind_btn.isHidden = false
                                self.ForwardLike_btn.isHidden = false
                                self.btn_back.isHidden = false
                                self.home_btn.isHidden = false
                                //self.RewindBtnView.isHidden = false
                                //self.ForwardLike_btn.isHidden = true
                                //self.ForwardLike_btn.isHidden = false
                                
                                //self.loadingView.isHidden = true
                                
                                self.loadingView.removeFromSuperview()
                                self.goingSwipeData.removeAll()
                                for arr in messageArray {
                                    
                                    self.goingSwipeData.append(UserGoingSwipeModel(json: arr))
                                    print("get going swipe data \(self.goingSwipeData.count)")
                                   // self.swipeCount = self.swipeData.count
                                    
                                    
                                    for i in 0..<self.goingSwipeData.count{
                                        
                                        
                                        
                                       // self.goingSwipeData[1].userGoingArray.count
                                        
                                        print("user going count \(self.goingSwipeData[0].userGoingArray.count)")
                                        
                                        
                                        
                                        
                                        for i in 0..<self.goingSwipeData[0].userGoingArray.count{
                                            
                                            print("user going view more name \(self.goingSwipeData[0].userGoingArray[i].userName)")
                                           
                                            print("user supervibe count :  \(self.goingSwipeData[0].userGoingArray[i].superVibeCount)")
                                            
                                            print("user supervibe category :  \(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)")
                                            self.viewMoreCategory = self.goingSwipeData[0].userGoingArray[i].userLikeCategory
                                            print("VMCategory : \(self.viewMoreCategory)")
                                            
                                           
                                            if self.goingSwipeData[0].userGoingArray[i].superVibeCount == 0{


                                                self.btn_supervibe.setImage(UIImage(named: "supervibe3")!, for: .normal)


                                            }else if self.goingSwipeData[0].userGoingArray[i].superVibeCount == 1{
                                                self.btn_supervibe.setImage(UIImage(named: "supervibe2")!, for: .normal)
                                            }else if self.goingSwipeData[0].userGoingArray[i].superVibeCount == 2{
                                                self.btn_supervibe.setImage(UIImage(named: "supervibe1")!, for: .normal)

                                            }else if self.goingSwipeData[0].userGoingArray[i].superVibeCount == 3{
                                                self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)

                                                self.btn_supervibe.isEnabled = false

                                            }

                                            
                                        }
                                       
                                        
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                           self.loadCardsforGoingViewMore()
                        }
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
        }
        
        
    }
    
    func loadCardsforGoingViewMore(){
        
        
        self.userIdArray.removeAll()
        self.userNameArray.removeAll()
        self.userOccupationArray.removeAll()
        self.userAgeArray.removeAll()
        self.userProfile.removeAll()
        self.supervibe.removeAll()
        self.userLocationArray.removeAll()
        self.cardValueArray.removeAll()
        self.userViewCategory.removeAll()
        for i in 0..<self.goingSwipeData.count{
            
            print("user going count \(self.goingSwipeData[0].userGoingArray.count)")
            
            for i in 0..<self.goingSwipeData[0].userGoingArray.count{
                
                print("user going view more name \(self.goingSwipeData[0].userGoingArray[i].userName)")
                self.userNameArray.append(self.goingSwipeData[0].userGoingArray[i].userName)
                print("user going view more id\(self.goingSwipeData[0].userGoingArray[i].userId)")
                self.userIdArray.append(self.goingSwipeData[0].userGoingArray[i].userId)
                print("user going view more user profile pic \(self.goingSwipeData[0].userGoingArray[i].userProfilePic)")
               // self.userProfile.append(self.goingSwipeData[0].userGoingArray[i].userProfilePic)
                print("user going view more age \(self.goingSwipeData[0].userGoingArray[i].userAge)")
                self.userAgeArray.append(self.goingSwipeData[0].userGoingArray[i].userAge)
                print("user going view more location \(self.goingSwipeData[0].userGoingArray[i].userLoaction)")
                self.userLocationArray.append(self.goingSwipeData[0].userGoingArray[i].userLoaction)
                print("user going view more occupation \(self.goingSwipeData[0].userGoingArray[i].userOccupation)")
                self.userOccupationArray.append(self.goingSwipeData[0].userGoingArray[i].userOccupation)
                
                print("user going view more like category \(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)")
                
                self.supervibe.append(Int(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)!)
                
                print("user going view more profile images \(self.goingSwipeData[0].userGoingArray[i].userProfilePic)")
                
                self.cardValueArray.append(self.goingSwipeData[0].userGoingArray[i].userProfilePic)
                
                print("user category \(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)")
                self.userViewCategory.append(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)
            }
    
    }
        
        let userDefault = UserDefaults.standard
        userDefault.set(userIdArray, forKey: "userIdArray")
        
        if self.cardValueArray.count > 0{
            
        let num_currentLoadedCardsArrayCap = (self.cardValueArray.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : self.cardValueArray.count
            
            
            
            for i in 0..<self.goingSwipeData.count{
                
                // self.goingSwipeData[1].userGoingArray.count
                
                print("user going count \(self.goingSwipeData[0].userGoingArray.count)")
                print("CardCategory 1: \(self.goingSwipeData[0].userGoingArray[i].userLikeCategory)")
                
                for i in 0..<self.goingSwipeData[0].userGoingArray.count{
                    
                    print("user going view more count :\(self.goingSwipeData[0].userGoingArray.count)")
                    print("print i : \(i)")
                    
                    let newCard = self.createDraggableViewWithData(at: i,userProfilePic:self.goingSwipeData[0].userGoingArray[i].userProfilePic,name:self.goingSwipeData[0].userGoingArray[i].userName,age:self.goingSwipeData[0].userGoingArray[i].userAge,loc:self.goingSwipeData[0].userGoingArray[i].userLoaction,occupation:self.goingSwipeData[0].userGoingArray[i].userOccupation,userId:self.goingSwipeData[0].userGoingArray[i].userId, userCat: self.goingSwipeData[0].userGoingArray[i].userLikeCategory)
                    
                    
                    self.allCardsArray.append(newCard)
                    print("card array : \(num_currentLoadedCardsArrayCap)")
                    if i < num_currentLoadedCardsArrayCap {
                        self.currentLoadedCardsArray.append(newCard)
                    }
                    
                    
                }
            }
            
            
            for (i,_) in self.currentLoadedCardsArray.enumerated() {
                if i > 0 {
                    self.viewTinderBackGround.insertSubview(self.currentLoadedCardsArray[i], belowSubview: self.currentLoadedCardsArray[i - 1])
                }else{
                    self.viewTinderBackGround.addSubview(self.currentLoadedCardsArray[i])
                }
                
                self.currentIndex += 1
                print("cIndex : \(self.currentIndex)")
            }
            
            self.animateCardAfterSwiping()
            self.perform(#selector(self.createDummyCard), with: nil, afterDelay: 1.0)
            
        }
        
        self.loadinImageViewAppIcon.isHidden = true
        
        self.activityIndicatorView.isHidden = true
    
        
    }
    func loadCardsforGoing(){
        
        
        self.userIdArray.removeAll()
        self.userNameArray.removeAll()
        self.userOccupationArray.removeAll()
        self.userAgeArray.removeAll()
        self.userOccupationArray.removeAll()
        self.supervibe.removeAll()
        self.userLocationArray.removeAll()
        self.cardValueArray.removeAll()
        
        print("swipeCount : \(self.swipeCount)")
        
        for i in 0..<self.swipeData.count{
            
            print("AllUserProPicCountArray : \(self.swipeData[i].userProfilePicUrl)")
            self.cardValueArray.append(self.swipeData[i].userProfilePicUrl)
            
            print("AllUserIdCountArray : \(self.swipeData[i].user_id)")
            self.userIdArray.append(self.swipeData[i].user_id)
            
            print("AllUserNameCountArray : \(self.swipeData[i].userName)")
            self.userNameArray.append(self.swipeData[i].userName)
            
            print("AllUserAgeCountArray : \(self.swipeData[i].userAge)")
            self.userAgeArray.append(self.swipeData[i].userAge)
            
            print("AllUserLocationCountArray : \(self.swipeData[i].userLocation)")
            self.userLocationArray.append(self.swipeData[i].userLocation)
            
            print("AllUserOccupationCountArray : \(self.swipeData[i].userOccupation)")
            self.userOccupationArray.append(self.swipeData[i].userOccupation)
            
            print("supercount : \(self.swipeData[i].superVibeCount)")
            self.supervibe.append(self.swipeData[i].superVibeCount)
            
        }
        
        let userDefault = UserDefaults.standard
        userDefault.set(userIdArray, forKey: "userIdArray")
        
        if self.cardValueArray.count > 0 {
            let num_currentLoadedCardsArrayCap = (self.cardValueArray.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : self.cardValueArray.count
            
            for i in 0..<self.swipeData.count {
                
                
                print("print i : \(i)")
                print("print total user count: \(self.swipeData.count)")
                let newCard = self.createDraggableViewWithData(at: i,userProfilePic:self.swipeData[i].userProfilePicUrl,name:self.swipeData[i].userName,age:self.swipeData[i].userAge,loc:self.swipeData[i].userLocation,occupation:self.swipeData[i].userOccupation,userId:self.swipeData[i].user_id, userCat: "1")
               // self.allCardsArray.removeAll()
                self.allCardsArray.append(newCard)
               // self.currentLoadedCardsArray.removeAll()
                print("card array : \(num_currentLoadedCardsArrayCap)")
                if i < num_currentLoadedCardsArrayCap {
                    self.currentLoadedCardsArray.append(newCard)
                }
            }
           
            
            for (i,_) in self.currentLoadedCardsArray.enumerated() {
                if i > 0 {
                    self.viewTinderBackGround.insertSubview(self.currentLoadedCardsArray[i], belowSubview: self.currentLoadedCardsArray[i - 1])
                }else{
                    self.viewTinderBackGround.addSubview(self.currentLoadedCardsArray[i])
                 }
                
                self.currentIndex += 1
            }
            
            self.animateCardAfterSwiping()
            self.perform(#selector(self.createDummyCard), with: nil, afterDelay: 1.0)
            
        }
        
        self.loadinImageViewAppIcon.isHidden = true
        
        self.activityIndicatorView.isHidden = true
        
    }
    
    
    
    

    func addComponents(){
       
        BackBtnView = UIView(frame: CGRect(x: 17, y: 40, width: 46 , height:46 ))
        BackBtnView.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        backImg = UIImage(named: "BackButton") as UIImage?
        btn_back = UIButton(type: UIButtonType.custom) as UIButton
        btn_back.frame = CGRect(x:5, y:5, width:36, height:36)
        btn_back.setImage(backImg, for:.normal)
        btn_back.isHidden = true
        btn_back.addTarget(self, action:#selector(backButtonTapped) , for: .touchUpInside)
        BackBtnView.addSubview(btn_back)
        
        self.view.addSubview(BackBtnView)
        
        
        HomeBtnView = UIView(frame: CGRect(x: screenWidth - 75, y: 15, width: 76 , height: 100 ))
        HomeBtnView.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        homeImg = UIImage(named: "NavHome") as UIImage?
        home_btn = UIButton(type: UIButtonType.custom) as UIButton
        home_btn.frame = CGRect(x:0, y:0, width:66, height:100)
        home_btn.setImage(homeImg, for:.normal)
        home_btn.isHidden = true
        home_btn.addTarget(self, action: #selector(home_Btn_Action), for: .touchUpInside)
        HomeBtnView.addSubview(home_btn)

        self.view.addSubview(HomeBtnView)
        
        RewindBtnView = UIView(frame: CGRect(x:0, y: 350, width: 39 , height: 96 ))
        RewindBtnView.backgroundColor = UIColor.clear
        
        rewindImg = UIImage(named: "Rewind") as UIImage?
        rewind_btn = UIButton(type: UIButtonType.custom) as UIButton
        rewind_btn.frame = CGRect(x:0, y:0, width:29, height:86)
        rewind_btn.setBackgroundImage(rewindImg, for: .normal)
        rewind_btn.isHidden = true
        //rewind_btn.setImage(rewindImg, for:.normal)
        rewind_btn.addTarget(self, action: #selector(rewind_Btn_Action), for: .touchUpInside)
        RewindBtnView.addSubview(rewind_btn)
        
        self.view.addSubview(RewindBtnView)
        
        ForwardLikeBtnView = UIView()
        ForwardLikeBtnView.frame = CGRect(x: screenWidth - 30, y: 350 , width: 39 , height: 96 )
        ForwardLikeBtnView.backgroundColor = UIColor.clear
        ForwardLikeImg = UIImage(named: "ForwardLike") as UIImage?
        ForwardLike_btn = UIButton(type: UIButtonType.custom) as UIButton
        ForwardLike_btn.frame = CGRect(x:0, y:0, width:29, height:86)
        ForwardLike_btn.setBackgroundImage(ForwardLikeImg, for: .normal)
        ForwardLike_btn.isHidden = true
        //ForwardLike_btn.setImage(ForwardLikeImg, for:.normal)
        ForwardLike_btn.addTarget(self, action: #selector(forward_Btn_Action), for: .touchUpInside)
        ForwardLikeBtnView.addSubview(ForwardLike_btn)
        
        self.view.addSubview(ForwardLikeBtnView)
        self.view.addSubview(viewActions)
        self.viewActions.addSubview(btn_dislike)
        self.viewActions.addSubview(btn_supervibe)
        self.viewActions.addSubview(likeBtn)
        self.btn_dislike.addTarget(self, action:#selector(dislikeBtnTapped), for:.touchUpInside)
         self.btn_supervibe.addTarget(self, action:#selector(superlikeBtnTapped), for:.touchUpInside)
        self.likeBtn.addTarget(self, action:#selector(likeBtnTapped), for:.touchUpInside)
        
        
    }
    
    
    @objc func dislikeBtnTapped(){

        let card = currentLoadedCardsArray.first
        
        card?.leftClickAction()
        print("dislike button tapped ")
    }
    
    
    @objc func superlikeBtnTapped(){

        let card = currentLoadedCardsArray.first
        card?.upClickAction()

    }
    
    
    func transformCount(superVibeCount:Int){
        
        
        print("superLikeCountFlag : \(superLikeCountFlag)")
        
        if superLikeCountFlag == 0{
            self.btn_supervibe.setImage(UIImage(named: "supervibe3")!, for: .normal)

        }else if superLikeCountFlag == 1{
            self.btn_supervibe.setImage(UIImage(named: "supervibe2")!, for: .normal)


        }else if superLikeCountFlag == 2{
            self.btn_supervibe.setImage(UIImage(named: "supervibe1")!, for: .normal)
            //self.btn_supervibe.isEnabled = false

        }else if superLikeCountFlag == 3{
            self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)
            self.btn_supervibe.isEnabled = false
        }
//        if superLikeCountFlag == 0{
//           self.btn_supervibe.setImage(UIImage(named: "supervibe2")!, for: .normal)
//
//        }else if superLikeCountFlag == 1{
//           self.btn_supervibe.setImage(UIImage(named: "supervibe1")!, for: .normal)
//
//
//        }else if superLikeCountFlag == 2{
//            self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)
//            self.btn_supervibe.isEnabled = false
//
//        }else if superLikeCountFlag == 3{
//           self.btn_supervibe.setImage(UIImage(named: "supervibe0")!, for: .normal)
//             self.btn_supervibe.isEnabled = false
//        }
        
    }
    
    @objc func likeBtnTapped(){
        
        print("Like button tapped")
        let card = currentLoadedCardsArray.first
        card?.rightClickAction()
    }
    @IBAction func NoSwipeLeft_btnAction(_ sender: UIButton) {
       
        let BaseController:BaseTabBarController = storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        self.present(BaseController, animated: true, completion: nil)
        
    }
    
    @objc func backButtonTapped(){
        
        
        if selectedGoingFlag == "1"{
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            
            let EventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
            print("EVID 5 :\(selectedEventIdFromSwipe)")
            EventDetailVC.selectedClubId = selectedClubIdFromSwipe
            EventDetailVC.selectedEventId = selectedEventIdFromSwipe
            present(EventDetailVC, animated: true, completion: nil)
            
        }else if selectedGoingFlag == "0"{
            
            let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
            
            let ClubDetailVC = storyboard.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
            ClubDetailVC.selectedClubId = selectedClubIdFromSwipe
            present(ClubDetailVC, animated: true, completion: nil)
            
        }else if selectedGoingFlag == "3"{
            
            let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
            
            let ViewMoreVC = storyboard.instantiateViewController(withIdentifier: "ViewMoreViewController") as! ViewMoreViewController
            print("EVID 6 :\(selectedEventIdFromSwipe)")
            ViewMoreVC.selectedGoingFlag = "3"
            ViewMoreVC.selectedVMClubId = selectedClubIdFromSwipe
            ViewMoreVC.selectedVMEventId = selectedEventIdFromSwipe
            present(ViewMoreVC, animated: true, completion: nil)
            
        }
        
        
    }
    
    @objc func home_Btn_Action() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        //EventDetailVC.selectedClubId = selectedClubIdFromSwipe
       // EventDetailVC.selectedEventId = selectedEventIdFromSwipe
        present(HomeVC, animated: true, completion: nil)
        
    }
    
    @objc func rewind_Btn_Action() {
       
        if !isMakeUndo {
            isMakeUndo = true
            // buttonUndo.isHidden = true
            
            
            
            currentIndex -= 1
            
            print("print current index :\(currentIndex)")
            print("print currentLoadedCardsArray count :\(currentLoadedCardsArray.count)")
            print("print allCard array : \(allCardsArray.count)")
            
            var manuplateIndex = currentIndex-currentLoadedCardsArray.count
            
            
            print("index value :\(manuplateIndex)")
           
            
            if manuplateIndex >= 0{
                
                let undoCard = allCardsArray[manuplateIndex]
                currentLoadedCardsArray.insert(undoCard, at: 0)
                viewTinderBackGround.addSubview(undoCard)
                undoCard.makeUndoAction()
                if (currentLoadedCardsArray.count > MAX_BUFFER_SIZE){
                    currentLoadedCardsArray.last?.removeFromSuperview()
                    currentLoadedCardsArray.removeLast()
                }else{
                    currentIndex = allCardsArray.count
                }
                animateCardAfterSwiping()
                print("currentIndex: \(currentIndex)")
                
                
            }else{
                
               print("handle index do nothing ")
                
            }
            
            
        }
        
    }
    
    @objc func forward_Btn_Action() {
        let card = currentLoadedCardsArray.first
        card?.rightClickAction()
        
    }
    
    
    @objc func createDummyCard() {
        
        let dummyCard = currentLoadedCardsArray.first;
        dummyCard?.shakeCard()
        UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveLinear, animations: {
            self.viewActions.alpha = 1.0
        }, completion: nil)
    }
    
    func createDraggableViewWithData(at index: Int , userProfilePic :String,name:String,age:String,loc:String,occupation:String,userId:String,userCat: String) -> TinderCard {
        
        let card = TinderCard(frame: CGRect(x: 10, y: 0, width: viewTinderBackGround.frame.size.width - 20 , height: viewTinderBackGround.frame.size.height - 10) ,userProfilePic: userProfilePic,name:name,age:age,loc:loc,occupation:occupation,userId:userId,userCat: userCat)
        
        print("userId :\(userId)")
        print("userProfilePic : \(userProfilePic)")
        print("name :\(name)")
        print("age :\(age)")
        print("loc :\(loc)")
        print("occupation :\(occupation)")
        
        card.delegate = self
        card.intex = index
        print("cardIndex: \(card.intex)")
        
        return card
    }
    
    func removeObjectAndAddNewValues() {
        
        currentLostCard = currentLoadedCardsArray.first!
      //  buttonUndo.isHidden = true
        Timer.scheduledTimer(timeInterval: 1.01, target: self, selector: #selector(enableUndoButton), userInfo: currentLoadedCardsArray.first, repeats: false)
        currentLoadedCardsArray.remove(at: 0)
        if currentIndex < allCardsArray.count {
            let card = allCardsArray[currentIndex]
            var frame = card.frame
            frame.origin.y = CGFloat(MAX_BUFFER_SIZE * SEPERATOR_DISTANCE)
            card.frame = frame
            currentLoadedCardsArray.append(card)
            currentIndex += 1
            print("CurrentI : \(currentIndex)")
            viewTinderBackGround.insertSubview(currentLoadedCardsArray[MAX_BUFFER_SIZE - 1], belowSubview: currentLoadedCardsArray[MAX_BUFFER_SIZE - 2])
            
            
        }
        
        print("current card array count:\(currentLoadedCardsArray.count)")
        
        //disbale all button if current card is empty
        if currentLoadedCardsArray.count == 0{
            
            self.likeBtn.isHidden = true
            self.btn_dislike.isHidden = true
            self.btn_supervibe.isHidden = true
            self.noSwipeLeftImage.isHidden = false
            
            self.noSwipeLeftButton.isHidden = false
            self.findEventNearMe.isHidden = false
            self.ForwardLike_btn.isHidden = true
            self.rewind_btn.isHidden = true
              self.findEventNearMe.addTarget(self, action:#selector(self.findEventNearMeAction), for: .touchUpInside)
           
        }
        print("currentIndex: \(currentIndex)")
        animateCardAfterSwiping()
    }
    
    func animateCardAfterSwiping() {
        
        for (i,card) in currentLoadedCardsArray.enumerated() {
            UIView.animate(withDuration: 0.5, animations: {
                if i == 0 {
                    card.isUserInteractionEnabled = true
                }
                var frame = card.frame
                frame.origin.y = CGFloat(i * SEPERATOR_DISTANCE)
                card.frame = frame
            })
        }
    }
    

    func getSuperVibeCount(){
        
        URLCache.shared.removeAllCachedResponses()
        
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("EVID 7 :\(selectedEventIdFromSwipe)")
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/supervibe_count.php?user_id=\(User_Id!)&event_id=\(selecteEventId!)"
        
        print("url string supervibe count : \(URL_String)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            let url = NSURL(string: URL_String )
            var urlRequest =  URLRequest(url: url! as URL)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,Int> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict superVibe respo: \(dict["response"]!)")
                print("dict superVibe message:\(dict["message"]!)")
                respo = dict["response"]!
                self.responseMessage = dict["message"]!
                
                
                print("message supervibe count : \(self.responseMessage)")
                
                
                
                
                let Udefaults = UserDefaults.standard
                Udefaults.removeObject(forKey: "responseMessage")
                Udefaults.set(self.responseMessage, forKey: "responseMessage")
                
                if(respo == 201){
                    print("Message: \(String(describing: self.responseMessage))")
                    
                }else if(respo == 200){
                    
                    self.superLikeCountFlag = self.responseMessage
                    print("superVibeCount : \(self.superLikeCountFlag)")
                    print("Message 49 :  \(String(describing: self.responseMessage))")
                    print("success")
                    
                    
                    
                     self.transformCount(superVibeCount:self.responseMessage)
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    
    @IBAction func btn_superVibe_action(_ sender: UIButton) {
        
        category = "3"
        self.getSuperVibeCount()//SuperVibe Restriction by fetching supervibe count
        self.getSwipeData()//To choose Category
        
        var superVibeIntArray = [Int]()
        
        
        
        for j in 0..<SuperVibeArray.count{
        
            superVibeIntArray.append(j)
        
        }
        
        for i in 0..<SuperVibeArray.count{
            
            print("imge count : \(SuperVibeArray.count)")
            
           // self.getSuperVibeCount()
            let Userdefaults = UserDefaults.standard
            let defaultSuperlikeArray = Userdefaults.integer(forKey: "responseMessage")
            
            if defaultSuperlikeArray == superVibeIntArray[i]{
                
                print("message response : \(defaultSuperlikeArray)")
                
                let image:UIImage = SuperVibeArray[i+1]
                
                print("image Name : \(SuperVibeArray[i+1])")
                
                btn_supervibe.setImage(image, for: .normal)
                
            }
            self.getSuperVibeCount()
        }
        
        let card = self.currentLoadedCardsArray.first
        card?.upClickAction()
        
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.btn_supervibe.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }


        UIView.animate(withDuration: 0.5, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.btn_supervibe.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)
        }, completion: nil)


        UIView.animate(withDuration: 0.5, animations: {
            print(self.btn_supervibe.frame.origin.y)
            self.btn_supervibe.frame.origin.y -= 300

        }){_ in

            UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: UInt(0.0)), animations: {self.btn_supervibe.frame.origin.y += 300 }, completion: nil)
        }

    }
    

    
    func getSwipeData(){
        //need to pass remaining data in webservice like user_id category event id
        var currenttUserId = String()
        var currenttUserPhoto = String()
        var matchtUserId = String()
        var matchtUserPhoto = String()
        
       
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/swipe.php?user_id=\(User_Id!)&swipe_user_id=\(uidfromtinder)&category=\(catfromtinder)&event_id=\(selecteEventId!)"
        print("URL in swipeVC : \(URL_String)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String , method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                //print("data user id \(dict["user_id"] as! String)")
                
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print(" dict data : \(dictData)")
                
                
                if dictData.count == 0{
                    
                    print("do nothing continue swiping ")
                    
                }else{
                    
                    for data in dictData{
                        
                        
                        print("data current user id \(data["user_id"] as! String)")
                        print("data current user photo \(data["user_photo"] as! String)")
                        print("data match user id \(data["match_id"] as! String)")
                        print("data match user photo \(data["match_photo"] as! String)")
                        
                        
                        currenttUserId  = data["user_id"] as! String
                        print("CUID : \(currenttUserId)")
                        currenttUserPhoto = data["user_photo"] as! String
                        print("CUPIC : \(currenttUserPhoto)")
                        matchtUserId = data["match_id"] as! String
                        print("MCUID : \(matchtUserId)")
                        matchtUserPhoto = data["match_photo"] as! String
                        print("MCUPIC : \(matchtUserPhoto)")
       
                    }
                    
                    self.sendMatchNotification()
                    DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
                    let goodvibesVC:goodvibesViewController = storyboard.instantiateViewController(withIdentifier: "goodvibesViewController") as! goodvibesViewController
                    //goodvibesVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
                    print("CUID 1 : \(currenttUserId)")
                    goodvibesVC.currentUserId = currenttUserId
                    print("CUPIC 2 : \(currenttUserPhoto)")
                    goodvibesVC.currentUserImageUrlString = currenttUserPhoto
                    print("MCUID 1: \(matchtUserId)")
                    goodvibesVC.matchUserId = matchtUserId
                    print("MCUPIC 2 : \(matchtUserPhoto)")
                    goodvibesVC.matchImageUrlString = matchtUserPhoto
                    goodvibesVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
                    
                    //                        self.addChildViewController(goodvibesVC)
                    //                        self.view.addSubview((goodvibesVC.view)!)
                    
                    //self.present(goodvibesVC, animated: true, completion: nil)
                        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                        appDelegate.window?.rootViewController = goodvibesVC
                    
                    }
                    
                    
//                    DispatchQueue.main.async {
//                        
//
//                        
//                        
//                        
//                        
//                    }
                    
                    
                }

                
                print(type(of:dictData))
                
                
                if(respo == 201){
                    print("Message: \(String(describing: responseMessage))")
                    
                }else if(respo == 200){
                    self.getSuperVibeCount()
                    print("success")
                    
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    func sendMatchNotification(){
        // print("ClubID : \(club_id)")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("Matchuserid :\(matchUserId)")
        
        // print("selected club id \(selectedClubId)")
        let URL_String = "http://www.nitevibe.in/webservices/match_notification.php?user_id=\(User_Id!)&match_id=\(matchUserId)"
        
        print("test Notification URL : \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response Noti :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
        
        
    }

    
   
    
    
    @objc func enableUndoButton(timer: Timer){
        
        let  lostCard = timer.userInfo as! TinderCard
        if currentLostCard.intex == lostCard.intex{
            //self.buttonUndo.isHidden = false
            self.isMakeUndo = false
        }
    }
    
    
    
    func cardSwipedLeft(_ card: TinderCard) {
        removeObjectAndAddNewValues()
    }
    
    func cardSwipedRight(_ card: TinderCard) {
        removeObjectAndAddNewValues()
    }
    
    func cardSwipedUpWithSupervibe(_ card: TinderCard) {
        removeObjectAndAddNewValues()
    }
    
    func cardSwipedUp(_ card: TinderCard, cat:String, userCat: String, userID: String) {
        self.userViewCategory = cat
        self.uidfromtinder = userID
        self.catfromtinder = userCat
        print("USERCATEGORIEE : \(self.userViewCategory)")
        
        if self.userViewCategory == "3"{
            
            
            print("PPP : \(self.userViewCategory)")
            //btn_supervibe.isEnabled = false
            
        }else{
            
            
            
            print("PPP!!! : \(self.userViewCategory)")
           // btn_supervibe.isEnabled = true
            self.getSwipeData()
            //self.getSuperVibeCount()
            UIView.animate(withDuration: 0.5) { () -> Void in
                self.btn_supervibe.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
            
            
            UIView.animate(withDuration: 0.5, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                self.btn_supervibe.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)
            }, completion: nil)
            
            
            
            
            
            UIView.animate(withDuration: 0.5, animations: {
                print(self.btn_supervibe.frame.origin.y)
                self.btn_supervibe.frame.origin.y -= 300
                
            }){_ in
                
                UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: UInt(0.0)), animations: {self.btn_supervibe.frame.origin.y += 300 }, completion: nil)
            }
        }
        removeObjectAndAddNewValues()
    }
    
    func updateCardView(_ card: TinderCard, withDistance distance: CGFloat) {
        //print("Hello")
    }
    
    func didInfoButtonTapped(id: String, name: String, age: String, work: String, location: String, photo: String) {
        print("Button tapped ")
        
        uID = id
        uName = name
        uAge = age
        uLoc = location
        uWorkAs = work
        uPhoto = photo
                print("EVID 9 :\(selectedEventIdFromSwipe)")
                let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        
                  nextViewController.selectedVCFlag = "3"
                  nextViewController.userId = uID
                  nextViewController.userName = uName
                  nextViewController.userLocation = uLoc
                  nextViewController.userWork = uWorkAs
                  nextViewController.userAge = uAge
                  nextViewController.userPic = uPhoto
                  nextViewController.selectedClubId = selectedClubIdFromSwipe
                  nextViewController.selectedEventId = selectedEventIdFromSwipe
        
        
                self.present(nextViewController, animated: true)
    }
    
    

    @objc func findEventNearMeAction(){
        
        print("back button tapped to instatiate back view ")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        BaseController.selectedIndex = 1
        self.present(BaseController, animated: true, completion: nil)
        
        
    }

}
