//
//  userGoingEvent_CollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 03/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class userGoingEvent_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_eventName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_eventName.layer.masksToBounds = true
        lbl_eventName.layer.cornerRadius = 18
        //self.eventName.text! = "1234"
        // Initialization code
    }
}
