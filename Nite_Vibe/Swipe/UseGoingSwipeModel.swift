//
//  UseGoingSwipeModel.swift
//  Nitevibe
//
//  Created by Vinayak Bhor on 01/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation

struct UserGoingSwipeModel{


    var userGoingArray = [userGoingSwipeModel]()

    init()
    {

        userGoingArray = []
    }


    init(json:[String:Any]){

        if let userGoingInfo = json["user_going"] as? [[String:Any]]{


            for userGoing in userGoingInfo{

                let goingData = userGoingSwipeModel(json: userGoing)

                self.userGoingArray.append(goingData)


            }

        }

    }


}

struct userGoingSwipeModel{

    var userId = String()
    var userName = String()
    var userAge = String()
    var userGender = String()
    var userCity = String()
    var userLoaction = String()
    var userOccupation = String()
    var userLikeCategory = String()
    var userProfilePic = String()
    var superVibeCount = Int()
    init()
    {}

    init(json:[String:Any]){

        if let user_id = json["going_user_id"] as? String{
            self.userId = user_id

        }

        if let user_name = json["name"] as? String{
            self.userName = user_name

        }
        if let user_age = json["age"] as? String{
            self.userAge = user_age
        }
        if let user_gender = json["gender"] as? String{

            self.userGender = user_gender
        }
        if let user_city = json["city"] as? String{

            self.userCity = user_city
        }

        if let user_occupation = json["occupation"] as? String{

            self.userOccupation = user_occupation
        }

        if let user_like_category = json["category"] as? String{

            self.userLikeCategory = user_like_category
        }
//        if let user_Profile_Pic = json["profile_url"] as? String{
//
//           self.userProfilePic = user_Profile_Pic
//        }
        
        if let user_Profile_Pic = json["user_profile_image"] as? String{
            
            self.userProfilePic = user_Profile_Pic
        }
        if let supervibecount = json["superlike_count"] as? Int{
            
            self.superVibeCount = supervibecount
        }


    }


}
//
//  SwipeCardModel.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 27/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

//import Foundation
//import SwiftyJSON
//
//struct UserGoingSwipeModel{
//
//    var userId = String()
//
//
//    var userProfilePic = String()
//
//    var userName = String()
//
//    var userAge = String()
//
//    var userLocation = String()
//
//    var userOccupation = String()
//
//    var superVibeCount = Int()
//
//    var userPhotoData = [userPhotoArrayModel]()
//
//    init()
//    {
//        userPhotoData = []
//    }
//
//    init(json:[String:Any]){
//
//        if let userId = json["user_id"] as? String{
//
//            self.userId = userId
//
//        }
//
//        if let userPropics = json["profile_url"] as? String{
//
//            self.userProfilePic = userPropics
//
//        }
//
//        if let user_Name = json["user_name"] as? String{
//
//            self.userName = user_Name
//
//        }
//
//        if let user_Age = json["user_age"] as? String{
//
//            self.userAge = user_Age
//
//        }
//
//        if let user_Location = json["user_loc"] as? String{
//
//            self.userLocation = user_Location
//
//        }
//
//        if let user_Occupation = json["user_occupation"] as? String{
//
//            self.userOccupation = user_Occupation
//
//        }
//
//        if let supervibe = json["superlikes"] as? Int{
//
//            self.superVibeCount = supervibe
//
//        }
//
//        if let userPhotoInfo = json["user_photos"] as? [[String:Any]]{
//
//
//            for userPhoto in userPhotoInfo{
//
//                let photo = userPhotoArrayModel(json: userPhoto)
//
//                self.userPhotoData.append(photo)
//
//
//            }
//
//        }
//
//    }
//}
//
//struct userPhotoArrayModel{
//
//    var user_image = String()
//    var user_id = String()
//
//    init()
//    {}
//
//    init(json:[String:Any]){
//
//        if let userImage = json["img"] as? String{
//            self.user_image = userImage
//
//        }
//
//        if let userId = json["id"] as? String{
//            self.user_id = userId
//
//        }
//
//
//
//    }
//
//
//
//}
//
//
//
//
