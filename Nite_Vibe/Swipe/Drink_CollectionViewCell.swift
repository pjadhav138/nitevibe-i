//
//  Drink_CollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 05/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class Drink_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_userSelectedDrinks: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        lbl_userSelectedDrinks.layer.masksToBounds = true
        lbl_userSelectedDrinks.layer.cornerRadius = 18
        //self.eventName.text! = "1234"
        // Initialization code
    }
    
}
