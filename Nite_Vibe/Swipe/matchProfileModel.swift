//
//  matchProfileModel.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 03/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

struct userPhotoMatchModel{
    
    var match_image = String()
    var match_id = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let matchImage = json["match_photo"] as? String{
            self.match_image = matchImage
            
        }
        
        if let matchId = json["match_id"] as? String{
            self.match_id = matchId
            
        }
        
        
        
    }
    
    
    
}
