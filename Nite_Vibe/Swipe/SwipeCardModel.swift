//
//  SwipeCardModel.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 27/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SwipeCardModel{
    
    var user_id = String()

    
    var userProfilePicUrl = String()
    
    var userName = String()
    
    var userAge = String()
    
    var userLocation = String()
    
    var userOccupation = String()
    
    var superVibeCount = Int()
    
    var userPhotoData = [userPhotoModel]()
    
    init()
    {
        userPhotoData = []
    }
    
    init(json:[String:Any]){
        
        if let userId = json["user_id"] as? String{
        
            self.user_id = userId
        
        }
        
//        if let userPropics = json["profile_url"] as? String{
//
//            self.userProfilePicUrl = userPropics
//
//        }
        if let userPropics = json["user_profile_image"] as? String{
            
            self.userProfilePicUrl = userPropics
            
        }
        
        if let user_Name = json["user_name"] as? String{
            
            self.userName = user_Name
            
        }
        
        if let user_Age = json["user_age"] as? String{
            
            self.userAge = user_Age
            
        }
        
        if let user_Location = json["user_loc"] as? String{
            
            self.userLocation = user_Location
            
        }
        
        if let user_Occupation = json["user_occupation"] as? String{
            
            self.userOccupation = user_Occupation
            
        }
        
        if let supervibe = json["superlikes"] as? Int{
            
            self.superVibeCount = supervibe
            
        }

        if let userPhotoInfo = json["user_photos"] as? [[String:Any]]{
                
                
                for userPhoto in userPhotoInfo{
                    
                    let photo = userPhotoModel(json: userPhoto)
                    
                    self.userPhotoData.append(photo)
                    
                    
            }
            
        }
        
    }
}

struct userPhotoModel{
    
    var user_image = String()
    var user_id = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let userImage = json["img"] as? String{
            self.user_image = userImage
            
            }
        
        if let userId = json["id"] as? String{
            self.user_id = userId
            
        }
        
        
        
    }
    
    
    
}




