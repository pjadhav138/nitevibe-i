//
//  UserDataModel.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 04/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON


struct UserDataModel {
 
    var userProfilePicUrl = String()
    
    var userName = String()
    
    var userAge = String()
    var userAgeStatus = String()
    
    var userLocation = String()
    var userLocStatus = String()
    
    var userOccupation = String()
    
    var userDrinks = String()
    var userDrinksStatus = String()
    
    var userBiodata = String()
    
    var userPhotoArray = [userPhotoDataModel]()
    
    init()
    {
        userPhotoArray = []
    }
    
    init(json:[String:Any]){
        
        
        if let userPropics = json["profile"] as? String{
            
            self.userProfilePicUrl = userPropics
            
        }
        
        if let user_Name = json["name"] as? String{
            
            self.userName = user_Name
            
        }
        
        if let user_Age = json["age"] as? String{
            
            self.userAge = user_Age
            
        }
        if let user_Age_Status = json["age_status"] as? String{
            
            self.userAgeStatus = user_Age_Status
            
        }
        
        if let user_Location = json["loc"] as? String{
            
            self.userLocation = user_Location
            
        }
        if let user_Location_Status = json["loc_status"] as? String{
            
            self.userLocStatus = user_Location_Status
            
        }
        
        if let user_Occupation = json["occupation"] as? String{
            
            self.userOccupation = user_Occupation
            
        }
        
        if let user_Drinks = json["drink"] as? String{
            
            self.userDrinks = user_Drinks
            
        }
       
        if let user_Drinks_Stutus = json["drink_status"] as? String{
            
            self.userDrinksStatus = user_Drinks_Stutus
            
        }
        
        if let user_Bio = json["bio"] as? String{
            
            self.userBiodata = user_Bio
            
        }
        
        if let userPhotoInfo = json["photos"] as? [[String:Any]]{
            
            
            for userPhoto in userPhotoInfo{
                
                let photo = userPhotoDataModel(json: userPhoto)
                
                self.userPhotoArray.append(photo)
                
                
            }
            
        }
        
    }
}

struct userPhotoDataModel{
    
    var user_image = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let userImage = json["img"] as? String{
            self.user_image = userImage
            
        }
 
    }
    
    
    
}

