//
//  TinderCard.swift
//  testingTinderSwipe
//
//  Created by Nicky on 11/16/17.
//  Copyright © 2017 Nicky. All rights reserved.
//

let ACTION_MARGIN = (UIScreen.main.bounds.size.width/2) * 0.75
let SCALE_STRENGTH : CGFloat = 4
let SCALE_MAX : CGFloat = 0.93
let ROTATION_STRENGTH = UIScreen.main.bounds.size.width

import UIKit
import Alamofire
import SwiftyJSON


protocol TinderCardDelegate: NSObjectProtocol {
    func cardSwipedLeft(_ card: TinderCard)
    func cardSwipedRight(_ card: TinderCard)
    func cardSwipedUp(_ card: TinderCard, cat:String, userCat:String, userID:String)
    func cardSwipedUpWithSupervibe(_ card: TinderCard)
    func updateCardView(_ card: TinderCard, withDistance distance: CGFloat)
    func didInfoButtonTapped(id:String,name:String,age:String,work:String,location:String,photo:String)
    
}

class TinderCard: UIView {
    
    var swipeUserId = String()
    var swipeData = [SwipeCardModel]()
    var selectedEventIdFromSwipe = ""
    var category = String()
    var selectedEventId = ""
    var seconds = 3
    var timer = Timer()
    var xFromCenter: CGFloat = 0.0
    var yFromCenter: CGFloat = 0.0
    var originalPoint = CGPoint.zero
    var imageViewStatus = UIImageView()
    var overLayImage = UIImageView()
    var isLiked = false
    var intex = 0
    var NameLabel = UILabel()
    var attributedText = NSMutableAttributedString()
    var infoButton = UIButton()
    var matchUserId = String()
    var userPhoto = String()
    var userName = String()
    var userWorkAs = String()
    var userLocation = String()
    var user_Id = String()
    var userAge = String()
    var usercat = String()
    var bgImage = UIImage()
    var UCAT = "3"
    
    
    weak var delegate: TinderCardDelegate?
    
    public init(frame: CGRect, userProfilePic: String,name: String,age: String,loc: String,occupation: String,userId: String,userCat: String) {
        super.init(frame: frame)
        
        setupValue(at: userProfilePic,name: name,age: age,loc: loc,occupation: occupation,userId: userId, userCategoryVM: userCat)
        
        self.swipeUserId = userId
        self.user_Id = userId
        self.userName = name
        self.userPhoto = userProfilePic
        self.userWorkAs = occupation
        self.userLocation = loc
        self.userAge = age
        self.usercat = userCat
        
        print("UserCategory \(self.usercat)")
        //var userCatDetails = [String : String]()
        let userdefaults = UserDefaults.standard
        userdefaults.set(self.usercat, forKey: "UserCat")
        print("UCcategory : \(String(describing: userdefaults.value(forKey: "UserCat")))")
        userdefaults.synchronize()
        
        
        print("UserId at tinder class :\(userId)")
        print("Image at tinder class : \(userProfilePic)")
        print("Name at tinder class : \(name)")
        print("Age at tinder class : \(age)")
        print("Loc at tinder class : \(loc)")
        print("Occupation at tinder class : \(occupation)")
        print("Category VM tinder class : \(userCat)")
        //TinderCard.addSubview(infoButton)
        
        
        //print("selected event id \(selectedEventId)")
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
        
    }
    
    
    func setupValue(at value:String,name:String,age:String,loc:String,occupation:String,userId:String,userCategoryVM:String) {
        
        
        
        layer.cornerRadius = 10
        layer.shadowRadius = 0
        layer.shadowOpacity = 0
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowColor = UIColor.darkGray.cgColor
        clipsToBounds = true
        originalPoint = center
        isUserInteractionEnabled = false
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.beingDragged))
        addGestureRecognizer(panGestureRecognizer)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.beingTapped))
        addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        
        let width = UIScreen.main.bounds.size.width
        let backGroundImageView = UIImageView(frame:CGRect(x:0, y:0, width: width, height: 633))
        
        
        if value.count == 0{
            print("do nothing for empty image ")
        }else{
          
            let imageURL = URL(string: value)
            
            
            if(imageURL != nil) {
                
                do {
                    let imageData = try Data(contentsOf: imageURL! as URL);
                     backGroundImageView.image = UIImage(data: imageData)
                } catch let error {
                
                    
                }
                
              
                
               
                
            }
            
            print("valueInsideSetupView : \(value)")
        }
        
        
        backGroundImageView.contentMode = .scaleAspectFill
        backGroundImageView.clipsToBounds = true;
        addSubview(backGroundImageView)
        
        let gradientBg = UIImageView(frame: CGRect(x:0, y:150, width: width, height: 633))
        gradientBg.image = UIImage(named: "2swipe_blur_bg")
        gradientBg.contentMode = .scaleAspectFill
        gradientBg.clipsToBounds = true;
        addSubview(gradientBg)
        
        NameLabel = UILabel(frame:CGRect(x: 20 , y: frame.size.height - 170, width: 250, height: 208))
        //frame.size.width - 320
        attributedText = NSMutableAttributedString(string:"\(name)" + " ", attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 28)])
        attributedText.append(NSAttributedString(string:"\(age)", attributes: [.foregroundColor:UIColor.white,.font:UIFont.boldSystemFont(ofSize: 28)]))
        attributedText.append(NSAttributedString(string: "\nWorking As \(occupation)\n", attributes: [.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 18)]))
        attributedText.append(NSAttributedString(string: loc, attributes: [.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 18)]))
        NameLabel.attributedText = attributedText
        NameLabel.numberOfLines = 5
        NameLabel.textAlignment = .left
        addSubview(NameLabel)
        
        
        //        infoButton = UIButton(frame:CGRect(x:10, y: frame.size.height - 190, width: 30, height: 30))
        //        let infoImg = UIImage(named: "infoIcon") as UIImage?
        //        infoButton = UIButton(type: UIButtonType.custom) as UIButton
        //        infoButton.setBackgroundImage(infoImg, for: .normal)
        //        infoButton.addTarget(self, action: #selector(info_Btn_Action), for: .touchUpInside)
        //        addSubview(infoButton)
        
        let button = UIButton()
        button.frame = CGRect(x: frame.size.width - 60, y: frame.size.height - 100, width: 30, height: 30)
        button.clipsToBounds = true
        button.layer.masksToBounds = true
        button.layer.cornerRadius = button.frame.height/2
        button.backgroundColor = UIColor.white.withAlphaComponent(1)
        button.setTitle("i", for: .normal)
        button.setTitleColor(UIColor.blue.withAlphaComponent(1), for: .normal)
        button.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 21)
       // button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        button.addTarget(self, action: #selector(info_Btn_Action), for: .touchUpInside)
        addSubview(button)
        
        
        //        infoButton = UIButton(frame:CGRect(x:10, y: frame.size.height - 190, width: 30, height: 30))
        //        let infoImg = UIImage(named: "infoIcon") as UIImage?
        //        infoButton = UIButton(type: UIButtonType.custom) as UIButton
        //        infoButton.setBackgroundImage(infoImg, for: .normal)
        //        infoButton.addTarget(self, action: #selector(info_Btn_Action), for: .touchUpInside)
        //        addSubview(infoButton)
        
        
        imageViewStatus = UIImageView(frame: CGRect(x: (frame.size.width / 2) - 37.5, y: 150, width: 100, height: 100))
        imageViewStatus.alpha = 0
        addSubview(imageViewStatus)
        
        
        overLayImage = UIImageView(frame:CGRect(x:0, y:0, width: width, height: 633))
        overLayImage.alpha = 0
        addSubview(overLayImage)
    }
    
    
    @objc func info_Btn_Action(sender: AnyObject) {
        
        print("tap")
        
        let getName = self.userName
        let getID = self.user_Id
        let getAge = self.userAge
        let getWork = self.userWorkAs
        let getLocation = self.userLocation
        let getPhoto = self.userPhoto
        
        
        
        
        self.delegate?.didInfoButtonTapped(id:getID,name:getName,age:getAge,work:getWork,location:getLocation,photo:getPhoto)
        
        
        
        
    }
    
    
    
    @objc func beingTapped(_ gestureRecognizer: UITapGestureRecognizer) {
        
        
    }
    
    
    @objc func beingDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        
        xFromCenter = gestureRecognizer.translation(in: self).x
        yFromCenter = gestureRecognizer.translation(in: self).y
        switch gestureRecognizer.state {
        // Keep swiping
        case .began:
            originalPoint = self.center;
            break;
            
        //in the middle of a swipe
        case .changed:
            let rotationStrength = min(xFromCenter / ROTATION_STRENGTH, 1)
            let rotationAngel = .pi/8 * rotationStrength
            let scale = max(1 - fabs(rotationStrength) / SCALE_STRENGTH, SCALE_MAX)
            center = CGPoint(x: originalPoint.x + xFromCenter, y: originalPoint.y + yFromCenter)
            let transforms = CGAffineTransform(rotationAngle: rotationAngel)
            let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
            self.transform = scaleTransform
            updateOverlay(xFromCenter)
            break;
            
        // swipe ended
        case .ended:
            afterSwipeAction()
            break;
            
        case .possible:break
        case .cancelled:break
        case .failed:break
        }
    }
    func updateOverlay(_ distance: CGFloat) {
        
        imageViewStatus.image = distance > 0 ? UIImage(named: "btn_like_pressed") : UIImage(named: "btn_skip_pressed")
        overLayImage.image = distance > 0 ? UIImage(named: "overlay_skip") : UIImage(named: "overlay_like")
        imageViewStatus.alpha = min(fabs(distance) / 100, 1)
        overLayImage.alpha = min(fabs(distance) / 100, 1)
        delegate?.updateCardView(self, withDistance: distance)
    }
    
    func afterSwipeAction() {
        
        if xFromCenter > ACTION_MARGIN {
            rightAction()
        }
        else if xFromCenter < -ACTION_MARGIN {
            leftAction()
        }
        else {
            //reseting image
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: [], animations: {
                self.center = self.originalPoint
                self.transform = CGAffineTransform(rotationAngle: 0)
                self.imageViewStatus.alpha = 0
                self.overLayImage.alpha = 0
            })
        }
    }
    
        func upAction(){

            let finishPoint = CGPoint(x: 2 * xFromCenter + originalPoint.x, y: -frame.size.height*2)
            UIView.animate(withDuration: 1.5, animations: {
                self.center = finishPoint
            }, completion: {(_) in
                self.removeFromSuperview()
            })
            isLiked = true
            delegate?.cardSwipedUpWithSupervibe(self)
            category = "3"
            print("Click SuperVibe button ")
            //getSwipeData()
            print("WATCHOUT UP")

        }

    func rightAction() {
        
        let finishPoint = CGPoint(x: frame.size.width*2, y: 2 * yFromCenter + originalPoint.y)
        UIView.animate(withDuration: 1.0, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = true
        delegate?.cardSwipedRight(self)
        category = "1"
        print("Click Like button ")
        getSwipeData()
        print("WATCHOUT RIGHT")
    }
    
    
    func leftAction() {
        
        let finishPoint = CGPoint(x: -frame.size.width*2, y: 2 * yFromCenter + originalPoint.y)
        UIView.animate(withDuration: 1.0, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardSwipedLeft(self)
        category = "2"
        print("Click dislike button")
        getSwipeData()
        print("WATCHOUT LEFT")
    }
    
    // up click action
    func upClickAction() {
        
        print("VMCATE : \(usercat)")
        //getSuperVibeCount()
        if usercat == "3"{

            print("Dont Supervibe")
           // delegate?.cardSwipedUp(self, cat: usercat, userCat: "3", userID: user_Id)
           // delegate?.cardSwipedUpWithSupervibe(self)

        }else{
        
            imageViewStatus.image = UIImage(named: "supervibe_stamp")
            overLayImage.image = UIImage(named: "overlay_supervibe" )
            let finishPoint = CGPoint(x: center.x , y: center.y - frame.size.height * 2)
            imageViewStatus.alpha = 0.5
            overLayImage.alpha = 0.5
            UIView.animate(withDuration: 1.5, animations: {() -> Void in
                self.center = finishPoint
                //self.transform = CGAffineTransform(rotationAngle: )
                self.imageViewStatus.alpha = 1.0
                self.overLayImage.alpha = 1.0
            }, completion: {(_ complete: Bool) -> Void in
                self.removeFromSuperview()
            })
            isLiked = true
            delegate?.cardSwipedUp(self, cat: usercat, userCat: UCAT, userID: user_Id)
           // category = "3"
            print("Click SuperVibe button ")
            print("Done with supervibe")
            //getSwipeData()
            print("WATCHOUT UP ACTION")

        }
        
    }
    
    // right click action
    func rightClickAction() {
        
        imageViewStatus.image = UIImage(named: "btn_like_pressed")
        overLayImage.image = UIImage(named: "overlay_skip" )
        let finishPoint = CGPoint(x: center.x + frame.size.width * 2, y: center.y)
        imageViewStatus.alpha = 0.5
        overLayImage.alpha = 0.5
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: 1)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_ complete: Bool) -> Void in
            self.removeFromSuperview()
        })
        isLiked = true
        delegate?.cardSwipedRight(self)
        category = "1"
        print("Click Like button ")
        getSwipeData()
        print("WATCHOUT RIGHT ACTION")
    }
    // left click action
    func leftClickAction() {
        
        imageViewStatus.image = UIImage(named: "btn_skip_pressed")
        overLayImage.image = UIImage(named:"overlay_like")
        let finishPoint = CGPoint(x: center.x - frame.size.width * 2, y: center.y)
        imageViewStatus.alpha = 0.5
        overLayImage.alpha = 0.5
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: -1)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_ complete: Bool) -> Void in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardSwipedLeft(self)
        category = "2"
        print("Click dislike button")
        getSwipeData()
        print("WATCHOUT LEFT ACTION")
    }
    
    // undoing  action
    func makeUndoAction() {
        
        imageViewStatus.image = UIImage(named: isLiked ? "btn_like_pressed" : "btn_skip_pressed")
        overLayImage.image = UIImage(named: isLiked ? "overlay_like" : "overlay_skip")
        imageViewStatus.alpha = 1.0
        overLayImage.alpha = 1.0
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.center = self.originalPoint
            self.transform = CGAffineTransform(rotationAngle: 0)
            self.imageViewStatus.alpha = 0
            self.overLayImage.alpha = 0
        })
        
        
        print("WATCHOUT UNDO ACTION")
    }
    
    func shakeCard(){
        
        imageViewStatus.image = UIImage(named: "btn_skip_pressed")
        overLayImage.image = UIImage(named: "overlay_skip")
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.center = CGPoint(x: self.center.x - (self.frame.size.width / 2), y: self.center.y)
            self.transform = CGAffineTransform(rotationAngle: -0.2)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_ complete: Bool) -> Void in
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                self.imageViewStatus.alpha = 0
                self.overLayImage.alpha = 0
                self.center = self.originalPoint
                self.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: {(_ complete: Bool) -> Void in
                self.imageViewStatus.image = UIImage(named: "btn_like_pressed")
                self.overLayImage.image =  UIImage(named: "overlay_like")
                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                    self.imageViewStatus.alpha = 1
                    self.overLayImage.alpha = 1
                    self.center = CGPoint(x: self.center.x + (self.frame.size.width / 2), y: self.center.y)
                    self.transform = CGAffineTransform(rotationAngle: 0.2)
                }, completion: {(_ complete: Bool) -> Void in
                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                        self.imageViewStatus.alpha = 0
                        self.overLayImage.alpha = 0
                        self.center = self.originalPoint
                        self.transform = CGAffineTransform(rotationAngle: 0)
                    })
                })
            })
        })
        
        print("WATCHOUT SHAKE ACTION")
    }
    
    func getSuperVibeCount(){
        
        
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/supervibe_count.php?user_id=\(User_Id!)&event_id=\(selecteEventId!)"
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String , method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                
                if(respo == 201){
                    print("Message: \(String(describing: responseMessage))")
                    
                }else if(respo == 200){
                    
                    print("success")
                    
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    func getSwipeData(){
        //need to pass remaining data in webservice like user_id category event id
        
        
        var currentUserId = String()
        var currentUserPhoto = String()
        var matchUserId = String()
        var matchUserPhoto = String()
        
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/swipe.php?user_id=\(User_Id!)&swipe_user_id=\(swipeUserId)&category=\(category)&event_id=\(selecteEventId!)"
        print("URL : \(URL_String)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String , method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                 //print("data user id \(dict["user_id"] as! String)")
                
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print(" dict data : \(dictData)")
                
                
                if dictData.count == 0{
                    
                    print("do nothing continue swiping ")
                }else{
                    
                    for data in dictData{
                        
                        
                        print("data current user id \(data["user_id"] as! String)")
                        print("data current user photo \(data["user_photo"] as! String)")
                        print("data match user id \(data["match_id"] as! String)")
                        print("data match user photo \(data["match_photo"] as! String)")
                        
                        
                        currentUserId  = data["user_id"] as! String
                        currentUserPhoto = data["user_photo"] as! String
                        matchUserId = data["match_id"] as! String
                        matchUserPhoto = data["match_photo"] as! String
                        
                        
                        self.sendMatchNotification()
                        
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                        let matchProfileVC = storyboard.instantiateViewController(withIdentifier: "MatchProfileViewController") as! MatchProfileViewController
//
//                        self.present(matchProfileVC, animated: true, completion: nil)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        //resolved view is not hiararchy issue.
                        
//                    let matchProfileVC = UIStoryboard(name: "Swipe", bundle:nil).instantiateViewController(withIdentifier: "MatchProfileViewController") as! MatchProfileViewController
//                        matchProfileVC.currentUserId = currentUserId
//                        matchProfileVC.currentUserImageUrlString = currentUserPhoto
//                        matchProfileVC.matchUserId = matchUserId
//                        matchProfileVC.matchImageUrlString = matchUserPhoto
//                        matchProfileVC.modalPresentationStyle = .overCurrentContext
//                        print("selected event id :\(self.selectedEventIdFromSwipe)")
//                        matchProfileVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
//
//                        //self.addChildViewController(matchProfileVC)
//                        //self.view.addSubview(matchProfileVC.view)
//
//
//                        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
//                        appDelegate.window?.rootViewController = matchProfileVC
                        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
                        let goodvibesVC:goodvibesViewController = storyboard.instantiateViewController(withIdentifier: "goodvibesViewController") as! goodvibesViewController
                        goodvibesVC.currentUserId = currentUserId
                        goodvibesVC.currentUserImageUrlString = currentUserPhoto
                        goodvibesVC.matchUserId = matchUserId
                        goodvibesVC.matchImageUrlString = matchUserPhoto
                        print("selected event id :\(self.selectedEventIdFromSwipe)")
                        goodvibesVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
                        
                      //  self.addChildViewController(goodvibesVC)
                      //  self.view.addSubview((goodvibesVC.view)!)
                        
                        // self.present(goodvibesVC, animated: true, completion: nil)
                        
                        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                        appDelegate.window?.rootViewController = goodvibesVC
                      
                        
                        
                    }

                    
                }
                
//                print("dict info : \(dict)")
//
//                for data in dict {
//
//                }
                
                print(type(of:dictData))
                
                
                if(respo == 201){
                    print("Message: \(String(describing: responseMessage))")
                    
                }else if(respo == 200){
                    
                    print("success")
                    
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    
    func sendMatchNotification(){
        // print("ClubID : \(club_id)")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("Matchuserid :\(matchUserId)")
        
        // print("selected club id \(selectedClubId)")
        let URL_String = "http://www.nitevibe.in/webservices/match_notification.php?user_id=\(User_Id!)&match_id=\(matchUserId)"
        
        print("test Notification URL : \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response Noti :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
        
        
    }
    
}












