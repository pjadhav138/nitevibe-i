//
//  ForgotPass_3VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 30/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore
class ForgotPass_3VC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var txt_ConfirmPass: UITextField!
    @IBOutlet weak var txt_PassWord: UITextField!
    var removeOptionalConfirmPassword = String()
    
    
    @IBOutlet weak var updatePasswordOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txt_PassWord.delegate = self
        self.txt_ConfirmPass.delegate = self
        
        updatePasswordOutlet.layer.cornerRadius = 10
        updatePasswordOutlet.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func updatePasswordBtn_action(_ sender: Any) {
    
        let password = txt_PassWord.text
        let confirmPassword = txt_ConfirmPass.text
        
        print("password: \(String(describing: password!))")
        print("confirmpassword: \(String(describing: confirmPassword!))")
        
        
     
        var removeOptionalPassword = (String(describing: password!))
        removeOptionalConfirmPassword = (String(describing: confirmPassword!))
        
        if removeOptionalPassword == removeOptionalConfirmPassword{
            
            print("password match")
            self.updatePassword()
            
            
            
        }else{
            
            print("password mismatch")
            let alert = UIAlertController(title: ":(", message: "Please enter valid password" as! String, preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    
    }
    
    
    func updatePassword(){
        var respo = Int()
        
        let getUsername:UserDefaults = UserDefaults.standard
        
        let getUserName = getUsername.value(forKey: "username")
        let username = (String(describing:getUserName!))
        
        print("username update function:\(username)")
        let URL_String = "http://www.nitevibe.in/webservices/forgot_password.php?username=\(username)&password=\(removeOptionalConfirmPassword)"
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    
                    //print("Valid otp response : ",responseMessage!)
                    
                    //let setOtpRespnse:UserDefaults = UserDefaults.standard
                    
                    //setOtpRespnse.set(responseMessage!, forKey: "signupotp")
                    //let textfieldOtp = txt_OTP.text!
                    
                    //if textfieldOtp == responseMessage
                    let alert = UIAlertController(title: "Updated", message: responseMessage as? String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: {
                        
                        action in
                        
                        
                        let LoginVC:LogIn_1_VC = self.storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
                        
                        self.present(LoginVC, animated: true, completion: nil)
                        
                        
                    })
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 202 ){
                    
                    
                    let alert = UIAlertController(title: ":(", message:responseMessage as? String , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    

    
}
