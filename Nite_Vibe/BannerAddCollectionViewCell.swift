//
//  BannerAddCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 12/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class BannerAddCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var bannerAddImage: UIImageView!
    
}
