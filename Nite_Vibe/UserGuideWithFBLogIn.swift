//
//  ViewController.swift
//  Nite_Vibe
//
//  Created by Appmonks on 21/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import FirebaseCore
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import FBSDKLoginKit
import Alamofire

class UserGuideWithFBLogIn: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIApplicationDelegate, UNUserNotificationCenterDelegate{
    
    
    let gcmMessageIDKey = "gcm.message_id"
    //var fromTerminated = false
    
    var instanceIDTokenMessage = ""
    //var locationManager = CLLocationManager()
    var clubID_value = String()
    var notificationText = String()
    var notificationTitle = String()
    
    @IBOutlet weak var splitterImageView: UIImageView!
    
    @IBOutlet weak var btn_policy: UIButton!
    @IBOutlet var collection_View: UICollectionView!
    @IBOutlet var pageControll: UIPageControl!
    var myLoginButton = UIButton(type: .custom)
    var login = FBSDKLoginManager()
    var buttonConstraints:[NSLayoutConstraint] = []
    
    
    var pageCount = Int()
    var user_facebook_id = String()
    var user_emailId = String()
    var user_first_name = String()
    var user_last_name = String()
    var user_age = Int()
    var user_gender = String()
    var user_image_url = String()
    var userFullName = String()
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var images:[UIImage] = [UIImage(named: "NV2")!,UIImage(named: "step11copy")!,UIImage(named: "step2")!,UIImage(named:"step3")!]
    var label_first:[String] = ["","Feel the vibe","Find the best","Hit it off"]
    var label_second:[String] = ["","Work hard, party harder! Find the nearest nightclubs, bars, events. Share it, spread the vibe","Club-hopping Tonight? Make most out your night. We update you about the club’s gender ratio, entry and costs","Start with a drink of their preference or with a club you both love. Spread the good vibes :D"]
    
    var userFacebookImage = UIImage()
    var userId = String()
    override func viewDidLoad() {

        setTimer()
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        myLoginButton.translatesAutoresizingMaskIntoConstraints = false
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //let width = UIScreen.main.bounds.width
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth, height: 375 )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collection_View!.collectionViewLayout = layout
        
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "isAlreadyLoggedIn")
        userDefault.synchronize()
        
        myLoginButton.layer.cornerRadius = 8
        myLoginButton.clipsToBounds = true
        myLoginButton.backgroundColor = UIColor(red: 255/255, green: 54/255, blue: 102/255, alpha: 1)
        
        //        myLoginButton.frame = CGRect(x: screenSize.width - 380, y: screenSize.height - 150, width:350, height:50);
        myLoginButton.setTitle("Login With Facebook", for: .normal)
        myLoginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        myLoginButton.layer.masksToBounds = true
        myLoginButton.layer.cornerRadius = 8
        // Handle clicks on the button
        
        //FBSDKLoginManager().logOut()
        myLoginButton.addTarget(self, action: #selector(self.loginButtonClicked), for: .touchUpInside)
        // Add the button to the view
        self.view.addSubview(myLoginButton)
        
        
        let buttonHeight = myLoginButton.heightAnchor.constraint(equalToConstant: 60)
        let buttonWidth = myLoginButton.widthAnchor.constraint(equalToConstant: 275)
        let topConstraint = myLoginButton.topAnchor.constraint(equalTo: self.btn_policy.topAnchor, constant: 40)
        let bottomConstraint = myLoginButton.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
        let leftConstraint = myLoginButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 50)
        let rightConstraint = myLoginButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -50)
        buttonConstraints = [buttonHeight,buttonWidth,topConstraint,bottomConstraint,leftConstraint,rightConstraint]
        NSLayoutConstraint.activate(buttonConstraints)
        
        
        
        //        let cellSize = CGSize(width:414 , height:311)
        //
        //        let layout = UICollectionViewFlowLayout()
        //        layout.scrollDirection = .horizontal //.vertical //.horizontal
        //        layout.itemSize = cellSize
        //        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        //        layout.minimumLineSpacing = 1.0
        //        layout.minimumInteritemSpacing = 1.0
        //        collection_View.setCollectionViewLayout(layout, animated: true)
        //
        //        collection_View.reloadData()
        //  let userDefault = UserDefaults.standard
        
        
        
        
    }
    
 
    
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    var x = 1
    
    @objc func autoScroll() {
        if self.x < self.label_second.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.collection_View.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        } else {
            self.x = 0
            self.collection_View.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    
    @objc func loginButtonClicked() {
        
        //self.fetchFacebookFields()
        print("click login")
        self.fetchFbUserData()
        
    }
    
    func fetchFbUserData() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        if ((FBSDKAccessToken.current()) == nil) {
            fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
                
                print(error?.localizedDescription as Any) // to print error from server
                //  if (error == nil) {
                if let fbloginresult : FBSDKLoginManagerLoginResult = result
                {
                    // if user cancel the login
                    if (fbloginresult.isCancelled){
                        return
                    }
                    
                    if(fbloginresult.grantedPermissions.contains("public_profile")) {
                        // self.startAnimation()
                        FBSDKGraphRequest(
                            graphPath: "/me",
                            parameters: ["fields": "id, first_name, last_name, email"]
                            ).start(completionHandler: { (connection, result, error) -> Void in
                                if let dict = result as? Dictionary<String, Any> {
                                    
                                    if let id = dict["id"] as? String {
                                        print("id: \(id)")
                                        self.user_facebook_id = id
                                    }
                                    if let fname = dict["first_name"] as? String {
                                        print("fname: \(fname)")
                                        self.user_first_name = "\(fname)"
                                    }
                                    if let lname = dict["last_name"] as? String {
                                        print("lname: \(lname)")
                                        self.user_last_name = "\(lname)"
                                    }
                                    if let email = dict["email"] as? String {
                                        print("email: \(email)")
                                        self.user_emailId = "\(email)"
                                    }
                                    
                                    self.userFullName = self.user_first_name + self.user_last_name
                                    print("userFullName: \(self.userFullName)")
                                    
                                    var respo = Int()
                                    
                                    let URL_String = "http://www.nitevibe.in/webservices/register.php?username=\(self.user_emailId)&password=\("")&mode=Facebook&provider=\(self.user_facebook_id)&photo=\("")&name=\(self.userFullName)&gender=\("")"
                                    
                                    print("URL String\(URL_String)")
                                    let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                    
                                    
                                    do {
                                        //disable cache using reloadIgnoringCacheData Not updating backend update in app
                                        var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
                                        urlRequest.cachePolicy = .reloadIgnoringCacheData
                                        
                                        Alamofire.request("https://api.geodatasource.com/city").responseJSON{
                                            
                                            
                                            (response) in
                                            print("ADD response :\(response)")
                                        }
                                        
                                        Alamofire.request(urlRequest).responseJSON{
                                            
                                            
                                            (response) in
                                            
                                            print(" response :\(String(describing: response.request))")
                                            
                                            guard response.result.isSuccess  else {
                                                FBSDKAccessToken.setCurrent(nil)
                                                FBSDKProfile.setCurrent(nil)

                                                FBSDKLoginManager().logOut()
                                                
                                                let alert = UIAlertController(title: "", message:"Something went wrong with the network. Please try again." as? String , preferredStyle: .alert)
                                                
                                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                                
                                                alert.addAction(alertAction)
                                                
                                                self.present(alert, animated: true, completion: nil)
                                                
                                                print("Block 1 ")
                                                print(" response :\(response)")
                                                
                                                print("Error with response: \(String(describing: response.result.error))")
                                                
                                                return
                                                
                                            }
                                            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                                                
                                                print(" response :\(response)")
                                                print("Error with dictionary: \(String(describing: response.result.error))")
                                                return
                                                
                                            }
                                            print("message\(dict)")
                                            print("dict respo: \(dict["response"]!)")
                                            print("dict message:\(dict["message"]!)")
                                            
                                            
                                            respo = dict["response"]! as! Int
                                            let responseMessage = dict["message"]
                                            
                                            
                                            print("type of response \(type(of:responseMessage))")
                                            
                                            if(respo == 200){
                                                
                                                
                                                
                                                let setUserId = UserDefaults.standard
                                                
                                                setUserId.set(String(describing: responseMessage!), forKey: "userid")
                                                print("user id :\(setUserId.value(forKey: "userid")!)")
                                                
                                                
                                                
                                                self.userId = String(describing: responseMessage!)
                                                
                                                print("print user id global variable \(self.userId)")
                                                
                                                
                                                self.myImageUploadRequest(facebookUserId:self.user_facebook_id)
                                                
                                                let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
                                                self.present(BaseController, animated: true, completion: nil)
                                                
                                                
                                            }
                                            else if(respo == 201){
                                                
                                                let alert = UIAlertController(title: "", message: (responseMessage as! String), preferredStyle: .alert)
                                                
                                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                                
                                                alert.addAction(alertAction)
                                                
                                                self.present(alert, animated: true, completion: nil)
                                                
                                            }else if(respo == 202){
                                                
                                                
                                                let alert = UIAlertController(title: "", message:responseMessage as? String , preferredStyle: .alert)
                                                
                                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                                
                                                alert.addAction(alertAction)
                                                
                                                self.present(alert, animated: true, completion: nil)
                                            }else if(respo == 205){
                                                let setUserId = UserDefaults.standard
                                                
                                                setUserId.set(String(describing: responseMessage!), forKey: "userid")
                                                print("user id :\(setUserId.value(forKey: "userid")!)")
                                                
                                                self.userId = responseMessage! as! String
                                                
                                                
                                                print("print user id global variable \(self.userId)")
                                                
                                                
                                                let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
                                                self.present(BaseController, animated: true, completion: nil)
                                            }
                                            
                                            
                                            
                                        }
                                        
                                        
                                    }catch {
                                        // TODO: find a better way to handle error
                                        print(error)
                                        
                                    }
                                    
                                    
                                    
                                } else {
                                    print("not working")
                                }
                            }
                        )
                    }
                }
            }
        }
         
    }

    
    
//    func fetchFacebookFields() {
//
//        //FBSDKLoginManager *loginMgr = [[FBSDKLoginManager alloc] init];
//        //loginMgr.loginBehavior = FBSDKLoginBehaviorWeb;
//
//        let facbookLoginManger = FBSDKLoginManager()
//
//
//        facbookLoginManger.loginBehavior = FBSDKLoginBehavior.native
//        //do login with permissions for email and public profile
//        facbookLoginManger.logIn(withReadPermissions: ["public_profile","email"], from: nil) {
//            (result, error) -> Void in
//            //if we have an error display it and abort
//            if let error = error {
//                print("FB Error : \(error.localizedDescription)")
//                return
//            }
//            //make sure we have a result, otherwise abort
//            guard let result = result else { return }
//            //if cancelled nothing todo
//            if result.isCancelled { return }
//            else {
//                //login successfull, now request the fields we like to have in this case first name and last name
//                FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "name,first_name, last_name, email,id"]).start() {
//                    (connection, result, error) in
//                    //if we have an error display it and abort
//                    if let error = error {
//                        print(error.localizedDescription)
//                        return
//                    }
//
//                    print()
//
//
//                    //parse the fields out of the result
//                    if
//                        let fields = result as? [String:Any],
//                        let firstName = fields["first_name"] as? String,
//                        let lastName = fields["last_name"] as? String,
//                        let email = fields["email"] as? String,
//                        let id = fields["id"] as? String,
//                        let fullName = fields["name"] as? String,
//                        let gender = fields["gender"] as? String
//
//                    {
//
//
//                        print("print userinfo fields :\(fields)")
//                        print("user name \(firstName) \(lastName) ")
//                        print("user email \(email)")
//                        self.user_emailId = "\(email)"
//
//                        print("user id \(id)")
//
//                        self.user_facebook_id = id
//                        print("user full name \(fullName)")
//                        self.userFullName = fullName
//                        print("user gender : \(gender)")
//
//                        //                                                        print("user pic url \(imageURL)")
//                        //                                                        let FBimageUrl:URL = URL(string: imageURL)!
//                        //                                                        print("FBimageUrl -> \(FBimageUrl)")
//                        //
//                        //                                                        let imageData:NSData = (NSData(contentsOf: FBimageUrl))!
//                        //                                                        print("imageData -> \(imageData)")
//                        //
//                        //                                                        let image = UIImage(data: (imageData as NSData) as Data)
//                        //
//                        //                                                        print("print image \(image as! UIImage) ")
//                        //
//                        //                                                        self.userFacebookImage = image!
//
//                        var respo = Int()
//
//                        let URL_String = "http://www.nitevibe.in/webservices/register.php?username=\(self.user_emailId)&password=\("")&mode=Facebook&provider=\(self.user_facebook_id)&photo=\("")&name=\(self.userFullName)&gender=\("")"
//
//                        print("URL String\(URL_String)")
//                        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//
//                        do {
//                            //disable cache using reloadIgnoringCacheData Not updating backend update in app
//                            var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
//                            urlRequest.cachePolicy = .reloadIgnoringCacheData
//
//                            Alamofire.request(urlRequest).responseJSON{
//
//                                (response) in
//
//                                print(" response :\(String(describing: response.request))")
//
//                                guard response.result.isSuccess  else {
//
//                                    print("Block 1 ")
//                                    print(" response :\(response)")
//
//                                    print("Error with response: \(String(describing: response.result.error))")
//
//                                    return
//
//                                }
//                                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
//
//                                    print(" response :\(response)")
//                                    print("Error with dictionary: \(String(describing: response.result.error))")
//                                    return
//
//                                }
//                                print("message\(dict)")
//                                print("dict respo: \(dict["response"]!)")
//                                print("dict message:\(dict["message"]!)")
//
//
//                                respo = dict["response"]! as! Int
//                                let responseMessage = dict["message"]
//
//
//                                print("type of response \(type(of:responseMessage))")
//
//                                if(respo == 200){
//
//
//
//                                    let setUserId = UserDefaults.standard
//
//                                    setUserId.set(String(describing: responseMessage!), forKey: "userid")
//                                    print("user id :\(setUserId.value(forKey: "userid")!)")
//
//
//
//                                    self.userId = String(describing: responseMessage!)
//
//                                    print("print user id global variable \(self.userId)")
//
//
//                                    self.myImageUploadRequest(facebookUserId:self.user_facebook_id)
//
//                                    let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
//                                    self.present(BaseController, animated: true, completion: nil)
//
//
//                                }
//                                else if(respo == 201){
//
//                                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)
//
//                                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                                    alert.addAction(alertAction)
//
//                                    self.present(alert, animated: true, completion: nil)
//
//                                }else if(respo == 202){
//
//
//                                    let alert = UIAlertController(title: "", message:responseMessage as? String , preferredStyle: .alert)
//
//                                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//
//                                    alert.addAction(alertAction)
//
//                                    self.present(alert, animated: true, completion: nil)
//                                }else if(respo == 205){
//
//
//
//                                    let setUserId = UserDefaults.standard
//
//                                    setUserId.set(String(describing: responseMessage!), forKey: "userid")
//                                    print("user id :\(setUserId.value(forKey: "userid")!)")
//
//                                    self.userId = responseMessage! as! String
//
//
//                                    print("print user id global variable \(self.userId)")
//
//
//                                    let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
//                                    self.present(BaseController, animated: true, completion: nil)
//
//
//
//                                }
//
//
//                            }
//
//
//                        }catch {
//                            // TODO: find a better way to handle error
//                            print(error)
//
//                        }
//
//
//
//                    }
//
//                }
//            }
//        }
//    }
    
    
    
    
    
    
    
    @IBAction func btn_Login_action(_ sender: Any) {
        
        
        let userLoginVC:LogIn_1_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
        
        present(userLoginVC, animated: true, completion: nil)
        
    }
    
    @IBAction func PageControll_Action(_ sender: Any) {
        pageControll.numberOfPages = images.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        let cell = collection_View.dequeueReusableCell(withReuseIdentifier: "UserCell", for: indexPath) as! UserGuideCellVC
        print("label_1\(label_first)")
        
        cell.frame.size.width = screenWidth
        cell.frame.size.height = 375
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 0
        cell.clipsToBounds = true
        cell.lbl_First.text = label_first[indexPath.row]
        cell.lbl_Second.text = label_second[indexPath.row]
        cell.UG_Img.image = images[indexPath.row]
        
        self.pageControll.numberOfPages = images.count
        
        return cell
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let indexPath = collection_View.indexPathForItem(at: center) {
            self.pageControll.currentPage = indexPath.row
        }
    }
    
    
    func myImageUploadRequest(facebookUserId:String){
        
        //very small Image is getting from facebook graph api
        
        // we get larg image manualy from below graph link
        
        print("print facebook id \(facebookUserId)")
        
        let facebookProfileImageURL = "http://graph.facebook.com/\(facebookUserId)/picture?type=large"
        
        print("facebook url image link \(facebookProfileImageURL)")
        
        
        
        let FBimageUrl:URL = URL(string: facebookProfileImageURL)!
        print("FBimageUrl -> \(FBimageUrl)")
        
        let imageInfo:NSData = (NSData(contentsOf: FBimageUrl))!
        print("imageData -> \(imageInfo)")
        
        let image = UIImage(data: (imageInfo as NSData) as Data)
       
        
        
        print("print image \(image as! UIImage) ")
        
        self.userFacebookImage = image!
        
        
        //let myUrl = NSURL(string: "http://www.swiftdeveloperblog.com/http-post-example-script/");
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        let myUrl = NSURL(string: "http://www.nitevibe.in/webservices/test_add_image.php?");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = [
            "user_id"  : "\(self.userId)",
            "mode"     : "Facebook",
            
        ]
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = UIImageJPEGRepresentation(self.userFacebookImage, 1)
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "fileToUpload", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print(json)
                self.userFacebookImage = UIImage();
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendFBString(string: "--\(boundary)\r\n")
                body.appendFBString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendFBString(string: "\(value)\r\n")
            }
        }
        
        let filename = "testImage"
        let mimetype = "image/jpg"
        
        body.appendFBString(string: "--\(boundary)\r\n")
        body.appendFBString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendFBString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendFBString(string: "\r\n")
        
        body.appendFBString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func privacyPolicyBtn_Action(_ sender: UIButton) {
        let storyBoard  = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        
        let faqVC =  storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        faqVC.selectedButtonFlag = "1"
        faqVC.selectedVCFlag = "2"
        present(faqVC, animated: true, completion: nil)
        
    }
}

extension NSMutableData {
    
    func appendFBString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
