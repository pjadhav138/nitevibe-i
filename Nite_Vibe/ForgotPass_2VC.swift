//
//  ForgotPass_2VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 30/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore
class ForgotPass_2VC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var lbl_EmailMob: UILabel!
    @IBOutlet weak var lbl_OTPtime: UILabel!
    @IBOutlet weak var txt_OTP: UITextField!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var verifyOtpOutlet: UIButton!
    var seconds = 60
    var timer = Timer()
    var usernametext = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txt_OTP.delegate = self
        let getUsername:UserDefaults = UserDefaults.standard
        
        let usernamelbl = getUsername.value(forKey: "username")
        print("Username:\(String(describing: usernamelbl))")
        usernameLbl.text = usernamelbl as? String
        lbl_OTPtime.isHidden = false
        
        verifyOtpOutlet.layer.cornerRadius = 10
        verifyOtpOutlet.clipsToBounds = true
     
        self.startTimer()
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn_action(_ sender: Any) {
        let ForgotPass_1VC:ForgotPass_1VC = storyboard?.instantiateViewController(withIdentifier: "ForgotPass_1VC") as! ForgotPass_1VC
        present(ForgotPass_1VC, animated: true, completion: nil)
    }
    
    @IBAction func resendBtn_action(_ sender: Any) {
        print("resend otp")
        self.sendOtp()
    }
    

    @IBAction func verifyOTP_action(_ sender: Any) {
        
        
        var otptextfield = txt_OTP.text
        
        let getOtpPassword:UserDefaults = UserDefaults.standard
        
        getOtpPassword.value(forKey: "forgetpasswordotp")
           let responseOtpMessage = (String(describing:getOtpPassword.value(forKey: "forgetpasswordotp")!))
        if otptextfield == responseOtpMessage{
            
            
            print("varify otp successfully")
            let ForgotPass_3VC:ForgotPass_3VC = storyboard?.instantiateViewController(withIdentifier: "ForgotPass_3VC") as! ForgotPass_3VC
            present(ForgotPass_3VC, animated: true, completion: nil)
            
            
        }else{
            print("please enter valid otp")
            let alert = UIAlertController(title: ":(", message: "Please enter validtp Successfully" as! String, preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func startTimer(){
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Register_1_VC.Counter), userInfo: nil, repeats: true)
    }
    
    @objc func Counter(){
        seconds -= 1
        lbl_OTPtime.text = ("OTP Valid for 30 Seconds:\(String(seconds))")
        
        print("Print counter : \(String(seconds))")
        
        if (seconds == 0){
            
            timer.invalidate()
            lbl_OTPtime.text = "Please Resend OTP"
            
        }
        
    }
    
    func sendOtp(){
        var respo = Int()
        
        
        let getUsername:UserDefaults = UserDefaults.standard
        
        let usernamelbl = getUsername.value(forKey: "username")
        print("Username:\(String(describing: usernamelbl))")
        usernametext = usernamelbl as! String
        let URL_String = "http://www.nitevibe.in/webservices/otp.php?username=\(usernametext)"
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    
                    print("Valid otp response : ",responseMessage!)
                    
                    let setOtpRespnse:UserDefaults = UserDefaults.standard
                    
                    setOtpRespnse.set(responseMessage!, forKey: "forgetpasswordotp")
                    //let textfieldOtp = txt_OTP.text!
                    
                    //if textfieldOtp == responseMessage
                    let alert = UIAlertController(title: ":(", message: responseMessage as? String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 408){
                    
                    
                    let alert = UIAlertController(title: "Yes", message:"Somthing went wrong" , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
