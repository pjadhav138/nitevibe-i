//
//  DateHelper.swift
//  Nitevibe
//
//  Created by Mac on 21/09/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import UIKit
import DateTools

extension Date {
    
    func getDateString(_ format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        df.amSymbol = "am"
        df.pmSymbol = "pm"
        //        df.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        return df.string(from: self)
    }
    
    func getDateString() -> String {
        return self.getDateString(
            DateFormateConstant.date
        )
    }
    
    func getNSDate() -> NSDate {
        return self as NSDate
    }
    
    func getTimeString() -> String {
        return self.getDateString(
            DateFormateConstant.timeWithSpace
        )
    }
    
    func getTimeStringWithoutSpace() -> String {
        return self.getDateString(
            DateFormateConstant.timeWithoutSpace
        )
    }
    
    func getDateTimeWithSpaceInTimeString() -> String {
        return self.getDateString(
            DateFormateConstant.dateTimeWithSpaceInTime
        )
    }
    
    func getDateTimeWithoutSpaceInTimeString() -> String {
        return self.getDateString(
            DateFormateConstant.dateTime
        )
    }
    
    func createDate(hour: Int, minutes: Int, seconds: Int) -> Date {
        return NSDate(
            year: self.getNSDate().year(),
            month: self.getNSDate().month(),
            day: self.getNSDate().day(),
            hour: hour,
            minute: minutes,
            second: seconds
            ) as Date
    }
}

struct DateFormateConstant {
    static let date = "dd/MM/yyyy"
    static let monthDayYear = "MMM dd yyyy"
    static let timeWithSpace = "hh:mm a"
    static let timeWithoutSpace = "hh:mma"
    static let dateTime = "MM/dd/yyyy hh:mma"
    static let dateTimeWithSpaceInTime = "MM/dd/yyyy hh:mm a"
    static let dateTimeWithSeconds = "yyyy-MM-dd HH:mm:ss"
    static let dayDateYear = "EEEE, MMM d, yyyy"
    static let smallDayDateYear = "EEE, MMM d, yyyy"
    static let dateMonthTextYearTime = "dd MMM, yyyy hh:mm a"
    static let dateTimeWithSecondsTimezone = "yyyy-MM-dd HH:mm:ssZ"
    static let dateFullMonthYear = "dd MMMM, yyyy"
}
