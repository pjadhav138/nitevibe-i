//
//  UserGuideCellVC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 10/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class UserGuideCellVC: UICollectionViewCell {
    @IBOutlet var lbl_Second: UILabel!
   
    @IBOutlet var bgView: UIView!
    @IBOutlet var UG_Img: UIImageView!
    @IBOutlet var lbl_First: UILabel!
}
