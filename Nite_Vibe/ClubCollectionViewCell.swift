//
//  ClubCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 12/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ClubCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var clubImageView: UIImageView!
    
  
   
    @IBOutlet weak var dateLabel: UILabel!
    
    
    @IBOutlet weak var clubNameLabel: UILabel!
    
    
    @IBOutlet weak var peopleAtClubLabel: UILabel!
    @IBOutlet weak var shareButtonOutlet: UIButton!
    
    @IBOutlet weak var favouriteButtonTapped: UIButton!
    
    
    @IBOutlet weak var ratioProgressView: UIProgressView!
    
    @IBOutlet weak var femaleCountLbl: UILabel!
    
    
    @IBOutlet weak var maleCountLbl: UILabel!
    
    @IBOutlet weak var femaleImageView: UIImageView!
    @IBOutlet weak var maleImgeView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //eventDateLbl.intrinsicContentSize.width
        clubImageView.layer.cornerRadius = 8
        clubImageView.clipsToBounds = true
        dateLabel.layer.cornerRadius = 4
        dateLabel.clipsToBounds = true
        
    }
    
    
   
    
    
    
    
    
}
