//
//  EventCouponVC.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 15/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class EventCouponVC: UIViewController {

    @IBOutlet weak var couponView: UIView!
    
    
    @IBOutlet weak var faqButtonTapped: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        couponView.layer.masksToBounds = true
        
        couponView.layer.cornerRadius = 10
        
        couponView.addSubview(faqButtonTapped)
        
        
        faqButtonTapped.addTarget(self, action: #selector(faqActionTapped), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }

    @objc func faqActionTapped(){
        
        print("tapped faq button")
        
        
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
       let faQVC = storyboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        //select help button now
      faQVC.selectedButtonFlag = "0"
        faQVC.selectedVCFlag = "0"
      present(faQVC, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first as? UITouch
        
        if touch?.view != couponView{
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
        
        
        
        
    }
    
//tou
//    override func touchesBegan(_ touches: Set<AnyHashable>, with event: UIEvent) {
//        var touch: UITouch? = touches.first as? UITouch
//        //location is relative to the current view
//        // do something with the touched point
//        if touch?.view != couponView {
//            couponView.isHidden = true
//        }
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
