//
//  ImageZoomViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 18/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ImageZoomViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        imageView.image = image
        
        
        imageView.layer.masksToBounds = true
        
        imageView.layer.cornerRadius = 16
        // Do any additional setup after loading the view.
    }

   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first as? UITouch
        
        if touch?.view != imageView{
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
