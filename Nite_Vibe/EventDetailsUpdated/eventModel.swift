//
//  event.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 13/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import  SwiftyJSON



struct eventModel{
      var eventId = String()
      var eventName = String()
      var eventUserGoing = [userGoingModel]()
      var eventImage = [eventImageModel]()
      var eventPrefArray = [eventPrefArrayModel]()
      var eventDate = String()
      var eventFeesStagEntry = String()
      var eventFeesCoupleEntry = String()
      var eventDescription = String()
      var eventPeopleGoingTotal = Int()
      var eventMaleCount = Int()
      var eventFemaleCount = Int()
    init()
    {
        
        self.eventUserGoing = []
        self.eventImage = []
        self.eventPrefArray = []
    }
    
    init(json:[String:Any]){
        
        if let evId = json["event_id"] as? String{
            self.eventId = evId
        }
        if let evDate = json["event_date"] as? String{
            self.eventDate = evDate
        }
        
        if let evtFeesStagEntry = json["stag_entry"] as? String{
            self.eventFeesStagEntry = evtFeesStagEntry
            }
            if let evFeesCoupleEntry = json["couple_entry"] as? String{
            
                self.eventFeesCoupleEntry = evFeesCoupleEntry
                }
                    if let evName = json["event_name"] as? String{
                    
                        self.eventName = evName
                        }
                            if let evDescription = json["event_des"] as? String{
                                
                                self.eventDescription = evDescription
            
                                }
                                    if let evPeopleGoingTotal = json["total"] as? Int{
                                        
                                        self.eventPeopleGoingTotal = evPeopleGoingTotal
                                        
                                        }
                                            if let evMaleCount = json["male_count"] as? Int{
                                                
                                                self.eventMaleCount = evMaleCount
            
                                                }
                                                    if let evFemaleCount = json["female_count"] as? Int{
                                                        
                                                        self.eventFemaleCount = evFemaleCount
            
                                                        }
        
        
            print("event Name \(eventName)")
            
            if let evUserGoing = json["going_arr"] as?  [[String:Any]]{
                
                for evUser in evUserGoing{
                    let  userGoing = userGoingModel(json: evUser)
                    
                    self.eventUserGoing.append(userGoing)
                }
                if let evImageData = json["event_pic"] as? [[String:Any]]{
                    
                    for evImage in evImageData{
                        
                        let eImage = eventImageModel(json: evImage)
                        
                        self.eventImage.append(eImage)
                        
                        
                    }
                    if let evEventPref = json["pref_arr"] as? [[String:Any]]{
                        
                        for evPref in evEventPref{
                            
                            let eventPref = eventPrefArrayModel(json: evPref)
                            self.eventPrefArray.append(eventPref)
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            
            
        }
    }

struct userGoingModel {
    
    var userId = String()
    var userName = String()
    var userGender = String()
    var userIamge = String()
    var userLikeCategory = String()
    init()
    {}
    
    init(json:[String:Any]){
        
        if let uId = json["user_going_id"] as? String{
            self.userId = uId
            }
        
            if let uName = json["name"] as? String{
            self.userName = uName
            }
        
            if let uGender = json["gender"] as? String{
            self.userGender = uGender
            }
            if let uImage = json["profile"] as? String{
            self.userIamge = uImage
            }
            if let uLikeCategory = json["category"] as? String{
            self.userLikeCategory = uLikeCategory
            
            }
    }
    
}

struct eventImageModel{
    
    var eventImage = String()
    
    init()
    {}
    
    init(json:[String:Any]){
    
        if let eImage = json["img"] as? String{
            
            self.eventImage = eImage
        }
    
    }
}


struct eventPrefArrayModel{
    
    var eventPrefAge = String()
    var eventPrefGender = String()
    
    init()
    {}
    
    init(json:[String:Any]){
        
        if let ePrefAge = json["age"] as? String{
            self.eventPrefAge = ePrefAge
        }
        if let ePrefGender = json["gender"] as? String{
            self.eventPrefGender = ePrefGender
        }
        
    }
    
    
}

//class eventModel{
//    
//    var eventId = String()
//    var eventName = String()
//    var eventUserGoing = [userGoingArray]()
//    
//    init()
//    {}
//    
//    init(json:JSON) {
//        
//        self.eventId = json["event_id"].stringValue
//        self.eventName = json["event_name"].stringValue
//        self.eventUserGoing = [userGoingArray(goingArrayJson:json["going_arr"])]
//        
//        print("event going model array \(self.eventUserGoing[0])")
//        
//        
//        for i in 0..<eventUserGoing.count{
//
//            print("user going array \(i) th data \(eventUserGoing[i])")
//        }
//    }
//    
//    
//}
//
//class userGoingArray{
//    
//    //var goingArray = [JSON]()
//    var userName = String()
//    var userGender = String()
//    
//    init()
//    {}
//    
//    init(goingArrayJson:JSON) {
//        
//        
//        self.userName = goingArrayJson["name"].stringValue
//        
//        self.userGender = goingArrayJson["gender"].stringValue
//        
//    }
//    
//    
//    
//}


//class eventModel{
//
//    var eventID:String = ""
//    var eventName:String = ""
//    var eventGoing = [userGoing]()
//
//
//    init()
//    {}
//
//    init(json:JSON){
//
//        self.eventID = json["event_id"].stringValue
//        self.eventName = json["event_name"].stringValue
//        self.eventGoing = [userGoing(userGoingJson:json["going_arr"])]
//        //self.userGoingPojo = userGoingMo(userGoingJson:json["going_arr"])
//
//        // eventGoingArr = json["going_arr"].arrayValue
//
//        print("event id in model class \(eventID)")
//    }
//}
//
//class userGoing{
//
//
//    var userName = String()
//    var userAge = String()
//    var userGender = String()
//
//    init()
//    {}
//
//    init(userGoingJson:JSON) {
//        self.userName = userGoingJson["name"].stringValue
//        self.userAge = userGoingJson["age"].stringValue
//        self.userGender = userGoingJson["gender"].stringValue
//
//        print(userGoingJson["name"].stringValue)
//        print("print user going user name \(userName)")
//    }
//
//
//}
//
