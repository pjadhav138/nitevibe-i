//
//  EventDetailsCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Appmonks on 05/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class EventDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var eventDataTblViewOutlet: UITableView!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var eventPictures: UIImageView!
    @IBOutlet weak var EventPageControll: UIPageControl!
    @IBOutlet weak var tblheight: NSLayoutConstraint!
    @IBOutlet weak var btn_ImGoingOutletcoll: UIButton!
    

    var aCategory:eventModel?//define an optional
    var aUserGoing:UserGoingModel?//define an optional
    
    func updateCollectionACellWith(uGoing:UserGoingModel){
        
        self.aUserGoing = uGoing
        
        
    }

    func updateCellWith(category:eventModel){
        
        self.aCategory = category
       
        self.aCategory?.eventId
        
        print("event id : \(self.aCategory?.eventId)")
        
    }
}



extension EventDetailsCollectionViewCell{
    func setParentTableViewDataSourceDelegate
        <D: UITableViewDelegate & UITableViewDataSource>
        (_ dataSourceDelegate: D, forRow row:Int)
    {
        eventDataTblViewOutlet.delegate = dataSourceDelegate
        eventDataTblViewOutlet.dataSource = dataSourceDelegate
        
        eventDataTblViewOutlet.reloadData()
        
    }
}
