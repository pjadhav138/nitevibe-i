//
//  EventDetailsUpdatedViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 07/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
//import AlamofireObjectMapper
import SwiftyJSON
import Kingfisher
import CTShowcase

class EventDetailsUpdatedViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var grandCollectionView: UICollectionView!
    var descriptionConstraints:[NSLayoutConstraint] = []
    var costConstraints:[NSLayoutConstraint] = []
    
   
   
    var label_first:[String] = ["Hello","Feel the vibe"]
    var oneUPArray:[String] = ["WakeUP"]
    var oneDownArray:[String] = ["Good Morning"]
    var eventIndexNumber = Int()
    // var eventImageArray:[UIImage] = [UIImage(named: "banner13")!,UIImage(named: "banner12")!,UIImage(named: "banner11")!,UIImage(named: "banner")!,UIImage(named: "event1")!,UIImage(named: "event2")!,UIImage(named: "event3")!]
    
    var eventImageArrayTest:[UIImage] = [UIImage(named: "banner13")!,UIImage(named: "banner12")!]
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    //var eventModel:[EventModel] = []
    //var eventImgDictionary = [Dictionary <String,AnyObject>]()
    var eventImageArray = [String]()
    var eventNameArray = [String]()
    var eventPeopleGoingArray = [Int]()
    var eventDateArray = [String]()
    
    var eventPriceArray = [String]()
    //for progress bar's male - female  label value population
    var eventMaleCountArray = [Int]()
    var eventFemaleCountArray = [Int]()
    //for progress bar's value insertion
    //we can pass only float value to progress bar
    var eventMaleCountFloatArray = [Float]()
    
    var eventDescriptionArray = [String]()
    var eventStagPriceArray = [String]()
    var eventCouplePriceArray = [String]()
    var userGoing = [UserGoingModel]()
    var dUserGoing:UserGoingModel?//to hold the data to be displayed
    var eventMaleCount:Float = 0
    var eventFemaleCount:Float = 0
    var eventNameString = String()
    
    //var arrData = [JsonGoingModel]()
    var arrData2 = [eventModel]()
    var selectedEventId = ""
    var selectedClubId = ""
   // var eventResponse:JSON = nil
    var userPrefFlag = Int()
    var userGoingProfileImageArray = [String]()
    var likeCategoryArray = [String]()
    var events = [eventModel]()
    var bCategory:eventModel?
   
    var OverlayCheckIn = "CheckIn First Time Launch"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.grandCollectionView.delegate = self // set delegate
        self.grandCollectionView.dataSource = self
        
        print("print event 1")
       
        //fetchData()
        print("print event 6")
      
        print("print event 13")
        fetchswiftyData()
        
//        DispatchQueue.main.async {
//           self.grandCollectionView.reloadData()
//        }
//
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //let width = UIScreen.main.bounds.width
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1)
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        grandCollectionView!.collectionViewLayout = layout
        
       
    }
    
    
    
    
    func fetchswiftyData(){
        
        print("p 1")
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        print("print event count fetchData function 5")
        let urlString = "http://www.nitevibe.in/webservices/event_details.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&selected_id=\(selectedEventId)"
        
        print("url string event details :\(urlString)")
        Alamofire.request(urlString, method: .get).responseJSON{ (response) -> Void in
            
            print("print response :\(response)")
         
            if let value = response.data {
               
                print("value :\(value)")
                
                do {
                     print("p 2")
                    let json = try JSON(data: value)
                    
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                
                                print("p 3")
                                self.events.append(eventModel(json: arr))
                                print("print event count fetchswiftyData function 2:\(self.events.count)")
                                
                                                    DispatchQueue.main.async {
                                
                                                        self.grandCollectionView.reloadData()
                                
                                                    }
                                
                            }
                            print("p 4")
                            
                           
                            
                            }
                         print("p 5")
                        
                        
                       
                       
                        }
                    
                    
//                    DispatchQueue.main.async {
//
//                        self.grandCollectionView.reloadData()
//
//                    }
                     print("p 6")
                       
//                    print("print event count fetchswiftyData function 3:\(self.events.count)")
//
//
//                     print("p 7")
//                    print("print event count fetchswiftyData function 4:\(self.events.count)")
                }catch {
                    print("cannot convert to Json")
                }
                 print("p 8")
            }
            
//            print("p 9")
//            print("print event count fetchswiftyData function 5:\(self.events.count)")
//            print("count of event thirsday \(self.events.count)")
//            print("count of event user thirsday \(self.events[0].eventUserGoing.count)")
//
//            print("user going user name \(self.events[0].eventUserGoing)")
            

            for i in 0..<self.events.count{
                
                print("user event pref array count \(i)")
                
                
                self.events[i].eventPrefArray.count
                
                
                print(" count zero \(self.events[i].eventPrefArray.count)")
                
                
                if self.events[i].eventPrefArray.count != 0{
                    
                    self.userPrefFlag = 1
                }else{
                    
                    self.userPrefFlag = 0
                }

                
            }
            
           
//
//            for i in 0..<self.events[0].eventUserGoing.count{
//                print("user going index \(i)th user name \(self.events[0].eventUserGoing[i].userName)")
//                print("user profile pic \(i)th user pic \(self.events[0].eventUserGoing[i].userIamge)")
//
//
//            }
            
        }
        
//        print("print event count fetchswiftyData function 49:\(self.events.count)")
        
    }
    
    
    func fetchData(){
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        print("print event count fetchData function 7")
        let urlString = "http://www.nitevibe.in/webservices/event_details.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&selected_id=\(selectedEventId)"
        
        
        print("event details :\(urlString)")
        
        do{
            
            var urlRequest = try URLRequest(url: urlString, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                //print("message\(dict)")
                //print("dict respo: \(dict["response"]!)")
                //print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                //self.dictData.append(dictData,])
                print(" dict data event detail \(dictData)")
                
                self.eventNameArray.removeAll()
                self.eventDateArray.removeAll()
                self.eventPeopleGoingArray.removeAll()
                self.eventDateArray.removeAll()
                self.eventPriceArray.removeAll()
                self.eventMaleCountFloatArray.removeAll()
                self.eventMaleCountArray.removeAll()
                self.eventDescriptionArray.removeAll()
                self.eventStagPriceArray.removeAll()
                self.eventCouplePriceArray.removeAll()
                for data in dictData{
                    
                    print("Event Pic Data: \(data["event_pic"] as? [Dictionary <String,AnyObject>] )")
                    
                    self.eventNameString = data["event_name"] as! String
                    print("Event Name Data: \(data["event_name"] as! String)")
                    self.eventNameArray.append(self.eventNameString)
                    self.eventPeopleGoingArray.append(data["total"] as! Int)
                    self.eventDateArray.append(data["event_date"] as! String)
                    self.eventPriceArray.append(data["stag_entry"] as! String)
                    self.eventStagPriceArray.append(data["stag_entry"] as! String)
                    self.eventCouplePriceArray.append(data["couple_entry"] as! String)
                    
                    let maleCount = data["male_count"] as! Float
                    self.eventMaleCount = (maleCount/100)
                    
                    print("event male count \(self.eventMaleCount)")
                    
                    self.eventMaleCountFloatArray.append(self.eventMaleCount)
                    self.eventFemaleCountArray.append(data["female_count"] as! Int)
                    self.eventMaleCountArray.append(data["male_count"] as! Int)
                    self.eventDescriptionArray.append(data["event_des"] as! String)
                    print("converted event male count \(self.eventMaleCount)")
                    print("Event Name Array \(self.eventNameArray)")
                    print("Event People going array \(self.eventPeopleGoingArray)")
                    print("Event Date Array \(self.eventDateArray)")
                    print("Event price Array \(self.eventPriceArray)")
                    print("Event male count array \(self.eventMaleCountFloatArray)")
                    print("Event female count  \(self.eventFemaleCountArray)")
                    print("Event description array \(self.eventDescriptionArray)")
                    
                    let defaults = UserDefaults.standard
                    defaults.removeObject(forKey: "eventnamearray")
                    defaults.removeObject(forKey: "eventpeoplegoingarray")
                    defaults.removeObject(forKey: "eventdatearray")
                    defaults.removeObject(forKey: "eventpricearray")
                    defaults.removeObject(forKey: "eventdescriptionarray")
                    defaults.removeObject(forKey: "eventmalecountfloatarray")
                    defaults.removeObject(forKey: "eventmalecountarray")
                    defaults.removeObject(forKey: "eventfemalecountarray")
                    defaults.removeObject(forKey: "eventstagarray")
                    defaults.removeObject(forKey: "eventcouplepricearray")
                    defaults.set(self.eventNameArray, forKey: "eventnamearray")
                    defaults.set(self.eventPeopleGoingArray, forKey: "eventpeoplegoingarray")
                    defaults.set(self.eventDateArray, forKey: "eventdatearray")
                    defaults.set(self.eventPriceArray, forKey: "eventpricearray")
                    defaults.set(self.eventDescriptionArray, forKey: "eventdescriptionarray")
                    defaults.set(self.eventMaleCountFloatArray, forKey: "eventmalecountfloatarray")
                    defaults.set(self.eventMaleCountArray, forKey: "eventmalecountarray")
                    defaults.set(self.eventFemaleCountArray, forKey: "eventfemalecountarray")
                    defaults.set(self.eventStagPriceArray, forKey: "eventstagarray")
                    defaults.set(self.eventCouplePriceArray, forKey: "eventcouplepricearray")
                    
                    guard let dictImage = data["event_pic"] as? [Dictionary <String,AnyObject>] else {
                        print("test 3")
                        print("Error with dictionary data: \(String(describing: response.result.error))")
                        return
                    }
                    
                    self.eventImageArray.removeAll()
                    for imgData in dictImage{
                        
                        let eventImages = imgData["img"] as! String
                        print("image Data \(eventImages)")
                        self.eventImageArray.append(eventImages)
                        
                        let defaults = UserDefaults.standard
                        
                        defaults.removeObject(forKey: "eventimagearray")
                        
                        defaults.set(self.eventImageArray, forKey: "eventimagearray")
                        
                        
                    }
                    
                    guard let dictUserGoing = data["going_arr"] as? [Dictionary <String,AnyObject>] else {
                        
                        print("test 4")
                        print("Error with dictionary data:\(String(describing: response.result.error))")
                        return
                        
                    }
                    
                    self.userGoingProfileImageArray.removeAll()
                    self.likeCategoryArray.removeAll()
                    print("userGoingArray Dict\(dictUserGoing)")
                    for userGoingImagesData in dictUserGoing{
                        
                        let userGoingProfileImages = userGoingImagesData["profile"]
                        
                        self.userGoingProfileImageArray.append(userGoingProfileImages as! String)
                        
                        let defaults = UserDefaults.standard
                        
                        defaults.removeObject(forKey: "usergoingprofileimagearray")
                        defaults.set(self.userGoingProfileImageArray, forKey: "usergoingprofileimagearray")
                       
                        let likeCategory = userGoingImagesData["category"] as! String
                        self.likeCategoryArray.append(likeCategory)
                        print("like category array count:\(self.likeCategoryArray.count)")
                        defaults.removeObject(forKey: "usergoinglikecategoryarray")
                        defaults.set(self.likeCategoryArray, forKey: "usergoinglikecategoryarray")
                        
                        
                    }
                   
                    //for checking user fill profile or not
                    guard let dictPrefArr = data["pref_arr"] as? [Dictionary <String,AnyObject>] else {
                        
                        print("test 4")
                        print("Error with dictionary data:\(String(describing: response.result.error))")
                        return
                        
                    }
                    
                    //set flag for checking user fill create profile or not
                    
                    if dictPrefArr.count == 0{
                        
                        self.userPrefFlag = 0
                        
                    }else{
                       
                        self.userPrefFlag = 1
                    }
                    
                    
                }
                
                //self.grandCollectionView.reloadData()
                print("print name array outside people going array \(self.eventPeopleGoingArray)")
                print("going array user profile image data \(self.userGoingProfileImageArray.count)")
                
                
                
                //self.grandCollectionView.reloadData()
            }
            print("print name array ouside for loop 2: \(self.eventImageArray)")
            
            
            
        }catch{
            print("error")
            
        }
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100 {
            
            
            
            return self.events.count
        } else if collectionView.tag == 90 {
            let count = self.events[eventIndexNumber].eventUserGoing.count
            
            if count <= 4 {
                return count
            }
            
            return 4
        }
        
        return 0
    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        print("print event count collection view   numberOfItemsInSection 8:\(self.events.count)")
//
//
//        var count = Int()
//        if self.events.count >= 0{
//
//            print("count 1 \(count)")
//            count = self.events.count
//
//            return count
//        }else{
//            print("count 2 \(count)")
//
//            if collectionView.tag == 100{
//
//                print("print event count 6:\(count)")
//
//                return count
//
//
//             }else if collectionView.tag == 90{
//
//                print("print event count 7:\(count)")
//
//                print("print going user id :\(self.events[eventIndexNumber].eventUserGoing.count)")
//
//                count = self.events[eventIndexNumber].eventUserGoing.count
//
//                   return count
//
//            }else{
//
//
//
//                return count
//            }
//
//
//
//
//        }
    
//        if collectionView  == grandCollectionView{
//
//
////            if self.events.count == 0{
////
////                print("do nothing")
////            }else{
////
////
////            }
//            return 2
//
//        }else{
//
//            print("print event count 7:\(events.count)")
//
//            return 0
//
//        }
        
    
 //   }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100
        {
            print("print event count  cellForItemAt 9:\(self.events.count)")
            
            print("Selected Index Path\(indexPath.row)")
            
            eventIndexNumber = indexPath.row
            
            let cell = grandCollectionView.dequeueReusableCell(withReuseIdentifier: "eventcollectioncell", for: indexPath) as! EventDetailsCollectionViewCell
           // cell.textLabel.sizeToFit()
            cell.eventDataTblViewOutlet.sizeToFit()
            //table.rowHeight = UITableViewAutomaticDimension
            //table.estimatedRowHeight = UITableViewAutomaticDimension
            
            cell.eventDataTblViewOutlet.rowHeight = UITableViewAutomaticDimension
            cell.eventDataTblViewOutlet.estimatedRowHeight = UITableViewAutomaticDimension
            
            //cell.btn_ImGoingOutletcoll.addTarget(self, action: #selector(imGoingButtonTapped), for: .touchUpInside)
            
           // cell.tblheight?.constant = cell.eventDataTblViewOutlet.contentSize.height
            
            
            
//            let aCategory = self.events[indexPath.row]
//
//            cell.updateCellWith(category: aCategory)
//
//            updateCellWithB(category: aCategory)
//            let defaults = UserDefaults.standard
//            let defaultEventImageArray = defaults.stringArray(forKey: "eventimagearray") ?? [String]()
//
//            print("print event image array in number of item \(defaultEventImageArray)")
//            print("print array \(defaultEventImageArray.count)")
//            let eventImageString = defaultEventImageArray[indexPath.item]
//            let imageURL = URL(string: eventImageString)
//            let imageData = try! Data(contentsOf: imageURL! as URL)
            
 //           print("\(imageData)")
//
            cell.back_btn.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
            
            cell.eventPictures.kf.setImage(with: URL(string: self.events[eventIndexNumber].eventImage[0].eventImage))
            
            
            
//            cell.eventPictures.image = UIImage(data:imageData)
//            //cell.eventPictures.image = eventImageArrayTest[indexPath.row]
//
//            cell.frame.size.width = screenWidth
//            cell.frame.size.height = screenHeight
//            cell.layer.borderColor = UIColor.black.cgColor
//            cell.layer.borderWidth = 1
//            cell.layer.cornerRadius = 8
//            cell.clipsToBounds = true
            
            
            
            
            // print("print collection  indexPath \(eventIndexNumber ) value \(arrData[eventIndexNumber].eventName)")
            //            let aCollection = self.eventModel[indexPath.item]
            //            cell.eventNameLbl.text = aCollection.eventsName?[indexPath.row]
            
            
            
            //            cell.clubFeatureImageView.image = UIImage(data: imageData)
            
            //error come because some file not found
            
            //        if(imageData != nil)
            //        {
            //            cell.clubFeatureImageView.image = clubArray[in]
            //        }
            //cell.clubFeatureImageView.image = clubArray[indexPath.row]
            
            
//            var infoDict = [String:Any]()
//            infoDict = dataForIndex(index:eventIndexNumber)
//
//            let aUserGoing = UserGoingModel(withInfo: infoDict)
//
//
//            userGoing.removeAll()
//            userGoing.append(aUserGoing)
//
//            print("print count categories \(userGoing.count)")
//            cell.updateCollectionACellWith(uGoing: aUserGoing)
//
            
            return cell
            
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "whosgoingcell", for: indexPath) as! Who_sGoingCollectionViewCell
            print("print event count  whosgoingcell cell 9:\(self.events.count)")
            
//            if self.events[eventIndexNumber].eventUserGoing.count == 0{
//
//
//
//            }
            
            
            //print("who is going array \(eventImageArray.count)")
//            let defaults = UserDefaults.standard
//            let defaultEventImageArray = defaults.stringArray(forKey: "eventimagearray") ?? [String]()
            //print("who is going array \(defaultEventImageArray.count)")
            
            
            //let eventImageString = defaultEventImageArray[indexPath.row]
            //let imageURL = URL(string: eventImageString)
            //let imageData =  try! Data(contentsOf: imageURL! as URL)
            
            //print("print image data \(imageData)")
            
            
            //cell.whosgoingImageView.image = eventImageArrayTest[indexPath.row]
            
            //cell.whosgoingImageView.image = UIImage(data:imageData)
            
            //let cUserGoing = self.userGoing[indexPath.section]
            
            //            if let subCategoryItem = self.dCategory?.subCatItems[indexPath.item]{
            //                //if let subCategoryItem = self.bCategory?.subDrinkDataSource[indexPath.section][indexPath.item]{
            //
            //                cell.updateCellWithSubDrinkName(name:subCategoryItem)
            //            }
            
//            let userGoingImageArray = defaults.stringArray(forKey:"usergoingprofileimagearray") ?? [String]()
//
//
//            //handle problem using user defaults
//            print("default userGoingProfileImageArray count :  \(userGoingImageArray.count)")
//
//            let userGoingLikeCategoryArray = defaults.stringArray(forKey:"usergoinglikecategoryarray") ?? [String]()
//
            
            //handle problem using user defaults
          // print("default userGoingLikeCategoryArray count :  \(userGoingLikeCategoryArray.count)")
            
            //imp code 1
            
            
            if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "1"{
                
            cell.whosgoingImageView.layer.borderColor = UIColor(red: 243/255, green: 40/255, blue: 104/255, alpha: 1.0).cgColor
            }else if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "2"{
            cell.whosgoingImageView.layer.borderColor = UIColor(red: 199/255, green: 202/255, blue: 219/255, alpha: 1.0).cgColor
                
            }else if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "3"{
            cell.whosgoingImageView.layer.borderColor = UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 1.0).cgColor
                
            }else{
                 cell.whosgoingImageView.layer.borderColor = UIColor(red: 158/255, green: 185/255, blue: 22/255, alpha: 1.0).cgColor
            }
            
//           // if self.userGoing_NonPref_Images[indexPath.row] == ""{
//
//                print("no image found")
//                cell1.otherUserImages.image = #imageLiteral(resourceName: "defaultProfile")
//                cell1.otherUserName.text = userGoing_NonPref_Name[indexPath.row]
//
//            }
            
            if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userIamge == ""{
                
                print("no image found")
                cell.whosgoingImageView.image = #imageLiteral(resourceName: "defaultIcon")
                
            }else{


                cell.whosgoingImageView.kf.setImage(with: URL(string:self.events[eventIndexNumber].eventUserGoing[indexPath.row].userIamge))
            }
            
           return cell
        }
        
        
        
    }
    
    
    func updateCellWithB(category:eventModel){
        
        self.bCategory = category
        
    }
    
    @objc func backButtonTapped(){
     
        let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        
        let ClubDetailVC = storyboard.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
        
        
        ClubDetailVC.selectedClubId = selectedClubId
        present(ClubDetailVC , animated: true, completion: nil)
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 100{
            
            print("Selected Collection view")
            
            print("print index path row \(indexPath.row)")
            
            
            print("in did select :\(events.count )")
//            let defaults = UserDefaults.standard
//            let defaultEventImageArray = defaults.stringArray(forKey: "eventimagearray") ?? [String]()
//             //imp code 2
//            print("print event image array in number of item \(defaultEventImageArray)")
//            print("print array \(defaultEventImageArray.count)")
//            let eventImageString = defaultEventImageArray[indexPath.row]
//            let imageURL = URL(string:  self.events[indexPath.row].eventImage[0].eventImage)
//            let imageData = try! Data(contentsOf: imageURL! as URL)
//
//           print("\(imageData)")
//
//
//
//
            
            let imageURL = URL(string:  self.events[indexPath.row].eventImage[0].eventImage)
            let imageData = try! Data(contentsOf: imageURL! as URL)
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)

            let zoomImageVC = storyboard.instantiateViewController(withIdentifier: "ImageZoomViewController") as! ImageZoomViewController

            zoomImageVC.image = UIImage(data:imageData)!
            zoomImageVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addChildViewController(zoomImageVC)
            self.view.addSubview(zoomImageVC.view)
            
            
            
            
        }else{
            
            print("Not selected ")
            
        }
        
    }
//    func dataForIndex(index:Int) -> [String:Any]{
////        print("print event count dataForIndex function 10:\(self.events.count)")
////        var data = [String:Any]()
////
////        let defaults = UserDefaults.standard
////        let defaultEventImageArray = defaults.stringArray(forKey: "eventimagearray") ?? [String]()
////
////
////        print("data for index \(index)")
//        //myarray is mainDrinkName
//        //let mainDrinkNameArray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
//
//
//        //let mainDrinkIdArray = defaults.stringArray(forKey: "drinkidarray") ?? [String]()
//
//
//
//        //print("main drink id \(mainDrinkIdArray)")
//        //print("index \(index)")
//        //let indexArray = index - 1
//        //print("main index id data \(mainDrinkIdArray[indexArray])")
//
//        print("Event detail Image array \(defaultEventImageArray)")
//        var indexArray = [Int]()
//
//        for indexPos in 0..<defaultEventImageArray.count{
//
//            indexArray.append(indexPos)
//
//            print("data for index array  \(indexArray)")
//
//        }
//
//
//
//        if index == indexArray[index]{
//
//            data["user_images_array"] = [defaultEventImageArray[index]]
//
//            
//            print("data for index \(index ) and \(data["user_images_array"])")
//
//        }
//        //        if index == defaultEventImageArray[index]{
//        //
//        //            print("print sunday 1 \(defaultEventImageArray[index])")
//        //
//        //
//        //
//        //            //data["drink_cat_name"] = "\(mainDrinkNameArray[index])"
//        //            //data["drink_cat_id"] = "\(mainDrinkIdArray[indexArray])"
//        //            data["user_images_array"] = [defaultEventImageArray[index]]
//        //
//        //
//        //
//        //        }
//        return data
//    }
//
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //let aTable = self.eventModel[]
        //print("table event \(String(describing: aTable.eventsName))")
        
        print("print event count table view numberOfRowsInSection 11:\(self.events.count)")
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tblcell", for: indexPath) as! ParentTableViewCell
        
        print("inside table view \(eventNameArray)")
        
        print("print event count 12:\(self.events.count)")
        cell.txtView_EventDetailds.sizeToFit()
        cell.txtView_EventDetailds.layoutIfNeeded()
        let fixedWidth = cell.txtView_EventDetailds.frame.size.width
        let newSize = cell.txtView_EventDetailds.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        cell.txtView_EventDetailds.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        cell.txtView_EventDetailds.isScrollEnabled = false
        
//
//        let defaults = UserDefaults.standard
//
//        let defaultEventNameArray = defaults.stringArray(forKey: "eventnamearray") ?? [String]()
//        let defaultEventPeopleGoingArray = defaults.value(forKey: "eventpeoplegoingarray") as? [Int] ?? [Int]()
//        let defaultEventDateArray = defaults.stringArray(forKey: "eventdatearray") ?? [String]()
//        let defaultEventPriceArray = defaults.stringArray(forKey: "eventpricearray") ?? [String]()
//        let defaultEventDescripton = defaults.stringArray(forKey: "eventdescriptionarray") ?? [String]()
//        let defaultEventMaleCountFloatArray = defaults.value(forKey: "eventmalecountfloatarray") as? [Float] ?? [Float]()
//        let defaultEventMaleCountArray = defaults.value(forKey: "eventmalecountarray") as? [Int] ?? [Int]()
//        let defaultEventFemaleCountArray = defaults.value(forKey: "eventfemalecountarray") as? [Int] ?? [Int]()
//        let defaultEventStagArray = defaults.stringArray(forKey: "eventstagarray") ?? [String]()
//        //let defaultEventMaleCountFloatArray = defaults.
        
//        print("male count  float array  \(defaultEventMaleCountFloatArray)")
//
//        print("collection view index row number \(defaultEventPeopleGoingArray)")
        
        
        //imp code 3
//        var eventArrayindex = [Int]()
//
//
//        for indexCount in 0..<defaultEventNameArray.count{
//
//            eventArrayindex.append(indexCount)
//
//            print("print event array index \(eventArrayindex)")
//
//        }
//
//
//        for indexNum in 0..<defaultEventNameArray.count{
//
//
//
//            if eventIndexNumber == eventArrayindex[indexNum]{
//
           cell.eventNameLbl.text = "\(self.events[eventIndexNumber].eventName)"
        
           print("print event name at :\(eventIndexNumber) \(self.events[eventIndexNumber].eventName)")

           cell.eventDate.text = "\(self.events[eventIndexNumber].eventDate)"
        
           print("print event date at :\(eventIndexNumber) \(self.events[eventIndexNumber].eventDate)")

        
//                   // if self.events[eventIndexNumber].eventFeesStagEntry.count == 0{
//
//                        if self.events[eventIndexNumber].eventFeesCoupleEntry.count == 0{
//
//                            cell.entryFees.text = "Free"
//                            }else{
//
//
//                            print("couple Entry : \(self.events[eventIndexNumber].eventFeesCoupleEntry)")
//                                    cell.entryFees.text = "\(self.events[eventIndexNumber].eventFeesCoupleEntry)"
//                                }
//
//                        }else{
//
//                        print("Stag Entry : \(self.events[eventIndexNumber].eventFeesStagEntry)")
//                        if self.events[eventIndexNumber].eventFeesStagEntry == "NA"{
////                        if self.events[eventIndexNumber].eventFeesStagEntry.count == 0{
//                            cell.entryFees.textColor = UIColor.init(named: "maleColor")
//                            cell.entryFees.text = "Free"
//                            //cell.entryFees.text = "\(self.events[eventIndexNumber].eventFeesStagEntry)"
//                    //    }
        
//        }
        
        print("EntryCount : \(self.events[eventIndexNumber].eventFeesStagEntry)")
        if (self.events[eventIndexNumber].eventFeesStagEntry == "0" && self.events[eventIndexNumber].eventFeesCoupleEntry == "0"){
            
            cell.IndianRupee.isHidden = true
            cell.IndianRupeeWidth.constant = 0
            if #available(iOS 11.0, *) {
                cell.entryFees.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            cell.entryFees.text = "Free"
            
            let leftConstraint = cell.entryFees.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 15)
            self.costConstraints = [leftConstraint]
            NSLayoutConstraint.activate(self.costConstraints)
            
        }else if(self.events[eventIndexNumber].eventFeesStagEntry == "NA" && self.events[eventIndexNumber].eventFeesCoupleEntry == "NA"){
            
            cell.IndianRupee.isHidden = true
            cell.IndianRupeeWidth.constant = 0
            if #available(iOS 11.0, *) {
                cell.entryFees.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            cell.entryFees.text = "Free"
            
            let leftConstraint = cell.entryFees.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 15)
            self.costConstraints = [leftConstraint]
            NSLayoutConstraint.activate(self.costConstraints)
            
        }else if(self.events[eventIndexNumber].eventFeesStagEntry > "0" && self.events[eventIndexNumber].eventFeesCoupleEntry > "0"){
            
            cell.IndianRupee.isHidden = false
            cell.IndianRupeeWidth.constant = 15
            cell.entryFees.text = self.events[eventIndexNumber].eventFeesStagEntry
            
//            if (self.events[eventIndexNumber].eventFeesStagEntry > self.events[eventIndexNumber].eventFeesCoupleEntry){
//
//                cell.entryFees.text = self.events[eventIndexNumber].eventFeesStagEntry
//
//            }else{
//
//                cell.entryFees.text = self.events[eventIndexNumber].eventFeesCoupleEntry
//                
//            }
            
        }else if(self.events[eventIndexNumber].eventFeesStagEntry == "0" && self.events[eventIndexNumber].eventFeesCoupleEntry == "NA"){
            
            cell.IndianRupee.isHidden = true
            cell.IndianRupeeWidth.constant = 0
            if #available(iOS 11.0, *) {
                cell.entryFees.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            cell.entryFees.text = "Free"
            
             let leftConstraint = cell.entryFees.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 15)
            self.costConstraints = [leftConstraint]
            NSLayoutConstraint.activate(self.costConstraints)
            
        }
        else if(self.events[eventIndexNumber].eventFeesStagEntry == "NA" && self.events[eventIndexNumber].eventFeesCoupleEntry == "0"){
            
            cell.IndianRupee.isHidden = true
            cell.IndianRupeeWidth.constant = 0
            if #available(iOS 11.0, *) {
                cell.entryFees.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            cell.entryFees.text = "Free"
            
             let leftConstraint = cell.entryFees.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 15)
            self.costConstraints = [leftConstraint]
            NSLayoutConstraint.activate(self.costConstraints)
            
        }else if(self.events[eventIndexNumber].eventFeesStagEntry > "0" && self.events[eventIndexNumber].eventFeesCoupleEntry == "NA"){
            
            cell.IndianRupee.isHidden = false
            cell.IndianRupeeWidth.constant = 15
            cell.entryFees.text = self.events[eventIndexNumber].eventFeesStagEntry
            
        }else if(self.events[eventIndexNumber].eventFeesStagEntry == "NA" && self.events[eventIndexNumber].eventFeesCoupleEntry > "0"){
            
            cell.IndianRupee.isHidden = false
            cell.IndianRupeeWidth.constant = 15
            cell.entryFees.text = self.events[eventIndexNumber].eventFeesCoupleEntry
            
        }
        
        
            cell.txtView_EventDetailds.text = "\(self.events[eventIndexNumber].eventDescription)"
        
        
        
        if self.events[eventIndexNumber].eventPeopleGoingTotal == 0{
            
                                 cell.peopleGoingForEvent.isHidden = true
                                 cell.pplgoingforEventHeight.constant = 0
            
                                 cell.malePercent.isHidden = true
                                 cell.malePerHeight.constant = 0
            
                                 cell.femalePercent.isHidden = true
                                 cell.femalePerHeight.constant = 0
            
                                 cell.maleImgeView.isHidden = true
                                 cell.maleImageHeight.constant = 0
            
                                 cell.femaleImageView.isHidden = true
                                 cell.femaleImageHeight.constant = 0
            
                                 cell.progressBar.isHidden = true
                                 cell.progressBarHeight.constant = 0
            
            let topConstraint = cell.lbl_description.topAnchor.constraint(equalTo: cell.eventNameLbl.topAnchor, constant: 40)
            //                                            let bottomConstraint = lbl_facilities.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
//            let leftConstraint = cell.lbl_description.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15)
//            let rightConstraint = cell.lbl_description.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 199)
            self.descriptionConstraints = [topConstraint]
                //,leftConstraint,rightConstraint]
            NSLayoutConstraint.activate(self.descriptionConstraints)
            
            }else{
            
            if self.events[eventIndexNumber].eventPeopleGoingTotal > 1{
            
            cell.pplgoingforEventHeight.constant = 26
            cell.peopleGoingForEvent.isHidden = false
            
            cell.malePerHeight.constant = 21
            cell.maleImageHeight.constant = 19
            cell.malePercent.isHidden = false
            cell.maleImgeView.isHidden = false
            
            cell.femaleImageHeight.constant = 19
            cell.femalePerHeight.constant = 21
            cell.femalePercent.isHidden = false
            cell.femaleImageView.isHidden = false
            
            cell.progressBarHeight.constant = 10
            cell.progressBar.isHidden = false
            
//             let topConstraint = cell.lbl_description.topAnchor.constraint(equalTo: cell.eventNameLbl.topAnchor, constant: 35)
//            self.descriptionConstraints = [topConstraint]
                             cell.malePercent.text = "\(self.events[eventIndexNumber].eventFemaleCount)\("%")"
                             cell.femalePercent.text = "\(self.events[eventIndexNumber].eventMaleCount)\("%")"
                             cell.peopleGoingForEvent.text = "\(self.events[eventIndexNumber].eventPeopleGoingTotal) \("People are going")"
            
                             cell.progressBar.progress = Float(self.events[eventIndexNumber].eventFemaleCount)/100
            
            
            print("More than one ")
            print("float male count : \(Float(self.events[eventIndexNumber].eventMaleCount))")
            }else if self.events[eventIndexNumber].eventPeopleGoingTotal == 1{
                
                cell.pplgoingforEventHeight.constant = 26
                cell.peopleGoingForEvent.isHidden = false
                
                cell.malePerHeight.constant = 21
                cell.maleImageHeight.constant = 19
                cell.malePercent.isHidden = false
                cell.maleImgeView.isHidden = false
                
                cell.femaleImageHeight.constant = 19
                cell.femalePerHeight.constant = 21
                cell.femalePercent.isHidden = false
                cell.femaleImageView.isHidden = false
                
                cell.progressBarHeight.constant = 10
                cell.progressBar.isHidden = false
                
                //             let topConstraint = cell.lbl_description.topAnchor.constraint(equalTo: cell.eventNameLbl.topAnchor, constant: 35)
                //            self.descriptionConstraints = [topConstraint]
                cell.malePercent.text = "\(self.events[eventIndexNumber].eventFemaleCount)\("%")"
                cell.femalePercent.text = "\(self.events[eventIndexNumber].eventMaleCount)\("%")"
                cell.peopleGoingForEvent.text = "\(self.events[eventIndexNumber].eventPeopleGoingTotal) \("Person is going")"
                
                cell.progressBar.progress = Float(self.events[eventIndexNumber].eventFemaleCount)/100
                
                
                print("Only one ")
               // print("float male count : \(Float(self.events[eventIndexNumber].eventMaleCount))")
                
            }
        }
        
        if self.events[eventIndexNumber].eventUserGoing.count == 0{
            cell.CollectionWhosGoing.isHidden = true
            cell.whoseGoinCollHeight.constant = 0
            cell.whosGoingLabel.isHidden = true
            cell.whoseGoingLblHeight.constant = 0
            cell.btnViewMoreOutlet.isHidden = true
            cell.viewMoreBtnHeight.constant = 0
        }else{
            
            cell.CollectionWhosGoing.isHidden = false
            cell.whoseGoinCollHeight.constant = 80.5
            cell.whosGoingLabel.isHidden = false
            cell.whoseGoingLblHeight.constant = 16
            cell.btnViewMoreOutlet.isHidden = false
            cell.viewMoreBtnHeight.constant = 55
            
        }
        
        //if self.events[eventIndexNumber].
        
        
        
//
//
//
//                if defaultEventPeopleGoingArray[indexNum] == 0{
//
//                     cell.peopleGoingForEvent.isHidden = true
//                     cell.malePercent.isHidden = true
//                     cell.femalePercent.isHidden = true
//
//                    cell.maleImgeView.isHidden = true
//                    cell.femaleImageView.isHidden = true
//
//                    cell.progressBar.isHidden = true
//                }else{
//
//
//                    cell.malePercent.text = "\(defaultEventMaleCountArray[indexNum]) \("%")"
//                    cell.femalePercent.text = "\(defaultEventFemaleCountArray[indexNum]) \("%")"
//
//                    cell.peopleGoingForEvent.text = "\(defaultEventPeopleGoingArray[indexNum]) \("people are going")"
//
//                    cell.progressBar.progress = defaultEventMaleCountFloatArray[indexNum]
//
//                }
//
//
//                if defaultEventPriceArray[indexNum].count == 0{
//
//                    cell.entryFees.text = "NA"
//
//                }else{
//
//                    cell.entryFees.text = "\(defaultEventPriceArray[indexNum])"
//                }
//
//
//                print("print data array \(indexNum)..\(defaultEventNameArray[indexNum])")
//
//
//            }
//        }
//
//
//
        cell.btn_entryfeesdetailsOutlet.addTarget(self, action: #selector(viewStagDetails), for: .touchUpInside)
        cell.btn_showCouponsOutlet.addTarget(self, action: #selector(viewEventCoupon), for: .touchUpInside)
        cell.btnViewMoreOutlet.addTarget(self, action: #selector(viewMore), for: .touchUpInside)
        cell.btn_ImGoingOutlet.addTarget(self, action: #selector(imGoingButtonTapped), for: .touchUpInside)

        
        
        
        //        let bUserGoing = self.userGoing[indexPath.section]
//
//        print("b usergoing \(bUserGoing)")
//        cell.updateTableBCellWith(uGoing : bUserGoing)
//        updateCellWithD(uGoing: bUserGoing)
        return cell
    }
    
    
    @objc func imGoingButtonTapped(){
        
        if userPrefFlag == 0{
              print("user profile is not fill on im going button tapped")
            
            let alert = UIAlertController(title: "", message: "Please create your profile", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            
            present(alert, animated: true, completion: nil)
            
        }else{
            
            print("user profile is fill on im going button tapped")
            
            
            print("I am going button tapped ")
            let getUserData :UserDefaults = UserDefaults.standard
            let User_Id = getUserData.value(forKey: "userid")
            print("userid :\(String(describing: User_Id))!)")
            
            print("print event count fetchData function 5:\(self.events.count)")
            let urlString = "http://www.nitevibe.in/webservices/going.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&event_id=\(events[eventIndexNumber].eventId)"
            //event_id=\(selectedEventId)"
            
            
            Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
                if let value = response.data {
                    do {
                        let json = try JSON(data: value)
                        if let dictionnary = json.dictionaryObject {
                            if let messageArray = dictionnary["message"] as? String{
                                for arr in messageArray {
                                    
                                    print("message array \(arr)")
                                    
                                }
                            }
                            
                        }
                        
                    }catch {
                        print("cannot convert to Json")
                    }
                }
                
                
                
            }
            
            let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
            
            let SwipeVC =  storyBoard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
            SwipeVC.selectedGoingFlag = "1"
            print("clubID : \(selectedClubId)")
            print("eventID : \(selectedEventId)")
            SwipeVC.selectedClubIdFromSwipe = selectedClubId
            SwipeVC.selectedEventIdFromSwipe = events[eventIndexNumber].eventId
            
            self.present(SwipeVC, animated: true, completion: nil)
        }
       
    }
    @objc func viewMore(){
        
        print("view more button tapped")
        
        
                let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        
                let viewMoreVC = storyboard.instantiateViewController(withIdentifier: "ViewMoreViewController") as! ViewMoreViewController
                viewMoreVC.selectedVMEventId = events[eventIndexNumber].eventId
               // viewMoreVC.selectedViewMoreEventId = selectedEventId
                viewMoreVC.selectedVMClubId = selectedClubId
                self.present(viewMoreVC, animated: true, completion: nil)
    }
    func updateCellWithD(uGoing:UserGoingModel){
        
        self.dUserGoing = uGoing
        //var bUserGoing:UserGoingModel?
        //self.clCollectionView.reloadData()
        
    }
    
    @objc func viewEventCoupon(){
        
        
        print("Button tapped ")
        
        let storyboard = UIStoryboard(name:"UserProfileInfo", bundle: nil)
        let  eventDetailCouponVC = storyboard.instantiateViewController(withIdentifier:"EventCouponVC") as! EventCouponVC
        // self.present(eventDetailCouponVC, animated: true, completion: nil)
        eventDetailCouponVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(eventDetailCouponVC)
        self.view.addSubview(eventDetailCouponVC.view)
        
    }
    
    @objc func viewStagDetails(){
        
        print("view Stag details button tapped")
        
        print("Event Index \(eventIndexNumber)")
        
//        var passStagPriceToNextVC = String()
//        var passCouplePriceToNextVC = String()
//
//        let defaults = UserDefaults.standard
//
//        let defaultEventStagArray = defaults.stringArray(forKey: "eventstagarray") ?? [String]()
//        let defaultEventCouplePriceArray = defaults.stringArray(forKey: "eventcouplepricearray") ?? [String]()
//
//
//        print("default event stag array \(defaultEventStagArray)")
//        print("default event coupon price \(defaultEventCouplePriceArray)")
//
//        var indexArray = [Int]()
//
//        for i in 0..<defaultEventStagArray.count{
//
//            indexArray.append(i)
//        }
//
//        print("stag index array \(indexArray)")
//        //Output = [0,1] like
//        for indexNum in 0..<defaultEventStagArray.count{
//
//            ("print event index number \(eventIndexNumber)")
//
//            if eventIndexNumber == indexArray[indexNum]{
//
//                passStagPriceToNextVC = defaultEventStagArray[indexNum]
//                passCouplePriceToNextVC  =  defaultEventCouplePriceArray[indexNum]
//            }
//
//        }
//
//        print("pass stag to next vc \(passStagPriceToNextVC)")
        
        
        
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let entryDetailVC = storyboard.instantiateViewController(withIdentifier: "EntryDetailsViewController") as! EntryDetailsViewController
        print("stag : \(self.events[eventIndexNumber].eventFeesStagEntry)")
        print("couple : \(self.events[eventIndexNumber].eventFeesCoupleEntry)")
        entryDetailVC.stagPrice = "\(self.events[eventIndexNumber].eventFeesStagEntry)"
        entryDetailVC.couplePrice = "\(self.events[eventIndexNumber].eventFeesCoupleEntry)"
       // entryDetailVC.couplePrice = "\(self.events[eventI)"
        entryDetailVC.selectedEventId = selectedEventId
        entryDetailVC.selectedClubId = selectedClubId
        entryDetailVC.eventIndexNumberOnEntryDetail = eventIndexNumber
        print("entry price stag \(entryDetailVC.stagPrice)")
        
        
        self.present(entryDetailVC, animated: true, completion: nil)
        //navigationController?.pushViewController(entryDetailVC, animated: true)
        //present(entryDetailVC, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "eventcollectioncell", for: indexPath) as! EventDetailsCollectionViewCell
        
//        cell.tblheight?.constant = cell.eventDataTblViewOutlet.contentSize.height
        //self.viewWillLayoutSubviews()
        //self.facilityTblHeight?.constant = self.facilitiesTableView.contentSize.height
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 628
        return UITableViewAutomaticDimension
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = grandCollectionView.contentOffset
        visibleRect.size = grandCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = grandCollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        print("print index path : \(indexPath)")
    }
    
    
    
}
