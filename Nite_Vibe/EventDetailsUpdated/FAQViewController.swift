//
//  FAQViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 15/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import WebKit
class FAQViewController: UIViewController {

    
    @IBOutlet weak var backButtonTapped: UIButton!
    
    @IBOutlet weak var lbl_Text: UILabel!
    
    @IBOutlet weak var webKitOutlet: WKWebView!
    
    var selectedVCFlag = ""
    
    var selectedButtonFlag = ""
    var stringURL = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        //if select helpnow button
        if selectedButtonFlag == "0"{
        lbl_Text.text = "Help"
        stringURL = "http://www.nitevibe.in/webservices/test_helpnow.php?"
        
        }else if selectedButtonFlag == "1"{
        //if select terms and condition url
        lbl_Text.text = "Legal"
        stringURL = "http://www.nitevibe.in/webservices/test_termsconditions.php?"
        
        }
        
        
        let request = URLRequest(url:URL(string:stringURL)!)
        //let request  = URLRequest(url:url!)
        
        webKitOutlet.load(request)
        //webKitOutlet.addSubview(backButtonTapped)
        
        backButtonTapped.addTarget(self, action:#selector(backButtonTappedAction), for: .touchUpInside)
        
        
        // Do any additional setup after loading the view.
    }

    @objc func backButtonTappedAction(){
        
        print("print back button tapped ")
        
        // selected from event detail screen
        if selectedVCFlag == "0"{
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            
            let EventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
            
            present(EventDetailVC, animated: true, completion: nil)
            
        }else if selectedVCFlag == "1"{
            //if selected from setting screen
            let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
            
            let settingVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            
            present(settingVC, animated: true, completion: nil)
            
        }else if selectedVCFlag == "2"{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let fbloginVC = storyboard.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
            present(fbloginVC, animated: true, completion: nil)
            
        }
        
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
