//
//  UserGoingModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 12/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
struct UserGoingConstant{
    static let user_images_array = "user_images_array"
}

class UserGoingModel{
    
    let user_images:[String] //this will hold the data
    
    init(withInfo infoDict:[String:Any]){
        
        self.user_images = infoDict[UserGoingConstant.user_images_array] as! [String]
    }
    
}
