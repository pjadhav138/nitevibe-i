//
//  Who'sGoingCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 08/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class Who_sGoingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var whosgoingImageView: UIImageView!
    
    var cUserGoing:UserGoingModel?
    
    var cellUserGoingImageString:String?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
            whosgoingImageView.layer.borderWidth=2
            whosgoingImageView.layer.masksToBounds = false
           
            whosgoingImageView.layer.cornerRadius = whosgoingImageView.frame.height/2
            whosgoingImageView.clipsToBounds = true
    }
    

    
//    func updateCellWithUserGoing(image:String){
//
//        //self.cellSubDrinkName = name
//        //self.collectionViewDrinkButtonOutlet.setTitle(cellSubDrinkName, for: .normal)
//
//        cellUserGoingImageString = image
//        print("user going image string \(cellUserGoingImageString!)")
//
//
//        let imageURL = URL(string: cellUserGoingImageString!)
//
//        let imageData =  try! Data(contentsOf: imageURL! as URL)
//
//        print("print image data \(imageData)")
//
//        whosgoingImageView.image = UIImage(data: imageData)
//
//    }
    
//    func updateCollectionCCell(uGoing:UserGoingModel){
//        
//         self.cUserGoing = uGoing
//        
//        //let imageString = cUserGoing?.user_images
////       //cUserGoing?.user_images
//        //let imageURL = URL(string: imageString!)
//        //let imageData =  try! Data(contentsOf: imageURL! as URL)
////        
//        //print("print image data \(imageData)")
////        
////        
//        //cell.whosgoingImageView.image = eventImageArrayTest[indexPath.row]
//        
//        
//        //let aCategory = self.categories[section]
//        
//        //drinkLabelView.text = aCategory.drinkName
//        
//        
//        //cell.whosgoingImageView.image = UIImage(data:cUserGoing?.user_images)
//        
//        print("user going images in update c cell \(String(describing: cUserGoing?.user_images))")
//       // drinkLabelView.text = aCategory.drinkName
//        
//    }
}
