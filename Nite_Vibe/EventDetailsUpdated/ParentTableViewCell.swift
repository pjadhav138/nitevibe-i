//
//  ParentTableViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 08/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import CTShowcase

var EventDetailsRatioLaunch = ""
var EventDetailsGoingLaunch = ""

class ParentTableViewCell: UITableViewCell {
   

    @IBOutlet weak var IndianRupee: UIImageView!
    @IBOutlet weak var progressBarHeight: NSLayoutConstraint!
    @IBOutlet weak var femalePerHeight: NSLayoutConstraint!
    @IBOutlet weak var femaleImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var IndianRupeeWidth: NSLayoutConstraint!
    @IBOutlet weak var malePerHeight: NSLayoutConstraint!
    @IBOutlet weak var maleImageHeight: NSLayoutConstraint!
    @IBOutlet weak var CollectionWhosGoing: UICollectionView!
    @IBOutlet weak var whoseGoinCollHeight: NSLayoutConstraint!
    @IBOutlet weak var progressBar: UIProgressView!
   
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var entryFees: UILabel!
   
    @IBOutlet weak var tbl_ContentView: UIView!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var pplgoingforEventHeight: NSLayoutConstraint!
    @IBOutlet weak var peopleGoingForEvent: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var malePercent: UILabel!
    @IBOutlet weak var femalePercent: UILabel!
    //@IBOutlet weak var textAboutEvent: UILabel!
    @IBOutlet weak var txtView_EventDetailds: UITextView!
    @IBOutlet weak var lbl_Entry: UILabel!
    @IBOutlet weak var btn_entryfeesdetailsOutlet: UIButton!
    @IBOutlet weak var btn_showCouponsOutlet: UIButton!
    @IBOutlet weak var btn_ImGoingOutlet: UIButton!
   
    
    @IBOutlet weak var maleImgeView: UIImageView!
    
    
    @IBOutlet weak var femaleImageView: UIImageView!
    @IBOutlet weak var btnViewMoreOutlet: UIButton!
    
    @IBOutlet weak var viewMoreBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var whoseGoingLblHeight: NSLayoutConstraint!
    @IBOutlet weak var whosGoingLabel: UILabel!
    var bUserGoing:UserGoingModel?//define an optional
    var textView_Constraints:[NSLayoutConstraint] = []
    var lblEntry_Constraints:[NSLayoutConstraint] = []
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var isIPhoneXR = false
    var isIPHONEX = false
    var isIPhoneXSMax = false
    
    var OverlayRatio = "Ratio First Time Launch"
    var OverlayGoing = "Going First Time Launch"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        selectionStyle = UITableViewCellSelectionStyle.none
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        txtView_EventDetailds.sizeToFit()
        //txtView_EventDetailds.translatesAutoresizingMaskIntoConstraints = false
        
        btn_ImGoingOutlet.layer.masksToBounds = true
        btn_ImGoingOutlet.layer.cornerRadius = 8
        eventDate.layer.masksToBounds = true
        eventDate.layer.cornerRadius = 4
        progressBar.layer.masksToBounds = true
        progressBar.layer.cornerRadius = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
        self.ratioOverlay()
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            self.goingOverlay()
        })
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("IPHONE 5,5S,5C")
            case 1334:
                print("IPHONE 6,7,8 IPHONE 6S,7S,8S ")
            case 1920, 2208:
                print("IPHONE 6PLUS, 6SPLUS, 7PLUS, 8PLUS")
            case 2436:
                print("IPHONE X, IPHONE XS")
                isIPHONEX = true
            case 2688:
                print("IPHONE XS_MAX")
                isIPhoneXSMax = true
            case 1792:
                print("IPHONE XR")
                isIPhoneXR = true
            default:
                print("UNDETERMINED")
            }
        }
   
       // let textViewHeight = txtView_EventDetailds.heightAnchor.constraint(equalToConstant: 60)
        //txtView_EventDetailds =
//        let textViewWidth = txtView_EventDetailds.widthAnchor.constraint(equalToConstant: screenWidth)
//        let topConstraint = txtView_EventDetailds.topAnchor.constraint(equalTo: self.lbl_description.topAnchor, constant: 15)
//        let topEntryConstraint = txtView_EventDetailds.topAnchor.constraint(equalTo: self.txtView_EventDetailds.topAnchor, constant: 15)
//        let bottomConstraint = txtView_EventDetailds.bottomAnchor.constraint(equalTo: self.lbl_Entry.bottomAnchor, constant: -20)
//        let leftConstraint = txtView_EventDetailds.leadingAnchor.constraint(equalTo: self.tbl_ContentView.leadingAnchor, constant: 10)
//        let rightConstraint = txtView_EventDetailds.trailingAnchor.constraint(equalTo: self.tbl_ContentView.trailingAnchor, constant: 10)
//        textView_Constraints = [textViewWidth,topConstraint,bottomConstraint,leftConstraint,rightConstraint]
//        lblEntry_Constraints = [topEntryConstraint]
//        NSLayoutConstraint.activate(textView_Constraints)
//        NSLayoutConstraint.activate(lblEntry_Constraints)
        //self.eventName.text! = "1234"
        // Initialization code
    }
    
    func ratioOverlay()
    {
        
        let ratioLaunchedBefore = UserDefaults.standard.bool(forKey: "ratioLaunchedBefore")
       
        if ratioLaunchedBefore {
            
            EventDetailsRatioLaunch = "Ratio Has Launched Before"
            print("RatioBeforeLaunchStatus : \(EventDetailsRatioLaunch)")
            
        }
        else{
            
            EventDetailsRatioLaunch = "Ratio First Time Launch"
            print("RatioFirstLaunchStatus : \(EventDetailsRatioLaunch)")
            self.RatioOverLay()
            UserDefaults.standard.set(true, forKey: "ratioLaunchedBefore")
            
        }

    }
    
    func RatioOverLay(){
        
        print("Print EventDetailsRatio Launch Status : \(OverlayRatio)")
        print("Print UserDefaultsRatio Launch Status : \(EventDetailsRatioLaunch)")
        
        if OverlayRatio == EventDetailsRatioLaunch{
            
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "ratio2-04.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.white
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            if isIPhoneXR || isIPhoneXSMax || isIPHONEX{
                showcase.setup(for: self.progressBar, offset: CGPoint(x: 5, y: 5), margin: 0)
                showcase.show()
            }
            else {
                showcase.setup(for: self.progressBar, offset: CGPoint.zero, margin: 0)
                showcase.show()
            }
        }
        else{
            
        }
        
    }
    
    func goingOverlay(){
        
        let goingLaunchedBefore = UserDefaults.standard.bool(forKey: "goingLaunchedBefore")
        
        if goingLaunchedBefore {
            
            EventDetailsGoingLaunch = "Going Has Launched Before"
            print("GoingBeforeLaunchStatus : \(EventDetailsGoingLaunch)")
            
        }
        else{
            
            EventDetailsGoingLaunch = "Going First Time Launch"
            print("GoingFirstLaunchStatus : \(EventDetailsGoingLaunch)")
            self.GoingOverlay()
            UserDefaults.standard.set(true, forKey: "goingLaunchedBefore")
            
        }
        
    }
    
    func GoingOverlay(){
        
        print("Print EventDetailsGoing Launch Status : \(OverlayGoing)")
        print("Print UserDefaultsGoing Launch Status : \(EventDetailsGoingLaunch)")
        
        if OverlayGoing == EventDetailsGoingLaunch{
            
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "Going1.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.white
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            showcase.setup(for: self.btn_ImGoingOutlet, offset: CGPoint.zero, margin: 0)
            showcase.show()
        }
        
    }
    
    

    func updateTableBCellWith(uGoing:UserGoingModel){
        
        self.bUserGoing = uGoing
        
        
    }

}

extension ParentTableViewCell{
    func setWhosGoingCollectionViewDataSourceDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row:Int)
    {
        CollectionWhosGoing.delegate = dataSourceDelegate
        CollectionWhosGoing.dataSource = dataSourceDelegate
        
        CollectionWhosGoing.reloadData()
    }
}
