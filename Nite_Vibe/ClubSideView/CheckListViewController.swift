//
//  CheckListViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 06/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
class CheckListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var array1 = ["1","2","3"]
    var array2 = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.reloadData()
        getClubHomeWithCouponData()
        // Do any additional setup after loading the view.
    }

    func getClubWithoutCouponData(){
        
        
        let getUserData:UserDefaults = UserDefaults.standard
        
        let userId = getUserData.value(forKey: "userid")
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let finalDate = formatter.string(from:date as Date)
        
        print("print date\(finalDate)")
        //let urlString = "http://www.nitevibe.in/webservices/club_side_home.php?user_id=\(String(describing: userId!))&\(String(describing: finalDate))"
        
        
        let clubSideData:UserDefaults = UserDefaults.standard
        
        //clubSideData.set(self.clubSideClubIdString, forKey: "clubsideclubid")
        
        
        let clubId =  clubSideData.value(forKey: "clubsideclubid")
        let urlString = "http://www.nitevibe.in/webservices/club_side_withoutcoupon.php?club_id=\(clubId!)"
        print("url string club side :\(urlString)")
        
        do{
            
            Alamofire.request(urlString, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print("club dict Data \(dictData)")
                
                self.array1.removeAll()
                
                for data in dictData{
                    
                   let clubNameString = data["name"] as! String
                   print("club name string: \(clubNameString)")
                   
                   self.array1.append(clubNameString)
                    
                    
                    DispatchQueue.main.async {
                        
                        self.tableView.reloadData()
                        
                    }
   
                }
                
            }
            
        }catch{
            
            
            print("Error")
        }
        
        
    }
    
    
    
    
    
    func getClubHomeWithCouponData(){
        
        
        let getUserData:UserDefaults = UserDefaults.standard
        
        let userId = getUserData.value(forKey: "userid")
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let finalDate = formatter.string(from:date as Date)
        
        print("print date\(finalDate)")
        
        let clubSideData:UserDefaults = UserDefaults.standard
        
        let clubId =  clubSideData.value(forKey: "clubsideclubid")
        let urlString = "http://www.nitevibe.in/webservices/club_side_withcoupon.php?club_id=\(clubId!)"
        print("url string club side :\(urlString)")
        
        do{
            
            Alamofire.request(urlString, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print("club dict Data \(dictData)")
                
                self.array1.removeAll()
                
                for data in dictData{
                    
                    let clubNameString = data["name"] as! String
                    print("club name string: \(clubNameString)")
                    
                    self.array1.append(clubNameString)
                    
                    
                    DispatchQueue.main.async {
                        
                        self.tableView.reloadData()
                        
                    }
                    
                }
                
                
                
            }
            
        }catch{
            
            
            print("Error")
        }
        
    
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let picVC = storyboard.instantiateViewController(withIdentifier:"PieChartViewController" ) as! PieChartViewController
        
        present(picVC, animated: true, completion: nil)
    }
    @IBAction func couponEntryButtonTapped(_ sender: Any) {
        
        self.getClubHomeWithCouponData()
    }
    
    @IBAction func withoutCouponEntryButtonTapped(_ sender: Any) {
        
        self.getClubWithoutCouponData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListTableViewCell") as! CheckListTableViewCell
        
        cell.userNameLbl.text = array1[indexPath.row]
        
        return cell
    }

}
