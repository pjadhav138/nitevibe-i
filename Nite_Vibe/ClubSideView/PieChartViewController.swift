//
//  PieChartViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 05/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Charts
import Alamofire
class PieChartViewController: UIViewController {
    let pieChart: PieChartView = {
        let p = PieChartView()
        p.translatesAutoresizingMaskIntoConstraints = false
        p.noDataText = ""
        p.legend.enabled = false
        p.chartDescription?.text = ""
        p.drawHoleEnabled = false
        return p
    }()
    
    
    
    @IBOutlet weak var clubNameLabel: UILabel!
    @IBOutlet weak var clubDateLabel: UILabel!
    @IBOutlet weak var femaleCountLabel: UILabel!
    @IBOutlet weak var maleCountLabel: UILabel!
    @IBOutlet weak var totalCountLabel: UILabel!
    
    
    var userCountDictionary = [String: Int]()
    var clubNameString = String()
    var clubID = String()
    var maleCountInt = Int()
    var femaleCountInt = Int()
    var currentDateString = String()
    var totalCountInt = Int()
    var clubSideClubIdString = String()
    
    var rightButtonTappedFlag = 0
    
    @IBOutlet weak var rightSidePopOverView: UIView!
    
    @IBOutlet weak var checkListButtonOutlet: UIButton!
    @IBOutlet weak var homeButtonOutlet: UIButton!
    @IBOutlet weak var logoutButtonOutlet: UIButton!
    
    
    
    
    
    var path: UIBezierPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rightSidePopOverView.isHidden = true
        view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.clubNameLabel.sizeToFit()
        self.clubNameLabel.numberOfLines = 0
        getClubHomeData()
        setupPieChart()
        // fillChart()
        
        
        self.view.addSubview(rightSidePopOverView)
        rightSidePopOverView.addSubview(homeButtonOutlet)
        rightSidePopOverView.addSubview(checkListButtonOutlet)
        rightSidePopOverView.addSubview(logoutButtonOutlet)
        homeButtonOutlet.addTarget(self, action: #selector(homeButtonAction), for: .touchUpInside)
        checkListButtonOutlet.addTarget(self, action: #selector(checkListAction), for: .touchUpInside)
        logoutButtonOutlet.addTarget(self, action: #selector(logOutAction), for: .touchUpInside)
        
        homeButtonOutlet.contentHorizontalAlignment = .left
        checkListButtonOutlet.contentHorizontalAlignment = .left
        logoutButtonOutlet.contentHorizontalAlignment = .left
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // getClubHomeData()
        let width: CGFloat = 240.0
        let height: CGFloat = 160.0
        
        let arcButtonView = ArcButtonView(frame: CGRect(x: self.view.frame.size.width/2 - width/2,
                                                        y: self.view.frame.size.height - height/2,
                                                        width: width,
                                                        height: height))
        
        
        print("X:\(self.view.frame.size.width)")
        print("Y:\(self.view.frame.size.height)")
        print("Width \(width)")
        print("Height \(height)")
        self.view.addSubview(arcButtonView)
        
        
        let addGuestButtonImage = UIImage(named: "Add photo.png")
        
        //arcButtonView.setBackgroundImage(addGuestButtonImage, for: .normal)
        
        arcButtonView.setImage(addGuestButtonImage, for: .normal)
        arcButtonView.backgroundColor = UIColor.clear
        
        arcButtonView.addTarget(self, action: #selector(buttonTappedAction), for: .touchUpInside)
        
        
    }
    
    
    @objc func buttonTappedAction(){
        
        print("Print Tapped")
        
        let userProfileStoryboard:UIStoryboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let addGuestVC = userProfileStoryboard.instantiateViewController(withIdentifier: "AddGuestViewController") as! AddGuestViewController
        
        present(addGuestVC, animated: false, completion: nil)
    }
    
    
    func getClubHomeData(){
        
        
        let getUserData:UserDefaults = UserDefaults.standard
        
        let userId = getUserData.value(forKey: "userid")
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let finalDate = formatter.string(from:date as Date)
        
        print("print date\(finalDate)")
        //let urlString = "http://www.nitevibe.in/webservices/club_side_home.php?user_id=\(String(describing: userId!))&\(String(describing: finalDate))"
        let urlString = "http://www.nitevibe.in/webservices/club_side_home.php?user_id=\(userId!)"
        
        print("url string club side :\(urlString)")
        
        do{
            
            Alamofire.request(urlString, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print("club dict Data \(dictData)")
                
                
                for data in dictData{
                    
                    var clubid = data["club_id"] as! String
                    self.clubNameString = data["club_name"] as! String
                    self.currentDateString = data["date"] as! String
                    self.maleCountInt = data["male_count"] as! Int
                    self.femaleCountInt = data["female_count"] as! Int
                    
                    self.clubSideClubIdString = data["club_id"] as! String
                    
                    print("club id : \(clubid)")
                    print("club name string: \(self.clubNameString)")
                    print("current date string: \(self.currentDateString)")
                    print("male count int: \(self.self.maleCountInt)")
                    print("female count int: \(self.femaleCountInt)")
                    print("club side club id \(self.clubSideClubIdString)")
                    
                    let clubSideData:UserDefaults = UserDefaults.standard
                    
                    clubSideData.set(self.clubSideClubIdString, forKey: "clubsideclubid")
                    
                    
                    //let todaysEventDict = data["today_event_arr"] as? [Dictionary <String,AnyObject>]
                    let todaysEventDict =  data["today_event_arr"] as? [Dictionary <String,AnyObject>]
                    print("club Events :\(String(describing: todaysEventDict))")
                    
                    
                    print("todays event array :\(String(describing: todaysEventDict))")
                    print("todays event array count :\(String(describing: todaysEventDict?.count))")
                    let todayseventcount = todaysEventDict?.count
                    print("eventCount : \(String(describing: todayseventcount))")
                    
                    let todaysEventCount = UserDefaults.standard
                    todaysEventCount.set(todayseventcount, forKey: "Eventcount")
                    todaysEventCount.synchronize()
                    let TEventCount = todaysEventCount.value(forKey:"Eventcount")
                    print("Event Values : \(TEventCount!)")
                    
                    if todaysEventDict!.count == 0{
                        
                        let leftCouponCountSring = "0"
                        let leftCoupon = UserDefaults.standard
                        leftCoupon.set(leftCouponCountSring, forKey: "leftcouponcount")
                        leftCoupon.synchronize()
                        let leftCouponCount = leftCoupon.value(forKey:"leftcouponcount")
                        print("leftcoupon when no event : \(leftCouponCount!)")
                        
                    }else{
                        
                        for eventDict in todaysEventDict!{
                            
                            
                            let leftCouponCountSring = eventDict["left_coupon"]
                            let leftCoupon = UserDefaults.standard
                            leftCoupon.set(leftCouponCountSring, forKey: "leftcouponcount")
                            leftCoupon.synchronize()
                            let leftCouponCount = leftCoupon.value(forKey:"leftcouponcount")
                            print("leftcoupon when there is event : \(leftCouponCount!)")
                            
                        }
                    }
                    self.totalCountInt = self.maleCountInt + self.femaleCountInt
                    
                    print("total count: \(self.totalCountInt)")
                    self.clubID = clubid
                    self.clubNameLabel.text =  self.clubNameString
                    self.clubDateLabel.text = self.currentDateString
                    //mistakenly took female label instead of male & vice-versa
                    self.femaleCountLabel.text = "\(self.maleCountInt)"
                    self.maleCountLabel.text = "\(self.femaleCountInt)"
                    self.totalCountLabel.text = "\(self.totalCountInt)"
                    let setClubUserCount:UserDefaults = UserDefaults.standard
                    setClubUserCount.set(self.maleCountInt, forKey: "malecount")
                    setClubUserCount.set(self.femaleCountInt, forKey: "femalecount")
                    setClubUserCount.synchronize()
                    print("male count user default \(setClubUserCount.value(forKey:"malecount")!)")
                    print("female count user default \(setClubUserCount.value(forKey: "femalecount")!)")
                    
                    
                    
                }
                
                self.fillChart()
                
            }
            
        }catch{
            
            
            print("Error")
        }
        
        
    }
    
    
    
    @IBAction func MenuButtonTapped(_ sender: Any) {
        
        if rightButtonTappedFlag == 0{
            
            rightSidePopOverView.isHidden = false
            
            rightButtonTappedFlag = 1
        }
        else if rightButtonTappedFlag == 1{
            
            rightSidePopOverView.isHidden = true
            
            rightButtonTappedFlag = 0
            
        }
        
    }
    
    @objc func homeButtonAction(){
        
        let setClubSideHomeFlag:UserDefaults = UserDefaults.standard
        
        setClubSideHomeFlag.set("1", forKey: "clubsidehomeflag")
        print("home button tapped")
        
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Swipe", bundle: nil)
        
        let clubDetailVC = mainStoryBoard.instantiateViewController(withIdentifier: "ClubSideHomeViewController") as! ClubSideHomeViewController
        print("CID : \(self.clubID)")
        
        clubDetailVC.selectedClubId = self.clubID
        
        self.present(clubDetailVC, animated: false, completion: nil)
        rightSidePopOverView.isHidden = true
    }
    
    @objc func checkListAction(){
        print("check list button tapped")
        
        
        let userProfileInfoStoryboard:UIStoryboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let checkListVC = userProfileInfoStoryboard.instantiateViewController(withIdentifier:"CheckListViewController") as! CheckListViewController
        
        self.present(checkListVC, animated: false, completion: nil)
    }
    
    @objc func logOutAction(){
        print("logout button tapped")
        
        
        
        let defaults = UserDefaults.standard
        
        let loginFlag = defaults.value(forKey: "loginflag") as? String
        // defaults.removeObject(forKey: "ovelayflag")
        // defaults.set("0", forKey: "ovelayflag")
        
        if loginFlag == "1"{
            
            
            defaults.removeObject(forKey: "loginflag")
            defaults.removeObject(forKey: "subadmin")
            defaults.set("0", forKey: "loginflag")
            //defaults.set("0", forKey: "ovelayflag")
            let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
            
            self.present(loginVC, animated: false, completion: nil)
            
        }
        
        
        
    }
    
    
    func addGuestButtonAction(){
        print("Guest button tapped")
    }
    func setupPieChart() {
        view.addSubview(pieChart)
        //        pieChart.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        //        pieChart.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -130).isActive = true
        //        pieChart.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        //        pieChart.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        pieChart.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        pieChart.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -60).isActive = true
        pieChart.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        pieChart.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
    }
    
    
    
    func fillChart() {
        
        var dataEntries = [PieChartDataEntry]()
        var maleCount = Int()
        var femaleCount = Int()
        let getDictionary:UserDefaults = UserDefaults.standard
        maleCount = getDictionary.value(forKey:"malecount") as! Int
        femaleCount = getDictionary.value(forKey: "femalecount") as! Int
        print("male count \(maleCount)")
        print("female count \(femaleCount)")
        userCountDictionary = ["MALE":maleCount, "FEMALE":femaleCount]
        print("UCOUNTDICT : \(userCountDictionary)")
        for (key, val) in userCountDictionary {
            let percent = Double(val) / 100.0
            let entry = PieChartDataEntry(value: percent, label: key)
            dataEntries.append(entry)
        }
        
        // userCountDictionary = ["MALE":maleCount, "FEMALE":femaleCount]
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "")
        var  color: NSUIColor?
        if #available(iOS 11.0, *) {
            let color = [UIColor(named: "maleColor"), UIColor(named: "femaleColor")]
        } else {
            // Fallback on earlier versions
        };if #available(iOS 11.0, *) {
            let color = [UIColor(named: "maleColor"), UIColor(named: "femaleColor")]
        } else {
            // Fallback on earlier versions
        }
        chartDataSet.colors = color as! [NSUIColor]
        //chartDataSet.colors =
        chartDataSet.sliceSpace = 2
        chartDataSet.selectionShift = 5
        
        let chartData = PieChartData(dataSet: chartDataSet)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0
        chartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        
        pieChart.data = chartData
        
    }
    
}





extension ViewController: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let index = highlight.y
        print("Selected \(index)")
    }
}

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat(Double.pi) / 180.0
    }
    
}
