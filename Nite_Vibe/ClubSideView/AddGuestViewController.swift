//
//  AddGuestViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 06/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire

class AddGuestViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var addGuest_Logout: UIButton!
    
    @IBOutlet weak var maleSlider: UISlider!
    
    
    @IBOutlet weak var maleCountLabel: UILabel!
    
    
    @IBOutlet weak var femaleCountLabel: UILabel!
    
    @IBOutlet weak var femaleSlider: UISlider!
    
    @IBOutlet weak var emailMaleTextField: CustomTextField!
    
    @IBOutlet weak var nameTextField: CustomTextField!
    
    
    @IBOutlet weak var emailTextField: CustomTextField!
    
    @IBOutlet weak var selectedGenderOutlet: UISegmentedControl!
    
    @IBOutlet weak var couponRemainLbl: UILabel!
    
    @IBOutlet weak var enterPriceTextField: CustomTextField!
    var selectedMaleCounterValue = Int()
    var selectedFemaleCounterValue = Int()
    var selectedGender = String()
    var selectedCouponType = String()
    var selectedCouponFlag = String()
    var coupon = String()
    
    var isSelected = Int()
    
    @IBOutlet weak var withCouponButtontapped: UIButton!
    
    
    @IBOutlet weak var withoutCouponButtonTapped:  UIButton!
    
    
    @IBOutlet weak var couponTypeView: UIView!
    
    
    @IBOutlet weak var selectedCouponSegmentOutlet: UISegmentedControl!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGuest_Logout.layer.masksToBounds = true
        addGuest_Logout.layer.cornerRadius = 8
        emailTextField.delegate = self
        emailMaleTextField.delegate = self
        nameTextField.delegate = self
        enterPriceTextField.delegate = self
        
        couponTypeView.isHidden = true
        
        isSelected = 0
        withCouponButtontapped.backgroundColor = .blue
        withoutCouponButtonTapped.backgroundColor = .blue
        selectedCouponSegmentOutlet.selectedSegmentIndex =  UISegmentedControlNoSegment
        
        selectedGenderOutlet.selectedSegmentIndex = UISegmentedControlNoSegment
        getCouponData()
//        let defaults = UserDefaults.standard
//
//        let leftCouponCount = defaults.value(forKey:"leftcouponcount")
//
//        self.couponRemainLbl.text = "Coupons Remaining :\(String(describing: leftCouponCount!))"
    }
    
    
    @IBAction func maleSliderAction(_ sender: UISlider) {
        
        self.maleCountLabel.text = String(Int(sender.value))
        
        selectedMaleCounterValue = Int(sender.value)
        
    }
    
    
    @IBAction func femaleSliderAction(_ sender: UISlider) {
        
        self.femaleCountLabel.text = String(Int(sender.value))
        
        selectedFemaleCounterValue = Int(sender.value)
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let userprofileStoryBoard:UIStoryboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let piaChartVC = userprofileStoryBoard.instantiateViewController(withIdentifier: "PieChartViewController") as! PieChartViewController
        
        self.present(piaChartVC, animated: false, completion: nil)
        
    }
    
    
    
    
    @IBAction func selectedGenderSegment(_ sender: Any) {
        
        let getIndex = selectedGenderOutlet.selectedSegmentIndex
        print("Selected gender Index \(getIndex)")
        switch (getIndex) {
        case 0:
            
            selectedGender = "Male"
            print("Selected Male")
        case 1:
            selectedGender = "Female"
            print("Selected Female")
            
        default:
            selectedGender = ""
            print("No Select")
            
        }
        
    }
    
    @IBAction func withCouponTappedAction(_ sender: Any) {
        
        let selectedImage = UIImage(named: "checkbox.selected")
        let unSelectedImage = UIImage(named: "checkbox.unselected")
        
        
        
        //        if let isSelected = UIImage(named: "checkbox.unselected") {
        //            self.withCouponButtontapped.setImage(isSelected, for: .normal)
        //        }
        if isSelected == 0{
            selectedCouponFlag = "1"
            self.withCouponButtontapped.setImage(unSelectedImage, for: .normal)
            
            couponTypeView.isHidden = false
            
            isSelected = 1
        }else{
            
            selectedCouponFlag = "1"
            withCouponButtontapped.backgroundColor = .blue
            couponTypeView.isHidden = true
            isSelected = 0
            
        }
    }
    
    @IBAction func withoutCouponTappedAction(_ sender: Any) {
        couponTypeView.isHidden = true
        //        let unSelectedImage = UIImage(named: "checkbox.unselected")
        //        self.withCouponButtontapped.setImage(unSelectedImage, for: .normal)
        //        selectedCouponFlag = "0"
        let unSelectedImage = UIImage(named: "checkbox.unselected")
        
        if isSelected == 0{
            selectedCouponFlag = "0"
            self.withCouponButtontapped.setImage(unSelectedImage, for: .normal)
            
            isSelected = 1
        }else{
            selectedCouponFlag = "0"
            withCouponButtontapped.backgroundColor = .blue
            
            isSelected = 0
            
        }
    }
    
    
    @IBAction func couponTypeSegment(_ sender: Any) {
        
        
        let getIndex = selectedCouponSegmentOutlet.selectedSegmentIndex
        print("Selected coupon Index \(getIndex)")
        switch (getIndex) {
        case 0:
            
            selectedCouponType = "Stag"
            print("Selected Stag")
            self.coupon = "1"
        case 1:
            selectedCouponType = "Couple"
            print("Selected Couple")
            self.coupon = "2"
        default:
            selectedCouponType = ""
            print("No Select")
            
        }
        
        
    }
    
    
    @IBAction func addGuestButtonTapped(_ sender: Any) {
        
        let getEventCount :UserDefaults = UserDefaults.standard
        let eventCount:Int = getEventCount.value(forKey: "Eventcount") as! Int
        print("EVENTCOUNT :\(eventCount)")
        
        if eventCount == 0{
            
            let alert = UIAlertController(title: "", message: "Event is Inactive", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            
            print("selected male counter :\(selectedMaleCounterValue)")
            print("selected female counter : \(selectedFemaleCounterValue)")
            print("selected gender \(selectedGender)")
            print("selected couple \(selectedCouponType)")
            
            print("user name text field \(self.emailMaleTextField.text!)")
            print("user name textfield \(self.emailTextField.text!)")
            print("user enter price textfield \(self.enterPriceTextField.text!)")
            
            if self.emailMaleTextField.text == "" && self.selectedGender == ""{
                
                print("Please fill all fields")
                
                let alert = UIAlertController(title: "", message: "Please fill all fields", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }else if self.emailMaleTextField.text == ""{
                
                print("Please enter Username")
                
                let alert = UIAlertController(title: "", message: "Please enter Username", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }else if self.selectedGender == ""{
                
                print("Please select Gender")
                
                let alert = UIAlertController(title: "", message: "Please select Gender", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }else{
                update()
                
                print("Entered Successfully")
            }
        }
        
    }
    
    func getCouponData(){
        
        
        let getUserData:UserDefaults = UserDefaults.standard
        
        let userId = getUserData.value(forKey: "userid")
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let finalDate = formatter.string(from:date as Date)
        
        print("print date\(finalDate)")
        //let urlString = "http://www.nitevibe.in/webservices/club_side_home.php?user_id=\(String(describing: userId!))&\(String(describing: finalDate))"
        let urlString = "http://www.nitevibe.in/webservices/club_side_home.php?user_id=\(userId!)"
        
        print("url string club side :\(urlString)")
        
        do{
            
            Alamofire.request(urlString, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print("club dict Data \(dictData)")
                
                
                for data in dictData{
                    
                    let todaysEventDict =  data["today_event_arr"] as? [Dictionary <String,AnyObject>]
                    print("club Events :\(String(describing: todaysEventDict))")
                    
                    
                    print("todays event array :\(String(describing: todaysEventDict))")
                    print("todays event array count :\(String(describing: todaysEventDict?.count))")
                    let todayseventcount = todaysEventDict?.count
                    print("eventCount : \(String(describing: todayseventcount))")
                    
                    if todaysEventDict!.count == 0{
                        
                        let leftCouponCountSring = "0"
                        let leftCoupon = UserDefaults.standard
                        leftCoupon.set(leftCouponCountSring, forKey: "leftcouponcount")
                        leftCoupon.synchronize()
                        let leftCouponCount = leftCoupon.value(forKey:"leftcouponcount")
                        print("leftcoupon when no event : \(leftCouponCount!)")
                        self.couponRemainLbl.text = "Coupons Remaining : \(String(describing: leftCouponCount!))"
                        
                    }else{
                        
                        for eventDict in todaysEventDict!{
                            
                            
                            let leftCouponCountSring = eventDict["left_coupon"]
                            let leftCoupon = UserDefaults.standard
                            leftCoupon.set(leftCouponCountSring, forKey: "leftcouponcount")
                            leftCoupon.synchronize()
                            let leftCouponCount = leftCoupon.value(forKey:"leftcouponcount")
                            print("leftcoupon when there is event : \(leftCouponCount!)")
                            self.couponRemainLbl.text = "Coupons Remaining :\(String(describing: leftCouponCount!))"
                            
                        }
                    }
                    
                }
                
            }
            
        }catch{
            
            
            print("Error")
        }
        
        
    }
    
    func update(){
        
        
        
        URLCache.shared.removeAllCachedResponses()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id!))")
        
        let clubSideData:UserDefaults = UserDefaults.standard
        let clubSideID =  clubSideData.value(forKey:"clubsideclubid")
        
        var respo = Int()
        
        let URL_String = "http://www.nitevibe.in/webservices/test_club_entry.php?mobile=\(self.emailMaleTextField.text!)&name=\(self.nameTextField.text!)&email=\(self.emailTextField.text!)&gender=\(selectedGender)&male=\(selectedMaleCounterValue)&female=\(selectedFemaleCounterValue)&price=\(self.enterPriceTextField.text!)&club_id=\(clubSideID!)&coupon=\(coupon)&type=\(selectedCouponType)"
        
        print("test club entry URL String\(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 201){
                    
                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 202){
                    
                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 200){
                    
                    print("Save data")
                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    self.getCouponData()
                    
                }
                
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
        
    }
    
    @IBAction func onlyEmailTFEditingChanged(_ sender: CustomTextField) {
        print("TextFieldDidChanged:\(String(describing: emailTextField.text))")
        let dataType = checkOnlyEmailDataType(text: emailTextField.text!)
        
        print("DataType = \(dataType)")
        
        if dataType == DataType.Email{
            print("true")
            
        }
        else if dataType == DataType.Number{
            
            print("true")
            
            
        }else if dataType == DataType.Other{
            print("false")
            
            
        }
    }
    @IBAction func textfieldEditingChanged(_ sender: CustomTextField) {
        
        print("TextFieldDidChanged:\(String(describing: emailMaleTextField.text))")
        let dataType = checkDataType(text: emailMaleTextField.text!)
        
        print("DataType = \(dataType)")
        
        if dataType == DataType.Email{
            print("true")
            
        }
        else if dataType == DataType.Number{
            
            print("true")
            
            
        }else if dataType == DataType.Other{
            print("false")
            
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func isValidEmail(email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: emailMaleTextField.text!)
        return result
    }
    
    func isValidPhone(phone: String) -> Bool {
        
        let PHONE_REGEX = "^[7-9][0-9]{9}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: emailMaleTextField.text!)
        return result
        
    }
    
    func isValidOnlyEmail(email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: emailTextField.text!)
        return result
    }
    
    func checkDataType(text:String)->DataType{
        
        //if let intVal = Int(text){
        if isValidPhone(phone: text){
            
            return DataType.Number
        }else if isValidEmail(email: text){
            return DataType.Email
            
        }else{
            
            return DataType.Other
        }
    }
    
    func checkOnlyEmailDataType(text:String)->DataType{
        
        //if let intVal = Int(text){
        if isValidEmail(email: text){
            
            return DataType.Email
            
        }else{
            
            return DataType.Other
        }
    }
    
}
