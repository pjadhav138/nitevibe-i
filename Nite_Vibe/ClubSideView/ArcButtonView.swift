//
//  ArcButtonView.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 06/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ArcButtonView: UIButton {

   
    var path: UIBezierPath!
    
    
    override func draw(_ rect: CGRect) {
        
        path = UIBezierPath(arcCenter: CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2),
                            radius: self.frame.size.height/2,
                            startAngle: CGFloat(180.0).toRadians(),
                            endAngle: CGFloat(0.0).toRadians(),
                            clockwise: true)
        // self.createRectangle()
        
        // Specify the fill color and apply it to the path.
        
        print("print x of Arc\(self.frame.size.width/2)")
        print("print y of Arc\(self.frame.size.height/2)")
        print("print radius \(self.frame.size.height/2)")
        UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 0.5).setFill()
        //UIColor.orange.setFill()
        path.fill()
        
        let addGuestButtonImage = UIImage(named: "addGuest")
        
        
        //ArcButtonView.setBackgroundImage(addGuestButtonImage, for: .normal)
        // Specify a border (stroke) color.
        setImage(addGuestButtonImage, for: .normal)
        //setBackgroundImage(addGuestButtonImage, for: .normal)
        UIColor.clear.setStroke()
        path.stroke()

}
}
