//
//  ClubDetailScreenViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 14/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import QuartzCore
import MessageUI
import SwiftyJSON
import Kingfisher
import SwiftIconFont
import CTShowcase

class ClubSideHomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,CLLocationManagerDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    @IBOutlet weak var ratinglbl: UILabel!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var costOfTwoconstrainlbl: UILabel!
    @IBOutlet weak var eventCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblEvent: NSLayoutConstraint!
    @IBOutlet weak var facilityTblHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lbl_facilities: UILabel!
    @IBOutlet weak var lbl_HappyHours: UILabel!
    var StagCoupon_Constraints:[NSLayoutConstraint] = []
    var clubnamelabel:[NSLayoutConstraint] = []
    var faciltylLblConstraints:[NSLayoutConstraint] = []
    var faciltylTableConstraints:[NSLayoutConstraint] = []
    var checkInCollectionConstraints:[NSLayoutConstraint] = []
    var locationManager = CLLocationManager()
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var couponHeight: CGFloat!
    var attributedText = NSMutableAttributedString()
    let currentDateTime = Date()
    let formatter = DateFormatter()
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var clubFeatureImagesCollectionView: UICollectionView!
    
    @IBOutlet weak var tblHeightShrinkLayout: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    
    @IBOutlet weak var clubNameLabel: UILabel!
    
    @IBOutlet weak var clubTimingStatusLabel: UILabel!
    
    
    @IBOutlet weak var costOfTwo: UILabel!
    
    @IBOutlet weak var clubHappyHoursLbl: UILabel!
    
    
    @IBOutlet weak var clubEventListCollectionView: UICollectionView!
    
    @IBOutlet weak var facilitiesTableView: UITableView!
    
    
    @IBOutlet weak var starRating1: UIImageView!
    
    @IBOutlet weak var starRating2: UIImageView!
    
    @IBOutlet weak var starRating3: UIImageView!
    
    @IBOutlet weak var starRating4: UIImageView!
    
    @IBOutlet weak var starRating5: UIImageView!
    
    
    @IBOutlet weak var mapLocationbtnOutlet: UIButton!
    @IBOutlet weak var callingbtnOutlet: UIButton!
    
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var splitTimeLbl: UILabel!
    var clubArray = [UIImage(named:"club1"),UIImage(named:"club2")]
    
    var AppDelegateLocationLatitude = Double()
    var AppDelegateLocationLongitude = Double()

    var lat = Double()
    var long = Double()
    var ClubRADIUS = Double()
    var eventID = String()
    var eventName = String()
    var eventCheckinNotificationReceived = String()
    var club_id = String()
    var clubName = String()
    var clubTiming = String()
    var clubStatus = String()
    var clubCostForTwo = String()
    var clubHappyHours = String()
    var eventCount = Int()
    
    var clubRate = String()
    
    var clubPicDataArray:[String] = []
    var clubEventNameArray:[String] = []
    var clubEventImageArray:[String] = []
    var clubEventDateArray:[String] = []
    var costOfTwoArray:[String] = []
    var facilitiesArray = ["Parking","Dinner","Lunch"]
    var facilityNameArray:[String] = []
    var facilityImageArray:[String] = []
    var selectedClubId = ""
    var setTimmer = Timer.self
    var eventIdArray:[String] = []
    
    var whosCheckInData = [WhosCheckInModel]()
    var clubDetailData = [ClubDetailModel]()
    var clubContactNo = String()
    var userLat = Double()
    var userLong = Double()
    var SendUSerIDToMap = ""

    var selectedCoupon = String()
    
    var clubFavFlagString = String()
    
    var selectedCouponFlag = String()
    
    var selectedEventName = String()
    
    var userPrefFlag = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        
        let getUserData :UserDefaults = UserDefaults.standard
        self.SendUSerIDToMap = getUserData.value(forKey: "userid") as! String
        print("userid :\(String(describing: self.SendUSerIDToMap))")
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
       
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //let width = UIScreen.main.bounds.width
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth, height: 289 )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        clubFeatureImagesCollectionView.collectionViewLayout = layout
        
        
        self.clubFeatureImagesCollectionView.delegate = self
        self.clubFeatureImagesCollectionView.dataSource = self
        self.clubEventListCollectionView.delegate = self
        self.clubEventListCollectionView.dataSource = self
        self.facilitiesTableView.delegate = self
        self.facilitiesTableView.dataSource = self

        pageController.frame = CGRect(x: 21, y: 240, width: 50, height: 15)
        getClubDetailData()
        
        //swiftyWhosCheckInData()
        //fetchswiftyClubDetailData()
        
        self.clubFeatureImagesCollectionView.reloadData()
        self.clubEventListCollectionView.reloadData()
        self.facilitiesTableView.reloadData()
        
        contentView.sizeToFit()
        
        pageController.sizeToFit()
        clubNameLabel.sizeToFit()
        
        clubTimingStatusLabel.sizeToFit()
        costOfTwo.sizeToFit()
        clubHappyHoursLbl.sizeToFit()
        facilitiesTableView.sizeToFit()
       
        self.clubEventListCollectionView.layer.masksToBounds = true
        self.clubEventListCollectionView.layer.cornerRadius = 12
        
        scrollview.resizeScrollViewContentSizeforClubSideHome()
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.facilityTblHeight?.constant = self.facilitiesTableView.contentSize.height
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        
        AppDelegateLocationLatitude = (locationManager.location?.coordinate.latitude)!
        AppDelegateLocationLongitude = (locationManager.location?.coordinate.longitude)!
        
        let locationUserDefaults = UserDefaults.standard
        locationUserDefaults.set(AppDelegateLocationLatitude, forKey: "AppDelegateLocationLatitude")
        locationUserDefaults.set(AppDelegateLocationLongitude, forKey: "AppDelegateLocationLongitude")
        
        let locationLatitude = locationUserDefaults.value(forKey: "AppDelegateLocationLatitude")
        let locationLongitude = locationUserDefaults.value(forKey: "AppDelegateLocationLongitude")
        
        print("UserLocationLatitude : \(locationLatitude!)")
        print("UserLocationLatitude : \(locationLongitude!)")
        
        let myLat = location.coordinate.latitude
        let myLong = location.coordinate.longitude
        
        print("Latitude : \(myLat)")
        print("Longitude : \(myLong)")
        
    }
   
//    func fetchswiftyClubDetailData(){
//
//        var URL_ClubString = String()
//
//        let getUserData :UserDefaults = UserDefaults.standard
//        let User_Id = getUserData.value(forKey: "userid")
//        print("userid :\(String(describing: User_Id))!)")
//        print("selected club ID on club detail screen \(selectedClubId)")
//        URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=\(User_Id!)&club_id=\(selectedClubId)"
//
//        print("print club string on club detail api:\(URL_ClubString)")
//        Alamofire.request(URL_ClubString, method: .get).validate().responseJSON { (response) -> Void in
//            if let value = response.data {
//                do {
//                    let json = try JSON(data: value)
//                    if let dictionnary = json.dictionaryObject {
//                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
//
//                            if messageArray.count == 0{
//
//                                print("Empty Message Array")
//
//
//                            }else{
//
//                                for arr in messageArray {
//
//
//                                    self.clubDetailData.append(ClubDetailModel(clubDetailJSON: arr))
//                                    if self.clubDetailData[0].userPreferenceDictionary.count == 0 {
//
//                                        print(" Empty userPreferenceDictionary data  ")
//                                        //Do nothing
//                                    }else{
//
//                                        self.prefAgeStringOfUser = self.clubDetailData[0].userPreferenceDictionary[0].pref_age
//                                        print("print clubData count array  \(self.clubDetailData.count)")
//
//                                        if self.clubDetailData[0].todaysEventDictionary.count == 0 && self.whosCheckInData.count == 0{
//
//                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
//                                            print("checkINUserCount : \(self.whosCheckInData.count)")
//
//                                            self.eventLbl.isHidden = true
//                                            self.lblEvent.constant = 0
//                                            self.clubEventListCollectionView.isHidden = true
//                                            self.eventCollectionHeight.constant = 0
//
//                                        }else if self.clubDetailData[0].todaysEventDictionary.count != 0 && self.whosCheckInData.count == 0{
//
//                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
//                                            print("checkINUserCount : \(self.whosCheckInData.count)")
//
//
//                                        }else{
//
//                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
//                                            print("checkINUserCount : \(self.whosCheckInData.count)")
//
//                                            if self.clubDetailData[0].todaysEventDictionary.count == 0{
//
//                                                print("Do Nothing")
//
//                                            }else{
//
//                                                self.selectedUserCheckInEvent = self.clubDetailData[0].todaysEventDictionary[0].eventId_for_swipe
//
//                                                print("user event id: \(self.selectedUserCheckInEvent)")
//
//                                                print("todays check in flag \(self.clubDetailData[0].todaysEventDictionary[0].event_check_in_flag)")
//
//                                            }
//                                        }
//
//                                        print("user pref age \(self.clubDetailData[0].userPreferenceDictionary[0].pref_age)")
//                                        print("user pref gender \(self.clubDetailData[0].userPreferenceDictionary[0].pref_gender)")
//
//                                    }
//
//                                }
//
//                            }
//
//
//                        }
//
//                    }
//
//                }catch {
//                    print("cannot convert to Json")
//                }
//            }
//
//
//        }
//    }

    
    @objc func getClubDetailData(){
        
        URLCache.shared.removeAllCachedResponses()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        let URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=\(User_Id!)&club_id=\(selectedClubId)"
        
        print("club detail string \(URL_ClubString)")
        let url = NSURL(string: URL_ClubString)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        do{
            
            Alamofire.request(urlRequest).validate().responseJSON {(response) -> Void in

                print("alamofire Request: \(String(describing: response.request))")
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("test1")
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    print("test 2")
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                print(" dict data for fav :\(dictData)")
                
                for data in dictData{
                    
                    
                    self.clubRate = data["club_rate"] as! String
                    
                    //self.clubRate = "2"
                    
                    print("Club Rate \(self.clubRate)")
                    
                    self.clubName = data["club_name"] as! String
                    print("Club Name \(self.clubName)")
                    self.clubStatus = data["club_time"] as! String
                    print("clubStatus \(self.clubStatus)")
                    self.clubCostForTwo = data["cost_two"] as! String
                    print("clubCostForTwo \(self.clubCostForTwo)")
                    
                    self.costOfTwoArray.append(self.clubCostForTwo)
                    self.clubHappyHours = data["happy_time"] as! String
                    print("clubHappyHours \(self.clubHappyHours)")
                    self.clubContactNo = data["club_contact"] as! String
                    let clubPicData  =  data["club_pic"] as! [String]
                    
                    print("view did load data Club Name: \(self.clubName) Club Status : \(self.clubStatus) cost for two \(self.clubCostForTwo) Club Happy hours \(self.clubHappyHours) ")
                    self.clubNameLabel.text = self.clubName
                    self.clubNameLabel.numberOfLines = 0

                    self.costOfTwo.text  = self.clubCostForTwo
                    self.clubHappyHoursLbl.text = self.clubHappyHours
                    //Split Time String
                    let fullStatus    = self.clubStatus
                    
                    
                    
                    
                    if self.clubStatus == "Closed"{
                        
                        self.clubTimingStatusLabel.text = "\(fullStatus)"
                        
                    }else{
                        
                        let fullStatusArr = fullStatus.components(separatedBy: "-")
                        let status    = fullStatusArr[0]
                        let time = fullStatusArr[1]
                        self.clubTimingStatusLabel.text = "\(status)"
                        self.splitTimeLbl.text = "- CLOSES \(time)"
                        print("check status : \(status) - CLOSES-\(time)")
                    }

                    if self.clubRate == "1"{
                        print("Club Rate 1")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = true
                        self.starRating3.isHidden = true
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                        
                    }else if(self.clubRate == "2"){
                        print("Club Rate 2")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = true
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                        
                    }else if(self.self.clubRate == "3"){
                        print("Club Rate 3")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                    }else if(self.clubRate == "4"){
                        print("Club Rate 4")
                        
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = false
                        self.starRating5.isHidden = true
                        
                    }else if(self.clubRate == "5"){
                        print("Club Rate 5")
                        
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = false
                        self.starRating5.isHidden = false
                    }
                    
                    print("club pic  Data \(String(describing: clubPicData))")
                    self.clubPicDataArray.removeAll()
                    for picData in clubPicData{
                        
                        print("Pic Data inside for loop: \(picData)")
                        
                        self.clubPicDataArray.append(picData)
                        
                        
                        print("Club Pic Data Array\(self.clubPicDataArray)")
                        
                        //set page controller count
                        
                        self.pageController.numberOfPages = self.clubPicDataArray.count
                    }
                    
                    let clubFacilityData = data["facility"] as? [Dictionary <String,AnyObject>]
                    
                    
                    print("club facility data \(String(describing: clubFacilityData))")
                    
                    self.facilityNameArray.removeAll()
                    self.facilityImageArray.removeAll()
                    for facility in clubFacilityData!{
                        
                        let facilityName  = facility["fac_name"] as! String
                        print("facility name : \(facilityName)")
                        self.facilityNameArray.append(facilityName)
                        let facilityImage = facility["fac_icon"] as! String
                        print("facility Image : \(facilityImage)")
                        self.facilityImageArray.append(facilityImage)
                        
                        
                    }
                    
                    let clubEventData = data["event"] as? [Dictionary <String,AnyObject>]
                    
                    print("---------------------club event data------------------\(String(describing: clubEventData))")
            
                    self.eventCount = clubEventData!.count
               
                    if clubEventData?.count == 0{
                        
                        self.eventLbl.isHidden = true
                        
                    }else{
                    
                        self.eventLbl.isHidden = false
                        self.eventIdArray.removeAll()
                        self.clubEventNameArray.removeAll()
                        self.clubEventImageArray.removeAll()
                        self.clubEventDateArray.removeAll()
                       
                        //self.clubEventMaleCountArray.removeAll()
                        for eventData in clubEventData!{
                           
                            let eventId = eventData["event_id"] as! String
                            print("Event id\(eventId)")
                            self.eventIdArray.append(eventId)
                            
                            let eventName = eventData["event_name"] as! String
                            print("Event Name\(eventName)")
                            self.clubEventNameArray.append(eventName)
                            
                            print("Club Event Name Array\(self.clubEventNameArray)")
                            let eventImage = eventData["event_img"] as! String
                            print("Event image\(eventImage)")
                            self.clubEventImageArray.append(eventImage)
                            
                            print("Club Image Array \(self.clubEventImageArray)")
                            let eventDate = eventData["event_date"] as! String
                            print("Event date\(eventDate)")
                            self.clubEventDateArray.append(eventDate)
                           
                        }
                        
                    }
            
                    
                    let userTodyasEventData = data["today_event"] as? [Dictionary <String,AnyObject>]
                    
                    
                    for userTodaysEventInfo in userTodyasEventData!{
 
                        self.selectedEventName = userTodaysEventInfo["today_event_name"] as! String
                        
                    }

                }
                
                
                DispatchQueue.main.async {
                    
                    self.clubFeatureImagesCollectionView.reloadData()
                    self.facilitiesTableView.reloadData()
                    self.clubEventListCollectionView.reloadData()
                    
                    
                }
                
            }
            
            
        }catch{
            
            print(error)
        }

    }
    
    @IBAction func PageControll_Action(_ sender: UIPageControl) {
        pageController.numberOfPages = clubArray.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == self.clubFeatureImagesCollectionView{
            
            return clubPicDataArray.count
        }else if collectionView == self.clubEventListCollectionView{
            
            
            return clubEventImageArray.count
            
            
        }else{
            
            
            if self.whosCheckInData.count == 0{
                
                
                return 0
                
            }else{
                
                print("whos user photo count in item in number of section \(self.whosCheckInData.count)")
                
                return self.whosCheckInData.count
            }
            
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.clubFeatureImagesCollectionView{
            
            let cell:ClubDetailFeatureImageViewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubDetailFeatureImageViewCollectionViewCell", for: indexPath) as! ClubDetailFeatureImageViewCollectionViewCell
            
            cell.frame.size.width = screenWidth
            cell.frame.size.height = 280
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 0
            cell.layer.cornerRadius = 0
            cell.clipsToBounds = true
            
            cell.clubFeatureImageView.kf.setImage(with: URL(string:self.clubPicDataArray[indexPath.row]))
            cell.backButtonTapped.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)

            return cell
            
        }else if collectionView == self.clubEventListCollectionView{
            
            let cell:ClubEventListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubEventListCollectionViewCell", for: indexPath) as! ClubEventListCollectionViewCell
            cell.layer.borderColor = UIColor.blue.cgColor
            cell.layer.borderWidth = 3
            cell.layer.cornerRadius = 8
            cell.layer.masksToBounds = true
            
            let eventImageString = self.clubEventImageArray[indexPath.row]
            let imageURL = URL(string: eventImageString)
            let imageData = try! Data(contentsOf: imageURL! as URL)
            
            print("\(imageData)")
            cell.clubEventListImageView.image = UIImage(data: imageData)
            
            cell.eventShareButtonTapped.tag = indexPath.row
            
            cell.eventNameLbl.text = clubEventNameArray[indexPath.row]
            cell.eventDateLbl.text = clubEventDateArray[indexPath.row]
            return cell
            
        }else{
            
            let cell:UserGoingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserGoingCollectionViewCell", for: indexPath) as! UserGoingCollectionViewCell
            
            
            if self.whosCheckInData.count == 0{
                
                print("Empty Whos check in ")
                
            }else{
                print("whos user photo count \(self.whosCheckInData[0].user_photo.count)")
                print("who check in user photo 121 \(self.whosCheckInData[0].user_photo)")
            }

            
            print("whos check in data : \(self.whosCheckInData[indexPath.row].user_photo)")
            
            if self.whosCheckInData[indexPath.row].user_photo == ""{
                
                print("no image found")
                
                cell.userGoingImageView.image = #imageLiteral(resourceName: "defaultIcon")
                
            }else{
                cell.userGoingImageView.kf.setImage(with: URL(string:self.whosCheckInData[indexPath.row].user_photo))
            }
            
            
            
            return cell
            
        }
        
    }
    
   
    
    @objc func backButtonClicked() {
        
        print("back button tapped to instatiate back view ")

        dismiss(animated: false, completion: nil)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let indexPath = clubFeatureImagesCollectionView.indexPathForItem(at: center) {
            self.pageController.currentPage = indexPath.row
        }
    }
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return facilityNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:EventVenueFacilitiesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EventVenueFacilitiesTableViewCell") as! EventVenueFacilitiesTableViewCell
        
        cell.facilityNameListLbl.text = facilityNameArray[indexPath.row]
        
        let facilityImageString = self.facilityImageArray[indexPath.row]
        
        if facilityImageString.count == 0{
            
            print("do nothing ")
        }else{
            
            let imageURL = URL(string: facilityImageString)
            
            let imageData = try! Data(contentsOf: imageURL! as URL)
            print(imageData)
            cell.facilityImageView.image = UIImage(data: imageData)
            
        }
        
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func callingbtn_Action(_ sender: UIButton){
        
        if let url = URL(string: "tel://\(self.clubContactNo)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func mapLocationbtn_Action(_ sender: UIButton){
        
        let storyboard : UIStoryboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        vc.userID = SendUSerIDToMap
        vc.selectedClubid = selectedClubId
        print("UserLat : \((locationManager.location?.coordinate.latitude)!)")
        print("UserLong : \((locationManager.location?.coordinate.longitude)!)")
        vc.myLatitude = (locationManager.location?.coordinate.latitude)!
        vc.myLongitude = (locationManager.location?.coordinate.longitude)!
        
        let navigationController = UINavigationController(rootViewController: vc)
        
        self.present(navigationController, animated: true, completion: nil)
        
        
    }
    

    
}

extension UIScrollView {
    
    func resizeScrollViewContentSizeforClubSideHome() {
        
        var contentRect = CGRect.zero
        
        for view in self.subviews {
            
            contentRect = contentRect.union(view.frame)
            
        }
        
        self.contentSize = contentRect.size
        
    }
    
}







