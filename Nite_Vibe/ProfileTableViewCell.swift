//
//  ProfileTableViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 10/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import QuartzCore
class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var eventNameLbl: UILabel!
    
    @IBOutlet weak var eventDateLbl: UILabel!
    var labelforFont = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //eventDateLbl.intrinsicContentSize.width
        eventDateLbl.layer.cornerRadius = 4
        eventDateLbl.clipsToBounds = true
        
        eventNameLbl.layer.cornerRadius = 20
        eventNameLbl.clipsToBounds = true
        
//        var attributedString = NSMutableAttributedString(string:labelforFont)
//        
//        var attrs = NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)
////var boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
//        
//        attributedString.append(attrs)
//        
//        eventNameLbl.attributedText = attributedString
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
