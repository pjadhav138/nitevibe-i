//
//  UserGoingCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 16/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import QuartzCore
class UserGoingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userGoingImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //eventDateLbl.intrinsicContentSize.width
        //userGoingImageView.layer.cornerRadius =
        //userGoingImageView.clipsToBounds = true
        //-------------------
        userGoingImageView.layer.borderWidth=2
        userGoingImageView.layer.masksToBounds = false
        userGoingImageView.layer.borderColor = UIColor.blue.cgColor
        userGoingImageView.layer.cornerRadius = userGoingImageView.frame.height/2
        userGoingImageView.clipsToBounds = true

        //eventNameLbl.layer.cornerRadius = 20
        //eventNameLbl.clipsToBounds = true
       // cell.imageView?.layer.cornerRadius = (image?.size.width)!/2
        
        //userGoingImageView.layer.cornerRadius = userGoingImageView.frame.size.width / 2
        //userGoingImageView.clipsToBounds = true
//
//        userGoingImageView.layer.borderWidth = 1.0
//        userGoingImageView.layer.masksToBounds = true
//        userGoingImageView.layer.borderColor = UIColor.white.cgColor
//        userGoingImageView.layer.cornerRadius = userGoingImageView.frame.size.width / 2
//        userGoingImageView.clipsToBounds = true
        
        
    }
    
}
