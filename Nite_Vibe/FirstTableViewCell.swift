//
//  FirstTableViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 21/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class FirstTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var CollectionView: UICollectionView!
    var collectionData = ["1","2"]
     var SuperVibeArray:[UIImage] = [UIImage(named: "supervibe0")!,UIImage(named: "supervibe1")!,UIImage(named: "supervibe2")!,UIImage(named: "supervibe3")!]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        CollectionView.delegate = self
        
        CollectionView.dataSource = self
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SuperVibeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! ParentTableFirstCellCollectionViewCell
        
        
        cell.userImage.image = SuperVibeArray[indexPath.row]
        return cell
    }

}
