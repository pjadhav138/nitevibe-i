 //
 //  ClubListViewController.swift
 //  Nite_Vibe
 //
 //  Created by Vinayak Bhor on 12/04/18.
 //  Copyright © 2018 Appmonks. All rights reserved.
 //
 
 import UIKit
 
 var ClubListLaunchScroll = ""
 var ClubListLaunchCreateProfile = ""
 import CoreLocation
 import QuartzCore
 import Alamofire
 import GoogleMaps
 import NVActivityIndicatorView
 import Kingfisher
 import CTShowcase
 import FBSDKLoginKit
 
 var FcmToken = ""

 class ClubListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate,NVActivityIndicatorViewable{
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    
    
    
    @IBOutlet weak var overView: UIView!
    @IBOutlet weak var overlaylabel: UILabel!
    
    @IBOutlet weak var scrollViewHeightConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var dropDownTable: UITableView!
    var locations = ["Thane","Thane","West","Pune"]
    let cellReuseIdentifier = "cell"
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    
    @IBOutlet weak var clubCollectionView: UICollectionView!
    
    @IBOutlet weak var bannerPageController: UIPageControl!
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var looadingImageView: UIImageView!
    
    @IBOutlet weak var loadinImageViewAppIcon: UIImageView!
    
    @IBOutlet weak var overlayView: UIView!
    
    
    var bannerArray = [UIImage(named:"banner11"),UIImage(named:"banner12"),UIImage(named:"banner13")]
    var clubArray = [UIImage(named:"club1"),UIImage(named:"club2"),UIImage(named:"club3")]
    
    var clubDateArray = [String]()
    var clubNameArray = [String]()
    var clubAtPeopleArray = [String]()
    
    var clubBannerImages = [String]()
    var clubLocationCitiesArray = [String]()
    var bannerImagesArray = [String]()
    var clubImagesArray:[String] = []
    var selectedClubLocation = String()
    
    var flagValue = "Inactive"
    var club_id = String()
    var selectedClubId = String()
    var favourite_mode = 0 //for favourite club
    var progressValue:Float = 0 // progress view data
    var clubMaleCount:Float = 0 // club male count
    
    var clubIdArray:[String] = []
    var clubMaleIntArray = [Int]()
    var clubFemaleIntArray = [Int]()
    var bannerClubId = String()
    var bannerEventId = String()
    var locationManager = CLLocationManager()
    var myLat = Double()
    var myLong = Double()
    var selectedFavClubId = Int()
    var clubFavFlagArray = [String]()
    var bannerEventIdArray = [String]()
    var bannerClubIdArray = [String]()
    var leftTextMargin : CGFloat = 5.0
    var locationPoints = CLLocationCoordinate2D()
    var CLUBlat = Double()
    var CLUBlong = Double()
    var timer = Timer()

    @IBOutlet weak var OverlayImage: UIImageView!
    @IBOutlet weak var blueOverlayView: UIVisualEffectView!

    var AppDelegateLocationLatitude = Double()
    var AppDelegateLocationLongitude = Double()

    var checkLoginStatus = ""
    var getRadiusFlag = 0
    var OverlayScroll = "Scroll First Time Launch"
    var OverlayCreateProfile = "CreateProfile First Time Launch"
    var setclubTimmer = Timer.self
 
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    
    @IBAction func textEditingDidChanged(_ sender: Any) {
        dropDownTable.isHidden = true
        
    }
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.getUserDeviceID()
        
        navigationController?.isNavigationBarHidden = true
        view.addSubview(loadingView)
        
        searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        loadingView.addSubview(looadingImageView)
        looadingImageView.addSubview(loadinImageViewAppIcon)
        
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: w / 2, y: h / 2, width: 200, height: 21),type: NVActivityIndicatorType(rawValue: 17))
       
        let animationTypeLabel = UILabel(frame: CGRect(x: w / 2, y: h / 2, width: 200, height: 21))
        
        dropDownTable.showsVerticalScrollIndicator = true
        activityIndicatorView.center = CGPoint(x: w / 2, y: h / 2)
        
        animationTypeLabel.center = CGPoint(x: w / 2, y: h / 2)
        
        animationTypeLabel.sizeToFit()
        animationTypeLabel.textColor = UIColor.red
        
        activityIndicatorView.padding = 25
        self.looadingImageView.addSubview(activityIndicatorView)
        self.looadingImageView.addSubview(animationTypeLabel)
        activityIndicatorView.startAnimating()
        
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.clubCollectionView.reloadData()
        self.bannerCollectionView.reloadData()
        getDefaultClubData()
        self.setclubTimmer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(self.getDefaultClubData), userInfo: nil, repeats: true)
        
            DispatchQueue.main.async {
            
                self.clubCollectionView.reloadData()
                self.bannerCollectionView.reloadData()
            
            }
        
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        self.dropDownTable.layer.zPosition = 10;
        self.clubCollectionView.layer.zPosition = 0;
        
        //self.clubCollectionView.frame.origin.y = 134;
        
        //self.clubCollectionView.collectionViewLayout.

        
        self.searchTextField.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1)
        layout.itemSize = CGSize(width: screenWidth - 20, height: 186)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 5
        clubCollectionView.collectionViewLayout = layout
        
        let mylayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        mylayout.scrollDirection = .horizontal
        mylayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        mylayout.itemSize = CGSize(width: screenWidth, height: 162)
        mylayout.minimumInteritemSpacing = 10
        mylayout.minimumLineSpacing = 0
        bannerCollectionView.collectionViewLayout = mylayout
        
        dropDownTable.backgroundColor = UIColor(red: 76/255, green: 0/255, blue: 165/255, alpha: 1)
        searchTextField.tintColor = UIColor.clear
        searchTextField.layer.cornerRadius = 8
        self.searchTextField.delegate = self
        self.dropDownTable.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        self.dropDownTable.delegate = self
        self.dropDownTable.dataSource = self
        self.searchTextField.delegate = self
        
        self.bannerCollectionView.delegate = self
        self.bannerCollectionView.dataSource = self
        self.clubCollectionView.delegate = self
        self.clubCollectionView.dataSource =  self
        
        dropDownTable.isHidden = true
        
        searchTextField.addTarget(self, action:#selector(textFieldActive), for: UIControlEvents.touchDown)
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            
        }
        
        locationManager.allowsBackgroundLocationUpdates = true
        
        locationManager.pausesLocationUpdatesAutomatically = false
        
        
        self.getLocation()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.checkOverlay()
        self.createProfileOverlay()
        
    }
    
    func checkOverlay()
    {
        
        let scrollLaunchedBefore = UserDefaults.standard.bool(forKey: "scrollLaunchedBefore")
        // let ddd = UserDefaults.standard.
        if scrollLaunchedBefore {
            
            ClubListLaunchScroll = "Scroll Has Launched Before"
            
        }
        else{
            
            ClubListLaunchScroll = "Scroll First Time Launch"
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                self.ScrollOverLay()
            })
            UserDefaults.standard.set(true, forKey: "scrollLaunchedBefore")
            
        }
        
        
        
    }
    
    func createProfileOverlay(){
        
        let createProfileLaunchedBefore = UserDefaults.standard.bool(forKey: "createProfileLaunchedBefore")
        
        if createProfileLaunchedBefore {
            
            ClubListLaunchCreateProfile = "CreateProfile Has Launched Before"
            
        }
        else{
            
            ClubListLaunchCreateProfile = "CreateProfile First Time Launch"
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                self.CreateProfileOverLay()
            })
            UserDefaults.standard.set(true, forKey: "createProfileLaunchedBefore")
            
        }
        
    }
    
    func ScrollOverLay(){
        
        print("Print AppDelegate Launch Status scroll : \(OverlayScroll)")
        print("Print ClubList Launch Status : \(ClubListLaunchScroll)")
        
        if OverlayScroll == ClubListLaunchScroll{
            
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "Scroll1.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.clear
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            showcase.setup(for: self.overlaylabel, offset: CGPoint.zero, margin: 0)
            showcase.show()
        }
        else{
            
        }
        
    }
    
    func CreateProfileOverLay(){
        
        print("Print AppDelegate Launch Status create profile : \(OverlayCreateProfile)")
        print("Print ClubList Launch Status : \(ClubListLaunchCreateProfile)")
        
        
        if OverlayCreateProfile == ClubListLaunchCreateProfile {
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "createP.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.clear
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            showcase.setup(for: self.overlaylabel, offset: CGPoint.zero, margin: 0)
            showcase.show()
            
        }
        else{
            
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
 
        
        AppDelegateLocationLatitude = (locationManager.location?.coordinate.latitude)!
        AppDelegateLocationLongitude = (locationManager.location?.coordinate.longitude)!
        
        let locationUserDefaults = UserDefaults.standard
        locationUserDefaults.set(AppDelegateLocationLatitude, forKey: "AppDelegateLocationLatitude")
        locationUserDefaults.set(AppDelegateLocationLongitude, forKey: "AppDelegateLocationLongitude")
        
        let locationLatitude = locationUserDefaults.value(forKey: "AppDelegateLocationLatitude")
        let locationLongitude = locationUserDefaults.value(forKey: "AppDelegateLocationLongitude")
        
        print("UserLocationLatitude : \(locationLatitude!)")
        print("UserLocationLatitude : \(locationLongitude!)")
       
        locationManager.stopUpdatingLocation()
        
    }
    
    func getUserDeviceID(){
        
        var respo = Int()
        
        let getUserData:UserDefaults = UserDefaults.standard
        
        let FCMRegistrationId = getUserData.value(forKey:"FCMToken")
        print("FCMRegistrationId1: \(String(describing: FCMRegistrationId))")
        
        let User_Id = getUserData.value(forKey: "userid")
        print("userid 1 :\(String(describing: User_Id))")

        if(User_Id == nil) {
            FBSDKAccessToken.setCurrent(nil)
            FBSDKProfile.setCurrent(nil)
            
            FBSDKLoginManager().logOut()
            
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            
            let UserGuide:UserGuideWithFBLogIn = storyboard.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
            self.present(UserGuide, animated: true, completion: nil)
            
            print("no user")

            //return
        }
        
        let URL_deviceID = "http://www.nitevibe.in/webservices/device_id.php?user_id=\(User_Id!)&device_id=\(FCMRegistrationId!)&platform=Ios"
        
        print("Device String: \(URL_deviceID)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_deviceID, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_deviceID , method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                
                if(respo == 201){
                    print("Message: \(String(describing: responseMessage))")
                    
                }else if(respo == 200){
                    
                    print("success")
                    
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }
    
    
    
    
   
    
    @objc func getDefaultClubData(){
        //Updated With Cahce Clearing
        URLCache.shared.removeAllCachedResponses()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))")
       // let UD = User_Id
        
        
        let URL_ClubString = "http://www.nitevibe.in/webservices/test_club_details.php?user_id=\(User_Id!)&loc=Thane"
        
        print("Club List Url String\(URL_ClubString)")
        
        print("UD : \(String(describing: User_Id))")
        
        let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = NSURL(string: escapeUrl!)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON{
            
            (response) in
            
            print("alamofire Request: \(String(describing: response.request))")
            
            print(" response :\(String(describing: response.request))")
            
            guard response.result.isSuccess  else {
                
                print("test1")
                
                print("Block 1 ")
                print(" response :\(response)")
                
                print("Error with response: \(String(describing: response.result.error))")
                
                return
                
            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                print("test 2")
                print(" response :\(response)")
                
                print("Error with dictionary: \(String(describing: response.result.error))")
                
                return
                
            }

            print("dict message:\(dict["message"]!)")
            
            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" dict data\(dictData)")
            
            for data in dictData{
                
                let clubData =  data["club"] as? [Dictionary <String,AnyObject>]
                print("club Data \(String(describing: clubData))")
                
                self.clubNameArray.removeAll()
                self.clubFavFlagArray.removeAll()
                self.clubDateArray.removeAll()
                self.clubIdArray.removeAll()
                self.clubAtPeopleArray.removeAll()
                self.clubMaleIntArray.removeAll()
                self.clubFemaleIntArray.removeAll()
                self.clubImagesArray.removeAll()
                for data in clubData!{
                    
                    self.club_id = data["club_id"] as! String
                    print("club id \(self.club_id)")
                    self.clubIdArray.append(self.club_id)
                    let club_name =  data["club_name"] as! String
                    print("club name \(club_name)")
                    self.clubNameArray.append(club_name)
                    let favourite =  data["favourite"] as! String
                    print("favourite \(favourite)")
                    let total = String(describing: data["total_ratio"]!)
                    //let total = data["total"] as! String
                    print("total \(total)")
                    
                    self.clubAtPeopleArray.append(total)
                    let male = data["male"] as! Float
                    
                    self.clubMaleCount = (male/100)
                    
                    let maleInt = data["male"] as! Int
                    print("male \(maleInt)")
                    
                    
                    
                    self.clubMaleCount = (male/100)
                    self.clubMaleIntArray.append(maleInt)
                    
                    
                    print("print male Int array \(self.clubMaleIntArray)")
                    let femaleInt = data["female"] as! Int
                    print("female Int \(femaleInt)")
                    self.clubFemaleIntArray.append(femaleInt)
                    print("male count: \(self.clubMaleCount)")
                    let female = data["female"] as! Int
                    print("female \(female)")
                    let picture = data["picture"] as! String
                    
                    print("Picture : \(picture)")
                    self.clubImagesArray.append(picture)
                    
                    let  date = data["date"] as! String
                    print("Date \(date)")
                    
                    self.clubDateArray.append(date)
                    
                    let total_ratio = String(describing: data["total_ratio"])
                    //let total_ratio = data["total_ratio"] as! String
                    print("total Ratio \(total_ratio)")
                    
                    let clubIsFav = data["favourite"] as! String
                    self.clubFavFlagArray.append(clubIsFav)
                    print("club fav array \(self.clubFavFlagArray)")
                    
                }
                
                let bannerData = data["banner"] as? [Dictionary <String,AnyObject>]
                
                print("banner data \(String(describing: bannerData))")
                
                self.bannerImagesArray.removeAll()
                for databanner in bannerData!{
                    
                    
                    let bannerImageStrings = databanner["banner_img"] as! String
                    self.bannerClubId = databanner["banner_club"] as! String
                    self.bannerEventId = databanner["banner_event"] as! String
                    
                    self.bannerEventIdArray.append(self.bannerEventId)
                    self.bannerImagesArray.append(bannerImageStrings)
                    self.bannerClubIdArray.append(self.bannerClubId)
                    
                    self.bannerPageController.numberOfPages = self.bannerImagesArray.count
                    
                }
                
            }
            
            DispatchQueue.main.async {
                
                self.clubCollectionView.reloadData()
                self.bannerCollectionView.reloadData()
                self.loadingView.isHidden = true
                
            }
            
        }
        
        
        
        
    }
    func getClubData(){
        //Updated With Cahce Clearing
        URLCache.shared.removeAllCachedResponses()
        
//        if(self.dropDownTable.isHidden) {
//            self.clubCollectionView.frame.origin.y = 134;
//        } else {
//            self.clubCollectionView.frame.origin.y = 349;
//        }
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let URL_ClubString = "http://www.nitevibe.in/webservices/test_club_details.php?userid=\(User_Id!)&loc=\(selectedClubLocation)"
        
        print("Club String\(URL_ClubString)")
        
        let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: escapeUrl!)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON{
            
            
            (response) in
            
            print("alamofire Request: \(String(describing: response.request))")
            
            print(" response :\(String(describing: response.request))")
            
            guard response.result.isSuccess  else {
                
                print("test1")
                
                print("Block 1 ")
                print(" response :\(response)")
                
                print("Error with response: \(String(describing: response.result.error))")
                
                return
                
            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                print("test 2")
                print(" response :\(response)")
                
                print("Error with dictionary: \(String(describing: response.result.error))")
                
                return
                
            }
            
            print("dict message:\(dict["message"]!)")
            
            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" dict data\(dictData)")
            
            
            for data in dictData{
                
                let clubData =  data["club"] as? [Dictionary <String,AnyObject>]
                print("club Data \(String(describing: clubData))")
                self.clubNameArray.removeAll()
                self.clubFavFlagArray.removeAll()
                self.clubDateArray.removeAll()
                self.clubIdArray.removeAll()
                self.clubAtPeopleArray.removeAll()
                self.clubMaleIntArray.removeAll()
                self.clubFemaleIntArray.removeAll()
                self.clubImagesArray.removeAll()
                for data in clubData!{
                    
                    self.club_id = data["club_id"] as! String
                    print("club id \(self.club_id)")
                    
                    self.clubIdArray.append(self.club_id)
                    let club_name =  data["club_name"] as! String
                    print("club name \(club_name)")
                    self.clubNameArray.append(club_name)
                    let favourite =  data["favourite"] as! String
                    print("favourite \(favourite)")
                    
                    let total = data["total"] as! Int
                    print("total \(total)")
                    
                    self.clubAtPeopleArray.append((String(total)))
                    let male = data["male"] as! Float
                    
                    print("male float \(male)")
                    let maleInt = data["male"] as! Int
                    
                    self.clubMaleIntArray.append(maleInt)
                    
                    let femaleInt = data["female"] as! Int
                    
                    self.clubFemaleIntArray.append(femaleInt)
                    
                    print("female count  \(femaleInt)")
                    print("female array \(self.clubFemaleIntArray)")
                    self.clubMaleCount = (male/100)
                    
                    print("male \(male)")
                    print("male count: \(self.clubMaleCount)")
                    let female = data["female"] as! Int
                    print("female \(female)")
                    let picture = data["picture"] as! String
                    
                    print("Picture : \(picture)")
                    self.clubImagesArray.append(picture)
                    
                    let  date = data["date"] as! String
                    print("Date \(date)")
                    
                    self.clubDateArray.append(date)
                    let total_ratio = data["total_ratio"] as! Int
                    print("total Ratio \(total_ratio)")
                    
                    let clubIsFav = data["favourite"] as! String
                    self.clubFavFlagArray.append(clubIsFav)
                    print("club fav array \(self.clubFavFlagArray)")
                    
                }
                
                let bannerData = data["banner"] as? [Dictionary <String,AnyObject>]
                
                print("banner data \(String(describing: bannerData))")
                
                self.bannerImagesArray.removeAll()
                for databanner in bannerData!{
                    
                    
                    let bannerImageStrings = databanner["banner_img"] as! String
                    self.bannerClubId = databanner["banner_club"] as! String
                    self.bannerEventId = databanner["banner_event"] as! String
                    self.bannerImagesArray.append(bannerImageStrings)
                    self.bannerClubIdArray.append(self.bannerClubId)
                    print("print banner images :\(self.bannerImagesArray)")
                    
                }
                
            }
            DispatchQueue.main.async {
                
//                if(self.dropDownTable.isHidden) {
//                    self.clubCollectionView.frame.origin.y = 134;
//                } else {
//                    self.clubCollectionView.frame.origin.y = 349;
//                }
                
                self.clubCollectionView.reloadData()
                self.bannerCollectionView.reloadData()
                self.loadingView.isHidden = true
            }
            
        }
        
        
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        
        guard let touch:UITouch = touches.first else
        {
            
            return ;
        }
        if touch.view != dropDownTable
        {
            //searchTextField.endEditing(true)
            dropDownTable.isHidden = true
           // clubCollectionView.isHidden = false
            //clubCollectionView.collectionViewLayout.
//            if(dropDownTable.isHidden) {
//                self.clubCollectionView.frame.origin.y = 134;
//            } else {
//                self.clubCollectionView.frame.origin.y = 349;
//            }
            
        }
        
        
    }
    @objc func textFieldActive() {
        dropDownTable.isHidden = !dropDownTable.isHidden
        //clubCollectionView.isHidden = !clubCollectionView.isHidden
//        if(dropDownTable.isHidden) {
//            self.clubCollectionView.frame.origin.y = 139;
//        } else {
//            self.clubCollectionView.frame.origin.y = 349;
//        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // TODO: Your app can do something when textField finishes editing
        print("The textField ended editing. Do something based on app requirements.")
        // self.getClubData()
    }
    
    
    
    func textFieldShouldBeginEditing(_ searchTextField : UITextField) -> Bool {
        
        
        //Hide keyboard
        
        if self.searchTextField.text != ""{
            return false
        }
        //view.endEditing(true)
        return true
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return clubLocationCitiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:UITableViewCell = dropDownTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        
        cell.backgroundColor = UIColor(red: 76/255, green: 0/255, blue: 165/255, alpha: 1)
        
        cell.textLabel?.text = clubLocationCitiesArray[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.layer.zPosition = 10
        
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        searchTextField.text = " " + " " + clubLocationCitiesArray[indexPath.row]
        
        dropDownTable.isHidden = true
        
        searchTextField.endEditing(true)
        
        print("table row selected table data \(clubLocationCitiesArray[indexPath.row])")
        
        
        selectedClubLocation = clubLocationCitiesArray[indexPath.row]
        let setSelectedLocation:UserDefaults = UserDefaults.standard
        
        setSelectedLocation.set(clubLocationCitiesArray[indexPath.row], forKey: "selectedcityloaction")
        
        //if user select table data value
        getClubData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollectionView{
            return bannerImagesArray.count
        }
        else{
            
            return clubImagesArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bannerCollectionView{
            let cell:BannerAddCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerAddCollectionViewCell", for: indexPath) as! BannerAddCollectionViewCell
            cell.frame.size.width = screenWidth
            cell.frame.size.height = 162
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 1
            cell.layer.cornerRadius = 0
            cell.clipsToBounds = true
            
            let bannerImageString = self.bannerImagesArray[indexPath.row]
            print("Banner Image String \(bannerImageString)")
            let BimageURL = URL(string: bannerImageString)
            
            let escapeBUrl = bannerImageString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            cell.bannerAddImage.kf.setImage(with: URL(string: escapeBUrl!))
            
//            cell.bannerAddImage.kf.setImage(with: URL(string:self.bannerImagesArray[indexPath.row]))
            
            
            self.bannerPageController.numberOfPages = bannerImagesArray.count
            return cell
        }else {
            
            let cell:ClubCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCollectionViewCell", for: indexPath) as! ClubCollectionViewCell
            //cell.frame.size.width = screenWidth
            // cell.frame.size.height = 169
            cell.layer.borderColor = UIColor.blue.cgColor
            cell.layer.borderWidth = 3
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            
            cell.favouriteButtonTapped.isHidden = true
            
            let clubImageString = self.clubImagesArray[indexPath.row]
            print("Club Image String \(clubImageString)")
            let imageURL = URL(string: clubImageString)
            
            let escapeUrl = clubImageString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
//            let view = UIView(frame: cell.clubImageView.frame)
            let view = UIView(frame: CGRect(x: 0, y: 0, width: cell.clubImageView.frame.width + 20, height: cell.clubImageView.frame.height + 20))
            
            let gradient = CAGradientLayer()
            
            gradient.frame = view.frame
            
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor ]
            
            gradient.locations = [0.05, 1.0]
            
            view.layer.insertSublayer(gradient, at: 0)
            
            cell.clubImageView.addSubview(view)

            cell.clubImageView.kf.setImage(with: URL(string: escapeUrl!))
            
            cell.dateLabel.text = clubDateArray[indexPath.row]
            cell.clubNameLabel.text = clubNameArray[indexPath.row]
            
            cell.favouriteButtonTapped.tag = indexPath.row
            cell.favouriteButtonTapped.addTarget(self, action: #selector(favouriteButtonAction), for: .touchUpInside)
            //cell.shareButtonOutlet.tag = indexPath.row
            cell.shareButtonOutlet.addTarget(self, action: #selector(shareButtonAction), for: .touchUpInside)
            

            cell.favouriteButtonTapped.tag = indexPath.row
            
            print("selected button tag \(cell.favouriteButtonTapped.tag)")
            print("default club list id array \(clubIdArray[indexPath.row])")
            
            print(" selected button tap 123 \(cell.favouriteButtonTapped.tag)")
            
            
            print("check club is seleted for fav for this user")
            

            print("flag array count \(clubFavFlagArray.count)")
            
            print("print fav array at cell for row: \(clubFavFlagArray[indexPath.row])")

            
            cell.favouriteButtonTapped.tag = indexPath.row
            
            if cell.favouriteButtonTapped.tag == indexPath.row{
                
                //check active or inactive
                //if incative
                 print("ClubFAVINDEX ; \(clubFavFlagArray[indexPath.row])")
                if clubFavFlagArray[indexPath.row] == "1"{
                    
                   
                    
                    print("selected button index \(cell.favouriteButtonTapped.tag) is Active")
                    //Change image index
                    cell.favouriteButtonTapped.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                    
                }else{
                    print("selected button index \(cell.favouriteButtonTapped.tag) is Inactive")
                    cell.favouriteButtonTapped.setBackgroundImage(UIImage(named: "favourite. selected.png"), for: .normal)
                    
                }
                
            }
            
            
            //pass club id to share button
            
            cell.shareButtonOutlet.tag = indexPath.row
            
            //if male count and female count is zero hide both label and progress view
            if clubAtPeopleArray[indexPath.row].count >= 10{
                
                cell.maleCountLbl.isHidden = false
                
                cell.femaleCountLbl.isHidden = false
                
                cell.ratioProgressView.isHidden = false
                
                cell.peopleAtClubLabel.isHidden = false
                
                cell.femaleImageView.isHidden = false
                cell.maleImgeView.isHidden = false
                //progress bar show if totol_ratio is more than 10
                
                cell.peopleAtClubLabel.text = "\(self.clubAtPeopleArray[indexPath.row]) people are here"
                cell.maleCountLbl.text = "\(String(clubFemaleIntArray[indexPath.row]))%"
                cell.femaleCountLbl.text = "\(String(clubMaleIntArray[indexPath.row]))%"
                print("progress value \(progressValue)")
                cell.ratioProgressView.progress = Float(clubFemaleIntArray[indexPath.row])/100
                cell.ratioProgressView.layer.masksToBounds = true
                cell.ratioProgressView.layer.cornerRadius = 4
                
            }else{
                print("hide progress bar at event")
                cell.peopleAtClubLabel.text = "\(self.clubAtPeopleArray[indexPath.row]) people are here"
                
                cell.maleCountLbl.text = "\(String(clubFemaleIntArray[indexPath.row]))%"
                cell.femaleCountLbl.text = "\(String(clubMaleIntArray[indexPath.row]))%"
                print("progress value float : \(Float(clubFemaleIntArray[indexPath.row])/100)")
                cell.ratioProgressView.layer.masksToBounds = true
                cell.ratioProgressView.layer.cornerRadius = 4
                cell.maleCountLbl.isHidden = true
                
                cell.femaleCountLbl.isHidden = true
                
                cell.ratioProgressView.isHidden = true
                
                cell.peopleAtClubLabel.isHidden = true
                
                cell.femaleImageView.isHidden = true
                cell.maleImgeView.isHidden = true
                
            }
            
            return cell
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let indexPath = bannerCollectionView.indexPathForItem(at: center) {
            self.bannerPageController.currentPage = indexPath.row
        }
    }
    
    
    @objc func favouriteButtonAction(favouriteButtonTapped:UIButton){
        
        print("tap fav button \(favouriteButtonTapped.tag)")
        //getting selected favourite Button tapped selected event id
        //this is doing for the default get data do for  also get data function
        print("selected event id \(clubIdArray[favouriteButtonTapped.tag])")
        selectedFavClubId = Int(clubIdArray[favouriteButtonTapped.tag])!
        
        print("selectedFavClubId \(selectedFavClubId)")
        print("check flag ")
        
        if clubFavFlagArray[favouriteButtonTapped.tag] == "0"{
            
            print("make Active ")
            flagValue = "Active"
            favourite()
            getClubData()
            
        }else if clubFavFlagArray[favouriteButtonTapped.tag] == "1"{
            
            print("make Inactive ")
            
            flagValue = "Inactive"
            favourite()
            getClubData()
        }
        
        
    }
    
    
    
    
    @objc func shareButtonAction(shareButtonOutlet:UIButton){
        
        
        print("share buttontapped")
        
        let clubIdString = clubIdArray[shareButtonOutlet.tag]
        
        print("print club string \(clubIdString)")
        //share only encoded club id
        let data = (clubIdString).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let  shareContent = "http://www.nitevibe.in/club_details.php?id=\(base64)"
        
        print("share string \(shareContent)")
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bannerCollectionView{
            
            if self.bannerEventIdArray[indexPath.row] == "0" && self.bannerClubIdArray[indexPath.row] == "0"{
                
                print("Do nothing")
            }else{
                
                
                let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
                
                
                let eventDetailDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController

                eventDetailDetailVC.selectedEventId = self.bannerEventIdArray[indexPath.row]
                eventDetailDetailVC.selectedClubId = self.bannerClubIdArray[indexPath.row]

                present(eventDetailDetailVC, animated: true, completion: nil)
                
            }
            
        }else {
            
            
            
            let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
            
            
            let clubDetailVC = storyboard.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
            
            selectedClubId = clubIdArray[indexPath.row]
            
            
            print("print selected id : \(selectedClubId)")
            
            
            clubDetailVC.selectedClubId = clubIdArray[indexPath.row]
            clubDetailVC.userLat = myLat
            clubDetailVC.userLong = myLong
            print("Selected Club ID : \(clubIdArray[indexPath.row])")
            
            present(clubDetailVC, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    func favourite(){
        //need to pass user id in API
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: "userid"))")
        
        print("selected club id \(selectedClubId)")
        let URL_String = "http://www.nitevibe.in/webservices/test_favourite.php?user_id=\(User_Id!)&fav_id=\(selectedFavClubId)&mode=\(favourite_mode)&status=\(flagValue)"
        
        print("test favourite Url String: \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")

                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
    }
    func getLocation(){
        //Updated With Cahce Clearing
        URLCache.shared.removeAllCachedResponses()
        
        let URL_String = "http://www.nitevibe.in/webservices/location_list.php"
        
        do{
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            Alamofire.request(urlRequest).responseJSON{
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                print(" dict data loaction cities data\(dictData)")
                
                self.clubLocationCitiesArray.removeAll()
                for data in dictData{
                    
                    let location = data["loc"] as! String
                    
                    print("club location \(location)")
                    
                    
                    
                    self.clubLocationCitiesArray.append(location)
                    
//                    let setUserLocation :UserDefaults = UserDefaults.standard
//
//                    setUserLocation.set(location,forKey:"location")
//                    setUserLocation.value(forKey: "location")
                    
                   
                }
                for element in self.clubLocationCitiesArray {
                    print("elements \(element)")
                }
                self.dropDownTable.reloadData()
                print("Location List arrayy : \(self.clubLocationCitiesArray)")
                
            }
        }catch{
            print("error")
        }
        
        
    }
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    var x = 1
    
    @objc func autoScroll() {
        if self.x < self.bannerImagesArray.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.bannerCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        } else {
            self.x = 0
            self.bannerCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    
 }

 
