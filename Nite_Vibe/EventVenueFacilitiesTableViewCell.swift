//
//  EventVenueFacilitiesTableViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 15/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class EventVenueFacilitiesTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var facilityNameListLbl: UILabel!
    
    @IBOutlet weak var facilityImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
