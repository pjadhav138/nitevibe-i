//
//  ClubHomePage_VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 29/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

import Alamofire

class ClubHomePage_VC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
  
   
    @IBOutlet weak var txt_location: UITextField!
    
   // @IBOutlet weak var txt_location: UITextField!
   // @IBOutlet weak var banner_Images: UIImageView!
   // @IBOutlet weak var banner_UIView: UIView!
   // @IBOutlet var banner_collectionView: UICollectionView!
   // @IBOutlet var Club_collectionView: UICollectionView!
    
    
    @IBOutlet weak var tblDropDown: UITableView!
    // @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var tblDropDownHC: NSLayoutConstraint!
    
    let clubID = String()
    let clubNmae = String()
    let favouriteCount = String()
    let totalCount = String()
    let maleCount = String()
    let femaleCount = String()
    let clubPicture = String()
    let clubDate = String()
    let totalRatio = String()
    var bannerList = [String]()
    var messageArray = [String]()
    var clubArray = [String]()
    
    
    
    let cellReuseIdentifier = "cell"
   
    var locationList = [String]()
//    var BannerImage: [UIImage] = [UIImage(named: "banner")!,UIImage(named: "banner11")!,UIImage(named: "banner12")!,UIImage(named: "banner13")!]
//    var ClubImageArray: [UIImage] = [UIImage(named: "club1")!,UIImage(named: "club2")!,UIImage(named: "club3")!,UIImage(named: "club4")!]
    
    
    @IBAction func textFieldEditingDidChanged(_ sender: Any) {
        tblDropDown.isHidden = true

    }
    
    
//    @IBAction func textFieldEditingDidChanged(_ sender: Any) {
//
//         tblDropDown.isHidden = true
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDropDown.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        tblDropDown.delegate = self
        tblDropDown.dataSource = self
        txt_location.delegate = self
//        banner_collectionView.delegate = self
//        Club_collectionView.delegate = self
//        banner_collectionView.dataSource = self
//        Club_collectionView.dataSource = self
        
        tblDropDown.isHidden = true

        txt_location.addTarget(self, action: #selector(textFieldActive), for: UIControlEvents.touchDown)
        
        self.getLocation()
      
       
    }
    
  
    func getLocation(){
    
    let URL_String = "http://www.nitevibe.in/webservices/location_list.php"
    
    Alamofire.request(URL_String, method: .get).responseJSON{
        (response) in
        
          print(" response :\(String(describing: response.request))")

        guard response.result.isSuccess  else {
            
            print("Block 1 ")
            print(" response :\(response)")
            
            print("Error with response: \(String(describing: response.result.error))")
            
            return
            
        }
        guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
            
            print(" response :\(response)")
            
            print("Error with dictionary: \(String(describing: response.result.error))")
            
            return
            
        }
        print("message\(dict)")
        print("dict respo: \(dict["response"]!)")
        print("dict message:\(dict["message"]!)")
        
        guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
         print("Error with dictionary data: \(String(describing: response.result.error))")
          return
         }
        print(" dict data\(dictData)")
        
        for data in dictData{
            
           let location = data["loc"] as! String
            
            print("club location \(location)")
            
            self.locationList.append(location)
            
            let setUserLocation :UserDefaults = UserDefaults.standard
            
            setUserLocation.set(location,forKey:"location")
            setUserLocation.value(forKey: "location")
            
            print("User Default id\(setUserLocation.value(forKey: "location")!)")
        }
        self.tblDropDown.reloadData()
        print("Location List arrayy : \(self.locationList)")
       
    }
    
    }
    
    

//    @objc func textFieldChanged(_ sender : AnyObject){
//
//         tblDropDown.isHidden = true
//
//
//    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//       tblDropDown.isHidden = false
//        var substring = textField.text
//        substring?.replacingCharacters(in: range, with: String)
//
//
//       // var substring = textField.text ?? ""
////       var substring = (substring as NSString).replacingCharacters(in: range, with: string)
//       searchAutocompleteEntries(withSubstring: substring)
//        return true
//    }
//
//    func searchAutocompleteEntries(withSubstring substring: String?) {
//        // Put anything that starts with this substring into the autocompleteUrls array
//        // The items in this array is what will show up in the table view
//       values.removeAll()
//        for curString: String in values {
//            let substringRange: NSRange? = (curString as NSString).range(of: substring ?? "")
//            if substringRange?.location == 0 {
//                values.append(curString)
//            }
//        }
//        tblDropDown.reloadData()
//    }


    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let touch:UITouch = touches.first else
        {
            return;
        }
        if touch.view != tblDropDown
        {
           txt_location.endEditing(true)
            tblDropDown.isHidden = true

        }
    }
    
  
    @objc func textFieldActive() {
        tblDropDown.isHidden = !tblDropDown.isHidden
    }
    
   
    // MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        // TODO: Your app can do something when textField finishes editing
        print("The textField ended editing. Do something based on app requirements.")
       // self.getClubData()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txt_location.resignFirstResponder()
        return true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationList.count;
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        // Set text from the data model
        //cell.textLabel?.text = self.locationList[indexPath.row]
        cell.textLabel?.text = locationList[indexPath.row]
        print("indexPath: \(indexPath.row)")
        cell.textLabel?.font = txt_location.font
        
        return cell
    }

   

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Row selected, so set textField to relevant value, hide tableView
        // endEditing can trigger some other action according to requirements
       // txt_location.text = self.locationList[indexPath.row]
        
        print("Selected index \(locationList[indexPath.row])")
        
        txt_location.text = locationList[indexPath.row]
        
        let locationCity = locationList[indexPath.row]
        print("Selected Location: \(txt_location!)")
        
        tblDropDown.isHidden = true
        txt_location.endEditing(true)
        
        
        let setLoactionCity:UserDefaults = UserDefaults.standard
        
        setLoactionCity.set(locationCity, forKey: "selectedlocationcity")
        self.getClubData()
        /*
         textField.text = values[indexPath.row]
         tableView.isHidden = true
         textField.endEditing(true)
        */
    }
    
   
    
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }

    
    
    func getClubData(){
        // let URL_ClubString = "http://www.nitevibe.in/webservices/club_details.php"
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        let User_Location = getUserData.value(forKey: "selectedlocationcity")
        print("userid :\(String(describing: User_Id))!)")
        
        
        //et userid = String(describing: User_Id)
        //let userlocation = String(describing: User_Location)
        //print("location :\(String(describing: User_Location))!)")
  
        
        //let URL_ClubString = "http://www.nitevibe.in/webservices/club_details.php?userid=\(userid)&location=\(userlocation)"
        let URL_ClubString = "http://www.nitevibe.in/webservices/test_club_details.php?userid=\(User_Id!)&loc=\(User_Location!)"
        
        print("Club String\(URL_ClubString)")

//        let parameters: Parameters = [
//
//            "userid" : User_Id,
//            "location" : User_Location
//
//        ]
        
       

        
        
        Alamofire.request(URL_ClubString, method: .get).responseJSON{
            
            (response) in

            print("alamofire Request: \(String(describing: response.request))")

            print(" response :\(String(describing: response.request))")

            guard response.result.isSuccess  else {

                print("test1")

                print("Block 1 ")
                print(" response :\(response)")

                print("Error with response: \(String(describing: response.result.error))")

                return

            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                  print("test 2")
                print(" response :\(response)")

                print("Error with dictionary: \(String(describing: response.result.error))")

                return

            }
            print("message\(dict)")
            print("dict respo: \(dict["response"]!)")
            print("dict message:\(dict["message"]!)")

            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                 print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" dict data\(dictData)")


        }
        
    }
    
 
    
    
    
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1
//    }
//


    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        if collectionView == self.banner_collectionView {
//
//
//            return BannerImage.count // Replace with count of your data for collectionViewA
//        }
//
//         ClubImageArray.count // Replace with count of your data for collectionViewB
//    }

 

    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        if collectionView == self.banner_collectionView {
//
////            let cellBanner = banner_collectionView.dequeueReusableCell(withReuseIdentifier: "Banner_cell", for: indexPath) as! BannerCollectionViewCell
//
////            cellBanner.banner_Images.image = BannerImage[indexPath.row]
////
////            // Set up cell
////            return cellBanner
//        }
//
////         if collectionView == self.Club_collectionView {
//            let cellClub = Club_collectionView.dequeueReusableCell(withReuseIdentifier: "Club_cell", for: indexPath) as! HomePageCollectionViewCell_VC
//
//            cellClub.ClubImage.image = ClubImageArray[indexPath.row]
//
//          //  cellClub.ClubImage.image = ClubImageArray[indexPath.row]
//
//
//            return cellClub
////        }
//
//    }


}


