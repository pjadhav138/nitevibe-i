//
//  ClubDetailScreenViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 14/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import QuartzCore
import MessageUI
import SwiftyJSON
import Kingfisher
import SwiftIconFont
import CTShowcase
var CheckInLaunch = ""

class ClubDetailScreenViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,CLLocationManagerDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    @IBOutlet weak var clubnamelblTextview: UITextView!
    @IBOutlet weak var ratinglbl: UILabel!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var costOfTwoconstrainlbl: UILabel!
    @IBOutlet weak var rayUIHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCouponHeight: NSLayoutConstraint!
    @IBOutlet weak var whoscheckinCollectionheight: NSLayoutConstraint!
    @IBOutlet weak var lblWhoscheckinHeight: NSLayoutConstraint!
    @IBOutlet weak var eventCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblEvent: NSLayoutConstraint!
    @IBOutlet weak var facilityTblHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lbl_facilities: UILabel!
    @IBOutlet weak var lbl_coupon: UILabel!
    @IBOutlet weak var lbl_whosCheckedIn: UILabel!
    @IBOutlet weak var lbl_HappyHours: UILabel!
    var StagCoupon_Constraints:[NSLayoutConstraint] = []
    var clubnamelabel:[NSLayoutConstraint] = []
    var faciltylLblConstraints:[NSLayoutConstraint] = []
    var faciltylTableConstraints:[NSLayoutConstraint] = []
    var checkInCollectionConstraints:[NSLayoutConstraint] = []
    var locationManager = CLLocationManager()
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var couponHeight: CGFloat!
    var attributedText = NSMutableAttributedString()
    let currentDateTime = Date()
    let formatter = DateFormatter()
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var clubFeatureImagesCollectionView: UICollectionView!
    @IBOutlet weak var tblHeightShrinkLayout: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var clubNameLabel: UILabel!
    @IBOutlet weak var clubTimingStatusLabel: UILabel!
    @IBOutlet weak var costOfTwo: UILabel!
    @IBOutlet weak var clubHappyHoursLbl: UILabel!
    @IBOutlet weak var clubEventListCollectionView: UICollectionView!
    @IBOutlet weak var facilitiesTableView: UITableView!
    @IBOutlet weak var userGoingCollectionView: UICollectionView!
    @IBOutlet weak var starRating1: UIImageView!
    @IBOutlet weak var starRating2: UIImageView!
    @IBOutlet weak var starRating3: UIImageView!
    @IBOutlet weak var starRating4: UIImageView!
    @IBOutlet weak var starRating5: UIImageView!
    @IBOutlet weak var CircularBar: RayUIView!
    @IBOutlet weak var mapLocationbtnOutlet: UIButton!
    @IBOutlet weak var callingbtnOutlet: UIButton!
    @IBOutlet weak var eventMaleCountLabel: UILabel!
    @IBOutlet weak var eventFemaleCountLabel: UILabel!
    @IBOutlet weak var couponCountLabel: UILabel!
    @IBOutlet weak var whosCheckInLabel: UILabel!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var overLayView: UIView!
    @IBOutlet weak var splitTimeLbl: UILabel!
    var clubArray = [UIImage(named:"club1"),UIImage(named:"club2")]
    var AppDelegateLocationLatitude = Double()
    var AppDelegateLocationLongitude = Double()
    var distanceInMeters = Double()
    var clubIDFromRadiusAPI = String()
    var eventCheckInStatus = Int()
    var eventCheckInNotificationReceivedStatus = Int()
    var lat = Double()
    var long = Double()
    var ClubRADIUS = Double()
    var eventID = String()
    var eventName = String()
    var eventCoupon = String()
    var eventCheckin = String()
    var eventCheckinNotificationReceived = String()
    var ClubEventArray = [AnyObject]()
    var couponStarts = String()
    var couponEnds = String()
    var timer = Timer()
    var flagValue = String()
    var eventflagValue = String()
    var club_id = String()
    var club_favourite_mode = "0"
    var event_favourite_mode = "1"
    var clubName = String()
    var clubTiming = String()
    var clubStatus = String()
    var clubCostForTwo = String()
    var clubHappyHours = String()
    var eventCount = Int()
    var clubRate = String()
    var clubPicDataArray:[String] = []
    var progressValue:Float = 0 // progress view data
    var clubMaleCount:Float = 0 // club male count
    var clubFemaleCount:Float = 0 // club float count
    var clubEventNameArray:[String] = []
    var clubEventImageArray:[String] = []
    var clubEventDateArray:[String] = []
    var clubEventMaleCountArray:[Float] = []
    var peopeleAtEventArray:[Int] = []
    var costOfTwoArray:[String] = []
    var userGoingImageArray:[String] = []
    var facilitiesArray = ["Parking","Dinner","Lunch"]
    var facilityNameArray:[String] = []
    var facilityImageArray:[String] = []
    var selectedClubId = ""
    var userClubEntryType = String()
    var setTimmer = Timer.self
    var clubEventMaleCountIntArray:[Int] = []
    var clubEventFemaleCountIntArray:[Int] = []
    var eventIdArray:[String] = []
    var whosCheckInData = [WhosCheckInModel]()
    var clubDetailData = [ClubDetailModel]()
    var prefAgeStringOfUser = String()
    var clubContactNo = String()
    var selectedUserCheckInEvent = String()
    var eventFavArray = [String]()
    var userLat = Double()
    var userLong = Double()
    var SendUSerIDToMap = ""
    var selectedEventId = String()
    var userAlreadyCheckInFlag = String()
    var selectedEventName =  String()
    var selectedEventAddress = String()
    var selectedEventDate = String()
    var selectedEventStarTime = String()
    var selectedEventEndTime = String()
    var selectedEventStagEntryFees = String()
    var selectedEventCoupleEntryFees = String()
    @IBOutlet weak var checkInButtonOutlet: UIButton!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var userProfileButtonOutlet: UIButton!
    @IBOutlet weak var homeButtonOutlet: UIButton!
    @IBOutlet weak var chatButtonOutlet: UIButton!
    let fav_btn = UIButton(type: UIButtonType.custom) as UIButton
    @IBOutlet weak var overLayEffectView: UIVisualEffectView!
    var selectedCoupon = String()
    var clubFavFlagString = String()
    var selectedCouponFlag = String()
    var userPrefFlag = Int()
    var OverlayCheckIn = "CheckIn First Time Launch"
    var segmentView: SMSegmentView!
    let appearance = SMSegmentAppearance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.couponCountLabel.text = nil
         timer = Timer.scheduledTimer(timeInterval: 600 , target: self, selector: #selector(sendNotificationToUser), userInfo: nil, repeats: true)
        let defaults = UserDefaults.standard
        let getUserData :UserDefaults = UserDefaults.standard
        self.SendUSerIDToMap = getUserData.value(forKey: "userid") as! String
        print("userid :\(String(describing: self.SendUSerIDToMap))")
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        couponHeight = lbl_coupon.frame.height
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth, height: 289 )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        clubFeatureImagesCollectionView.collectionViewLayout = layout
        self.clubFeatureImagesCollectionView.delegate = self
        self.clubFeatureImagesCollectionView.dataSource = self
        self.clubEventListCollectionView.delegate = self
        self.clubEventListCollectionView.dataSource = self
        self.facilitiesTableView.delegate = self
        self.facilitiesTableView.dataSource = self
        self.userGoingCollectionView.delegate = self
        self.userGoingCollectionView.dataSource = self
        checkInButtonOutlet.layer.cornerRadius = 10
        checkInButtonOutlet.clipsToBounds = true
        pageController.frame = CGRect(x: 21, y: 240, width: 50, height: 15)
        
        getClubDetailData()
        swiftyWhosCheckInData()
        fetchswiftyClubDetailData()
        self.clubFeatureImagesCollectionView.reloadData()
        self.clubEventListCollectionView.reloadData()
        self.facilitiesTableView.reloadData()
        self.userGoingCollectionView.reloadData()
        contentView.sizeToFit()
       // pageController.sizeToFit()
        clubNameLabel.sizeToFit()
        clubTimingStatusLabel.sizeToFit()
        costOfTwo.sizeToFit()
        clubHappyHoursLbl.sizeToFit()
        facilitiesTableView.sizeToFit()
        userGoingCollectionView.sizeToFit()
        self.clubEventListCollectionView.layer.masksToBounds = true
        self.clubEventListCollectionView.layer.cornerRadius = 12
        if selectedCouponFlag == "0" || selectedCouponFlag == "1"{
            defaults.set("1", forKey: "checkflagentrytype")
        }else {
            defaults.set("0", forKey: "checkflagentrytype")
        }
        tabBarView.addSubview(homeButtonOutlet)
        tabBarView.addSubview(userProfileButtonOutlet)
        tabBarView.addSubview(chatButtonOutlet)
        view.addSubview(tabBarView)
        homeButtonOutlet.addTarget(self, action: #selector(homeButtonTapped), for: .touchUpInside)
        userProfileButtonOutlet.addTarget(self, action: #selector(userProfileButtonTapped), for: .touchUpInside)
        chatButtonOutlet.addTarget(self, action: #selector(chatButtonTapped), for: .touchUpInside)
        appearance.segmentOnSelectionColour = UIColor(red: 255.0/255.0, green: 54.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        appearance.segmentOffSelectionColour = UIColor.white
        appearance.titleOnSelectionFont = UIFont.systemFont(ofSize: 12.0)
        scrollview.resizeScrollViewContentSize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.checkInOverlay()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    func checkInOverlay()
    {
        
        let checkINLaunchedBefore = UserDefaults.standard.bool(forKey: "checkINLaunchedBefore")
        
        if checkINLaunchedBefore {
            
            CheckInLaunch = "CheckIn Has Launched Before"
            print("SwipeBeforeLaunchStatus : \(CheckInLaunch)")
            
        }
        else{
            
            CheckInLaunch = "CheckIn First Time Launch"
            print("SwipeFirstLaunchStatus : \(CheckInLaunch)")
            self.CheckINOverLay()
            UserDefaults.standard.set(true, forKey: "checkINLaunchedBefore")
            
        }
        
    }
    
    func CheckINOverLay(){
        
        print("Print EventDetailsRatio Launch Status : \(OverlayCheckIn)")
        print("Print UserDefaultsRatio Launch Status : \(CheckInLaunch)")
        
        if OverlayCheckIn == CheckInLaunch{
            
            
            
            let showcase = CTShowcaseView(title: "", message: "", image: #imageLiteral(resourceName: "CheckIn1.png"), key: nil) { () -> Void in
                print("dismissed")
            }
            
            let highlighter = CTDynamicGlowHighlighter()
            highlighter.highlightColor = UIColor.white
            highlighter.animDuration = 0.5
            highlighter.glowSize = 5
            highlighter.maxOffset = 10
            
            showcase.highlighter = highlighter
            
            showcase.setup(for: self.checkInButtonOutlet, offset: CGPoint.zero, margin: 0)
            showcase.show()
        }
        else{
            
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.facilityTblHeight?.constant = self.facilitiesTableView.contentSize.height
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
         let location = locations.last! as CLLocation

        AppDelegateLocationLatitude = (locationManager.location?.coordinate.latitude)!
        AppDelegateLocationLongitude = (locationManager.location?.coordinate.longitude)!
        
        let locationUserDefaults = UserDefaults.standard
        locationUserDefaults.set(AppDelegateLocationLatitude, forKey: "AppDelegateLocationLatitude")
        locationUserDefaults.set(AppDelegateLocationLongitude, forKey: "AppDelegateLocationLongitude")
        
        let locationLatitude = locationUserDefaults.value(forKey: "AppDelegateLocationLatitude")
        let locationLongitude = locationUserDefaults.value(forKey: "AppDelegateLocationLongitude")
        
        print("UserLocationLatitude : \(locationLatitude!)")
        print("UserLocationLatitude : \(locationLongitude!)")
        
        let myLat = location.coordinate.latitude
        let myLong = location.coordinate.longitude
        
        print("Latitude : \(myLat)")
        print("Longitude : \(myLong)")
  
    }
    
    
    @objc func sendNotificationToUser(){
        
        getClubRadius()
        
        
    }
    
    func getClubRadius(){
        
        
        URLCache.shared.removeAllCachedResponses()
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let URL_ClubString = "http://www.nitevibe.in/webservices/user_checkin_radius.php?user_id=\(User_Id!)"
        
        print("Notification String\(URL_ClubString)")
        
        let escapeUrl = URL_ClubString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: escapeUrl!)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON{
            
            
            (response) in
            
            print("alamofire Request: \(String(describing: response.request))")
            
            print(" Notification response :\(String(describing: response.request))")
            
            guard response.result.isSuccess  else {
                
                print("test1")
                
                print("Block 1 ")
                print(" response :\(response)")
                
                print("Error with response: \(String(describing: response.result.error))")
                
                return
                
            }
            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                print("test 2")
                print("response :\(response)")
                
                print("Error with dictionary: \(String(describing: response.result.error))")
                
                return
                
            }
            
            print("dict message:\(dict["message"]!)")
            
            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }
            print(" Event dict data : \(dictData)")
            
            
            for data in dictData{
                
                let clubID =  data["club_id"] as! String
                print("club ID :\(String(describing: clubID))")
                self.clubIDFromRadiusAPI = clubID
                
                let clubName =  data["club_name"] as! String
                print("club Name :\(String(describing: clubName))")
                
                
                let clubLat =  data["lat"] as! String
                print("club Lat :\(String(describing: clubLat))")
                self.lat = Double(clubLat)!
                print("clubLatitude : \(self.lat)")
                
                
                let clubLong =  data["lng"] as! String
                print("club Long :\(String(describing: clubLong))")
                self.long = Double(clubLong)!
                print("clubLongitude : \(self.long)")
                
                
                let clubRadius =  data["radius"] as! String
                print("club Radius :\(String(describing: clubRadius))")
                self.ClubRADIUS = Double(clubRadius)!
                
                let clubEvents =  data["event"] as? [Dictionary <String,AnyObject>]
                print("club Events :\(String(describing: clubEvents))")
                
                
                for eventData in clubEvents!{
                    
                    print("EventDATA : \(eventData)")
                    
                    self.eventID = eventData["event_id"] as! String
                    print("Event ID : \(self.eventID)")
                    let setEventId = UserDefaults.standard
                    setEventId.set(self.eventID, forKey: "eventID")
                    let EventID = setEventId.value(forKey: "eventID")
                    print("EventIdforCheckOutNotification : \(String(describing: EventID))")
                    
                    self.eventName = eventData["event_name"] as! String
                    print("Event Name : \(self.eventName)")
                    
                    self.eventCoupon = eventData["event_coupon"] as! String
                    print("Event Coupon : \(self.eventCoupon)")
                    
                    self.eventCheckin = eventData["checkin"] as! String
                    print("Event Checkin : \(self.eventCheckin)")
                    
                    self.eventCheckInStatus = Int(self.eventCheckin)!
                    print("EventCheckIn : \(self.eventCheckInStatus)")
                    
                    self.eventCheckinNotificationReceived = eventData["received"] as! String
                    print("Event Checkin Notification Received : \(self.eventCheckinNotificationReceived)")
                    self.eventCheckInNotificationReceivedStatus = Int(self.eventCheckinNotificationReceived)!
                    print("Event Checkin Notification Received : \(self.eventCheckInNotificationReceivedStatus)")
                    
                }
                
                self.ClubEventArray.append(clubEvents as AnyObject)
                print("EventArray : \(self.ClubEventArray)")
                self.locationManager.startUpdatingLocation()
                let getmyLocation :UserDefaults = UserDefaults.standard
                let myLatLocation = getmyLocation.value(forKey: "AppDelegateLocationLatitude")
                let myLongLocation = getmyLocation.value(forKey: "AppDelegateLocationLongitude")
                print("myLatLocation :\(String(describing: myLatLocation))")
                print("myLongLocation :\(String(describing: myLongLocation))")
                
                let coordinate₀ = CLLocation(latitude:myLatLocation as! Double, longitude:myLongLocation as! Double)
                print("MyCordinates : \(coordinate₀)")
                
                let coordinate₁ = CLLocation(latitude:self.lat, longitude:self.long)
                print("ClubCordinates : \(coordinate₁)")
                
                self.distanceInMeters = coordinate₀.distance(from: coordinate₁)
                print("distanceInMeters : \(self.distanceInMeters)")
                print("CheckINSTATUS : \(self.eventCheckInStatus)")
                print("ClubRadius : \(self.ClubRADIUS)")
                
                if self.eventCount == 0 {
                    
                }else{
                
                if(self.distanceInMeters <= self.ClubRADIUS && self.eventCheckInStatus == 0 && self.eventCheckInNotificationReceivedStatus == 0)
                {
                    let defaults = UserDefaults.standard
                    
                    let checkInNotificationflag =  defaults.value(forKey: "checkInNotificationFlag") as? String
                    
                    print("checkINFlag :\(checkInNotificationflag)")
                    
                    
                    
                    print("checkINflag if already ON:\(checkInNotificationflag)")
                    
                    print("You are Inside Radius")
                    self.sendNotification()
                    
                    
                    //  }
                    
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                    
                    if(self.distanceInMeters > self.ClubRADIUS && self.eventCheckInStatus == 1){
                        
                        self.updateUserCheckOut()
                        
                    }
                    
                })
            }
                
                
            }
            
            
        }
        
        
    }
    
    
    
    
    func sendNotification(){
        // print("ClubID : \(club_id)")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        // print("selected club id \(selectedClubId)")
        let URL_String = "http://www.nitevibe.in/webservices/checkin_notification.php?user_id=\(User_Id!)&club_id=\(clubIDFromRadiusAPI)"
        
        print("test Notification URL : \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response Noti :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
        
        
    }
    
    func fetchNumberOFCoupons(){
        let urlString = "http://www.nitevibe.in/webservices/club_coupon.php?club_id=\(selectedClubId)"
        
        Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                
                            }
                        }
                    
                    }
                }catch {
                    print("cannot convert to Json")
                }
            }
        }
    }
    
    func updateUserCheckOut(){
        // print("ClubID : \(club_id)")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        //        let Event_Id = getUserData.value(forKey: "userid")
        //        print("userid :\(String(describing: Event_Id))")
        
         print("selected event id \(eventID)")
        let URL_String = "http://www.nitevibe.in/webservices/check_out.php?user_id=\(User_Id!)&event_id=\(eventID)"
        
        print("test Notification URL : \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response Noti :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
        
        
    }
    
    
    
   
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
//    {
//
//        let location = locations.last! as CLLocation
//
//        /* you can use these values*/
//
//    }
    
    
    
    //    override func viewWillLayoutSubviews() {
    //        super.updateViewConstraints()
    //        self.tblHeightShrinkLayout?.constant = self.facilitiesTableView.intrinsicContentSize.height
    //    }
    
    //    override func viewDidAppear(_ animated: Bool) {
    //        var contentRect = CGRect.zero
    //
    //        for view in scrollView.subviews {
    //            contentRect = contentRect.union(view.frame)
    //        }
    //        scrollView.contentSize = contentRect.size
    //    }
    
    //    @IBAction func useCouponButtonAction(_ sender: UIButton) {
    //        if useCouponBtnOutlet.isTouchInside {
    //            CircularBar.counter += 1
    //        } else {
    //            if CircularBar.counter > 0 {
    //                CircularBar.counter -= 1
    //            }
    //        }
    //        usedCouponCountLabel.text = String(CircularBar.counter)
    //    }
    
    
    
    func swiftyWhosCheckInData(){
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        print("selected club ID on club detail screen \(selectedClubId)")
        
        
        //let URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=22&club_id=1"
        
        //let urlString = "http://www.nitevibe.in/webservices/club.php?user_id=22&club_id=1"
        let urlString = "http://www.nitevibe.in/webservices/whos_checkin.php?user_id=\(User_Id!)&club_id=\(selectedClubId)"
        
        print("user string ..\(urlString)")
        Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            
                            if messageArray.count == 0{
                                
                                print("Empty Array")
                                self.lbl_whosCheckedIn.isHidden = true
                                self.lblWhoscheckinHeight.constant = 0
                                self.userGoingCollectionView.isHidden = true
                                self.whoscheckinCollectionheight.constant = 0
                                
//                                let segmentFrame = CGRect(x: 15, y: self.couponHeight + 525, width: 95, height: 80)
//                                //
//                                self.segmentView = SMSegmentView(frame: segmentFrame, dividerColour: UIColor(white: 0.95, alpha: 0), dividerWidth: 13, segmentAppearance: self.appearance)
//                                self.segmentView.backgroundColor = UIColor.clear
//
//                                self.segmentView.layer.cornerRadius = 5.0
//                                self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 0).cgColor
//                                self.segmentView.layer.borderWidth = 1.0
//
//                                // Add segments
//                                self.segmentView.addSegmentWithTitle("Stag", onSelectionImage: UIImage(named: "clip_light"), offSelectionImage: UIImage(named: "clip"))
//
//                                self.segmentView.addSegmentWithTitle("Couple", onSelectionImage: UIImage(named: "bulb_light"), offSelectionImage: UIImage(named: "bulb"))
//                                // self.segmentView.addSegmentWithTitle("Cloud", onSelectionImage: UIImage(named: "cloud_light"), offSelectionImage: UIImage(named: "cloud"))
//                                //segmentView.organiseMode = .vertical
//                                self.segmentView.addTarget(self, action: #selector(self.selectSegmentInSegmentView(segmentView:)), for: .valueChanged)
//
//                                // Set segment with index 0 as selected by default
//                                self.segmentView.selectedSegmentIndex = UISegmentedControlNoSegment
//                                self.contentView.addSubview(self.segmentView)
//
//                                let topKConstraint = self.couponLabel.topAnchor.constraint(equalTo: self.clubEventListCollectionView.topAnchor, constant: 90)
//                               // let bottomCKConstraint = couponLabel.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
//                                let leftKConstraint = self.couponLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
//                                let rightKConstraint = self.couponLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 194)
//                                self.checkInCollectionConstraints = [topKConstraint,leftKConstraint,rightKConstraint]
//                                NSLayoutConstraint.activate(self.checkInCollectionConstraints)
//
                                
                                
                            }else{
                                
                                for arr in messageArray {
                                    
                                    
                                    self.whosCheckInData.append(WhosCheckInModel(whosJSON: arr))
                                    
//                                    self.lbl_whosCheckedIn.isHidden = false
//                                    self.lblWhoscheckinHeight.constant = 21
//                                    self.userGoingCollectionView.isHidden = false
//                                    self.whoscheckinCollectionheight.constant = 65
//
                                    print("print whos check in count array  \(self.whosCheckInData.count)")
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.userGoingCollectionView.reloadData()
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                print("who check in user age  \(self.whosCheckInData[0].user_age)")
                                print("who check in user  id \(self.whosCheckInData[0].user_id)")
                                print("who check in user photo \(self.whosCheckInData[0].user_photo)")
                                print("who check in user gender \(self.whosCheckInData[0].user_gender)")
                                
                            }
                            
                            
                        }
                        
                        
                        
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
            
            
        }
    }
    
    
    
    func fetchswiftyClubDetailData(){
        
        var URL_ClubString = String()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        print("selected club ID on club detail screen \(selectedClubId)")
        URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=\(User_Id!)&club_id=\(selectedClubId)"
        
        print("print club string on club detail api:\(URL_ClubString)")
        //URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=22&club_id=1"
        
        
        Alamofire.request(URL_ClubString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            if messageArray.count == 0{
                                
                                print("Empty Message Array")
                                
                                
                            }else{
                                
                                for arr in messageArray {
                                    
                                    
                                    self.clubDetailData.append(ClubDetailModel(clubDetailJSON: arr))
                                    
                                    
                                    print("check pref userAge Count \(self.clubDetailData[0].userPreferenceDictionary.count)")
                                    
                                    
                                    
                                    if self.clubDetailData[0].userPreferenceDictionary.count == 0 {
                                        
                                        print(" Empty userPreferenceDictionary data  ")
                                        //Do nothing
                                    }else{
                                        
//                                        if self.clubDetailData[0].todaysEventDictionary.count == 0{
//
//                                        }else{
                                        
                                        self.prefAgeStringOfUser = self.clubDetailData[0].userPreferenceDictionary[0].pref_age
                                        print("print clubData count array  \(self.clubDetailData.count)")
                               
                                        if self.clubDetailData[0].todaysEventDictionary.count == 0 && self.whosCheckInData.count == 0{
                                            
                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
                                            print("checkINUserCount : \(self.whosCheckInData.count)")
                                            
                                            self.eventLbl.isHidden = true
                                            self.lblEvent.constant = 0
                                            self.clubEventListCollectionView.isHidden = true
                                            self.eventCollectionHeight.constant = 0
                                            //self.whosCheckInLabel.isHidden = true
                                            self.lblWhoscheckinHeight.constant = 0
                                            self.userGoingCollectionView.isHidden = true
                                            self.whoscheckinCollectionheight.constant = 0
                                            self.CircularBar.isHidden = true
                                            self.rayUIHeight.constant = 0
                                           
                                            
//                                            let Height = myLoginButton.heightAnchor.constraint(equalToConstant: 60)
//                                            let buttonWidth = myLoginButton.widthAnchor.constraint(equalToConstant: 275)
                                            let topConstraint = self.couponLabel.topAnchor.constraint(equalTo: self.costOfTwoconstrainlbl.topAnchor, constant: 30)
//                                            let bottomConstraint = lbl_facilities.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
                                            let leftConstraint = self.couponLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
                                            let rightConstraint = self.couponLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 194)
                                            self.faciltylLblConstraints = [topConstraint,leftConstraint,rightConstraint]
                                            NSLayoutConstraint.activate(self.faciltylLblConstraints)
                                            
                                           // self.segmentView.removeFromSuperview()
                                            
                                            let segmentFrame = CGRect(x: 15, y: self.couponHeight + 525, width: 95, height: 80)
                                            //
                                            self.segmentView = SMSegmentView(frame: segmentFrame, dividerColour: UIColor(white: 0.95, alpha: 0), dividerWidth: 13, segmentAppearance: self.appearance)
                                            self.segmentView.backgroundColor = UIColor.clear
                                            
                                            self.segmentView.layer.cornerRadius = 5.0
                                            self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 0).cgColor
                                            self.segmentView.layer.borderWidth = 1.0
                                            
                                            // Add segments
                                            self.segmentView.addSegmentWithTitle("Stag", onSelectionImage: UIImage(named: "clip_light"), offSelectionImage: UIImage(named: "clip"))
                                            
                                            self.segmentView.addSegmentWithTitle("Couple", onSelectionImage: UIImage(named: "bulb_light"), offSelectionImage: UIImage(named: "bulb"))
                                            // self.segmentView.addSegmentWithTitle("Cloud", onSelectionImage: UIImage(named: "cloud_light"), offSelectionImage: UIImage(named: "cloud"))
                                            //segmentView.organiseMode = .vertical
                                            self.segmentView.addTarget(self, action: #selector(self.selectSegmentInSegmentView(segmentView:)), for: .valueChanged)
                                            
                                            // Set segment with index 0 as selected by default
                                            self.segmentView.selectedSegmentIndex = UISegmentedControlNoSegment
                                            self.contentView.addSubview(self.segmentView)
                                            
                                           
                                            print("empty todaysEventDictionary ")
                                            
                                        }else if self.clubDetailData[0].todaysEventDictionary.count != 0 && self.whosCheckInData.count == 0{
                                            
                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
                                            print("checkINUserCount : \(self.whosCheckInData.count)")
                                            
                                            //self.whosCheckInLabel.isHidden = true
                                            self.lblWhoscheckinHeight.constant = 0
                                            self.userGoingCollectionView.isHidden = true
                                            self.whoscheckinCollectionheight.constant = 0
                                            
                                            
//                                            self.eventLbl.isHidden = false
//                                            self.lblEvent.constant = 21
//                                            self.clubEventListCollectionView.isHidden = false
//                                            self.eventCollectionHeight.constant = 196
//                                            self.whosCheckInLabel.isHidden = true
//
//                                            self.CircularBar.isHidden = false
//                                            self.rayUIHeight.constant = 115
                                            
//                                            let topConstraint = self.couponLabel.topAnchor.constraint(equalTo: self.clubEventListCollectionView.topAnchor, constant: 40)
//                                            //                                            let bottomConstraint = lbl_facilities.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
//                                            let leftConstraint = self.couponLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
//                                            let rightConstraint = self.couponLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 194)
//                                            self.faciltylLblConstraints = [topConstraint,leftConstraint,rightConstraint]
//                                            NSLayoutConstraint.activate(self.faciltylLblConstraints)
                                            
                                           // self.segmentView.removeFromSuperview()
                                            
                                            let segmentFrame = CGRect(x: 15, y: self.couponHeight + 795, width: 95, height: 80)
                                            //
                                            self.segmentView = SMSegmentView(frame: segmentFrame, dividerColour: UIColor(white: 0.95, alpha: 0), dividerWidth: 13, segmentAppearance: self.appearance)
                                            self.segmentView.backgroundColor = UIColor.clear
                                            
                                            self.segmentView.layer.cornerRadius = 5.0
                                            self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 0).cgColor
                                            self.segmentView.layer.borderWidth = 1.0
                                            
                                            // Add segments
                                            self.segmentView.addSegmentWithTitle("Stag", onSelectionImage: UIImage(named: "clip_light"), offSelectionImage: UIImage(named: "clip"))
                                            
                                            self.segmentView.addSegmentWithTitle("Couple", onSelectionImage: UIImage(named: "bulb_light"), offSelectionImage: UIImage(named: "bulb"))
                                            // self.segmentView.addSegmentWithTitle("Cloud", onSelectionImage: UIImage(named: "cloud_light"), offSelectionImage: UIImage(named: "cloud"))
                                            //segmentView.organiseMode = .vertical
                                            self.segmentView.addTarget(self, action: #selector(self.selectSegmentInSegmentView(segmentView:)), for: .valueChanged)
                                            
                                            // Set segment with index 0 as selected by default
                                            self.segmentView.selectedSegmentIndex = UISegmentedControlNoSegment
                                            self.contentView.addSubview(self.segmentView)
                                            
                                            
                                        }else{
                                            
                                            print("eventCount : \(self.clubDetailData[0].todaysEventDictionary.count)")
                                            print("checkINUserCount : \(self.whosCheckInData.count)")
                                            
                                            self.lbl_whosCheckedIn.isHidden = false
                                            self.lblWhoscheckinHeight.constant = 21
                                            self.userGoingCollectionView.isHidden = false
                                            self.whoscheckinCollectionheight.constant = 65
                                            
                                            let segmentFrame = CGRect(x: 15, y: self.couponHeight + 890, width: 95, height: 80)
                                            //
                                            self.segmentView = SMSegmentView(frame: segmentFrame, dividerColour: UIColor(white: 0.95, alpha: 0), dividerWidth: 13, segmentAppearance: self.appearance)
                                                    self.segmentView.backgroundColor = UIColor.clear
                                            
                                                    self.segmentView.layer.cornerRadius = 5.0
                                                    self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 0).cgColor
                                                    self.segmentView.layer.borderWidth = 1.0
                                            
                                                    // Add segments
                                                    self.segmentView.addSegmentWithTitle("Stag", onSelectionImage: UIImage(named: "clip_light"), offSelectionImage: UIImage(named: "clip"))
                                            
                                                    self.segmentView.addSegmentWithTitle("Couple", onSelectionImage: UIImage(named: "bulb_light"), offSelectionImage: UIImage(named: "bulb"))
                                                    // self.segmentView.addSegmentWithTitle("Cloud", onSelectionImage: UIImage(named: "cloud_light"), offSelectionImage: UIImage(named: "cloud"))
                                                    //segmentView.organiseMode = .vertical
                                            self.segmentView.addTarget(self, action: #selector(self.selectSegmentInSegmentView(segmentView:)), for: .valueChanged)
                                            
                                                    // Set segment with index 0 as selected by default
                                                    self.segmentView.selectedSegmentIndex = UISegmentedControlNoSegment
                                                    self.contentView.addSubview(self.segmentView)
                                            
//                                            //self.eventLbl.isHidden = false
//                                            self.lblEvent.constant = 21
//                                            //self.clubEventListCollectionView.isHidden = false
//                                            self.eventCollectionHeight.constant = 196
//                                         // self.whosCheckInLabel.isHidden = false
//                                            self.lblWhoscheckinHeight.constant = 21
//                                           // self.userGoingCollectionView.isHidden = false
//                                            self.whoscheckinCollectionheight.constant = 65
//                                           // self.CircularBar.isHidden = false
//                                            self.rayUIHeight.constant = 115
//
//                                            let topConstraint = self.couponLabel.topAnchor.constraint(equalTo: self.userGoingCollectionView.topAnchor, constant: 30)
//                                            //                                            let bottomConstraint = lbl_facilities.bottomAnchor.constraint(equalTo: self.splitterImageView.bottomAnchor, constant: -30)
//                                            let leftConstraint = self.couponLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
//                                            let rightConstraint = self.couponLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 194)
//                                            self.faciltylLblConstraints = [topConstraint,leftConstraint,rightConstraint]
//                                            NSLayoutConstraint.activate(self.faciltylLblConstraints)
                                            
//                                            self.contentView.layoutIfNeeded()
                                            
                                            if self.clubDetailData[0].todaysEventDictionary.count == 0{
                                                
                                                print("Do Nothing")
                                                
                                            }else{
                                        
                                            self.selectedUserCheckInEvent = self.clubDetailData[0].todaysEventDictionary[0].eventId_for_swipe
                                            
                                            print("user event id: \(self.selectedUserCheckInEvent)")
                                            
                                            //self.userAlreadyCheckInFlag = self.clubDetailData[0].todaysEventDictionary[0].event_check_in_flag
                                            
                                            
                                            //MARK:Not able to parse check in flag through swifty then parse through only Alamofire
                                            print("todays check in flag \(self.clubDetailData[0].todaysEventDictionary[0].event_check_in_flag)")
                                            
                                            }
                                        }
                                        
                                        print("user pref age \(self.clubDetailData[0].userPreferenceDictionary[0].pref_age)")
                                        print("user pref gender \(self.clubDetailData[0].userPreferenceDictionary[0].pref_gender)")
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                                if self.clubDetailData[0].todaysEventDictionary.count == 0{
                                    
                                    print(" Empty todays Event Data  ")
                                    
                                    
                                    self.CircularBar.isHidden = true
                                
                                    
                                }else{
                                    
                                    if self.clubDetailData[0].todaysEventDictionary[0].total_coupon_count == 0 {
                                        
                                        print("No Coupons Do Nothing")
                                        self.CircularBar.isHidden = true
                                        
                                    }else{
                                        //self.CircularBar.isHidden = false
                                        
//                                        self.formatter.timeStyle = .short
//                                        self.formatter.dateFormat = "hh:mm a"
//                                        self.formatter.string(from: self.currentDateTime)
//                                        let Date = self.formatter.string(from: self.currentDateTime)
//                                        print("CDate : \(Date)")
//                                        self.formatter.dateFormat = "hh:mm a"
//                                        let date = self.formatter.date(from: Date)
//                                        print("Current Device Date in String : \(date!)")
//                                        
                                        
                                    print("CouponStartTime\(self.clubDetailData[0].todaysEventDictionary[0].couponStarttime)")
                                    print("CouponStartTime\(self.clubDetailData[0].todaysEventDictionary[0].couponEndTime)")
                                        self.couponStarts = self.clubDetailData[0].todaysEventDictionary[0].couponStarttime
                                        self.couponEnds = self.clubDetailData[0].todaysEventDictionary[0].couponEndTime
                                    print("Total Coupons : \(self.clubDetailData[0].todaysEventDictionary[0].total_coupon_count)")
                                    print("TotalLeftCouponCount : \(self.clubDetailData[0].todaysEventDictionary[0].left_coupon_count)")
                                        RayUIView.Constants.numberOfCoupons = Int(self.clubDetailData[0].todaysEventDictionary[0].total_coupon_count)
                                    print("RayUIViewConstantsNumberOfCoupons : \(RayUIView.Constants.numberOfCoupons)")
                                        self.CircularBar.counter = Int(self.clubDetailData[0].todaysEventDictionary[0].left_coupon_count)
                                    print("CircularBarCounter : \(self.CircularBar.counter)")
                                    self.couponCountLabel.text = "\(self.clubDetailData[0].todaysEventDictionary[0].left_coupon_count)"
                                    print("couponCountLabel : \(self.couponCountLabel.text!)")
                                        
                                    }
                     
                                    
                                }
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
            
            
        }
}
    
    
    
    //call display_profile api for checking user
    
    
    
    @objc func selectSegmentInSegmentView(segmentView: SMSegmentView) {
        /*
         Replace the following line to implement what you want the app to do after the segment gets tapped.
         */
        print("Select segment at index: \(segmentView.selectedSegmentIndex)")
        
        if segmentView.selectedSegmentIndex == 0{
            
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "checkflagentrytype")
            defaults.set("1", forKey: "checkflagentrytype")
            
            let selectedEntry = "Stag"
            // defaults.removeObject(forKey: "selectedentrytype")
            defaults.set(selectedEntry, forKey: "selectedentrytype")
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            let couponVC = storyboard.instantiateViewController(withIdentifier: "couponViewController") as! couponViewController
            couponVC.selectedEntryType = selectedEntry
            couponVC.selectedClubId = selectedClubId
            couponVC.EventID = eventID
            couponVC.eventName = selectedEventName
            couponVC.eventAddress = selectedEventAddress
            couponVC.eventDate = selectedEventDate
            couponVC.eventStartTime = selectedEventStarTime
            couponVC.eventEndTime = selectedEventEndTime
            couponVC.eventEntryStagFees = selectedEventStagEntryFees
            couponVC.eventEntryCoupleFees = selectedEventCoupleEntryFees
            couponVC.couponStartingTime = self.couponStarts
            couponVC.couponEndingTime = self.couponEnds
            self.present(couponVC, animated: true, completion: nil)
            print("selected index ")
            
        } else if segmentView.selectedSegmentIndex == 1{
            
            
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "checkflagentrytype")
            defaults.set("1", forKey: "checkflagentrytype")
            
            
            print(" selected index 1")
            let selectedEntry = "Couple"
            defaults.set(selectedEntry, forKey: "selectedentrytype")
            
            
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            let couponVC = storyboard.instantiateViewController(withIdentifier: "couponViewController") as! couponViewController
            couponVC.selectedEntryType = selectedEntry
            couponVC.selectedClubId = selectedClubId
           // couponVC.selectedEntryType = selectedEntry
            //couponVC.selectedClubId = selectedClubId
            couponVC.EventID = eventID
            couponVC.eventName = selectedEventName
            couponVC.eventAddress = selectedEventAddress
            couponVC.eventDate = selectedEventDate
            couponVC.eventStartTime = selectedEventStarTime
            couponVC.eventEndTime = selectedEventEndTime
            couponVC.eventEntryStagFees = selectedEventStagEntryFees
            couponVC.eventEntryCoupleFees = selectedEventCoupleEntryFees
            couponVC.couponStartingTime = self.couponStarts
            couponVC.couponEndingTime = self.couponEnds
            
            self.present(couponVC, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    //    @IBAction func selectedCouponTypeSegmentTapped(_ sender: UISegmentedControl) {
    //
    //        print("print selected segment \(sender.selectedSegmentIndex)")
    //
    //
    //        if sender.selectedSegmentIndex == 0{
    //
    //            let defaults = UserDefaults.standard
    //           // defaults.removeObject(forKey: "checkflagentrytype")
    //            defaults.set("1", forKey: "checkflagentrytype")
    //
    //            var selectedEntry = "stag"
    //            // defaults.removeObject(forKey: "selectedentrytype")
    //            defaults.set(selectedEntry, forKey: "selectedentrytype")
    //            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
    //            let couponVC = storyboard.instantiateViewController(withIdentifier: "couponViewController") as! couponViewController
    //            couponVC.selectedEntryType = selectedEntry
    //            couponVC.selectedClubId = selectedClubId
    //            self.present(couponVC, animated: true, completion: nil)
    //            print("selected index ")
    //
    //           }else if sender.selectedSegmentIndex == 1{
    //
    //            let defaults = UserDefaults.standard
    //
    //            defaults.set("1", forKey: "checkflagentrytype")
    //
    //
    //            print(" selected index 1")
    //            var selectedEntry = "couple"
    //            defaults.set(selectedEntry, forKey: "selectedentrytype")
    //            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
    //            let couponVC = storyboard.instantiateViewController(withIdentifier: "couponViewController") as! couponViewController
    //            couponVC.selectedEntryType = selectedEntry
    //            couponVC.selectedClubId = selectedClubId
    //            self.present(couponVC, animated: true, completion: nil)
    //        }
    //    }
    
    
    
    
    func favouriteEvent(){
        //need to pass user id in API
        //pass event id
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        let URL_String = "http://www.nitevibe.in/webservices/test_favourite.php?user_id=\(User_Id!)&fav_id=\(selectedEventId)&mode=\(event_favourite_mode)&status=\(eventflagValue)"
        
        
        print("test favourite Url String: \(URL_String)")
        
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
    }
    
    @objc func getClubDetailData(){
        
        URLCache.shared.removeAllCachedResponses()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        let URL_ClubString = "http://www.nitevibe.in/webservices/club.php?user_id=\(User_Id!)&club_id=\(selectedClubId)"
        
        print("club detail string \(URL_ClubString)")
        let url = NSURL(string: URL_ClubString)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        do{
            
            Alamofire.request(urlRequest).validate().responseJSON {(response) -> Void in
                
                
                
                print("alamofire Request: \(String(describing: response.request))")
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("test1")
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    print("test 2")
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                // print("message\(dict)")
                // print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                print(" dict data for fav :\(dictData)")
                
                for data in dictData{
                    
                    
                    self.clubRate = data["club_rate"] as! String
                    
                    //self.clubRate = "2"
                    
                    print("Club Rate \(self.clubRate)")
                    
                    self.clubName = data["club_name"] as! String
                    print("Club Name \(self.clubName)")
                    self.clubStatus = data["club_time"] as! String
                    print("clubStatus \(self.clubStatus)")
                    self.clubCostForTwo = data["cost_two"] as! String
                    print("clubCostForTwo \(self.clubCostForTwo)")
                    
                    self.costOfTwoArray.append(self.clubCostForTwo)
                    self.clubHappyHours = data["happy_time"] as! String
                    print("clubHappyHours \(self.clubHappyHours)")
                    self.clubContactNo = data["club_contact"] as! String
                    let clubPicData  =  data["club_pic"] as! [String]
                    
                    print("view did load data Club Name: \(self.clubName) Club Status : \(self.clubStatus) cost for two \(self.clubCostForTwo) Club Happy hours \(self.clubHappyHours) ")
                    self.clubNameLabel.text = self.clubName
                    self.clubNameLabel.numberOfLines = 0
                    //let maximumLabelSize: CGSize = CGSize(width: 238, height: 0)
                    //let expectedLabelSize: CGSize = self.clubNameLabel.sizeThatFits(maximumLabelSize)
                    //var newFrame: CGRect = self.clubNameLabel.frame
                   // newFrame.size.height = expectedLabelSize.height
                   // self.clubNameLabel.frame = newFrame
                    //let bottomCNConstraint = self.clubNameLabel.bottomAnchor.constraint(equalTo: self.ratinglbl.bottomAnchor, constant: -30)
                    //self.clubnamelabel = [bottomCNConstraint]
                   // NSLayoutConstraint.activate(self.clubnamelabel)
                    
//                    let paragraphStyle = NSMutableParagraphStyle()
//                    paragraphStyle.lineSpacing = 1.4
//                    let attrString = NSMutableAttributedString(string: self.clubName)
//                   // attrString.addAttribute(NSMutableParagraphStyle., value:paragraphStyle, range:NSMakeRange(0, attrString.length))
//                    attrString.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
//
//                    //NSParagraphStyleAttributeName
//                    self.clubNameLabel.attributedText = attrString
//                    self.clubNameLabel.textAlignment = NSTextAlignment.left
//                    self.attributedText = NSMutableAttributedString(string:"\(self.clubName)", attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 28)])
//                    self.clubNameLabel.attributedText = self.attributedText
//                    self.clubNameLabel.numberOfLines = 3
//                    self.clubNameLabel.textAlignment = .left
                    //self.clubTimingStatusLabel.text = self.clubStatus
                    self.costOfTwo.text  = self.clubCostForTwo
                    self.clubHappyHoursLbl.text = self.clubHappyHours
                    //Split Time String
                    let fullStatus    = self.clubStatus
                    
                    
                    
                    
                    if self.clubStatus == "Closed"{
                        
                        self.clubTimingStatusLabel.text = "\(fullStatus)"
                        
                    }else{
                        
                        let fullStatusArr = fullStatus.components(separatedBy: "-")
                        let status    = fullStatusArr[0]
                        let time = fullStatusArr[1]
                        self.clubTimingStatusLabel.text = "\(status)"
                        self.splitTimeLbl.text = "- CLOSES \(time)"
                        print("check status : \(status) - CLOSES-\(time)")
                    }
                    
                    
                    // self.clubTimingStatusLabel.text = "\(status)"
                    
                    
                    
                    //                    if fullStatusArr[1].count == 0{
                    //
                    //                         self.splitTimeLbl.isHidden = true
                    //                    }else{
                    //
                    //
                    //
                    //                    }
                    
                    
                    
                    if self.clubRate == "1"{
                        print("Club Rate 1")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = true
                        self.starRating3.isHidden = true
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                        
                    }else if(self.clubRate == "2"){
                        print("Club Rate 2")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = true
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                        
                    }else if(self.self.clubRate == "3"){
                        print("Club Rate 3")
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = true
                        self.starRating5.isHidden = true
                    }else if(self.clubRate == "4"){
                        print("Club Rate 4")
                        
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = false
                        self.starRating5.isHidden = true
                        
                    }else if(self.clubRate == "5"){
                        print("Club Rate 5")
                        
                        self.starRating1.isHidden = false
                        self.starRating2.isHidden = false
                        self.starRating3.isHidden = false
                        self.starRating4.isHidden = false
                        self.starRating5.isHidden = false
                    }
                    
                    print("club pic  Data \(String(describing: clubPicData))")
                    
                    
                    //get fav club flag
                    
                    
                    self.clubFavFlagString  = data["fav_club"] as! String
                    
                    print("clubFavFlagString in for loop\(self.clubFavFlagString)")
                    
                    //
                    //                    let defaults = UserDefaults.standard
                    //
                    //
                    //                    defaults.set(self.clubFavFlagString, forKey: "clubfavflagString")
                    //
                    
                    //                    if self.clubFavFlagString == "1"{
                    //
                    //
                    //                        print("fav button active ")
                    //                        // print("selected button index \(cell.feventFavouriteButtonTapped.tag) is Active")
                    //                        //Change image index
                    //                        self.fav_btn.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                    //
                    //                    }else{
                    //                        print("fav button Inactive ")
                    //                        self.fav_btn.setBackgroundImage(UIImage(named: "favourite. selected.png"), for: .normal)
                    //                        //fav_btn.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                    //                    }
                    
                    
                    self.clubPicDataArray.removeAll()
                    for picData in clubPicData{
                        
                        print("Pic Data inside for loop: \(picData)")
                        
                        self.clubPicDataArray.append(picData)
                        
                        
                        print("Club Pic Data Array\(self.clubPicDataArray)")
                        
                        //set page controller count
                        
                        self.pageController.numberOfPages = self.clubPicDataArray.count
                    }
                    
                    let clubFacilityData = data["facility"] as? [Dictionary <String,AnyObject>]
                    
                    
                    print("club facility data \(String(describing: clubFacilityData))")
                    
                    self.facilityNameArray.removeAll()
                    self.facilityImageArray.removeAll()
                    for facility in clubFacilityData!{
                        
                        let facilityName  = facility["fac_name"] as! String
                        print("facility name : \(facilityName)")
                        self.facilityNameArray.append(facilityName)
                        let facilityImage = facility["fac_icon"] as! String
                        print("facility Image : \(facilityImage)")
                        self.facilityImageArray.append(facilityImage)
                        
                        
                    }
                    
                    let clubEventData = data["event"] as? [Dictionary <String,AnyObject>]
                    
                    print("---------------------club event data------------------\(String(describing: clubEventData))")
                    
                    print(" fav event Count : \(String(describing: clubEventData!.count))")
                    
                    self.eventCount = clubEventData!.count
                    
                    
                    
                    if clubEventData?.count == 0{
                        
                        
                        self.eventLbl.isHidden = true
                        
                    }else{
                        
                        
                        self.eventLbl.isHidden = false
                        self.eventIdArray.removeAll()
                        self.clubEventNameArray.removeAll()
                        self.clubEventDateArray.removeAll()
                        self.clubEventImageArray.removeAll()
                        self.clubEventFemaleCountIntArray.removeAll()
                        self.clubEventMaleCountIntArray.removeAll()
                        self.eventFavArray.removeAll()
                        //self.clubEventMaleCountArray.removeAll()
                        for eventData in clubEventData!{
                            /*
                             var clubEventNameArray:[String] = []
                             var clubEventImageArray:[String] = []
                             var clubEventDateArray:[String] = []
                             var clubEventMaleCount:[String] = []
                             
                             
                             */
                            let eventId = eventData["event_id"] as! String
                            print("Event id\(eventId)")
                            self.eventIdArray.append(eventId)
                            let eventName = eventData["event_name"] as! String
                            print("Event Name\(eventName)")
                            self.clubEventNameArray.append(eventName)
                            
                            print("Club Event Name Array\(self.clubEventNameArray)")
                            let eventImage = eventData["event_img"] as! String
                            print("Event image\(eventImage)")
                            self.clubEventImageArray.append(eventImage)
                            
                            print("Club Image Array \(self.clubEventImageArray)")
                            let eventDate = eventData["event_date"] as! String
                            print("Event date\(eventDate)")
                            self.clubEventDateArray.append(eventDate)
                            let maleCount = eventData["male_count"] as! Float
                            print("Male Count \(maleCount)")
                            self.clubMaleCount = (maleCount/100)
                            
                            print("male \(maleCount)")
                            print("male count: \(self.clubMaleCount)")
                            self.clubEventMaleCountArray.append(self.clubMaleCount)
                            let maleCountInt = eventData["male_count"] as! Int
                            print("Male Int Count\(maleCountInt)")
                            self.clubEventMaleCountIntArray.append(maleCountInt)
                            
                            let femaleCountInt = eventData["female_count"] as! Int
                            
                            let femaleCount = eventData["female_count"] as! Float
                            
                            self.clubFemaleCount = (femaleCount/100)
                            print("Female Int Count \(femaleCountInt)")
                            self.clubEventFemaleCountIntArray.append(femaleCountInt)
                            
                            let peopleAtEvent = eventData["user_going"] as! Int
                            
                            print("People At Event\(peopleAtEvent)")
                            
                            self.peopeleAtEventArray.append(peopleAtEvent)
                            
                            let eventFavString = eventData["favourite"] as! String
                            
                            print("event fav \(eventFavString)")
                            
                            self.eventFavArray.append(eventFavString)
                            
                        }
                        
                    }
                    
                    
                    
                    //check user_prf array is empty or not if empty tell user to edit user profile
                    
                    
                    let userProfileData = data["user_pref"] as? [Dictionary <String,AnyObject>]
                    
                    if userProfileData?.count == 0{
                        //on check in button tap tell user fill edit detail
                        
                        //pass flag to variable
                        
                        self.userPrefFlag = 0
                        
                        
                    }else{
                        
                        self.userPrefFlag = 1
                        
                        //do nothing
                    }
                    
                    
                    let userTodyasEventData = data["today_event"] as? [Dictionary <String,AnyObject>]
                    
                    
                    for userTodaysEventInfo in userTodyasEventData!{
                        
                        self.userAlreadyCheckInFlag = userTodaysEventInfo["checkin"] as! String
                        
                        
                        print("userAlreadyCheckInFlag : \(self.userAlreadyCheckInFlag)")
                        
                        self.selectedEventName = userTodaysEventInfo["today_event_name"] as! String
                        self.selectedEventAddress = userTodaysEventInfo["club_address"] as! String
                        self.selectedEventDate = userTodaysEventInfo["event_date"] as! String
                        self.selectedEventStarTime = userTodaysEventInfo["today_event_start"] as! String
                        self.selectedEventEndTime = userTodaysEventInfo["today_event_end"] as! String
                        self.selectedEventStagEntryFees = userTodaysEventInfo["stag"] as! String
                        self.selectedEventCoupleEntryFees = userTodaysEventInfo["couple"] as! String
                        
                    }
                    
                    
                    let eventUserGoingData = data["going_arr"] as? [Dictionary <String,AnyObject>]
                    
                    //handle no going array data
                    
                    
                    if eventUserGoingData?.count == 0{
                        
                       
                        //do nothing
                    }else{
                       
                        //append data
                        self.userGoingImageArray.removeAll()
                        
                        for userGoingData in eventUserGoingData!{
                            
                            
                            let userImages = userGoingData["user_photo"] as! String
                            print("user Images \(userImages)")
                            
                            self.userGoingImageArray.append(userImages)
                            
                            print("user going image array \(self.userGoingImageArray)")
                        }
                        
                        
                    }
                    
                    
                }
                
                
                DispatchQueue.main.async {
                    
                    self.clubFeatureImagesCollectionView.reloadData()
                    self.facilitiesTableView.reloadData()
                    self.clubEventListCollectionView.reloadData()
                    self.userGoingCollectionView.reloadData()
                    
                }
                
            }
            
            
        }catch{
            
            print(error)
        }
        
        
        
        
    }
    
    @IBAction func PageControll_Action(_ sender: UIPageControl) {
        pageController.numberOfPages = clubArray.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == self.clubFeatureImagesCollectionView{
            
            return clubPicDataArray.count
        }else if collectionView == self.clubEventListCollectionView{
            
            
            return clubEventImageArray.count
            
            //return clubArray.count
        }else{
            
            
            if self.whosCheckInData.count == 0{
                
                
                return 0
                
            }else{
                
                print("whos user photo count in item in number of section \(self.whosCheckInData.count)")
                
                return self.whosCheckInData.count
            }
            
            //print("print return item in number of section \()")
            //return userGoingImageArray.count
        }
        
        
        //return clubPicDataArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.clubFeatureImagesCollectionView{
            
            let cell:ClubDetailFeatureImageViewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubDetailFeatureImageViewCollectionViewCell", for: indexPath) as! ClubDetailFeatureImageViewCollectionViewCell
            
            cell.frame.size.width = screenWidth
            cell.frame.size.height = 280
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 0
            cell.layer.cornerRadius = 0
            cell.clipsToBounds = true
            
            cell.clubFeatureImageView.kf.setImage(with: URL(string:self.clubPicDataArray[indexPath.row]))
            
            if self.clubFavFlagString == "1"{
                
                
                print("fav button active ")
                // print("selected button index \(cell.feventFavouriteButtonTapped.tag) is Active")
                //Change image index
//                cell.favButtontTapped.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                cell.favButtontTapped.isHidden = true
                
            }else{
                print("fav button Inactive ")
                cell.favButtontTapped.setBackgroundImage(UIImage(named: "favourite. selected.png"), for: .normal)
                //fav_btn.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                cell.favButtontTapped.isHidden = true
            }
            
            
            cell.backButtonTapped.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
            cell.shareButtonTapped.addTarget(self, action: #selector(shareButtonClicked), for: .touchUpInside)
            cell.favButtontTapped.addTarget(self, action: #selector(favButtonClicked), for: .touchUpInside)
            // let clubImageString = self.clubPicDataArray[indexPath.row]
            // let imageURL = URL(string: clubImageString)
            // let imageData = try! Data(contentsOf: imageURL! as URL)
            //cell.clubFeatureImageView.kf.setImage(with: URL(string:self.whosCheckInData[indexPath.row].user_photo))
            //print("\(imageData)")
            // cell.clubFeatureImageView.image = UIImage(data: imageData)
            //   placeholder: self.pageController.numberOfPages = clubPicDataArray.count
            
            //error come because some file not found
            
            //        if(imageData != nil)
            //        {
            //            cell.clubFeatureImageView.image = clubArray[in]
            //        }
            //cell.clubFeatureImageView.image = clubArray[indexPath.row]
            
            
            return cell
            
        }else if collectionView == self.clubEventListCollectionView{
            
            let cell:ClubEventListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubEventListCollectionViewCell", for: indexPath) as! ClubEventListCollectionViewCell
            cell.layer.borderColor = UIColor.blue.cgColor
            cell.layer.borderWidth = 3
            cell.layer.cornerRadius = 8
            cell.layer.masksToBounds = true
            
            let eventImageString = self.clubEventImageArray[indexPath.row]
            let imageURL = URL(string: eventImageString)
            let imageData = try! Data(contentsOf: imageURL! as URL)
            
            
            let view = UIView(frame: cell.clubEventListImageView.frame)
            let gradient = CAGradientLayer()
            gradient.frame = view.frame
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor ]
            gradient.locations = [0.0, 1.0]
            view.layer.insertSublayer(gradient, at: 0)
            cell.clubEventListImageView.addSubview(view)
            
            print("\(imageData)")
            cell.clubEventListImageView.image = UIImage(data: imageData)
            
            cell.feventFavouriteButtonTapped.isHidden = true
            //handle fav button tap
            cell.feventFavouriteButtonTapped.tag = indexPath.row
            
            
            if cell.feventFavouriteButtonTapped.tag == indexPath.row{
                
                //check active or inactive
                //if incative
                
                if eventFavArray[indexPath.row] == "1"{
                    
                    print("selected button index \(cell.feventFavouriteButtonTapped.tag) is Active")
                    //Change image index
                    cell.feventFavouriteButtonTapped.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                    
                }else{
                    print("selected button index \(cell.feventFavouriteButtonTapped.tag) is Inactive")
                    cell.feventFavouriteButtonTapped.setBackgroundImage(UIImage(named: "favourite. selected.png"), for: .normal)
                    // cell.feventFavouriteButtonTapped.setBackgroundImage(UIImage(named: "star_on_2.png"), for: .normal)
                }
                
            }
            
            cell.eventShareButtonTapped.tag = indexPath.row
            
            
            //cell.clubEventListImageView.image = clubArray[indexPath.row]
            
            //clubEventImageArray
            /*
             cell.favouriteButtonTapped.tag = indexPath.row
             cell.favouriteButtonTapped.addTarget(self, action: #selector(favouriteButtonAction), for: .touchUpInside)
             cell.shareButtonOutlet.tag = indexPath.row
             cell.shareButtonOutlet.addTarget(self, action: #selector(shareButtonAction), for: .touchUpInside)
             
             progressValue = self.clubMaleCount;
             cell.ratioProgressView.progress = progressValue
             
             */
            
            cell.eventNameLbl.text = clubEventNameArray[indexPath.row]
            cell.eventDateLbl.text = clubEventDateArray[indexPath.row]
            
            cell.feventFavouriteButtonTapped.tag = indexPath.row
            cell.feventFavouriteButtonTapped.addTarget(self, action: #selector(eventFavouriteAction) , for: .touchUpInside)
            cell.eventShareButtonTapped.tag = indexPath.row
            cell.eventShareButtonTapped.addTarget(self, action: #selector(eventShareAction), for: .touchUpInside)
            progressValue = self.clubFemaleCount;
            
            // cell.costOfLbl.text = costOfTwoArray[indexPath.row]
            //cell.peopleAtEventLbl.text = peopeleAtEventArray[indexPath.row]
            
            if peopeleAtEventArray[indexPath.row] >= 10{
                //if peopele at event count is greater than 10
                
                print("if peopele at event count is greater than 10")
                cell.maleImageView.isHidden = false
                cell.femaleImageView.isHidden = false
                
                cell.peopleAtEventLbl.text = ("\(peopeleAtEventArray[indexPath.row]) people are here")
                //cell.maleCountLabel.text = "\(clubEventFemaleCountIntArray[indexPath.row])%"
                //cell.femaleCountLabel.text = "\(clubEventMaleCountIntArray[indexPath.row])%"
                cell.progressView.progress =  progressValue
                cell.progressView.layer.masksToBounds = true
                cell.progressView.layer.cornerRadius = 4
                
                cell.costOfLbl.isHidden = true
                cell.rupeeImageView.isHidden = true
                
            }else{
                
                //                cell.maleImageView.isHidden = false
                //                cell.femaleImageView.isHidden = false
                
                cell.peopleAtEventLbl.text = ("\(peopeleAtEventArray[indexPath.row]) people are here")
                //cell.maleCountLabel.text = "\(clubEventFemaleCountIntArray[indexPath.row])%"
                //cell.femaleCountLabel.text = "\(clubEventMaleCountIntArray[indexPath.row])%"
                cell.progressView.progress =  progressValue
                cell.progressView.layer.masksToBounds = true
                cell.progressView.layer.cornerRadius = 4
                //hide progress bar at event
                print("hide progress bar at event")
                cell.maleImageView.isHidden = true
                cell.femaleImageView.isHidden = true
                //cell.maleCountLabel.isHidden = true
                //cell.femaleCountLabel.isHidden = true
                cell.progressView.isHidden = true
                cell.peopleAtEventLbl.isHidden = true
                cell.costOfLbl.isHidden = true
                cell.rupeeImageView.isHidden = true
            }
            
            
            return cell
        }else{
            
            let cell:UserGoingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserGoingCollectionViewCell", for: indexPath) as! UserGoingCollectionViewCell
            
            
            if self.whosCheckInData.count == 0{
                
                print("Empty Whos check in ")
                
            }else{
                print("whos user photo count \(self.whosCheckInData[0].user_photo.count)")
                print("who check in user photo 121 \(self.whosCheckInData[0].user_photo)")
            }
            //Previous code
            //            let eventImageString = self.userGoingImageArray[indexPath.row]
            //            let imageURL = URL(string: eventImageString)
            //            let imageData = try! Data(contentsOf: imageURL! as URL)
            //            print("\(imageData)")
            //
            //cell.userGoingImageView.image = UIImage(data: imageData)
            
            
            
            
            //            if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "1"{
            //
            //                cell.whosgoingImageView.layer.borderColor = UIColor(red: 243/255, green: 40/255, blue: 104/255, alpha: 1.0).cgColor
            //            }else if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "2"{
            //                cell.whosgoingImageView.layer.borderColor = UIColor(red: 199/255, green: 202/255, blue: 219/255, alpha: 1.0).cgColor
            //
            //            }else if self.events[eventIndexNumber].eventUserGoing[indexPath.row].userLikeCategory == "3"{
            //                cell.whosgoingImageView.layer.borderColor = UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 1.0).cgColor
            //
            //            }else{
            //                cell.whosgoingImageView.layer.borderColor = UIColor(red: 158/255, green: 285/255, blue: 22/255, alpha: 1.0).cgColor
            //            }
            //
            
            print("whos check in data : \(self.whosCheckInData[indexPath.row].user_photo)")
            
            if self.whosCheckInData[indexPath.row].user_photo == ""{
                
                print("no image found")
                //cell.userGoingImageView.image = UIImage(named: "Profile.png")
                cell.userGoingImageView.image = #imageLiteral(resourceName: "defaultIcon")
                
            }else{
                cell.userGoingImageView.kf.setImage(with: URL(string:self.whosCheckInData[indexPath.row].user_photo))
            }
            
            //userGoingImageArray
            //cell.userGoingImageView.image = clubArray[indexPath.row]
            
            return cell
            
        }
        
    }
    
    
    
    @objc func shareButtonClicked() {
        
        let clubIdString = selectedClubId
        
        print("print club string \(clubIdString)")
        let data = (clubIdString).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let  shareContent = "http://rednwhiteco.in/club_details.php?id=\(base64)"
        
        
        print("club sharing string \(shareContent)")
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
        
    }
    
    @objc func favButtonClicked() {
        
        print("Fav")
        
        
        print("print selected club id :\(selectedClubId)")
        
        if clubFavFlagString == "0"{
            
            print("make Active ")
            flagValue = "Active"
            favourite()
            getClubDetailData()
            
            
            
        }else if clubFavFlagString == "1"{
            
            print("make Inactive ")
            
            flagValue = "Inactive"
            favourite()
            getClubDetailData()
            
        }
        
    }
    
    @objc func backButtonClicked() {
        
        print("back button tapped to instatiate back view ")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        
        BaseController.selectedIndex = 1
        self.present(BaseController, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let indexPath = clubFeatureImagesCollectionView.indexPathForItem(at: center) {
            self.pageController.currentPage = indexPath.row
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.clubEventListCollectionView{
            
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            
            let eventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
             print("selected event id \(eventID)")
            eventDetailVC.selectedEventId = eventIdArray[indexPath.row]
           // eventDetailVC.selectedEventId = eventID
            eventDetailVC.selectedClubId = selectedClubId
            
            print("Selected club Id \(eventDetailVC.selectedClubId)")
            print("Selected event ID : \(eventIdArray[indexPath.row])")
            //print("Selected event ID : \(eventID)")
            
            present(eventDetailVC, animated: true, completion: nil)
            
        }
    }
    
    @objc func eventFavouriteAction(feventFavouriteButtonTapped:UIButton){
        
        print("fav button tapped: \(feventFavouriteButtonTapped.tag)")
        
        print("selected event id on fav button tapped  \(eventIdArray[feventFavouriteButtonTapped.tag])")
        
        selectedEventId = eventIdArray[feventFavouriteButtonTapped.tag]
        
        print("selected fav event id\(selectedEventId)")
        
        //Graet Job !!Vinu bhai you resolved this issue..
        //getting selected favourite Button tapped selected event id
        //this is doing for the default get data do for  also get data function
        print("selected event id \(eventIdArray[feventFavouriteButtonTapped.tag])")
        selectedEventId = eventIdArray[feventFavouriteButtonTapped.tag]
        
        print("selectedFavClubId \(selectedEventId)")
        print("check flag ")
        
        if  eventFavArray[feventFavouriteButtonTapped.tag] == "0"{
            
            print("make Active ")
            eventflagValue = "Active"
            favouriteEvent()
            getClubDetailData()
            
            
        }else if  eventFavArray[feventFavouriteButtonTapped.tag] == "1"{
            
            print("make Inactive ")
            
            eventflagValue = "Inactive"
            favouriteEvent()
            getClubDetailData()
        }
        
        
        
    }
    
    @objc func eventShareAction(eventShareButtonTapped:UIButton){
        print("share buttontapped")
        
        let eventIdString = eventIdArray[eventShareButtonTapped.tag]
        
        print("print club string \(eventIdString)")
        //share only encoded event id
        let data = (eventIdString).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let  shareContent = "http://www.nitevibe.in/event_details.php?id=\(base64)"
        
        print("share string \(shareContent)")
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
        
    }
    func favourite(){
        //need to pass user id in API
        //for club favourite
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        let URL_String = "http://www.nitevibe.in/webservices/test_favourite.php?user_id=\(User_Id!)&fav_id=\(selectedClubId)&mode=\(club_favourite_mode)&status=\(flagValue)"
        
        
        print("test favourite Url String: \(URL_String)")
        
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                
            }
            
        }catch{
            
            print(error)
        }
    }
    
    
    @IBAction func clubFavouriteButtonTapped(_ sender: Any) {
        
        
    }
    
    
    @IBAction func clubShareButtonTapped(_ sender: Any) {
        
        let  shareContent = "www.nitevibe.in"
        
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return facilityNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:EventVenueFacilitiesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EventVenueFacilitiesTableViewCell") as! EventVenueFacilitiesTableViewCell
        
        cell.facilityNameListLbl.text = facilityNameArray[indexPath.row]
        /*
         
         let lawImagesString = self.lawImageArray[indexPath.row]
         
         let imageURL = URL(string: lawImagesString)
         
         let imageData = try! Data(contentsOf: imageURL! as URL)
         
         
         cell.titleLabel.text = self.lawNameArray[indexPath.row]
         
         if(imageData != nil)
         {
         cell.featuredImage.image = UIImage(data: imageData as! Data)
         }
         */
        
        
        let facilityImageString = self.facilityImageArray[indexPath.row]
        
        if facilityImageString.count == 0{
            
            print("do nothing ")
        }else{
            
            let imageURL = URL(string: facilityImageString)
            
            let imageData = try! Data(contentsOf: imageURL! as URL)
            print(imageData)
            cell.facilityImageView.image = UIImage(data: imageData)
            
        }
        //cell.layer.
        
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    
    @IBAction func CheckInButtonTapped(_ sender: Any) {
        
        print("button tapped ")
        
        
        let defaults =  UserDefaults.standard
        
        defaults.integer(forKey:"checkflagentrytype")
        
        //var checkEntryTypeFlag = Int()
        print("print flag \(String(describing: defaults.value(forKey: "checkflagentrytype")!))")
        
        print("user check in flag \(userAlreadyCheckInFlag)")
        
        //check flag for coupon is selected or not
        if defaults.value(forKey: "checkflagentrytype")! as! String == "1"{
            
            print("Do something on check in button tapped ")
            
            if userPrefFlag == 0{
                //if not
                
                print("profile is not created")
                let alert = UIAlertController(title: "", message: "Please create your profile", preferredStyle: .alert)
                
                //                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
                
            }else{
                
                print("profile is created and check type is also selected ")
                //check user already check in or not
                
                if userAlreadyCheckInFlag == "1"{
                    
                    
                    checkIn()
                    let storybord = UIStoryboard(name: "Swipe", bundle: nil)
                    
                    let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
                    SwipeVC.selectedGoingFlag = "0"
                    SwipeVC.selectedClubIdFromSwipe = selectedClubId
                    //SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
                     print("selected event id \(eventID)")
                    
                    SwipeVC.selectedCkeckInEventId = eventID
                    
                    self.present(SwipeVC, animated:true, completion: nil)
                    
                }else{
                    
                    
                    if selectedCouponFlag == "1"{
                        
                        print("call with coupon api")
                        
                        let selectedEntry =  defaults.value(forKey: "selectedentrytype")!
                        print("selected entry \(selectedEntry)")
                        let selectedCouponFlag = defaults.value(forKey: "withoutcoupon")
                        print("selectedCouponFlag \(String(describing: selectedCouponFlag))")
                        
                        print("selected withcoupon \(String(describing: selectedCouponFlag))")
                        
                        userClubEntryType = selectedEntry as! String
                        
                        //call check in API
                        checkIn()
                        
                        let storybord = UIStoryboard(name: "Swipe", bundle: nil)
                        
                        let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
                        SwipeVC.selectedGoingFlag = "0"
                        SwipeVC.selectedClubIdFromSwipe = selectedClubId
                       // SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
                         print("selected event id \(eventID)")
                        SwipeVC.selectedCkeckInEventId = eventID
                        
                        self.present(SwipeVC, animated:true, completion: nil)
                        //SwipeVC.loadCardsforCheckIn()
                        // SwipeVC.loadCardsforGoing()
                        
                        //                    }else if defaults.value(forKey: "withoutcoupon") as! String == "0"{
                        
                    }else if selectedCouponFlag == "0"{
                        print("call without coupon api")
                        
                        print("selected without coupon  \(selectedCouponFlag)")
                        //call Check in API
                        checkIn()
                        
                        let storybord = UIStoryboard(name: "Swipe", bundle: nil)
                        
                        let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
                        SwipeVC.selectedGoingFlag = "0"
                        SwipeVC.selectedClubIdFromSwipe = selectedClubId
                         print("selected event id \(eventID)")
                        //SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
                        SwipeVC.selectedCkeckInEventId = eventID
                        
                        print("detail vc event id data \(SwipeVC.selectedCkeckInEventId)")
                        self.present(SwipeVC, animated:true, completion: nil)
                    }
                    
                }
                
                
                
            }
            
        }else{
            
            //stag or couple is not selected
            //1.check profile is fill or not
            //if profile is fill check
            //if flag is not set then check user profile is fill or not
            if userPrefFlag == 0{
                //if not
                
                print("profile is not created")
                let alert = UIAlertController(title: "", message: "Please create your profile", preferredStyle: .alert)
                
                //                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
                
            }else{
                
                //again check stag or couple is selected or not
                if defaults.value(forKey: "checkflagentrytype")! as! String == "1"{
                    
                    
                    print("profile is created and checkin type is also selected ")
                    
                    
                    
                    checkIn()
                    
                    let storybord = UIStoryboard(name: "Swipe", bundle: nil)
                    
                    let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
                    SwipeVC.selectedGoingFlag = "0"
                    SwipeVC.selectedClubIdFromSwipe = selectedClubId
                     print("selected event id \(eventID)")
                   // SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
                    SwipeVC.selectedCkeckInEventId = eventID
                    
                    print("detail vc event id data \(SwipeVC.selectedCkeckInEventId)")
                    self.present(SwipeVC, animated:true, completion: nil)
                    
                    
                    
                }else{
                    
                    //if not selected pop alert
                    
                    let alert = UIAlertController(title: "", message: "Please select stag or couple ", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    
                }
                
                
                
            }
        }
        
        
        
        //
        //        if userAlreadyCheckInFlag == "1"{
        //
        //            //if user check in
        //
        //            //instantitae to swipe view
        //
        //            let storybord = UIStoryboard(name: "Main", bundle: nil)
        //
        //            let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
        //            SwipeVC.selectedGoingFlag = "0"
        //            SwipeVC.selectedClubIdFromSwipe = selectedClubId
        //            SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
        //
        //            self.present(SwipeVC, animated:true, completion: nil)
        //
        //        }else if userAlreadyCheckInFlag == "0"{
        //
        //
        //
        //                print("profile is created")
        //
        //                if defaults.value(forKey: "checkflagentrytype")! as! String == "1"{
        //
        //
        //                    let selectedEntry =  defaults.value(forKey: "selectedentrytype")!
        //                    print("selected entry \(selectedEntry)")
        //                    //let selectedCouponFlag = defaults.value(forKey: "withoutcoupon")
        //                    //print("selectedCouponFlag \(selectedCouponFlag)")
        //
        //                    userClubEntryType = selectedEntry as! String
        //
        //
        //                    print("call api")
        //                    print("selected coupon flag property :\(selectedCouponFlag)")
        //                    //                    if defaults.value(forKey: "withcoupon") as! String == "1"{
        //
        //                    if selectedCouponFlag == "1"{
        //
        //                        print("call with coupon api")
        //
        //                        print("selected withcoupon \(selectedCouponFlag)")
        //                        //call check in API
        //                        checkIn()
        //
        //                        let storybord = UIStoryboard(name: "Main", bundle: nil)
        //
        //                        let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
        //                        SwipeVC.selectedGoingFlag = "0"
        //                        SwipeVC.selectedClubIdFromSwipe = selectedClubId
        //                        SwipeVC.selectedCkeckInEventId = selectedUserCheckInEvent
        //
        //                        self.present(SwipeVC, animated:true, completion: nil)
        //                        //SwipeVC.loadCardsforCheckIn()
        //                        // SwipeVC.loadCardsforGoing()
        //
        //                        //                    }else if defaults.value(forKey: "withoutcoupon") as! String == "0"{
        //
        //                    }else if selectedCouponFlag == "0"{
        //                        print("call without coupon api")
        //
        //
        //
        //
        //                        print("selected without coupon  \(selectedCouponFlag)")
        //                        //call Check in API
        //                        checkIn()
        //
        //                        let storybord = UIStoryboard(name: "Main", bundle: nil)
        //
        //                        let SwipeVC = storybord.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
        //                        SwipeVC.selectedGoingFlag = "0"
        //                        SwipeVC.selectedClubIdFromSwipe = selectedClubId
        //
        //
        //
        //                        print("detail vc event id data \(SwipeVC.selectedCkeckInEventId)")
        //                        self.present(SwipeVC, animated:true, completion: nil)
        //                    }
        //
        //
        //                }else{
        //                    let alert = UIAlertController(title: "", message: "Please select stag or couple ", preferredStyle: .alert)
        //
        //                    //                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        //                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        //
        //                    self.present(alert, animated: true)
        //
        //
        //
        //                    //API Call Here
        //
        //                }
        //
        //
        //
        //
        //
        //        }
        
        
        
    }
    
    
    
    func checkIn(){
        
        
        URLCache.shared.removeAllCachedResponses()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        
        var respo = Int()
        
        let URL_String = "http://www.nitevibe.in/webservices/check_in.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&entry_type=\(userClubEntryType)&coupon=\(selectedCouponFlag)"
        
        print("URL String : \(URL_String)")
        
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(escapeUrl!, method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message \(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                
                if(dict["response"] as! Int == 201){
                    
                    let alert = UIAlertController(title: "", message:"No event in club", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else if(dict["response"] as! Int == 202){
                    let alert = UIAlertController(title: "", message:"Failed to save data", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else if(dict["response"] as! Int == 200){
                    
                    let alert = UIAlertController(title: "", message:"Successful", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    let alert = UIAlertController(title: "Oops", message:"Request method not accepted", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func callingbtn_Action(_ sender: UIButton){
        
        if let url = URL(string: "tel://\(self.clubContactNo)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func mapLocationbtn_Action(_ sender: UIButton){
        
        let storyboard : UIStoryboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        // vc.teststring = "hello"
        
        
        //        let vc = UIStoryboard.init(name: "DrinkPreference", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        
        //        let MapViewController:MapViewController = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        vc.userID = SendUSerIDToMap
        vc.selectedClubid = selectedClubId
        print("UserLat : \((locationManager.location?.coordinate.latitude)!)")
        print("UserLong : \((locationManager.location?.coordinate.longitude)!)")
        vc.myLatitude = (locationManager.location?.coordinate.latitude)!
        vc.myLongitude = (locationManager.location?.coordinate.longitude)!
        //self.navigationController?.pushViewController(vc, animated: true)
        //present(vc, animated: true, completion: nil)
        let navigationController = UINavigationController(rootViewController: vc)
        
        self.present(navigationController, animated: true, completion: nil)
        
        
    }
    
    @objc func homeButtonTapped(){
        
        print("home button tapped")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        
        BaseController.selectedIndex = 1
        self.present(BaseController, animated: true, completion: nil)
        
        
    }
    @objc func userProfileButtonTapped(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        BaseController.selectedIndex = 0
        self.present(BaseController, animated: true, completion: nil)
    }
    @objc func chatButtonTapped(){
        print("chat button tapped ")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        BaseController.selectedIndex = 2
        self.present(BaseController, animated: true, completion: nil)
    }
    
    
    @objc func showOverLay(){
        
        self.overLayView.isHidden = true
        
        
    }
}

extension UIScrollView {
    
    func resizeScrollViewContentSize() {
        
        var contentRect = CGRect.zero
        
        for view in self.subviews {
            
            contentRect = contentRect.union(view.frame)
            
        }
        
        self.contentSize = contentRect.size
        
    }
    
}

//extension Date {
//    func string(format: String) -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = format
//        return formatter.string(from: self)
//    }
//}





