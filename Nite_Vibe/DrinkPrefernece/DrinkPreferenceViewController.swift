//
//  DrinkPreferenceViewController.swift
//  Nite_Vibe
//
//  Created by Appmonks on 28/05/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout
import Alamofire
import SwiftyJSON

protocol  DrinkPreferenceVCDelegate: class {
    func getDataFromDrinkPrefVC(drinks: [SaveDrinkModel])
}





class DrinkPreferenceViewController: UIViewController {
    
    var drinks = [DrinkModel]()
    var saveSelectedDrinks = [Int]()
    var isSelected = 0
    var twoDimensionalArrayList = [ExpandableDrinks]()
    var sectionNumber = Int()
    var isExpanded = Bool()
    @IBOutlet weak var tableView: UITableView!
    var categories = [DrinkModel]()//to hold the data to be displayed
//    var bCategory:DrinkModel?
    var storeSubDrink = [SaveDrinkModel]()
    var selectedIndex = Int()
    var selectEnabled = false
    var userName = ""
    //2.create object of the delegate and called the method of the protocol
    
    weak var delegate: DrinkPreferenceVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        fetchswiftyDrinkData()
        
    }
    
    func fetchswiftyDrinkData(){
        let urlString = "http://www.nitevibe.in/webservices/drinks.php"
        
        Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                self.drinks.append(DrinkModel(drinkJson: arr))
                                self.twoDimensionalArrayList.append(ExpandableDrinks(isExpandable: false, names: ["11"]))
                            }
                        }
                        self.tableView.reloadData()
                    }
                }catch {
                    print("cannot convert to Json")
                }
            }
        }
    }
    
    @objc func handleExapndCloseOfLabelButton(drinkNameButtonTapped:UIButton) {
        sectionNumber = drinkNameButtonTapped.tag
        var indexPaths = [IndexPath]()
        for row in twoDimensionalArrayList[sectionNumber].names.indices{
            let indexPath = IndexPath(row:row,section:sectionNumber)
            indexPaths.append(indexPath)
        }
        isExpanded = twoDimensionalArrayList[sectionNumber].isExpandable
        twoDimensionalArrayList[sectionNumber].isExpandable = !isExpanded
        
        if isExpanded {
            tableView.deleteRows(at:indexPaths , with: .fade)
        } else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
    }
    
    @objc func handleExpandClosedForCheckBox(checkBoxButtonTapped:UIButton){
        //pass this tag to collection view cell
        sectionNumber = checkBoxButtonTapped.tag
        var indexPaths = [IndexPath]()
        
        for row in twoDimensionalArrayList[sectionNumber].names.indices{
            let indexPath = IndexPath(row:row,section:sectionNumber)
            indexPaths.append(indexPath)
        }
        isExpanded = twoDimensionalArrayList[sectionNumber].isExpandable
        
        twoDimensionalArrayList[sectionNumber].isExpandable = !isExpanded
        
        let selectedImage = UIImage(named: "checkbox.selected")
        let unSelectedImage = UIImage(named: "checkbox.unselected")
        
        checkBoxButtonTapped.setImage(isExpanded ?  unSelectedImage : selectedImage , for: .normal)
        
        if isExpanded {
            tableView.deleteRows(at: indexPaths , with: .fade)
        } else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if storeSubDrink.count > 0 {
            delegate?.getDataFromDrinkPrefVC(drinks: storeSubDrink)
        }
        dismiss(animated: true, completion: nil)
    }
}

extension DrinkPreferenceViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return drinks.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !twoDimensionalArrayList[section].isExpandable{
            return 0
        }
        return twoDimensionalArrayList[section].names.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrinkPreferenceTableViewCell") as! DrinkPreferenceTableViewCell
        let aCategory = self.drinks[indexPath.section]
        cell.updateCellWith(category: aCategory)
        
        return cell
    }
}

extension DrinkPreferenceViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //code for drinkLabel View
        let headerView: UIView = UIView.init(frame: CGRect(x: 1, y: 50, width: 276, height: 50))
        headerView.backgroundColor = .clear
        let drinkNameButtonTapped: UIButton = UIButton.init(frame: CGRect(x: 60, y: 5, width: 350, height: 30))
        
        drinkNameButtonTapped.contentHorizontalAlignment = .left
        drinkNameButtonTapped.contentEdgeInsets = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 10)
        drinkNameButtonTapped.backgroundColor = .clear
        drinkNameButtonTapped.setTitle("\(drinks[section].drink_name)", for: .normal)
        drinkNameButtonTapped.addTarget(self, action: #selector(handleExapndCloseOfLabelButton), for: .touchUpInside)
        
        drinkNameButtonTapped.tag = section
        let checkBoxButtonTapped: UIButton = UIButton.init(frame: CGRect(x: 12, y: 5, width: 24, height: 24))
        if let inSelectedImage = UIImage(named:"checkbox.unselected") {
            checkBoxButtonTapped.setImage(inSelectedImage, for: .normal)
        }
        checkBoxButtonTapped.addTarget(self, action: #selector(handleExpandClosedForCheckBox), for: .touchUpInside)
        checkBoxButtonTapped.tag = section
        let splitterImageView: UIImageView = UIImageView.init(frame:CGRect(x: 4, y: 40, width:400, height:2))
        splitterImageView.image = UIImage(named:"splitter")
        headerView.addSubview(splitterImageView)
        headerView.addSubview(checkBoxButtonTapped)
        headerView.addSubview(drinkNameButtonTapped)
        
        return headerView
    }
}

extension DrinkPreferenceViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.drinks[sectionNumber].subDrinkDictionary.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DrinkPreferneceCollectionViewCell", for: indexPath) as! DrinkPreferneceCollectionViewCell
            let subdrink = self.drinks[sectionNumber].subDrinkDictionary[indexPath.row]
        print("SDDDDRinks :\(subdrink)")
        print("storesubdrinks: \(storeSubDrink)")
        let isselected = storeSubDrink.contains(where: {$0.sDrinkId == subdrink.sub_drink_id && $0.sDrinkanme == subdrink.sub_drink_name})
        print("selwctedDrinks :\(isselected)")
        cell.updateCellWithSubDrinkName(
            name: subdrink.sub_drink_name,
            id: subdrink.sub_drink_id,
            isSelected: isselected
        )
        
        return cell
    }
}

extension DrinkPreferenceViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        //append data at selected item data
        
        var infoDict =  [String:Any]()
        
        infoDict["sub_drink_id"] = self.drinks[sectionNumber].subDrinkDictionary[indexPath.row].sub_drink_id
        infoDict["sub_drink_name"] = self.drinks[sectionNumber].subDrinkDictionary[indexPath.row].sub_drink_name
        
        let sdrink = SaveDrinkModel(infoDict: infoDict)
        
        if let index = storeSubDrink.index(where: {$0.sDrinkId == sdrink.sDrinkId && $0.sDrinkanme == sdrink.sDrinkanme}) {
           // storeSubDrink.remove(at: index)
            storeSubDrink.remove(at: index)
        } else {
            storeSubDrink.append(sdrink)
        }

        collectionView.reloadData()
        
    }
}
