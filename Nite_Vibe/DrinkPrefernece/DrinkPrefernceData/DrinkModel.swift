//
//  DrinkModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 16/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON


struct DrinkModel{
    var drink_id = String()
    var drink_name = String()
    var subDrinkDictionary = [subDrinkModel]()
    init()
    {
        self.subDrinkDictionary = []
    }
    
    init(drinkJson:[String:Any]){
        
        if let drink_ID = drinkJson["drink_id"] as? String{
            
            self.drink_id = drink_ID
        }
        
        if let drink_NAME = drinkJson["drink_name"] as? String{
            
            self.drink_name = drink_NAME
        }
        
        if let subDinkData = drinkJson["sub_drink"] as? [[String:Any]] {
            
            for subDrinkInfo in subDinkData{
                
                let subDrink = subDrinkModel(drinkJson: subDrinkInfo)
                
                self.subDrinkDictionary.append(subDrink)
            }
        }
        
    }
    
}

struct subDrinkModel{
    
    var sub_drink_id = String()
    var sub_drink_name = String()
    
    init()
    {}
    
    init(drinkJson:[String:Any]) {
        
        if let sub_drink_ID = drinkJson["sub_drink_id"] as? String{
            
            self.sub_drink_id = sub_drink_ID
            
        }
        if let sub_drink_NAME = drinkJson["sub_drink_name"] as? String{
            
            self.sub_drink_name = sub_drink_NAME
        }
        
    }
    
}

struct subDrinkConstant{
    
    static let  s_drink_id = "sub_drink_id"
    static let  s_drink_name = "sub_drink_name"
}

class SaveDrinkModel {
    
    let sDrinkId:String
    let sDrinkanme:String
    
    init(name: String, id: String) {
        self.sDrinkanme = name
        self.sDrinkId = id
    }
    
    init(infoDict:[String:Any]) {
        
        self.sDrinkId = infoDict[subDrinkConstant.s_drink_id] as! String
        self.sDrinkanme = infoDict[subDrinkConstant.s_drink_name] as! String
    }
}

