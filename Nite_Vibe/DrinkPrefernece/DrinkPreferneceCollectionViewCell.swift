//
//  DrinkPreferneceCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Appmonks on 28/05/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class DrinkPreferneceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        bgView.layer.borderColor = UIColor.blue.cgColor
        bgView.clipsToBounds = true
        bgView.layer.borderWidth = 1
        bgView.layer.cornerRadius = 4
    }

    func updateCellWithSubDrinkName(name:String?, id:String, isSelected: Bool) {
        titleLabel.text = nil
        if let txt = name {
            titleLabel.text = txt
        }
        bgView.backgroundColor = UIColor.clear
        if isSelected {
            bgView.backgroundColor = UIColor.blue
        }
    }
}
