//
//  DrinkPreferenceTableViewCell.swift
//  Nite_Vibe
//
//  Created by Appmonks on 28/05/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

class DrinkPreferenceTableViewCell: UITableViewCell{
    
    var aCategory:DrinkModel?
    
    @IBOutlet weak var checkButtonOutletTapped: UIButton!
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var clCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        clCollectionView.isHidden = false
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        print("collection view tag \(clCollectionView.tag)")
        
        // Set up the flow layout's cell alignment:
        let flowLayout = clCollectionView?.collectionViewLayout as? AlignedCollectionViewFlowLayout
        flowLayout?.horizontalAlignment = .left
        
        // Enable automatic cell-sizing with Auto Layout:
        flowLayout?.estimatedItemSize = .init(width: UIScreen.main.bounds.size.width, height: 120)
        clCollectionView?.allowsMultipleSelection = true
       // self.clCollectionView.allowsSelection = true
    }

    func updateCellWith( category: DrinkModel) {
        self.aCategory = category
        self.clCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
