//
//  ExpandableDrinks.swift
//  Nite_Vibe
//
//  Created by Appmonks on 29/05/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation

struct ExpandableDrinks{
    
    var isExpandable : Bool
    let names : [String]
}
