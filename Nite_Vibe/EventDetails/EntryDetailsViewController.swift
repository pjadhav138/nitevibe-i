//
//  EntryDetailsViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 02/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class EntryDetailsViewController: UIViewController {

    
    @IBOutlet weak var stagEntryPriceLabel: UILabel!
    
    
    var eventIndexNumberOnEntryDetail = 0
    
    @IBOutlet weak var coupleEntryPriceLabel: UILabel!
    
    var selectedEventId = ""
    
    var selectedClubId = ""
    var stagPrice = String()
    var couplePrice = String()

     override func viewDidLoad() {
        super.viewDidLoad()
        //print(<#T##items: Any...##Any#>)

        stagEntryPriceLabel.font =  UIFont(name:"OpenSans-SemiboldItalic", size: 18)
        coupleEntryPriceLabel.font = UIFont(name:"OpenSans-SemiboldItalic", size: 18)
        
        print("StagP :\(stagPrice)")
        print("CoupleP :\(couplePrice)")
        
      
        
        if stagPrice == "NA"{
            print("StagPrice : \(stagPrice)")
            if #available(iOS 11.0, *) {
                stagEntryPriceLabel.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            stagEntryPriceLabel.text = "Not allowed"
        }else if stagPrice == "0"{
            print("StagPrice : \(stagPrice)")
            if #available(iOS 11.0, *) {
                stagEntryPriceLabel.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            stagEntryPriceLabel.text = "Free"
        }else{
            print("StagPrice : \(stagPrice)")
            stagEntryPriceLabel.textColor = UIColor.white
            stagEntryPriceLabel.text = stagPrice
        }
        
        print("couple price 1234 :\(couplePrice)")
        
        if couplePrice == "NA"{
            print("CouplePrice : \(couplePrice)")
            if #available(iOS 11.0, *) {
                coupleEntryPriceLabel.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
           coupleEntryPriceLabel.text = "Not allowed"
        }else if couplePrice == "0"{
            print("CouplePrice : \(couplePrice)")
            if #available(iOS 11.0, *) {
                coupleEntryPriceLabel.textColor = UIColor.init(named: "maleColor")
            } else {
                // Fallback on earlier versions
            }
            coupleEntryPriceLabel.text = "Free"
        }else{
            print("CouplePrice : \(couplePrice)")
            coupleEntryPriceLabel.textColor = UIColor.white
            coupleEntryPriceLabel.text = couplePrice
        }
        
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func back_btn(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let EventDetailUpdatedVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
        
        EventDetailUpdatedVC.selectedEventId = selectedEventId
        
        EventDetailUpdatedVC.selectedClubId = selectedClubId
//        let PieChartViewController:PieChartViewController = storyboard?.instantiateViewController(withIdentifier: "PieChartViewController") as! PieChartViewController
        EventDetailUpdatedVC.eventIndexNumber = eventIndexNumberOnEntryDetail
        present(EventDetailUpdatedVC, animated: true, completion: nil)
    }
    

    

}
