//
//  couponViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 02/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class couponViewController: UIViewController {
   
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var lbl_eventName: UILabel!
    @IBOutlet weak var eventCouponImageView: UIImageView!
    @IBOutlet weak var lbl_eventAddress: UILabel!
    @IBOutlet weak var lbl_eventDate: UILabel!
    @IBOutlet weak var lbl_eventTime: UILabel!
    @IBOutlet weak var lbl_eventEntryFees: UILabel!
    @IBOutlet weak var btnOutlet_useCoupon: UIButton!
    @IBOutlet weak var btnOutlet_skipCoupon: UIButton!
    
    var clicked = true
    var selectedEntryType = ""
    var selectedClubId = ""
    var EventID = ""
    var eventName = ""
    var eventAddress = ""
    var eventDate = ""
    var eventStartTime = ""
    var eventEndTime = ""
    var eventEntryStagFees = ""
    var eventEntryCoupleFees = ""
    var couponStartingTime = String()
    var couponEndingTime = String()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        btnOutlet_useCoupon.isHidden = true
//        btnOutlet_skipCoupon.isHidden = true
        // Do any additional setup after loading the view.
        
        btnView.layer.cornerRadius = 8
        btnOutlet_useCoupon.layer.cornerRadius = 5
        btnOutlet_skipCoupon.layer.cornerRadius = 5
        couponView.layer.cornerRadius = 8
        
        
        print("print selected entry type : \(selectedEntryType)")
        
        print("print selected coupon details : \(selectedEntryType) - \(eventName) - \(eventAddress) - \(eventDate) - \(eventStartTime) - \(eventStartTime) - \(eventEntryStagFees) - \(eventEntryCoupleFees)")
        
        self.lbl_eventName.text = "\(eventName)"
        self.lbl_eventAddress.text = "\(eventAddress)"
        self.lbl_eventDate.text = "\(eventDate)"
        self.lbl_eventTime.text = "\(eventStartTime) - \(eventStartTime)"
       
        if selectedEntryType == "Stag"{
            
            self.lbl_eventEntryFees.text = "\(eventEntryStagFees)"
            
        }else if selectedEntryType == "Couple"{
            
             self.lbl_eventEntryFees.text = "\(eventEntryCoupleFees)"
        }
        
        
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    @IBAction func btnAction_openCoupon(_ sender: UIButton) {
//        if clicked{
//
//            btnOutlet_useCoupon.isHidden = false
//            btnOutlet_skipCoupon.isHidden = false
//
//            self.clicked = !clicked
//            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 5, options: .curveEaseOut, animations: {
//                //self.btnOutlet_openCoupon.setTitle("Coupon", for: .normal)
//                self.constHeightBottomView.constant = 595
//                self.view.layoutIfNeeded()
//            })
//
//        }
//        else{
//            self.clicked = true
//
//            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 5, options: .curveEaseOut, animations: {
//                //self.btnOutlet_openCoupon.setTitle("Coupon", for: .normal)
//                self.constHeightBottomView.constant = 0
//                self.view.layoutIfNeeded()
//            })
//
//            btnOutlet_useCoupon.isHidden = true
//            btnOutlet_skipCoupon.isHidden = true
//        }
//
//
//    }
    
    @IBAction func btn_skipCoupon(_ sender: UIButton) {
//        print("SssKKKK : \(String(describing: couponStartingTime))")
//        print("SssKKKK : \(String(describing: couponEndingTime))")
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "hh:mm a" //Your date format
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
//        let date:Date = dateFormatter.date(from: couponStartingTime)! //according to date format your date string
//        print("String to date : \(String(describing: date))") //Convert String to Date
        
        
//        let dateFormat = "hh:mm a"
//        let formatter = DateFormatter()
//        formatter.dateFormat = dateFormat
//        print("SssKKKK : \(String(describing: couponStartingTime))")
//        print("SssKKKK : \(String(describing: couponEndingTime))")
//        let startDate:Date = formatter.dateFormat.date(from: couponStartingTime as String)
//        let endDate:Date = formatter.dateFormat.date(from: couponEndingTime)
//        print("SssKooo : \(String(describing: startDate))")
//        print("SssKooo : \(String(describing: endDate))")
//
//        let currentDate = Date()
//
////        guard let startDate = startDate, let endDate = endDate else {
////            fatalError("Date Format does not match ⚠️")
////        }
//
//        if startDate < currentDate && currentDate < endDate {
//            print("Start : \(startDate)")
//            print("✅")
//        } else {
//            print("End : \(endDate)")
//            print("❌")
//        }

        
//        let date = Date()
//        formatter.dateFormat = "hh:mm a"
//        formatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
//        let time: String = formatter.string(from: date as Date)
//        print("TTT : \(time)")
//
        
        
//        //let time = formatter.string(from: date as Date)
//        formatter.dateFormat =
//        formatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
//        let serverDate: Date = formatter.date(from: time as String)!
        
 //       print("TxxxxxTT : \(serverDate)")
//        guard let couponStartingTime = date, let couponEndingTime = date else {
//            fatalError("Date Format does not match ⚠️")
//        }
        
//        if startDate < currentDate && currentDate < endDate {
//            print("✅")
//        } else {
//            print("❌")
//        }
////
//        if (couponStartingTime == date as Date && couponEndingTime == date as Date){
//            print("cccddd : \(date)")
//            print("Sss : \(couponStartingTime)")
//            print("Eee : \(couponEndingTime)")
//            
//        }else{
//            print("cccdddhhh : \(date)")
//            print("STtt : \(couponStartingTime)")
//            print("ETtt : \(couponEndingTime)")
//            
//        }
//        
        
        
        
        let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        
        let clubDetailVC = storyboard.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
        
        clubDetailVC.selectedClubId = selectedClubId
        clubDetailVC.selectedCouponFlag = "0"
        
        clubDetailVC.userClubEntryType = selectedEntryType
        self.present(clubDetailVC, animated: false, completion: nil)
        
    }
    @IBAction func btn_useCoupon(_ sender: UIButton) {
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        if selectedEntryType == "Stag"{
            
            let urlString = "http://www.nitevibe.in/webservices/check_in.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&entry_type=\(selectedEntryType)&coupon=1"
            
            print("URL String : \(urlString)")
            
            let url = NSURL(string: urlString)
            let urlRequest =  URLRequest(url: url! as URL)
            
            let req = Alamofire.request(urlRequest).validate().responseJSON { (response) -> Void in
                if let value = response.data {
                    do {
                        let json = try JSON(data: value)
                        if let dictionnary = json.dictionaryObject {
                            if let messageArray = dictionnary["message"] as? [[String: Any]] {
                                //                                for msgDict in messageArray {
                                //                                    //let msg = ChatModel.init(ChatListJSON: msgDict)
                                //
                                //                                }
                                
                                print("\(messageArray)")
                                
                            }
                        }
                    } catch {
                        print("cannot convert to Json")
                    }
                }
            }
            print(req.debugDescription)
            
            
            
        }else if selectedEntryType == "Couple"{
            
            let urlString = "http://www.nitevibe.in/webservices/check_in.php?user_id=\(User_Id!)&club_id=\(selectedClubId)&entry_type=\(selectedEntryType)&coupon=2"
            
            print("URL String : \(urlString)")
            
            let url = NSURL(string: urlString)
            let urlRequest =  URLRequest(url: url! as URL)
            
            let req = Alamofire.request(urlRequest).validate().responseJSON { (response) -> Void in
                if let value = response.data {
                    do {
                        let json = try JSON(data: value)
                        if let dictionnary = json.dictionaryObject {
                            if let messageArray = dictionnary["message"] as? [[String: Any]] {
//                                for msgDict in messageArray {
//                                    //let msg = ChatModel.init(ChatListJSON: msgDict)
//
//                                }
                                
                                print("\(messageArray)")
  
                            }
                        }
                    } catch {
                        print("cannot convert to Json")
                    }
                }
            }
            print(req.debugDescription)
            
        }
        

        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
        
        let SwipeVC = storyboard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
        SwipeVC.selectedClubIdFromSwipe = selectedClubId
        SwipeVC.selectedCkeckInEventId = EventID
        SwipeVC.selectedGoingFlag = "0"
     
        self.present(SwipeVC, animated: false, completion: nil)
    }
    
    

}
