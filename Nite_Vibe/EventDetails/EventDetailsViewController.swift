//
//  EventDetailsViewController.swift
//  Nite_Vibe
//
//  Created by Appmonks on 05/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire

class EventDetailsViewController: UIViewController {
    @IBOutlet weak var lbl_eventFees: UILabel!
    @IBOutlet weak var lbl_malePercent: UILabel!
    @IBOutlet weak var lbl_femalePercent: UILabel!
    @IBOutlet weak var whosGoing: UICollectionView!
    @IBOutlet weak var progressBar_Outlet: UIProgressView!
    @IBOutlet weak var lbl_entryFees: UILabel!
    @IBOutlet weak var lbl_peopleGoingforEvent: UILabel!
    @IBOutlet weak var lbl_eventDate: UILabel!
    @IBOutlet weak var lbl_textAboutevents: UILabel!
    @IBOutlet weak var lbl_eventName: UILabel!
    @IBOutlet weak var pageControllOutlet: UIPageControl!
    @IBOutlet weak var eventPicture_ImageView: UIImageView!
    
    var eventDatesString = String()
    var eventNameString = String()
    var peopleAreGoingstring = String()
    var priceString = String()
    var maleCountInt = Int()
    var femaleCountInt = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.couponView.isHidden = true


        getEventDetailData()
        // Do any additional setup after loading the view.
    }



    @IBAction func btn_ViewCoupons(_ sender: UIButton) {

//        self.couponView.isHidden = false
//        print("Coupon Clicked")
    }

    @IBAction func btn_ViewEntryFeesDetails(_ sender: UIButton) {
        let EntryDetailsViewController:EntryDetailsViewController = storyboard?.instantiateViewController(withIdentifier: "EntryDetailsViewController") as! EntryDetailsViewController

        present(EntryDetailsViewController, animated: true, completion: nil)
    }
    @IBAction func back_btn(_ sender: UIButton) {
    }

    @IBAction func eventPageControll_Action(_ sender: UIPageControl) {
    }
    
    @IBAction func imGoing_action(_ sender: UIButton) {
    }

//    @IBAction func couponClose_btn(_ sender: UIButton) {
//        self.couponView.isHidden = true
//    }

    func getEventDetailData(){

        print("get Event")


        let URL_String = "http://www.nitevibe.in/webservices/event_details.php?user_id=4&club_id=1&selected_id=1"
        print("Login Url String: \(URL_String)")

        Alamofire.request(URL_String,method:.get).responseJSON{
            response in

            print(" response :\(response.request!)")
            print(" response :\(response.request!)")

            guard response.result.isSuccess  else {
                print("Block 1 ")
                print(" response :\(response)")
                print("Error with response: \(String(describing: response.result.error))")

                return

            }

            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {

                print(" response :\(response)")

                print("Error with dictionary: \(String(describing: response.result.error))")

                return

            }

            print("message...\(dict)")
            print("dict respo: \(dict["response"]!)")
            // print("dict message:\(dict["message"]!)")

            guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                print("test 3")
                print("Error with dictionary data: \(String(describing: response.result.error))")
                return
            }

            //self.dictData.append(dictData,])
            print(" dict event  data..\(dictData)")

            for dict in dictData{

                print("Event Name: \(String(describing: dict["event_name"]!))")
                print("Event Date : \(String(describing: dict["event_date"]!))")

                self.lbl_eventName.text = "\(String(describing: dict["event_name"]!))"
                self.lbl_eventDate.text = "\(String(describing: dict["event_date"]!))"


            }

        }




    }
    
}
