//
//  WhosCheckInModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 20/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation

import SwiftyJSON

struct WhosCheckInModel{
    var user_id = String()
    var user_age = String()
    var user_gender = String()
    var user_photo = String()
    
    init()
    {}
    
    init(whosJSON:[String:Any]){
        
        if let userID = whosJSON["user_id"] as? String{
            
            self.user_id = userID
        }
        
        if let userAge = whosJSON["user_age"] as? String{
            
            self.user_age = userAge
            
        }
        if let userGender = whosJSON["user_gender"] as? String{
            self.user_gender = userGender
            
        }
        if let userPhoto = whosJSON["user_photo"] as? String{
            self.user_photo = userPhoto
        }
        
    }
    
}
