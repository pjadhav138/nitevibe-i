//
//  ViewMoreViewController.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 10/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class ViewMoreViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{

    @IBOutlet weak var otherImage: UIImageView!
    @IBOutlet weak var agePrefImage: UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var otherHeight: NSLayoutConstraint!
    @IBOutlet weak var viewMoreHeight: NSLayoutConstraint!
    @IBOutlet weak var otherCollection: UICollectionView!
    @IBOutlet weak var viewMoreCollection: UICollectionView!
    
    var selectedGoingFlag = ""
    var selectedVMEventId = ""
    var selectedVMClubId = ""
    var userGoingIds = [String]()
    var userGoingImages = [String]()
    var viewMoreCategory = [String]()
    var userGoingName = [String]()
    var userGoing_NonPref_Ids = [String]()
    var userGoing_NonPref_Images = [String]()
    var otherCategory = [String]()
    var userGoing_NonPref_Name = [String]()
    

    @IBOutlet weak var agePrefLabel: UILabel!
    
    @IBOutlet weak var othersLabel: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("CLUB ID 0:\(selectedVMClubId)")
        print("EVENT ID 0:\(selectedVMEventId)")
        self.othersLabel.isHidden = true
        self.agePrefLabel.isHidden = true
        viewMoreCollection.delegate = self
        viewMoreCollection.dataSource = self
        //viewMoreCollection.sizeToFit()
       // otherCollection.sizeToFit()
        fetchswiftyviewMoreData()
        //viewMoreCollection.isScrollEnabled = false
       // otherCollection.isScrollEnabled = false
        otherCollection.delegate = self
        otherCollection.dataSource = self
        fetchswifty_ViewMore_NonPrefData()
        
        
     self.backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
     //   scrollview.resizeScrollViewContentSizeforViewMore()
        
    }
    
//    func viewWillLayoutSubviewsForView() {
//        super.updateViewConstraints()
//        self.viewMoreHeight?.constant = self.viewMoreCollection.contentSize.height
//    }
    
//    func viewWillLayoutSubviewsForOther() {
//        super.updateViewConstraints()
//        self.otherHeight?.constant = self.otherCollection.contentSize.height
//    }
    
    @objc func back(){
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        
        let eventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
        print("CLUB ID 1:\(selectedVMClubId)")
        print("EVENT ID 1:\(selectedVMEventId)")
        eventDetailVC.selectedClubId = selectedVMClubId
        eventDetailVC.selectedEventId = selectedVMEventId
        self.present(eventDetailVC, animated: true, completion: nil)
        
    }
    
    func fetchswiftyviewMoreData(){
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("CLUB ID 2:\(selectedVMClubId)")
        print("EVENT ID 2:\(selectedVMEventId)")
        //print("selectedViewMore : \(selectedVMEventId)")

       let urlString1 = "http://www.nitevibe.in/webservices/user_going_list.php?user_id=\(User_Id!)&club_id=\(selectedVMClubId)&event_id=\(selectedVMEventId)"
        print("userDataUrl: \(urlString1)")
        
        do{
            
            var urlRequest = try URLRequest(url: urlString1, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                if dictData.isEmpty{
                    
                    
                    self.agePrefLabel.isHidden = true
                    self.viewMoreCollection.isHidden = true
                    self.agePrefImage.isHidden = true
                    
                }else{
                    self.agePrefLabel.isHidden = false
                    self.viewMoreCollection.isHidden = false
                    self.agePrefImage.isHidden = false
                    
                    print(" dict data event detail \(dictData)")
                    
                    
                    
                    for data in dictData{
                        
                        self.userGoingIds.append(data["user_going_id"] as! String)
                        self.userGoingImages.append(data["profile"] as! String)
                        self.userGoingName.append(data["name"] as! String)
                        self.viewMoreCategory.append(data["category"] as! String)
                        
                        print("UserGoingIds : \(self.userGoingIds)")
                        print("UserGoingImages : \(self.userGoingImages)")
                        print("UserGoingNames : \(self.userGoingName)")
                        print("viewMoreCategory : \(self.viewMoreCategory)")
                        
                        self.viewMoreCollection.reloadData()
//                        let height = self.viewMoreCollection.collectionViewLayout.collectionViewContentSize.height
//                        self.viewMoreHeight.constant = height
                       // self.view.setNeedsLayout() Or self.view.layoutIfNeeded()
                        
                    }
                    
                   
                
            }
                    
        }
                
        }  catch{
                print("error")
                
            }
        
    }
    
    func fetchswifty_ViewMore_NonPrefData(){
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        //print("selectedViewMore : \(selectedVMEventId)")
        
        
        print("CLUB ID 3:\(selectedVMClubId)")
        print("EVENT ID 3:\(selectedVMEventId)")
        
        let urlString = "http://www.nitevibe.in/webservices/going_nonpref_list.php?user_id=\(User_Id!)&club_id=\(selectedVMClubId)&event_id=\(selectedVMEventId)"
        print("userDataUrl: \(urlString)")
        
        do{
            
            var urlRequest = try URLRequest(url: urlString, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print(" dict data event detail \(dictData)")
                
                if dictData.count == 0{
                    
                    self.othersLabel.isHidden = true
                    self.otherCollection.isHidden = true
                    self.otherImage.isHidden = true
                   
                    
                    
                }else{
                    
                    //self.othersLabel.isHidden = false
                    //self.otherCollection.isHidden = false
                    //self.otherImage.isHidden = false
                self.userGoing_NonPref_Images.removeAll()
                for data in dictData{
                    
                    self.userGoing_NonPref_Ids.append(data["user_going_id"] as! String)
                    self.userGoing_NonPref_Images.append(data["profile"] as! String)
                    self.otherCategory.append(data["category"] as! String)
                    self.userGoing_NonPref_Name.append(data["name"] as! String)
                    
                    print("userGoing_NonPref_Ids : \(self.userGoing_NonPref_Ids)")
                    print("userGoing_NonPref_Images : \(self.userGoing_NonPref_Images)")
                    print("otherCategory : \(self.otherCategory)")
                    
                    DispatchQueue.main.async {
                        self.otherCollection.reloadData()
//                        let height = self.otherCollection.collectionViewLayout.collectionViewContentSize.height
//                        self.otherHeight.constant = height
                    }
    
                    
                    }
                }
            }
            
        }catch{
            print("error")
            
        }
        
        
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == viewMoreCollection{

            
            return userGoingImages.count
        }
        else{
            

            return userGoing_NonPref_Images.count
            
        }
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == viewMoreCollection{
            let cell:ViewMoreCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "viewMoreCell", for: indexPath) as! ViewMoreCollectionViewCell
            
            //cell.layer.borderWidth = 1
            cell.userGoingImageView.layer.borderWidth = 2
            cell.clipsToBounds = true
            
            
            if viewMoreCategory[indexPath.row] == "1"{
               
                
                cell.userGoingImageView.layer.borderColor = UIColor(red: 243/255, green: 40/255, blue: 104/255, alpha: 1.0).cgColor
                
                
            }else if viewMoreCategory[indexPath.row] == "2"{
                
                //cell.userGoingImageView.layer.borderColor = UIColor.white.cgColor
                cell.userGoingImageView.layer.borderColor = UIColor(red: 199/255, green: 202/255, blue: 219/255, alpha: 1.0).cgColor
                
            }else if viewMoreCategory[indexPath.row] == "3"{
                
                cell.userGoingImageView.layer.borderColor = UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 1.0).cgColor
                
            }else{
                cell.userGoingImageView.layer.borderColor = UIColor(red: 158/255, green: 285/255, blue: 22/255, alpha: 1.0).cgColor
                
            }
            
            
            
            if self.userGoingImages[indexPath.row] == ""{
                
                print("no image found")
                // cell.userGoingImageView.image = UIImage(named: "Profile.png")
                cell.userGoingImageView.image = #imageLiteral(resourceName: "defaultIcon")
                cell.userGoingName.text = userGoingName[indexPath.row]
                
            }else{
                cell.userGoingImageView.kf.setImage(with: URL(string:userGoingImages[indexPath.row]))
                cell.userGoingName.text = userGoingName[indexPath.row]
            }
            
            
            return cell
        
        }else {
            
            let cell1:OtherCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherCell", for: indexPath) as! OtherCollectionViewCell

            cell1.clipsToBounds = true
            
            cell1.otherUserImages.layer.borderWidth = 2
            
            if otherCategory[indexPath.row] == "1"{
                
                
                cell1.otherUserImages.layer.borderColor = UIColor(red: 243/255, green: 40/255, blue: 104/255, alpha: 1.0).cgColor
                
            }else if otherCategory[indexPath.row] == "2"{
                
                
                cell1.otherUserImages.layer.borderColor = UIColor(red: 199/255, green: 202/255, blue: 219/255, alpha: 1.0).cgColor
                
                
            }else if otherCategory[indexPath.row] == "3"{
                
               
                cell1.otherUserImages.layer.borderColor = UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 1.0).cgColor
                
            }else{
                
                cell1.otherUserImages.layer.borderColor = UIColor(red: 158/255, green: 285/255, blue: 22/255, alpha: 1.0).cgColor
            
            }
           
            
            if self.userGoing_NonPref_Images[indexPath.row] == ""{
                
                print("no image found")
                cell1.otherUserImages.image = #imageLiteral(resourceName: "defaultIcon")
                cell1.otherUserName.text = userGoing_NonPref_Name[indexPath.row]
                
            }else{
                cell1.otherUserImages.kf.setImage(with: URL(string:userGoing_NonPref_Images[indexPath.row]))
                cell1.otherUserName.text = userGoing_NonPref_Name[indexPath.row]
                
            }
            
            
            
            
            
            return cell1
        }
    }

//    @IBAction func backBtn_action(_ sender: UIButton) {
//
//
//
//
//    }
//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == viewMoreCollection{
            
            print("viewmoreCategory : \(viewMoreCategory[indexPath.row])")
            
            if viewMoreCategory[indexPath.row] == "2"{
                print("CLUB ID 4:\(selectedVMClubId)")
                print("EVENT ID 4:\(selectedVMEventId)")
                let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
                let useProfileVC = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                useProfileVC.selectedVCFlag = "3"
                useProfileVC.userId = userGoingIds[indexPath.row]
                useProfileVC.userPic = userGoingImages[indexPath.row]
                useProfileVC.userName = userGoingName[indexPath.row]
                useProfileVC.selectedClubId = selectedVMClubId
               // print("selectedViewMore : \(selectedVMEventId)")
                useProfileVC.selectedEventId = selectedVMEventId
                self.present(useProfileVC, animated: true)
                
            }else{
                print("CLUB ID 5:\(selectedVMClubId)")
                print("EVENT ID 5:\(selectedVMEventId)")
                let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
                
                let SwipeVC =  storyBoard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
                SwipeVC.selectedGoingFlag = "3"
                SwipeVC.selectedViewMoreUserId = userGoingIds[indexPath.row]
                SwipeVC.selectedClubIdFromSwipe = selectedVMClubId
                print("selectedViewMore : \(selectedVMEventId)")
                SwipeVC.selectedEventIdFromSwipe = selectedVMEventId
                
                self.present(SwipeVC, animated: true, completion: nil)
            }
//            }else if viewMoreCategory[indexPath.row] == "2"{                    //for dislike
//
//                let defaults = UserDefaults.standard
//
//                defaults.set("3", forKey: "selectedgoingflag")
//                //let selectedGoingFlag = defaults.value(forKey: "selectedgoingflag") as! String
//
//                let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
//                let useProfileVC = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//                useProfileVC.selectedVCFlag = "3"
//                useProfileVC.userId = userGoingIds[indexPath.row]
//                useProfileVC.userPic = userGoingImages[indexPath.row]
//                useProfileVC.userName = userGoingName[indexPath.row]
//                useProfileVC.selectedClubId = selectedViewMoreClubId
//                print("selectedViewMore : \(selectedViewMoreEventId)")
//                useProfileVC.selectedEventId = selectedViewMoreEventId
//                self.present(useProfileVC, animated: true)
//
//            }else if viewMoreCategory[indexPath.row] == "3"{
//
//                let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
//
//                let SwipeVC =  storyBoard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
//                SwipeVC.selectedGoingFlag = "3"
//                SwipeVC.selectedViewMoreUserId = userGoingIds[indexPath.row]
//                SwipeVC.selectedClubIdFromSwipe = selectedViewMoreClubId
//                print("selectedViewMore : \(selectedViewMoreEventId)")
//                SwipeVC.selectedEventIdFromSwipe = selectedViewMoreEventId
//
//                self.present(SwipeVC, animated: true, completion: nil)
//            }
            
        }else{
            print("CLUB ID 6:\(selectedVMClubId)")
            print("EVENT ID 6:\(selectedVMEventId)")
            let defaults = UserDefaults.standard

            defaults.set("3", forKey: "selectedgoingflag")
            //let selectedGoingFlag = defaults.value(forKey: "selectedgoingflag") as! String
            
            let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            let useProfileVC = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            useProfileVC.selectedVCFlag = "3"
            useProfileVC.userId = userGoing_NonPref_Ids[indexPath.row]
            useProfileVC.userPic = userGoing_NonPref_Images[indexPath.row]
            useProfileVC.userName = userGoing_NonPref_Name[indexPath.row]
            useProfileVC.selectedClubId = selectedVMClubId
            print("selectedViewMore : \(selectedVMEventId)")
            useProfileVC.selectedEventId = selectedVMEventId
            self.present(useProfileVC, animated: true)
         
            
        }
        
        
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        self.viewWillLayoutSubviews()
//    }
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        //if collectionView == viewMoreCollection{
//            
//            self.viewWillLayoutSubviewsForView()
//            
//        //}else{
//          
//            self.viewWillLayoutSubviewsForOther()
//            
//        //}
//    }
    



//extension UIScrollView {
//
//    func resizeScrollViewContentSizeforViewMore() {
//
//        var contentRect = CGRect.zero
//
//        for view in self.subviews {
//
//            contentRect = contentRect.union(view.frame)
//
//        }
//
//        self.contentSize = contentRect.size
//
//    }
//
//}
}
