//
//  ClubDetailModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 20/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ClubDetailModel{
    
    var userPreferenceDictionary = [UserPreferenceModel]()
    var todaysEventDictionary = [TodaysEventModel]()
    
    init()
    {
        self.todaysEventDictionary = []
        self.userPreferenceDictionary = []
    }
    
    init(clubDetailJSON:[String:Any]){
        
        if let userPrefData = clubDetailJSON["user_pref"] as? [[String:Any]]{
            
            for userPrefInfo in userPrefData{
                
                let userPref = UserPreferenceModel(userPrefJson: userPrefInfo)
                
                self.userPreferenceDictionary.append(userPref)
                
            }
            if let todaysEventData = clubDetailJSON["today_event"] as? [[String:Any]]{
                
                for todaysEventInfo in todaysEventData{
                    
                    let todaysEvent =  TodaysEventModel(todaysEventJSON: todaysEventInfo)
                    
                    self.todaysEventDictionary.append(todaysEvent)
                }
            }
            
            
            
        }
        
    }
}


struct UserPreferenceModel{
    
    var pref_age = String()
    var pref_gender = String()
    
    init()
    {}
    
    init(userPrefJson:[String:Any]){
        
        if let prefAge = userPrefJson["age"] as? String{
            
            self.pref_age = prefAge
        }
        if let prefGender = userPrefJson["gender"] as? String{
            
            self.pref_gender = prefGender
        }
        
    }
    
}

struct TodaysEventModel{
    
    var total_coupon_count = Int()
    var left_coupon_count = Int()
    var eventId_for_swipe = String()
    var event_check_in_flag = String()
    var couponStarttime = String()
    var couponEndTime = String()
    var CouponDateTimeStart: Date?
    var CouponDateTimeEnd: Date?
    
    static let coupondf = DateFormatter()
    
    init()
    {}
    
    init(todaysEventJSON:[String:Any]){
        
        if let totalCouponCount = todaysEventJSON["coupon"] as? Int{
            
            self.total_coupon_count = totalCouponCount
        }
        
        if let leftCouponcount = todaysEventJSON["left_coupon"] as? Int{
            
            self.left_coupon_count = leftCouponcount
        }
        
        if let todays_EventID = todaysEventJSON["today_event_id"] as? String{
            
            self.eventId_for_swipe = todays_EventID
        }
        
        if let eventCheckInFlag = todaysEventJSON["checkin"] as? String{
            
            self.event_check_in_flag = eventCheckInFlag
        }
        
        if let couponSTime = todaysEventJSON["coupon_start"] as? String{
            
            self.couponStarttime = couponSTime

        }
        
        if let couponETime = todaysEventJSON["coupon_end"] as? String{
            
            self.couponEndTime = couponETime

        }
        
        
    }
    
    
}

