//
//  OtherCollectionViewCell.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 10/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class OtherCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var otherUserImages: UIImageView!
    
    @IBOutlet weak var otherUserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        otherUserImages.layer.borderWidth=2
        otherUserImages.layer.masksToBounds = false
//        otherUserImages.layer.borderColor = UIColor.green.cgColor
        otherUserImages.layer.cornerRadius = otherUserImages.frame.height/2
        otherUserImages.clipsToBounds = true
        
    }
}
