//
//  ViewMoreCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 21/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ViewMoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userGoingImageView: UIImageView!
    
    @IBOutlet weak var userGoingName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
//    userGoingImageView.layer.borderWidth=2
    userGoingImageView.layer.masksToBounds = false
//    userGoingImageView.layer.borderColor = UIColor.green.cgColor
    userGoingImageView.layer.cornerRadius = userGoingImageView.frame.height/2
    userGoingImageView.clipsToBounds = true
    
    }
    
}
