//
//  goodvibesViewController.swift
//  Nitevibe
//
//  Created by Vinayak Bhor on 12/10/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
class goodvibesViewController: UIViewController {
    
    @IBOutlet weak var keepSwiping_btnOutlet: UIButton!
    @IBOutlet weak var textFirst_btnOutlet: UIButton!
    @IBOutlet weak var yourProfilePic: UIImageView!
    @IBOutlet weak var machedProfilePic: UIImageView!
    var yourProfilePicConstraints:[NSLayoutConstraint] = []
    var matchedProfilePicConstraints:[NSLayoutConstraint] = []
    
    var matchImageUrlString = ""
    var matchUserId = ""
    var currentUserImageUrlString = ""
    var currentUserId = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(" matchImageUrlString \(matchImageUrlString)")
        print(" matchUserId \(matchUserId)")
        print(" currentUserImageUrlString\(currentUserImageUrlString)")
        print(" currentUserId \(currentUserId)")
        //view.alpha = 0.6
        yourProfilePic.kf.setImage(with: URL(string: currentUserImageUrlString))
        machedProfilePic.kf.setImage(with: URL(string: matchImageUrlString))
        
        
        
        keepSwiping_btnOutlet.layer.cornerRadius = 10
        keepSwiping_btnOutlet.clipsToBounds = true
        
        textFirst_btnOutlet.layer.cornerRadius = 10
        textFirst_btnOutlet.clipsToBounds = true
        
        //        let yourImageHeight = yourProfilePic.heightAnchor.constraint(equalToConstant: 240)
        //        let yourImageWidth = yourProfilePic.widthAnchor.constraint(equalToConstant: 200)
        //        yourProfilePicConstraints = [yourImageHeight,yourImageWidth]
        //        NSLayoutConstraint.activate(yourProfilePicConstraints)
        
        yourProfilePic.layer.borderWidth = 3
        yourProfilePic.layer.masksToBounds = false
        yourProfilePic.layer.borderColor = UIColor.blue.cgColor
        self.yourProfilePic.layer.cornerRadius = yourProfilePic.frame.height/2
        
        //self.yourProfilePic.layer.cornerRadius = self.yourProfilePic.bounds.height / 2
        
        self.yourProfilePic.clipsToBounds = true
        
        
        //        let matchedImageHeight = machedProfilePic.heightAnchor.constraint(equalToConstant: 240)
        //        let matchedImageWidth = machedProfilePic.widthAnchor.constraint(equalToConstant: 200)
        //        matchedProfilePicConstraints = [matchedImageHeight,matchedImageWidth]
        //        NSLayoutConstraint.activate(matchedProfilePicConstraints)
        
        machedProfilePic.layer.borderWidth=3
        machedProfilePic.layer.masksToBounds = false
        machedProfilePic.layer.borderColor = UIColor.blue.cgColor
        machedProfilePic.layer.borderColor = UIColor(red: 255/255, green: 54/255, blue: 104/255, alpha: 1.0).cgColor
        self.machedProfilePic.layer.cornerRadius = self.machedProfilePic.frame.height/2
        
        // self.machedProfilePic.layer.cornerRadius = self.machedProfilePic.bounds.height / 2
        
        self.machedProfilePic.clipsToBounds = true


        
    }
    
    @IBAction func textFirst_btnAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        
        BaseController.selectedIndex = 2
        self.present(BaseController, animated: true, completion: nil)
    }
    @IBAction func keepSwipingAction(_ sender: Any) {
        print("Keep Swiping Tapped")
        
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        
//        self.addChildViewController(goodvibesVC)
//        self.view.addSubview((goodvibesVC.view)!)
 //       self.dismiss(animated: true, completion: nil)
        
    }
    
//    @IBAction func keepSwiping_btnAction(_ sender: UIButton) {
//        
//        print("Keep Swiping Tap")
//        dismiss(animated: true, completion: nil)
//       // self.dismiss(animated: true, completion: nil)
//        
////        let defaults = UserDefaults.standard
////
////        var selectedGoingFlag = defaults.value(forKey: "selectedgoingflag") as! String
////        var selecteEventId = defaults.value(forKey:"selectedEventIdFromSwipe") as! String
////        print("selected user event id \(String(describing: selecteEventId))")
////        var selectedCkeckInEventId = defaults.value(forKey:"selectedCkeckInEventId") as! String
////        //userDefault.set(selectedCkeckInEventId, forKey: "selectedCkeckInEventId")
////
////        if selectedGoingFlag == "0"{
////
//////            let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
//////            let SwipeVC =  storyBoard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
//////            SwipeVC.selectedGoingFlag = "0"
//////            SwipeVC.selectedClubIdFromSwipe = selectedCkeckInEventId
//////            SwipeVC.selectedEventIdFromSwipe = selecteEventId
//////            self.present(SwipeVC, animated: true, completion: nil)
////            self.dismiss(animated: true, completion: nil)
////
////        }else if selectedGoingFlag == "1"{
////
////
//////            let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
//////            let SwipeVC =  storyBoard.instantiateViewController(withIdentifier: "SwipeViewController") as! SwipeViewController
//////            SwipeVC.selectedGoingFlag = "1"
//////            SwipeVC.selectedClubIdFromSwipe = selectedCkeckInEventId
//////            SwipeVC.selectedEventIdFromSwipe = selecteEventId
//////            self.present(SwipeVC, animated: true, completion: nil)
////            self.dismiss(animated: true, completion: nil)
////
////        }else if selectedGoingFlag == ""{
////            self.dismiss(animated: true, completion: nil)
////        }
////
//        
//        
//    }
//    

}
