//
//  SuperVibedViewController.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 21/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON

class SuperVibedViewController: UIViewController {
    @IBOutlet weak var imgOverlaySymbol: UIImageView!
    @IBOutlet weak var likeOverlyImg: UIImageView!
    @IBOutlet weak var noswipeleftImg: UIImageView!
    @IBOutlet weak var btnBackOnSuperview: UIButton!
    
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var lblNoSwipeLeft: UILabel!
    
    @IBOutlet weak var btnFindevents: UIButton!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var btnSupervibed: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var locationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var lbl_Occupation: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var blurImage: UIImageView!
    @IBOutlet weak var whoSupervibeMe_userImage: UIImageView!
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var swipeCardView: UIView!
    
    var SuperVibeArray:[UIImage] = [UIImage(named: "supervibe3")!,UIImage(named: "supervibe2")!,UIImage(named: "supervibe1")!,UIImage(named: "supervibe0")!]
    
    var whoSupervibeMeID = ""
    var eventId = ""
    var WhoSupervibeData = [SupervibeModel]()
    var supervibeCount = Int()
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var BackBtnView: UIView!
    var btn_back: UIButton!
    var backImg: UIImage!
    
    var HomeBtnView: UIView!
    var home_btn: UIButton!
    var homeImg: UIImage!
    
    var uID = String()
    var uName = String()
    var uLoc = String()
    var uAge = String()
    var uWorkAs = String()
    var uPhoto = String()
    var responseMessage = Int()
    var superLikeCountFlag = Int()
    var currentIndex = 0
    var isMakeUndo = false
    
    var cardValueArray = [String]()
    var userIdArray = [String]()
    var userNameArray = [String]()
    var userAgeArray = [String]()
    var userLocationArray = [String]()
    var userOccupationArray = [String]()
    var userProfile = [String]()
    var swipeCount = Int()
    var supervibe = [Int]()
    var swipeData = [SupervibeModel]()
    
    var selectedGoingFlag = ""
    var selectedClubIdFromSwipe = ""
    var selectedEventIdFromSwipe = ""
    
    var category = String()
    var likeCategory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        print("Who Supervibe ME : \(whoSupervibeMeID)")
        print("Event Id : \(eventId)")
        swipeCardView.layer.cornerRadius = 8
        whoSupervibeMe_userImage.layer.cornerRadius = 12
        whoSupervibeMe_userImage.clipsToBounds = true
        
        blurImage.layer.cornerRadius = 12
        self.blurImage.clipsToBounds = true
        self.noswipeleftImg.isHidden = true
        self.btnBackOnSuperview.isHidden = true
        self.btnFindevents.isHidden = true
        self.lblNoSwipeLeft.isHidden = true
        self.likeOverlyImg.isHidden = true
        self.imgOverlaySymbol.isHidden = true
        btnInfo.clipsToBounds = true
        btnInfo.layer.masksToBounds = true
        btnInfo.layer.cornerRadius = btnInfo.frame.height/2
        btnInfo.backgroundColor = UIColor.white.withAlphaComponent(1)
        btnInfo.setTitle("i", for: .normal)
        btnInfo.setTitleColor(UIColor.blue.withAlphaComponent(1), for: .normal)
        btnInfo.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 21)
        getSupervibedData()
        //getGoingData()
        
    }
    
    func addComponents(){
        
        BackBtnView = UIView(frame: CGRect(x: 17, y: 40, width: 46 , height:46 ))
        BackBtnView.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        backImg = UIImage(named: "BackButton") as UIImage?
        btn_back = UIButton(type: UIButtonType.custom) as UIButton
        btn_back.frame = CGRect(x:5, y:5, width:36, height:36)
        btn_back.setImage(backImg, for:.normal)
        btn_back.isHidden = true
        btn_back.addTarget(self, action:#selector(backButtonTapped) , for: .touchUpInside)
        BackBtnView.addSubview(btn_back)
        
        self.view.addSubview(BackBtnView)
        
        
        HomeBtnView = UIView(frame: CGRect(x: screenWidth - 75, y: 15, width: 76 , height: 100 ))
        HomeBtnView.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        homeImg = UIImage(named: "NavHome") as UIImage?
        home_btn = UIButton(type: UIButtonType.custom) as UIButton
        home_btn.frame = CGRect(x:0, y:0, width:66, height:100)
        home_btn.setImage(homeImg, for:.normal)
        home_btn.isHidden = true
        home_btn.addTarget(self, action: #selector(home_Btn_Action), for: .touchUpInside)
        HomeBtnView.addSubview(home_btn)
        
        self.view.addSubview(HomeBtnView)
        
        
        
    }
    @IBAction func btnActionBackOnCard(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        present(HomeVC, animated: true, completion: nil)
    }
    @IBAction func btnActionBackOnSuperview(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        present(HomeVC, animated: true, completion: nil)
    }
    @IBAction func btnActionFindEvents(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        present(HomeVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnActionInfo(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        
        nextViewController.selectedVCFlag = "6"
        nextViewController.userId = uID
        nextViewController.userName = uName
        nextViewController.userLocation = uLoc
        nextViewController.userWork = uWorkAs
        nextViewController.userAge = uAge
        nextViewController.userPic = uPhoto
        nextViewController.selectedClubId = selectedClubIdFromSwipe
        nextViewController.selectedEventId = selectedEventIdFromSwipe
        
        
        self.present(nextViewController, animated: true)
        
    }
    
    
    
    
    @objc func backButtonTapped(){
        
        
        if selectedGoingFlag == "1"{
            
            let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
            
            let EventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
            EventDetailVC.selectedClubId = selectedClubIdFromSwipe
            EventDetailVC.selectedEventId = selectedEventIdFromSwipe
            present(EventDetailVC, animated: true, completion: nil)
            //self.dismiss(animated: false, completion: nil)
            
        }
        
        
    }
    
    @objc func home_Btn_Action() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let HomeVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        //EventDetailVC.selectedClubId = selectedClubIdFromSwipe
        // EventDetailVC.selectedEventId = selectedEventIdFromSwipe
        //present(HomeVC, animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func getSupervibedData(){
        print("Who Supervibe ME 1: \(whoSupervibeMeID)")
        print("Event Id 1: \(eventId)")
        //need to pass remaining data in webservice like user_id category event id
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        
        let URL_String = "http://www.nitevibe.in/webservices/who_supervibe.php?login_userid=\(User_Id!)&user_id=\(whoSupervibeMeID)&event_id=\(eventId)"
        print("Url 1: \(URL_String)")
        
        let url = NSURL(string: URL_String)
        var urlRequest =  URLRequest(url: url! as URL)
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).responseJSON{
            
            (response) in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        
                        print("Dict : \(dictionnary.count)")
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            // print("print swipe data  \(messageArray)")
                            print("print count: \(messageArray.count)")
                            
                            if messageArray.count == 0{
                                
                                print("no data found")
                                
                                
                            }else{
                                
                                self.WhoSupervibeData.removeAll()
                                for arr in messageArray {
                                    
                                    self.WhoSupervibeData.append(SupervibeModel(json: arr))
                                    
                                    print("get swipe data \(self.WhoSupervibeData.count)")
                                    self.supervibeCount = self.WhoSupervibeData.count
                                    
                                    
                                    for i in 0..<self.WhoSupervibeData.count{
                                        
                                        print("print whoSupervibe id : \(self.WhoSupervibeData[i].user_id)")
                                        self.uID = self.WhoSupervibeData[i].user_id
                                        
                                        print("print whoSupervibe name : \(self.WhoSupervibeData[i].userName)")
                                        self.uName = self.WhoSupervibeData[i].userName
                                        self.lbl_Name.text = self.WhoSupervibeData[i].userName
                                        
                                        print("print whoSupervibe images : \(self.WhoSupervibeData[i].userProfilePicUrl)")
                                        self.uPhoto = self.WhoSupervibeData[i].userProfilePicUrl
                                        self.whoSupervibeMe_userImage.kf.setImage(with: URL(string:self.WhoSupervibeData[i].userProfilePicUrl))
                                        
                                        print("print whoSupervibe age : \(self.WhoSupervibeData[i].userAge)")
                                        self.uAge = self.WhoSupervibeData[i].userAge
                                        
                                        print("print whoSupervibe occupation : \(self.WhoSupervibeData[i].userOccupation)")
                                        self.uWorkAs = self.WhoSupervibeData[i].userOccupation
                                        self.lbl_Occupation.text = self.WhoSupervibeData[i].userOccupation
                                        
                                        
                                        print("print whoSupervibe location : \(self.WhoSupervibeData[i].userLocation)")
                                        self.uLoc = self.WhoSupervibeData[i].userLocation
                                        
                                        self.lbl_Location.text = self.WhoSupervibeData[i].userLocation
                                        
                                        print("print whoSupervibe goingEvent : \(self.WhoSupervibeData[i].supervibeForEvent)")
                                        
                                        print("print supervibe count :\(self.WhoSupervibeData[i].superVibeCount)")
                                        
                                        if self.WhoSupervibeData[i].superVibeCount == 0{
                                            
                                            
                                            self.btnSupervibed.setImage(UIImage(named: "supervibe3")!, for: .normal)
                                            
                                            
                                        }else if self.WhoSupervibeData[i].superVibeCount == 1{
                                            self.btnSupervibed.setImage(UIImage(named: "supervibe2")!, for: .normal)
                                        }else if self.WhoSupervibeData[i].superVibeCount == 2{
                                            self.btnSupervibed.setImage(UIImage(named: "supervibe1")!, for: .normal)
                                            
                                        }else if self.WhoSupervibeData[i].superVibeCount == 3{
                                            self.btnSupervibed.setImage(UIImage(named: "supervibe0")!, for: .normal)
                                            
                                            self.btnSupervibed.isEnabled = false
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
        }
        
        
    }
    
    func transformCount(superVibeCount:Int){
        
        //let defaults = UserDefaults.standard
        // Udefaults.set(self.responseMessage, forKey: "responseMessage")
        // var superLikeCountFlag = defaults.value(forKey: "responseMessage") as! Int
        
        
        print("superLikeCountFlag : \(superLikeCountFlag)")
        if superLikeCountFlag == 0{
            self.btnSupervibed.setImage(UIImage(named: "supervibe2")!, for: .normal)
            
        }else if superLikeCountFlag == 1{
            self.btnSupervibed.setImage(UIImage(named: "supervibe1")!, for: .normal)
            
            
        }else if superLikeCountFlag == 2{
            self.btnSupervibed.setImage(UIImage(named: "supervibe0")!, for: .normal)
            self.btnSupervibed.isEnabled = false
            
        }else if superLikeCountFlag == 3{
            self.btnSupervibed.setImage(UIImage(named: "supervibe0")!, for: .normal)
            self.btnSupervibed.isEnabled = false
        }
        
    }
    
    
    func getSuperVibeCount(){
        
        URLCache.shared.removeAllCachedResponses()
        
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("EVID 7 :\(selectedEventIdFromSwipe)")
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/supervibe_count.php?user_id=\(User_Id!)&event_id=\(eventId)"
        
        print("url string : \(URL_String)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            let url = NSURL(string: URL_String )
            var urlRequest =  URLRequest(url: url! as URL)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,Int> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict superVibe respo: \(dict["response"]!)")
                print("dict superVibe message:\(dict["message"]!)")
                respo = dict["response"]!
                self.responseMessage = dict["message"]!
                
                
                print("message supervibe count : \(self.responseMessage)")
                
                
                
                
                let Udefaults = UserDefaults.standard
                Udefaults.removeObject(forKey: "responseMessage")
                Udefaults.set(self.responseMessage, forKey: "responseMessage")
                
                if(respo == 201){
                    print("Message: \(String(describing: self.responseMessage))")
                    
                }else if(respo == 200){
                    
                    self.superLikeCountFlag = self.responseMessage
                    print("superVibeCount : \(self.superLikeCountFlag)")
                    print("Message 49 :  \(String(describing: self.responseMessage))")
                    print("success")
                    
                    
                    
                    self.transformCount(superVibeCount:self.responseMessage)
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    func getSwipeData(){
        //need to pass remaining data in webservice like user_id category event id
        
        
        var currentUserId = String()
        var currentUserPhoto = String()
        var matchUserId = String()
        var matchUserPhoto = String()
        
        var respo = Int()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        let selecteEventId = getUserData.value(forKey:"selectedEventIdFromSwipe")
        print("selected user event id \(String(describing: selecteEventId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/swipe.php?user_id=\(User_Id!)&swipe_user_id=\(whoSupervibeMeID)&category=\(category)&event_id=\(eventId)"
        print("URL : \(URL_String)")
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String , method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                //print("data user id \(dict["user_id"] as! String)")
                
                
                guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                    print("test 3")
                    print("Error with dictionary data: \(String(describing: response.result.error))")
                    return
                }
                
                
                print(" dict data : \(dictData)")
                
                
                if dictData.count == 0{
                    
                    print("do nothing continue swiping ")
                }else{
                    
                    for data in dictData{
                        
                        
                        print("data current user id \(data["user_id"] as! String)")
                        print("data current user photo \(data["user_photo"] as! String)")
                        print("data match user id \(data["match_id"] as! String)")
                        print("data match user photo \(data["match_photo"] as! String)")
                        
                        
                        currentUserId  = data["user_id"] as! String
                        currentUserPhoto = data["user_photo"] as! String
                        matchUserId = data["match_id"] as! String
                        matchUserPhoto = data["match_photo"] as! String
                        
                        
                        // self.sendMatchNotification()
                        
                        //                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        //
                        //                        let matchProfileVC = storyboard.instantiateViewController(withIdentifier: "MatchProfileViewController") as! MatchProfileViewController
                        //
                        //                        self.present(matchProfileVC, animated: true, completion: nil)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        //resolved view is not hiararchy issue.
                        
//                        let matchProfileVC = UIStoryboard(name: "Swipe", bundle:nil).instantiateViewController(withIdentifier: "MatchProfileViewController") as! MatchProfileViewController
//                        matchProfileVC.currentUserId = currentUserId
//                        matchProfileVC.currentUserImageUrlString = currentUserPhoto
//                        matchProfileVC.matchUserId = matchUserId
//                        matchProfileVC.matchImageUrlString = matchUserPhoto
//
//                        matchProfileVC.modalPresentationStyle = .overCurrentContext
//                        print("selected event id :\(self.selectedEventIdFromSwipe)")
//                        matchProfileVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
//
//                        self.addChildViewController(matchProfileVC)
//                        self.view.addSubview(matchProfileVC.view)
                        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
                        let goodvibesVC:goodvibesViewController = storyboard.instantiateViewController(withIdentifier: "goodvibesViewController") as! goodvibesViewController
                        goodvibesVC.currentUserId = currentUserId
                        goodvibesVC.currentUserImageUrlString = currentUserPhoto
                        goodvibesVC.matchUserId = matchUserId
                        goodvibesVC.matchImageUrlString = matchUserPhoto
                        print("selected event id :\(self.selectedEventIdFromSwipe)")
//                        goodvibesVC.view.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
                        goodvibesVC.view.backgroundColor = UIColor(red: 166/255, green: 52/255, blue: 31/255, alpha: 0.9)
                        
                        self.addChildViewController(goodvibesVC)
                        self.view.addSubview((goodvibesVC.view)!)
                        
                       // self.present(goodvibesVC, animated: true, completion: nil)
                        
                        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                        appDelegate.window?.rootViewController = goodvibesVC
                        
                        
                        
                    }
                    
                    
                }
                
                //                print("dict info : \(dict)")
                //
                //                for data in dict {
                //
                //                }
                
                print(type(of:dictData))
                
                
                if(respo == 201){
                    print("Message: \(String(describing: responseMessage))")
                    
                }else if(respo == 200){
                    
                    print("success for match")
                    
                }
                
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
    }
    
    @IBAction func DislikeBtnAction(_ sender: UIButton) {
        print("DisLike button tapped")
        category = "2"
        likeCategory = category
        getSwipeData()
        self.likeOverlyImg.isHidden = false
        self.imgOverlaySymbol.isHidden = false
        self.likeOverlyImg.image = UIImage(named: "overlay_like")
        self.imgOverlaySymbol.image = UIImage(named: "btn_skip_pressed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.swipeCardView.isHidden = true
            self.actionView.isHidden = true
            self.noswipeleftImg.isHidden = false
            self.lblNoSwipeLeft.isHidden = false
            self.btnFindevents.isHidden = false
        })
        
        
    }
    
    @IBAction func SupervibeBtnAction(_ sender: UIButton) {
        
        category = "3"
        self.getSuperVibeCount()//SuperVibe Restriction by fetching supervibe count
        // self.getSwipeData()
        self.likeOverlyImg.isHidden = false
        self.imgOverlaySymbol.isHidden = false
        self.likeOverlyImg.image = UIImage(named: "overlay_supervibe")
        self.imgOverlaySymbol.image = UIImage(named: "supervibe_stamp")
        
        // var superVibeIntArray = [Int]()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.getSwipeData()//To choose Category
        })
        
        self.getSuperVibeCount()
        
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.btnSupervibed.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        
        
        UIView.animate(withDuration: 0.5, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.btnSupervibed.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)
        }, completion: nil)
        
        
        
        UIView.animate(withDuration: 0.5, animations: {
            print(self.btnSupervibed.frame.origin.y)
            self.btnSupervibed.frame.origin.y -= 300
            
        }){_ in
            
            UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: UInt(0.0)), animations: {self.btnSupervibed.frame.origin.y += 300 }, completion: nil)
        }
        
        
        
    }
    @IBAction func LikeBtnAction(_ sender: UIButton) {
        
        print("Like button tapped")
        category = "1"
        likeCategory = category
        
        
        self.likeOverlyImg.isHidden = false
        self.imgOverlaySymbol.isHidden = false
        self.likeOverlyImg.image = UIImage(named: "overlay_skip")
        self.imgOverlaySymbol.image = UIImage(named: "btn_like_pressed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.getSwipeData()
        })
        
    }
    
    
    
    
    
}
