//
//  MatchProfileChatViewTableViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 22/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class MatchProfileChatViewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var chatUserImageView: UIImageView!
    
    
    @IBOutlet weak var chatUserNameLbl: UILabel!
    
    @IBOutlet weak var chatUserLastChatLbl: UILabel!
    
    @IBOutlet weak var optionButtonTapped: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        chatUserImageView.layer.masksToBounds = false
        self.chatUserImageView.layer.cornerRadius = self.chatUserImageView.frame.size.width / 2
        //self.matchProfileImageView.layer.borderColor =  UIColor.blue.cgColor
        self.chatUserImageView.layer.borderWidth = 2
        self.chatUserImageView.clipsToBounds = true
        
        
        
        //self.optionButtonTapped.addTarget(self, action: #selector(optionAction), for: .touchUpInside)
    }

   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
