//
//  MatchProfileChatViewCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 22/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class MatchProfileChatViewCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var matchProfileImageView: UIImageView!
    
    
    @IBOutlet weak var superLikeImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        matchProfileImageView.layer.masksToBounds = false
        self.matchProfileImageView.layer.cornerRadius = self.matchProfileImageView.frame.size.width / 2
        //self.matchProfileImageView.layer.borderColor =  UIColor.blue.cgColor
        self.matchProfileImageView.layer.borderWidth = 2
        self.matchProfileImageView.clipsToBounds = true
        
        // Initialization code
        //eventDateLbl.intrinsicContentSize.width
        //eventDateLbl.layer.cornerRadius = 4
        //eventDateLbl.clipsToBounds = true
        
        //eventNameLbl.layer.cornerRadius = 20
        //eventNameLbl.clipsToBounds = true
        
        
    }
}
