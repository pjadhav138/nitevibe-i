//
//  MatchProfileChatModel.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 22/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

 struct MatchProfileChatModel{
    
    var profileMatchUserData = [ProfileMatchUserDataModel]()
    var profileChatUserData = [ProfileChatUserDataModel]()
    init()
    {
       self.profileMatchUserData = []
       self.profileChatUserData = []
    }
    
    
    init(profileMatchJson:[String:Any]){
        
        if let matchUserData = profileMatchJson["match"] as? [[String:Any]]{
            for matchData in matchUserData{
                let userData = ProfileMatchUserDataModel(profileMatchJson: matchData)
                //let userData = profileMatchUserDataModel(json: matchData)
                self.profileMatchUserData.append(userData)
            }
            
            if let chatUserData = profileMatchJson["chat"] as? [[String:Any]]{
                
                for chatData in chatUserData{
                    
                    let userInfo = ProfileChatUserDataModel(profileChatJson: chatData)
                    //let userData = profileChatUserDataModel(json: chatData)
                    self.profileChatUserData.append(userInfo)
                    
                }
            }
            
            
        }
        
    }
    
    
}

 struct ProfileMatchUserDataModel{

    var matchId = String()
    var name = String()
    var occupation = String()
    var profileImage = String()
    var event = String()
    var category = String()
    var message = String()
    
    init()
    {}
    
    init(profileMatchJson:[String:Any]){
        
        if let match_id = profileMatchJson["match_id"] as? String{
            self.matchId = match_id
        }
            if let userName = profileMatchJson["name"] as? String{
                self.name = userName
            }
                if let user_occupation = profileMatchJson["occupation"] as? String{
                    self.occupation = user_occupation
            
                }
                    if let user_profile_image = profileMatchJson["profile"] as? String{
               
                        self.profileImage = user_profile_image
                    }
                if let user_event = profileMatchJson["event"] as? String{
                    
                    self.event = user_event
                }
            if let user_category = profileMatchJson["category"] as? String{
                
                self.category = user_category
            }
        if let user_message = profileMatchJson["message"] as? String{
            
            self.message = user_message
        }
        
        
    }
    

}




 struct ProfileChatUserDataModel{
    
    var matchId = String()
    var name = String()
    var occupation = String()
    var profileImage = String()
    var event = String()
    var category = String()
    var message = String()
    
    init()
    {}
    
    
    init(profileChatJson:[String:Any]){
        
        if let match_id = profileChatJson["match_id"] as? String{
            self.matchId = match_id
        }
        if let userName = profileChatJson["name"] as? String{
            self.name = userName
        }
        if let user_occupation = profileChatJson["occupation"] as? String{
            self.occupation = user_occupation
            
        }
        if let user_profile_image = profileChatJson["profile"] as? String{
            
            self.profileImage = user_profile_image
        }
        if let user_event = profileChatJson["event"] as? String{
            
            self.event = user_event
        }
        if let user_category = profileChatJson["category"] as? String{
            
            self.category = user_category
        }
        if let user_message = profileChatJson["message"] as? String{
            
            self.message = user_message
        }
        
        
    }
    
    
}
