//
//  Chat_ViewController.swift
//  Nite_Vibe
//
//  Created by Appmonks on 05/05/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher



class Chat_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource{
    
    var matchProfileChatData = [MatchProfileChatModel]()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var matchCollectionView: UICollectionView!
    
    @IBOutlet weak var emptyChatImageView: UIImageView!
    
    @IBOutlet weak var findEventNearMeButton: UIButton!
    
    @IBOutlet weak var youGotSwipeChatLbl: UILabel!
    
    @IBOutlet weak var newMatchesLbl: UILabel!
    
    var collectionCountArray = [String]()
    var tableNameArray = [String]()
    var userCategoryArray = [String]()
    var userLastChatArray = [String]()
    var userProfileImageLink = [String]()
    var userOccupationArray = [String]()
    var matchUserIdArray = [String]()
    @IBOutlet weak var chatListView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        navigationController?.isNavigationBarHidden = true
        //self.scrollView.isHidden = false
        
        //self.youGotSwipeChatLbl.isHidden = true
        //self.findEventNearMeButton.isHidden = true
        //self.emptyChatImageView.isHidden = true
        
        DispatchQueue.main.async {
             self.fetchswiftyMatchData()
            self.matchCollectionView.reloadData()
            
        }
       
        self.matchCollectionView.delegate = self
        self.matchCollectionView.dataSource = self
        
        self.chatListView.isHidden = true
        self.matchCollectionView.isHidden = true
        self.tableView.isHidden = true
        //self.matchCollectionView.reloadData()
        //self.scrollView.isHidden = true
        print("check scroll of the scroll view 1.\(scrollView.isScrollEnabled)")
        print("check scroll of table view 2.\(tableView.isScrollEnabled)")
        //Set table height to cover entire view
        //if navigation bar is not translucent, reduce navigation bar height from view height
       
        
        ////tableHeight.constant = self.view.frame.height
        
        
        //no need to write following if checked in storyboard
        
        //print("check scroll of the scroll view 1.\(scrollView.isScrollEnabled)")
        print("check scroll of table view 2\(tableView.isScrollEnabled)")
        self.youGotSwipeChatLbl.isHidden = true
        self.findEventNearMeButton.isHidden = true
        self.emptyChatImageView.isHidden = true
        self.scrollView.bounces = true
        self.tableView.bounces = true
        self.tableView.isScrollEnabled = true
        
        //self.scrollView.isHidden = false
        
        
        self.tableView.sizeToFit()
        
        scrollView.resizeScrollViewContentSizeChat()
        
        //self.tableView.isScrollEnabled = true
        // Do any additional setup after loading the view.
        
        //if match_profile api count is 0 hide scroll view otherwise hide no match found image view and label hide
        
    }
    
    

    func fetchswiftyMatchData(){
        
        
        
        URLCache.shared.removeAllCachedResponses()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        //http://www.nitevibe.in/webservices/match_profile.php?user_id=3
        //clear cache
        
        let urlString = "http://www.nitevibe.in/webservices/match_profile.php?user_id=\(User_Id!)"
        print("url string match_profile  : \(urlString)")
        
        let url = NSURL(string: urlString)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON {(response) -> Void in
        
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            
                            print("message arry count :\(messageArray.count)")
                            
                            if messageArray.count == 0{
                                self.chatListView.isHidden = true
                                self.scrollView.isHidden = true
                                self.youGotSwipeChatLbl.isHidden = false
                                self.findEventNearMeButton.isHidden = false
                                
                                self.findEventNearMeButton.addTarget(self, action:#selector(self.findEventNearMeAction), for: .touchUpInside)
                                self.emptyChatImageView.isHidden = false
                                self.newMatchesLbl.isHidden = true
                                self.matchCollectionView.isHidden = true
                                self.tableView.isHidden = true
                                print("print nothing")
                                
                            }else{
                                
                               
                                for arr in messageArray {
                                    self.matchProfileChatData.append(MatchProfileChatModel(profileMatchJson: arr))
                                    
                                    print("print matchProfileData count :\(self.matchProfileChatData.count)")
                                    
                                    //self.matchCollectionView.reloadData()
                                    //self.clubData.append(ClubModel(clubJson: arr))
                                    
                                    //print("print clubData count array  \(self.clubData.count)")
                                    //print("ClubName : \(self.clubData[0].club_name)")
                                    //self.clubname = self.clubData[0].club_name
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.matchCollectionView.reloadData()
                                        self.tableView.reloadData()
                                      
                                    }
                                    
                                    self.collectionCountArray.removeAll()
                                    self.tableNameArray.removeAll()
                                    self.userCategoryArray.removeAll()
                                    self.userLastChatArray.removeAll()
                                    self.userProfileImageLink.removeAll()
                                    self.userOccupationArray.removeAll()
                                    for i in 0..<self.matchProfileChatData[0].profileChatUserData.count{
                                        
                                        print("print i \(i) th \(self.matchProfileChatData[0].profileMatchUserData[i].name)")
                                        self.collectionCountArray.append(self.matchProfileChatData[0].profileMatchUserData[i].name)
                                        self.tableNameArray.append(self.matchProfileChatData[0].profileChatUserData[i].name)
                                        self.userCategoryArray.append(self.matchProfileChatData[0].profileChatUserData[i].category)
                                        self.userLastChatArray.append(self.matchProfileChatData[0].profileChatUserData[i].message)
                                        self.userProfileImageLink.append(self.matchProfileChatData[0].profileChatUserData[i].profileImage)
                                        self.userOccupationArray.append(self.matchProfileChatData[0].profileChatUserData[i].occupation)
                                        
                                        self.matchUserIdArray.append(self.matchProfileChatData[0].profileChatUserData[i].matchId)
                                        
                                        
                                        print("user name array in side for \(self.tableNameArray.count)")
                                        
                                        print("user category array :\(self.userCategoryArray[i])")
                                        let defaults = UserDefaults.standard
                                        
                                        defaults.removeObject(forKey: "chatnamearr")
                                        defaults.removeObject(forKey: "chatcategoryarr")
                                        defaults.removeObject(forKey: "chatmessagearr")
                                        defaults.removeObject(forKey: "chatprofileimagearray")
                                        defaults.removeObject(forKey: "chatuseroccupationarray")
                                        defaults.removeObject(forKey: "chatmatchuserid")
                                        defaults.set(self.tableNameArray, forKey: "chatnamearr")
                                        defaults.set(self.userCategoryArray, forKey: "chatcategoryarr")
                                        defaults.set(self.userLastChatArray, forKey: "chatmessagearr")
                                        defaults.set(self.userProfileImageLink,forKey:"chatprofileimagearray")
                                        defaults.set(self.userOccupationArray,forKey:"chatuseroccupationarray")
                                        defaults.set(self.matchUserIdArray,forKey:"chatmatchuserid")
                                        //print("user  Category array \(self.))
                                        
                                        print("print match profile data user name : \(self.matchProfileChatData[0].profileChatUserData.count)")
                                        print("profileMatchUserData count \(self.matchProfileChatData[0].profileMatchUserData.count)")
                                        
                                        print("profileChatUserData count \(self.matchProfileChatData[0].profileChatUserData.count)")
                                        
                                        //set newMatches lable text with match count
                                        
                                        self.scrollView.isHidden = false
                                        self.chatListView.isHidden = false
                                        self.matchCollectionView.isHidden = false
                                        self.tableView.isHidden = false
                                        self.youGotSwipeChatLbl.isHidden = true
                                        self.findEventNearMeButton.isHidden = true
                                        self.emptyChatImageView.isHidden = true
                                        
                                        self.newMatchesLbl.text! = "New Matches(\(self.collectionCountArray.count))"
                                        
                                        print("print chat name array : \(self.tableNameArray)")
                                    
                                        
                                    
                                }
                            }
                           
                                
                            }
                            
                            
                        }
                        
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //self.fetchswiftyMatchData()
        
        print("match profile data count 1 :\(matchProfileChatData.count)")
        //print("profile match count :\(matchProfileChatData[0].profileMatchUserData.count) ")
        
        //print("match profile user count \(matchProfileChatData[0].profileMatchUserData.count)")
        //print("print match count: \(matchProfileChatData[0].profileChatUserData.)")
        
        
        print("print collectionCountArray\(collectionCountArray.count)")
        return collectionCountArray.count
        
        //return matchProfileChatData[0].profileMatchUserData[section].
    
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchProfileChatViewCollectionViewCell", for: indexPath) as! MatchProfileChatViewCollectionViewCell
        
        
        
        cell.matchProfileImageView.layer.borderColor = UIColor.blue.cgColor
        
        
        let defaults = UserDefaults.standard
        
        let chatCategoryArray = defaults.stringArray(forKey: "chatcategoryarr") ?? [String]()
        
        if chatCategoryArray[indexPath.row] == "3"{
            
            cell.superLikeImageView.isHidden = false
            
        }else{
            
            cell.superLikeImageView.isHidden = true
        }
        
//        if self.matchProfileChatData[0].profileMatchUserData[indexPath.row].category == "1"{
//            //category color allocation
//            //1:red
//            //2:gray
//            //3:blue
//           // self.matchProfileImageView.layer.borderColor =  UIColor.blue.cgColor
//            
//            cell.matchProfileImageView.layer.borderColor = UIColor.red.cgColor
//            
//        }else if self.matchProfileChatData[0].profileMatchUserData[indexPath.row].category == "2"{
//            
//            cell.matchProfileImageView.layer.borderColor = UIColor.gray.cgColor
//            
//        }else if self.matchProfileChatData[0].profileMatchUserData[indexPath.row].category == "3"{
//            
//            cell.matchProfileImageView.layer.borderColor = UIColor.blue.cgColor
//            
//        }else{
//            print("do nothing ")
//        }
        if self.matchProfileChatData[0].profileMatchUserData[indexPath.row].profileImage == ""{
            
            cell.matchProfileImageView.image = #imageLiteral(resourceName: "NavProflle")
            
        }else{
    
        cell.matchProfileImageView.kf.setImage(with: URL(string:self.matchProfileChatData[0].profileMatchUserData[indexPath.row].profileImage))
        }
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("table array  count in side number of row in section  \(collectionCountArray.count)")
        
        
        //fetching data from array always give 0 count
        
        //then array store in userdefaults resolved issue
        let defaults = UserDefaults.standard
        
        let chatArray = defaults.stringArray(forKey: "chatnamearr") ?? [String]()
        print("table array  count in side number of default  \(chatArray.count)")
        
        //print("\()")
        return chatArray.count
       // return self.matchProfileChatData[section].profileChatUserData.count
    }
    
    
//
//    let headerView: UIView = UIView.init(frame: CGRect(x: 1, y: 50, width: 276, height: 50))
//    headerView.backgroundColor = .clear
//    let drinkNameButtonTapped: UIButton = UIButton.init(frame: CGRect(x: 60, y: 5, width: 350, height: 30))
//
//    drinkNameButtonTapped.contentHorizontalAlignment = .left
//    //        drinkNameButtonTapped.contentVerticalAlignment = .top
//    drinkNameButtonTapped.contentEdgeInsets = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 10)
//    //drinkLabelView.isUserInteractionEnabled = true
//    print("print  drinks name inside viewForHeaderInSection \(drinks[section].drink_name)")
//    //drinkLabelView.isUserInteractionEnabled = true
//    //drinkNameButtonTapped.text = drinks[section].drink_name
//    print("selected drink label tag \(drinkNameButtonTapped.tag)")
//    drinkNameButtonTapped.backgroundColor = .clear
//
//    drinkNameButtonTapped.setTitle("\(drinks[section].drink_name)", for: .normal)
//    drinkNameButtonTapped.addTarget(self, action: #selector(handleExapndCloseOfLabelButton), for: .touchUpInside)
//    drinkNameButtonTapped.tag = section
//    //drinkLabelView.font = UIFont.boldSystemFont(ofSize: 18)
//    let checkBoxButtonTapped: UIButton = UIButton.init(frame: CGRect(x: 12, y: 5, width: 24, height: 24))
//
//    if let inSelectedImage = UIImage(named: "checkbox.unselected") {
//        checkBoxButtonTapped.setImage(inSelectedImage, for: .normal)
//    }
//
//    checkBoxButtonTapped.addTarget(self, action: #selector(handleExpandClosedForCheckBox), for: .touchUpInside)
//    checkBoxButtonTapped.tag = section
//    let splitterImageView: UIImageView = UIImageView.init(frame:CGRect(x: 4, y: 40, width:400, height:2))
//    splitterImageView.image = UIImage(named:"splitter")
//    headerView.addSubview(splitterImageView)
//    headerView.addSubview(checkBoxButtonTapped)
//    headerView.addSubview(drinkNameButtonTapped)
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
         let headerView: UIView = UIView.init(frame: CGRect(x: 1, y: 50, width: 276, height: 50))
         headerView.backgroundColor = .clear
         let label: UILabel = UILabel.init(frame: CGRect(x: 23, y: 3, width: 250, height: 25))
         label.backgroundColor = .clear
         let defaults = UserDefaults.standard
         let chatArray = defaults.stringArray(forKey: "chatnamearr") ?? [String]()
         print("table array  count in side number of default  \(chatArray.count)")
         label.text = "Chat(\(chatArray.count))"
         label.textColor = UIColor(red: 123/255, green: 151/255, blue: 233/255, alpha: 1)
        
         label.font = UIFont.boldSystemFont(ofSize: 20)

        
        headerView.addSubview(label)
       return headerView
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchProfileChatViewTableViewCell", for: indexPath) as! MatchProfileChatViewTableViewCell
        
        let defaults = UserDefaults.standard
        
        let chatNameArray = defaults.stringArray(forKey: "chatnamearr") ?? [String]()
        print("table array  count in side number of default  \(chatNameArray.count)")
        cell.chatUserNameLbl.text = chatNameArray[indexPath.row]
        
        
        let chatCategoryArray = defaults.stringArray(forKey: "chatcategoryarr") ?? [String]()
        print("chat catgory array count in side number of default  \(chatCategoryArray.count)")
        
        
        let chatProfileImageArray = defaults.stringArray(forKey: "chatprofileimagearray") ?? [String]()
        
        print("chat profile array count in side number of default  \(chatCategoryArray.count)")
        
        let chatLastMessageArray = defaults.stringArray(forKey: "chatmessagearr") ?? [String]()
        
        print("chat message array count \(chatLastMessageArray.count)")
        
        
         cell.chatUserLastChatLbl.text = chatLastMessageArray[indexPath.row]
   
        
        cell.chatUserImageView.layer.borderColor = UIColor.blue.cgColor
//        print("chat category array :\(chatCategoryArray[indexPath.row])")
//        if chatCategoryArray[indexPath.row] == "1"{
//
//        cell.chatUserImageView.layer.borderColor = UIColor.red.cgColor
//
//        }else if chatCategoryArray[indexPath.row] == "2"{
//
//        cell.chatUserImageView.layer.borderColor = UIColor.gray.cgColor
//
//
//        }else if chatCategoryArray[indexPath.row] == "3"{
//            cell.chatUserImageView.layer.borderColor = UIColor.blue.cgColor
//        }else{
//
//            print("do nothing ")
//        }
        
        if chatProfileImageArray[indexPath.row] == ""{
            
            cell.chatUserImageView.image = #imageLiteral(resourceName: "NavProflle")
            
        }else{
        
        print("chat profile image array count \(chatProfileImageArray.count)")
        
        cell.chatUserImageView.kf.setImage(with: URL(string: chatProfileImageArray[indexPath.row]))
            
        }
        
        cell.optionButtonTapped.tag = indexPath.row
        cell.optionButtonTapped.addTarget(self, action: #selector(optionAction), for: .touchUpInside)
        
        
        return cell
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("\(indexPath.row)")
        
        
        let defaults = UserDefaults.standard
        let matchProfileImageArray = defaults.stringArray(forKey: "chatprofileimagearray") ?? [String]()
        let matchUserArray = defaults.stringArray(forKey: "chatmatchuserid") ?? [String]()
        let matchUserName = defaults.stringArray(forKey: "chatnamearr") ?? [String]()
        
        let ChatViewController = ChatWithJSQViewController()
        
        ChatViewController.receiverId = matchUserArray[indexPath.row]
        ChatViewController.receiverName = matchUserName[indexPath.row]
        ChatViewController.receiverImage = matchProfileImageArray[indexPath.row]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100.0)
//        self.navigationController?.navigationBar.alpha = 0.0
        //self.navigationController?.view.backgroundColor = .clear
        
        let chatNavigationController = UINavigationController(rootViewController: ChatViewController)
        present(chatNavigationController, animated: true, completion: nil)
        
       // self.navigationController?.pushViewController(ViewController, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    @objc func optionAction(optionButtonTapped:UIButton){
        
        
        print("print option button tap tag\(optionButtonTapped.tag)")
        
        
         let defaults = UserDefaults.standard
         let chatProfileImageArray = defaults.stringArray(forKey: "chatprofileimagearray") ?? [String]()
        
        let chatNameArray = defaults.stringArray(forKey: "chatnamearr") ?? [String]()
        let chatCategoryArray = defaults.stringArray(forKey: "chatcategoryarr") ?? [String]()
        let chatUserOccupationArray = defaults.stringArray(forKey: "chatuseroccupationarray") ?? [String]()
        let matchUserArray = defaults.stringArray(forKey: "chatmatchuserid") ?? [String]()
       // let ViewController = DemoChatViewController()
        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
        
        let manuOptionVC = storyboard.instantiateViewController(withIdentifier: "MenuOptionViewController") as! MenuOptionViewController
        
        manuOptionVC.userProfileImageString = chatProfileImageArray[optionButtonTapped.tag]
        manuOptionVC.userNameString = chatNameArray[optionButtonTapped.tag]
        manuOptionVC.userCategoryString = chatCategoryArray[optionButtonTapped.tag]
        manuOptionVC.userOccupationString = chatUserOccupationArray[optionButtonTapped.tag]
        manuOptionVC.selectedChatUserId = matchUserArray[optionButtonTapped.tag]
        
        self.present(manuOptionVC, animated: true, completion: nil)
        
        
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == self.scrollView {
//            tableView.isScrollEnabled = (self.scrollView.contentOffset.y >= 200)
//
//            print("check scroll of table view 3.\(tableView.isScrollEnabled)")
//        }
//
//        if scrollView == self.tableView {
//            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)
//
//
//            tableView.isScrollEnabled = true
//            print("check scroll of table view 4\(tableView.isScrollEnabled)")
//        }
//    }
    
    
    @objc func findEventNearMeAction(){
        
        print("back button tapped to instatiate back view ")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        BaseController.selectedIndex = 1
        self.present(BaseController, animated: true, completion: nil)
        
    }
   
        
}

extension UIScrollView {
    
    func resizeScrollViewContentSizeChat() {
        
        var contentRect = CGRect.zero
        
        for view in self.subviews {
            
            contentRect = contentRect.union(view.frame)
            
        }
        
        self.contentSize = contentRect.size
        
    }
    
}
