//
//  MenuOptionViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 10/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class MenuOptionViewController: UIViewController {

    
    @IBOutlet weak var chatMenuMuteOutlet: UISwitch!
    
    @IBOutlet weak var userProfileImage: UIImageView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    
    @IBOutlet weak var userdesignationLbl: UILabel!
    
    
    var userProfileImageString = ""
    var userNameString = ""
    var userOccupationString = ""
    var userCategoryString = ""
    var selectedChatUserId = ""
    var selectedVCflag = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()
        muteChat()
     let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
     
        
        userProfileImage.addGestureRecognizer(tap)
        userProfileImage.isUserInteractionEnabled = true
        
        
        
      print("selected chat user id \(selectedChatUserId)")
        
     self.userProfileImage.kf.setImage(with: URL(string: userProfileImageString))
        
        self.userNameLbl.text = "\(userNameString)"
        
        self.userdesignationLbl.text = "\(userOccupationString)"
        self.userProfileImage.layer.masksToBounds = false
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.width / 2
        
        self.userProfileImage.layer.borderWidth = 2
        self.userProfileImage.clipsToBounds = true
        if userCategoryString == "1"{
            
            self.userProfileImage.layer.borderColor = UIColor.red.cgColor
            
        }else if userCategoryString == "2"{
            
            self.userProfileImage.layer.borderColor = UIColor.gray.cgColor
            
            
        }else if userCategoryString == "3"{
            self.userProfileImage.layer.borderColor = UIColor.blue.cgColor
        }else{
            
            print("do nothing ")
        }
        
    }

    func  muteChat(){
        if setMuteSwitch.object(forKey: "setMuteSwitch") == nil {
            chatMenuMuteOutlet.isOn = false
        }else{
            
            let showNotificationState = setMuteSwitch.bool(forKey: "setMuteSwitch")
            print("showNotificationDefaultValue : \(showNotificationState)")
            if showNotificationState == false{
                
                chatMenuMuteOutlet.isOn = false
                
            }else if showNotificationState == true{
                
                chatMenuMuteOutlet.isOn = true
                
            }
            
        }

    }
    
    @objc func tappedMe()
    {
        print("Tapped on Image")
        print("selected chat user id :\(selectedChatUserId)")
        
        let defaults = UserDefaults.standard
        
        defaults.removeObject(forKey:"selectedgoingflag")
       
        defaults.set("2", forKey:"selectedgoingflag")
        
        let storyBoard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
        let useProfileVC = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        useProfileVC.userId = selectedChatUserId
        useProfileVC.userPic = userProfileImageString
        self.present(useProfileVC, animated: true)
        
    }
    
    @IBAction func UnmatchUserButtonTapped(_ sender: Any) {
        
        requestToUnmatchUser()
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        
        BaseController.selectedIndex = 2
        self.present(BaseController, animated: true, completion: nil)
        
    }
    
    @IBAction func BlockUserButtonTapped(_ sender: Any) {
       let blockUserVC = self.storyboard?.instantiateViewController(withIdentifier: "BlockUserCustomAlertViewController") as! BlockUserCustomAlertViewController
        blockUserVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
      
        blockUserVC.selectedChatUserId = selectedChatUserId
        self.addChildViewController(blockUserVC)
        self.view.addSubview((blockUserVC.view)!)
    }
    
    @IBAction func chatMenuMuteAction(_ sender: UISwitch) {
        
        let muteSwitchState = setMuteSwitch.bool(forKey: "setMuteSwitch")
        
        if muteSwitchState == false{
            
            chatMenuMuteOutlet.isOn = true
            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
            setMuteSwitch.set(true, forKey: "setMuteSwitch")
            setMuteSwitch.synchronize()
            print("muteDefaultValue 1 : \(muteSwitchState)")
            
        }else if muteSwitchState == true{
            
            chatMenuMuteOutlet.isOn = false
            setMuteSwitch.removeObject(forKey: "setMuteSwitch")
            setMuteSwitch.set(false, forKey: "setMuteSwitch")
            setMuteSwitch.synchronize()
            print("muteDefaultValue 2 : \(muteSwitchState)")
        }
    }
    
    @IBAction func Report(_ sender: Any) {
        
        //need to get user id and unmatch_id
        var respo = Int()
        let getUserId:UserDefaults = UserDefaults.standard
        
        let userId = getUserId.value(forKey: "userid")
        
        let URL_String = "http://www.nitevibe.in/webservices/span_user.php?user_id=\(userId!)&spam_user_id=\(selectedChatUserId)"
        print("spam user url string : \(URL_String)")
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData

            Alamofire.request(URL_String, method: .get).responseJSON{

                (response) in

                print(" response :\(String(describing: response.request))")

                guard response.result.isSuccess  else {

                    print("Block 1 ")
                    print(" response :\(response)")

                    print("Error with response: \(String(describing: response.result.error))")

                    return

                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {

                    print(" response :\(response)")

                    print("Error with dictionary: \(String(describing: response.result.error))")

                    return

                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")


                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]


                print(type(of:responseMessage))

                if (respo == 200){

                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }
                else if(respo == 201){

                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }else if(respo == 408){


                    let alert = UIAlertController(title: "", message:"Somthing went wrong" , preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)
                }


            }


        }catch {
            // TODO: find a better way to handle error
            print(error)

        }
        
        
        
    }
    
   func requestToUnmatchUser(){
        
        //need to get user id and unmatch_id
        var respo = Int()
        let getUserId:UserDefaults = UserDefaults.standard
        
        let userId = getUserId.value(forKey: "userid")
        
        let URL_String = "http://www.nitevibe.in/webservices/unmatch_user.php?user_id=\(userId!)&unmatch_id=\(selectedChatUserId)"
        print("unmatch user url string : \(URL_String)")
    
    
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData

            Alamofire.request(URL_String, method: .get).responseJSON{

                (response) in

                print(" response :\(String(describing: response.request))")

                guard response.result.isSuccess  else {

                    print("Block 1 ")
                    print(" response :\(response)")

                    print("Error with response: \(String(describing: response.result.error))")

                    return

                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {

                    print(" response :\(response)")

                    print("Error with dictionary: \(String(describing: response.result.error))")

                    return

                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")


                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]


                print(type(of:responseMessage))

                if (respo == 200){

                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }
                else if(respo == 201){

                    let alert = UIAlertController(title: "", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }else if(respo == 408){


                    let alert = UIAlertController(title: "", message:"Somthing went wrong" , preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)
                }


            }


        }catch {
            // TODO: find a better way to handle error
            print(error)

        }
    
    }
    
    

    
}
