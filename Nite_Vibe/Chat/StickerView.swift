//
//  StickerView.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 07/09/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//


// MARK:- Delegate
protocol StickerDelegate: class {
    //Called when keyboard button tapped
    func keyWasTapped(image: UIImage,imageNo: String)
}

import UIKit
import Kingfisher

class StickerView: UIView {
    
     var stickerCollectionview: UICollectionView!
    static let collectionViewIdentifier = "stickerCollectionCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 5.0, bottom: 0.0, right: 0.0)
    // This variable will be set as the view controller so that
    // the keyboard can send messages to the view controller.
    weak var delegate: StickerDelegate?
    //weak var datasource: StickerDataSource?
    
    var allStickerImages = Array<UIImage>()
   // var collStickerImages = Array<UIImage>()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initializeSubviews()
    }
    
    // 1
    func initializeSubviews() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        stickerCollectionview = UICollectionView.init(frame: frame, collectionViewLayout: layout)
        self.backgroundColor = UIColor(red: 24/255, green: 0/255, blue: 76/255, alpha: 1.0)
        self.addSubview(stickerCollectionview)
        let cellNib = UINib(nibName: "StickerCollectionCell", bundle: nil)
        stickerCollectionview.register(cellNib, forCellWithReuseIdentifier: StickerView.collectionViewIdentifier)
        stickerCollectionview.delegate = self
        stickerCollectionview.dataSource = self
        stickerCollectionview.backgroundColor = UIColor(red: 24/255, green: 0/255, blue: 76/255, alpha: 1.0)
        //self.stickerCollectionview.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
    }
    
    func confiureStckerCollectionView(stickerImages: Array<UIImage>) {
        
        //allStickerImages.kf.setImage(with: URL(string:self.matchProfileChatData[0].profileMatchUserData[indexPath.row].profileImage))
       // allStickerImages = stickerImages
        allStickerImages = stickerImages
        stickerCollectionview.reloadData()
    }

}

// MARK:- UICollectionViewDelegate
extension StickerView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("somthing is Tapped")
       // let message = allStickerImages[indexPath.row]
        let message = allStickerImages[indexPath.row]
        
        
        if indexPath.row == 0{
            print("image tapped 1: \(message)")
            self.delegate?.keyWasTapped(image: message, imageNo: "1")
            //self.stickerCollectionview.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
            
        }else{
            print("image tapped 2: \(message)")
            self.delegate?.keyWasTapped(image: message, imageNo: "2")
            //self.stickerCollectionview.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
        }
        
        
    }
}

// MARK:- UICollectionViewDataSource
extension StickerView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allStickerImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StickerView.collectionViewIdentifier, for: indexPath) as! StickerCollectionCell
        cell.stickerImage.image = allStickerImages[indexPath.row] //Set text for TextView
        
        return cell
    }
}

// MARK:- UICollectionViewDelegateFlowLayout
extension StickerView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Setup grid like view
        //Device screen width
        let width = UIScreen.main.bounds.size.width
        //calculation of cell size
        return CGSize(width: (width / 10 * 3), height: (collectionView.frame.height) - 20 )
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
