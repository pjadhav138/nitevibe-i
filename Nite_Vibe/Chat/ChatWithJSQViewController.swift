//
//  ChatWithJSQViewController.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 25/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SwiftyJSON
import Alamofire
import Kingfisher

//var senderID = ""
//var receiverID = ""

struct User {
    let senderId: String
    let senderName: String
}
let setUserIdfornotification = UserDefaults.standard
class ChatWithJSQViewController: JSQMessagesViewController, UIGestureRecognizerDelegate {
    
    var StickerKeyboardView: StickerView?
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var allMessages = [ChatModel]()
    var ChatUserNameConstraints:[NSLayoutConstraint] = []
    var ProfileImageConstraints:[NSLayoutConstraint] = []
    var backButton = UIBarButtonItem()
    //var image = UIImage()
    
    var setTimmer = Timer.self
    
    var isStickerTapped: Bool = false
    
    var senderID = ""           //actual user
    var receiverId = ""
    var receiverName = ""
    var receiverImage = ""
    var userId: String? = ""
    
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(
        with: UIColor(red: 11/255, green: 3/255, blue: 105/255, alpha: 1)
    )
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(
        with: UIColor(red: 77/255, green: 64/255, blue: 166/255, alpha: 1)
    )
    
    //let input = JSQMessagesInputToolbar().contentView.backgroundColor = UIColor(red: 11/255, green: 3/255, blue: 105/255, alpha: 1)
    
    var picture:[UIImage] = [UIImage(named: "Sticker1")!,UIImage(named: "Sticker2")!]
    var collPic:[UIImage] = [UIImage(named: "CollStickers01")!,UIImage(named: "CollStickers02")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        
        print("received image : \(receiverImage)")
        print("received id : \(receiverId)")
        
        setUserIdfornotification.set(receiverId, forKey: "receiver_user_id")
        let RuserID = setUserIdfornotification.value(forKey: "receiver_user_id")
        print("UDSENDERID : \(String(describing: RuserID))")
        setUserIdfornotification.synchronize()
        
        
        //        if (NotificationTitleCommingFromNotification == "New Message" && self.receiverId == userIDCommingFromNotification){
        //            UIApplication.shared.registerForRemoteNotifications()
        //        }else
        if NotificationTitleCommingFromNotification == "New Message"{
            
            if self.receiverId == userIDCommingFromNotification{
                
                // UIApplication.shared.unregisterForRemoteNotifications()
                //self.receiverId = ""
            }else{
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }else{
            UIApplication.shared.registerForRemoteNotifications()
        }
        //addNav()
        //setupNavigationbar()
        // setupBackButton()
        self.fetchswiftyChatData()
        addNavBarImage()
        edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        //DispatchQueue.main.async {
        
        //}
        let getUserData :UserDefaults = UserDefaults.standard
        userId = getUserData.value(forKey: "userid") as? String
        
        if let id = userId {
            senderID = id
            print("SenderID : \(senderID)")
            
        }
        
        addTapGestures()
        
        
        collectionView.bounces = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        senderId = userId
        senderDisplayName = "Shraddha"
        collectionView.dataSource = self
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        self.collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
        self.inputToolbar.contentView.textView.autocorrectionType = .no
        self.inputToolbar.contentView.backgroundColor = UIColor(red: 11/255, green: 3/255, blue: 105/255, alpha: 1)
        //self.inputToolbar.contentView.leftBarButtonItem = nil
        
        self.setTimmer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.fetchswiftyChatData), userInfo: nil, repeats: true)
        
        StickerKeyboardView = StickerView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 120))
        StickerKeyboardView?.delegate = self
        StickerKeyboardView?.confiureStckerCollectionView(stickerImages: mockData())
        
        //  StickerKeyboardView?.backgroundColor = UIColor(red: 24/255, green: 0/255, blue: 76/255, alpha: 1.0)
        
        self.inputToolbar.contentView!.textView!.delegate = self
        
        self.inputToolbar.contentView!.textView!.becomeFirstResponder()
        
        self.scrollToBottom(animated: true)
        self.automaticallyScrollsToMostRecentMessage = true
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100.0)
        //        let image = "bg_new.png"
        //        let imgBackground:UIImageView = UIImageView(frame: self.view.bounds)
        //        imgBackground.image = [UIImage(named: "bg_new.png")!]
        //        imgBackground.contentMode = UIViewContentMode.scaleAspectFill
        //        imgBackground.clipsToBounds = true
        //        self.collectionView?.backgroundView = imgBackground
        //        self.scrollToBottom(animated: true)
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "bg_new.png")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        backgroundImage.clipsToBounds = true
        self.collectionView?.backgroundView = backgroundImage
        self.view.insertSubview(backgroundImage, at: 0)
        
        //navigationController?.navigationBar.isTranslucent = true
        //navigationController?.navigationBar.tintColor = .red
        //navigationController?.view.backgroundColor = UIColor(red: 11/255, green: 3/255, blue: 105/255, alpha: 1)
        navigationController?.navigationBar.alpha = 1
        navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        navigationController?.view.backgroundColor = UIColor.blue
        navigationController?.navigationBar.tintColor = UIColor.blue
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func addTapGestures() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ChatWithJSQViewController.tapAndHideKeyboard(_:)))
        // let gesture = UITapGestureRecognizer(target: self, action: #selector(tapAndHideKeyboard(_:)))
        
        self.collectionView.addGestureRecognizer(gesture)
    }
    
    @objc func tapAndHideKeyboard(_ sender: UITapGestureRecognizer) {
        print("Sender for tap : \(sender)")
        if(sender.state == UIGestureRecognizerState.ended) {
            if(self.inputToolbar.contentView.textView.isFirstResponder) {
                self.inputToolbar.contentView.textView.resignFirstResponder()
            }
        }
    }
    
    func addNav(){
        
        let navView = UIView()
        navView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 100)
        navView.backgroundColor = UIColor.blue
        
        let backbtn = UIButton(frame: CGRect(x: 15, y: 10, width: 35, height: 35))
        backbtn.setImage(#imageLiteral(resourceName: "BackButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backbtn.addTarget(self, action: #selector(backButtonTapped), for: UIControlEvents.touchUpInside)
        
        navView.addSubview(backbtn)
        
        
    }
    
    func setupBackButton() {
        
        
        self.backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
        // let profileImage = UIB
        
    }
    
    //    func addNavBarImage(){
    //
    //        let navController = navigationController!
    //        navController.navigationBar.topItem?.title = self.receiverName
    //        navController.navigationBar.barStyle = UIBarStyle.blackTranslucent
    //        navController.navigationBar.tintColor = UIColor.blue
    //        navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    //
    //    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.scrollToBottom(animated: true)
    }
    
    func mockData() ->[UIImage] {
        return collPic
    }
    
    func addNavBarImage() {
        
        //        let navController = navigationController!
        //        navController.navigationBar.topItem?.title = self.receiverName
        //        navController.navigationBar.barStyle = UIBarStyle.blackTranslucent
        //        navController.navigationBar.tintColor = UIColor.blue
        //        navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.isTranslucent = true
        //navigationController?.navigationBar.tintColor = .red
        navigationController?.view.backgroundColor = UIColor.blue
        navigationController?.navigationBar.alpha = 1
        // navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100.0)
        
        let backbtn = UIButton(type: .system)
        backbtn.setImage(#imageLiteral(resourceName: "BackButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backbtn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        backbtn.addTarget(self, action: #selector(backButtonTapped), for: UIControlEvents.touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbtn)
        
        let optionbtn = UIButton(type: .system)
        optionbtn.setImage(#imageLiteral(resourceName: "options.png").withRenderingMode(.alwaysOriginal), for: .normal)
        optionbtn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        optionbtn.addTarget(self, action: #selector(optionButtonTapped), for: UIControlEvents.touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: optionbtn)
        
        
        
        let navView = UIView(frame: CGRect(x: 55, y: 0, width: self.view.frame.size.width - 20, height: 50))
        
        // Create the label
        let label = UILabel(frame: CGRect(x: 55, y: 10, width: navView.frame.width - 20, height: 50))
        label.text = " " + self.receiverName
        label.textColor = UIColor.white
        label.sizeToFit()
        //label.center = navView.center
        label.textAlignment = NSTextAlignment(rawValue: NSTextAlignment.center.rawValue + 10)!
        
        if receiverImage == ""{
            
            // Create the image view
            let image = UIImageView()
            
            image.image = #imageLiteral(resourceName: "NavProflle")
            //image.kf.setImage(with: URL(string: receiverImage))
            // To maintain the image's aspect ratio:
            let imageAspect = image.image!.size.width/image.image!.size.height
            // Setting the image frame so that it's immediately before the text:
            //        image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect - 30, y: label.frame.origin.y - 15, width: label.frame.size.height*imageAspect + 25, height: label.frame.size.height + 25)
            
            image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect - 30, y: label.frame.origin.y - 8, width: label.frame.size.height*imageAspect + 18, height: label.frame.size.height + 18)
            //*imageAspect x
            //*imageAspect width
            image.layer.borderColor = UIColor.blue.cgColor
            image.layer.borderWidth = 1
            image.clipsToBounds = true
            image.layer.cornerRadius = image.frame.height / 2
            image.layer.masksToBounds = true
            image.contentMode = UIViewContentMode.scaleAspectFit
            
            // Add both the label and image view to the navView
            navView.addSubview(label)
            navView.addSubview(image)
            
            // Set the navigation bar's navigation item's titleView to the navView
            self.navigationItem.titleView = navView
            
            // Set the navView's frame to fit within the titleView
            //navView.sizeToFit()
            
            
        }else{
            
            
            // Create the image view
            let image = UIImageView()
            image.kf.setImage(with: URL(string: receiverImage))
            // To maintain the image's aspect ratio:
            let imageAspect = image.image!.size.width/image.image!.size.height
            // Setting the image frame so that it's immediately before the text:
            //        image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect - 30, y: label.frame.origin.y - 15, width: label.frame.size.height*imageAspect + 25, height: label.frame.size.height + 25)
            
            image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect - 30, y: label.frame.origin.y - 8, width: label.frame.size.height*imageAspect + 18, height: label.frame.size.height + 18)
            //*imageAspect x
            //*imageAspect width
            image.layer.borderColor = UIColor.blue.cgColor
            image.layer.borderWidth = 1
            image.clipsToBounds = true
            image.layer.cornerRadius = image.frame.height / 2
            image.layer.masksToBounds = true
            image.contentMode = UIViewContentMode.scaleAspectFit
            
            // Add both the label and image view to the navView
            navView.addSubview(label)
            navView.addSubview(image)
            
            // Set the navigation bar's navigation item's titleView to the navView
            self.navigationItem.titleView = navView
            
            // Set the navView's frame to fit within the titleView
            navView.sizeToFit()
        }
    }
    //    override func viewDidLayoutSubviews() {
    //        self.collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
    //        collectionView?.contentInset.bottom = inputToolbar.frame.height
    //        collectionView?.scrollIndicatorInsets.bottom = inputToolbar.frame.height
    //    }
    
    
    
    
    
    //    func setupNavigationbar() {
    //
    //        let backbtn = UIButton(type: .system)
    //        backbtn.setImage(#imageLiteral(resourceName: "BackButton").withRenderingMode(.alwaysOriginal), for: .normal)
    //        backbtn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
    //        backbtn.addTarget(self, action: #selector(backButtonTapped), for: UIControlEvents.touchUpInside)
    //        //let backbutton = UIBarButtonItem(customView: backbtn)
    //
    ////        let userProfilebtn = UIButton(type: .system)
    //////        //userProfilebtn.setImage(UIImage(named: receiverImage), for: .normal)
    //////        //image.kf.setImage(with: URL(string: receiverImage))
    //////        let myImage = UIImage()
    ////        userProfilebtn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
    ////        print("ProImage : \(receiverImage)")
    ////        userProfilebtn.kf.setImage(with: URL(string: receiverImage), for: .normal)
    //////        myButton.setImage(myImage , forState: UIControlState.Normal)
    //       // userProfilebtn.setBackgroundImage(myImage, for: .normal)
    //
    //        //userProfilebtn.isUserInteractionEnabled = false
    //        //let userimgbtn = UIBarButtonItem(customView: userProfilebtn)
    //
    //        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbtn)
    //        //navigationItem.leftBarButtonItem = userimgbtn
    //
    //        let optionbtn = UIButton(type: .system)
    //        optionbtn.setImage(#imageLiteral(resourceName: "options.png").withRenderingMode(.alwaysOriginal), for: .normal)
    //        optionbtn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
    //        optionbtn.addTarget(self, action: #selector(backButtonTapped), for: UIControlEvents.touchUpInside)
    //        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: optionbtn)
    //
    //    }
    
    @objc func backButtonTapped() {
        
        setUserIdfornotification.removeObject(forKey: "receiver_user_id")
        let useridforNoti = "0"
        setUserIdfornotification.set(useridforNoti, forKey: "receiver_user_id")
        let RuserID = setUserIdfornotification.value(forKey: "receiver_user_id")
        print("UDSENDERID Removed: \(String(describing: RuserID))")
        setUserIdfornotification.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        BaseController.selectedIndex = 2
        self.present(BaseController, animated: true, completion: nil)
        // dismiss(animated: true, completion: nil)
    }
    
    @objc func optionButtonTapped() {
        
        let alertController = UIAlertController(title: "Alert!", message: "Do you want to clear the chat ?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.clearView()
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func clearView(){
        
        //need to get user id and block_id
        var respo = Int()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        //let User_Location = getUserData.value(forKey: "selectedlocationcity")
        print("userid :\(String(describing: User_Id))!)")
        
        //mode 0 for block 1 for unblock
        let URL_String = "http://www.nitevibe.in/webservices/clear_chat.php?sender_id=\(User_Id!)&receiver_id=\(receiverId)"
        
        print("block user url string : \(URL_String)")
        
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    let alert = UIAlertController(title: ":)", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.fetchswiftyChatData()
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 408){
                    
                    
                    let alert = UIAlertController(title: ":(", message:"Somthing went wrong" , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
        
    }
    
    @objc func fetchswiftyChatData(){
       // allMessages.removeAll()
        URLCache.shared.removeAllCachedResponses()
        //let defaults = UserDefaults.standard
        
        
        // defaults.set(self.senderID, forKey: "userid")
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        
        print("receiver id:\(receiverId)")
        
        getUserData.set(receiverId, forKey: "receiverid")
        
        let urlString = "http://www.nitevibe.in/webservices/chat_list.php?sender_id=\(User_Id!)&receiver_id=\(receiverId)"
        
        
        
        print("urlstring chat 1 : \(urlString)")
        
        
        let url = NSURL(string: urlString)
        
        var urlRequest = URLRequest(url: url! as URL)
        
        urlRequest.cachePolicy =  NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let req = Alamofire.request(urlRequest).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for msgDict in messageArray {
                                let msg = ChatModel.init(ChatListJSON: msgDict)
                                if !self.allMessages.contains(where: {$0.chatId == msg.chatId}) {
                                    self.allMessages.append(msg)
                                }
                            }
                            if(self.allMessages.count > 0) {
                                self.collectionView.reloadData()
                            }
                            
                            
                            
                            
                        }
                    }
                } catch {
                    print("cannot convert to Json")
                }
            }
        }
        
        print("Receiver ID :\(self.receiverId)")
        print("userIDCommingFromNotification App :\(userIDCommingFromNotification)")
        
        if NotificationTitleCommingFromNotification == "New Message"{
            
            if self.receiverId == userIDCommingFromNotification{
                
                // UIApplication.shared.unregisterForRemoteNotifications()
                // self.receiverId = ""
            }else{
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }else{
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        //            UIApplication.shared.unregisterForRemoteNotifications()
        //        }else if self.receiverId != userIDCommingFromNotification{
        //            UIApplication.shared.registerForRemoteNotifications()
        //        }else{
        //
        //            UIApplication.shared.registerForRemoteNotifications()
        //        }
        
        print(req.debugDescription)
    }
}

extension ChatWithJSQViewController {
    
    fileprivate func getJSQMessageFrom(message: ChatModel) -> JSQMessage? {
        if message.isSticketMessage(),
            let image = message.getStickerImage()
        {
            return JSQMessage.init(senderId: message.chatMe, displayName: "sender name", media: JSQPhotoMediaItem.init(image: image))
        } else {
            return JSQMessage.init(senderId: message.chatMe, displayName: "sender name", text: message.chatMessage)
        }
    }
}


extension ChatWithJSQViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMessages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return getJSQMessageFrom(message: allMessages[indexPath.row])
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = allMessages[indexPath.row]
        
        if message.chatYou == receiverId{
            
            return self.outgoingBubble
            
        } else{
            
            return self.incomingBubble
        }
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(
            collectionView,
            cellForItemAt: indexPath
            ) as! JSQMessagesCollectionViewCell
        
        let message = allMessages[indexPath.row]
        
        if message.isMessageFromCurrentUser() {
            
            cell.textView?.textColor = UIColor.white
            
        } else {
            
            cell.textView?.textColor = UIColor.white
            cell.textView?.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : UIColor.blue]
            
        }
        cell.textView?.dataDetectorTypes = .all
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        
        return 17
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        let message = allMessages[indexPath.row]
        
        if let date = message.chatDate {
            return NSAttributedString(string: date.getTimeString())
        }
        
        return NSAttributedString(string: message.chatEntry)
    }
    
    //    func addMedia(_ media:JSQMediaItem) {
    //        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
    //       // self.allMessages.append(message)
    //
    //        //Optional: play sent sound
    //
    //        self.finishSendingMessage(animated: true)
    
    //    func addMedia(_ media:JSQMediaItem) {
    //
    //        let getUserData :UserDefaults = UserDefaults.standard
    //        let User_Id = getUserData.value(forKey: "userid")
    //        print("userid :\(String(describing: User_Id))!)")
    //
    //        let _mediaMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, media: mediMessageImage)
    //        self.messages += [_mediaMessage]
    //        self.finishSendingMessage()
    //
    //        let message = JSQMessage(senderId: User_Id! as! String, displayName: self.senderDisplayName, media: media)
    //        self.allMessages.append(message)
    //
    //        //Optional: play sent sound
    //
    //        self.finishSendingMessage(animated: true)
    //    }
    //
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        self.collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
        //self.automaticallyAdjustsScrollViewInsets = false
        self.automaticallyScrollsToMostRecentMessage = true
        self.isStickerTapped = true
        //self.inputToolbar.contentView.textView.resignFirstResponder()
        self.inputToolbar.contentView!.textView!.inputView = StickerKeyboardView
        self.inputToolbar.contentView!.textView!.becomeFirstResponder()
//        self.inputToolbar.contentView!.textView!.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        //manage send message here
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        //let receiverId = getUserData.value(forKey: "receiverid")
        
        //print("receiver id:\(String(describing: receiverId))")
        
        let URL_String = "http://www.nitevibe.in/webservices/test_send_chat.php?sender_id=\(User_Id!)&receiver_id=\(receiverId)&message=\(text!)&sticker=null"
        
        print("Chat Url String: \(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                
                if(dict["response"] as! Int == 200){
                    
                    
                    
                    self.collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 320, 0)
                    //self.automaticallyAdjustsScrollViewInsets = false
                    self.sendMessageNotification()
                    //self.automaticallyScrollsToMostRecentMessage = true
                    
                    
                    print("Done")
                    
                    
                }else{
                    
                    print("Do Nothing")
                    
                }
                
            }
        }catch{
            
            print("print chat error")
        }
        
        self.fetchswiftyChatData()

        self.finishSendingMessage(animated: true)
        
        self.scrollToBottom(animated: true)
        self.automaticallyScrollsToMostRecentMessage = true
        
        
    }
    
    
    func sendMessageNotification(){
        // print("ClubID : \(club_id)")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))")
        
        print("Receiver_id :\(receiverId)")
        
        // print("selected club id \(selectedClubId)")
        let URL_String = "http://www.nitevibe.in/webservices/msg_notification.php?sender_id=\(User_Id!)&receiver_id=\(receiverId)"
        
        print("test Notification URL : \(URL_String)")
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response Noti :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    print("Done")
                    
                }else if(dict["response"] as! Int == 201){
                    print("Fail to Load")
                    
                    
                }else if(dict["response"] as! Int == 408){
                    
                    print("Wrong Request Method")
                    
                }
                // self.scrollToBottom(animated: true)
                
            }
            
        }catch{
            
            print(error)
        }
        
    }
    
    override func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        
        return true
    }
    
}

extension ChatWithJSQViewController: StickerDelegate {
    func keyWasTapped(image: UIImage, imageNo: String) {
        print("Return Image")
        
        let photoItem = JSQPhotoMediaItem(image: image)
        photoItem?.image = image
        photoItem?.appliesMediaViewMaskAsOutgoing = true
        
        let _mediaMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, media: photoItem)
        print("MediaMsg : \(String(describing: _mediaMessage))")
        
        print("PhotoItem : \(String(describing: photoItem))")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        let URL_String = "http://www.nitevibe.in/webservices/test_send_chat.php?sender_id=\(User_Id!)&receiver_id=\(receiverId)&message=\("")&sticker=\(imageNo)"
        
        print("Chat Url String: \(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(String(describing: response.request))")
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                
                if(dict["response"] as! Int == 200){
                    
                    
                    
                    self.collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 420, 0)
                    self.sendMessageNotification()
                    
                    
                    print("Done")
                    
                    
                }else{
                    
                    print("Do Nothing")
                    
                }
                
            }
        }catch{
            
            print("print chat error")
        }
        
        
        self.finishSendingMessage(animated: true)
        self.inputToolbar.contentView.textView.resignFirstResponder()
    }
    
}

extension ChatWithJSQViewController {
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        if self.isStickerTapped == true {
            self.isStickerTapped = false
            self.inputToolbar.contentView!.textView!.inputView = nil
        }
        else {
//            self.inputToolbar.contentView!.textView!.resignFirstResponder()
            self.inputToolbar.contentView!.textView!.becomeFirstResponder()
        }
//        self.inputToolbar.contentView!.textView.becomeFirstResponder()
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        if !isStickerTapped {
            self.inputToolbar.contentView!.textView!.inputView = nil
        }
        
        self.isStickerTapped = false
    }
}

