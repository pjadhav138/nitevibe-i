//
//  StickerCollectionCell.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 07/09/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class StickerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var stickerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 10
        mainView.backgroundColor = UIColor(red: 24/255, green: 0/255, blue: 76/255, alpha: 1.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
}
