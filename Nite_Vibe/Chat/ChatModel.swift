//
//  ChatModel.swift
//  Nitevibe
//
//  Created by Shraddha Mahajan on 25/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ChatModel {
    
    var chatId = String()
    var chatMe = String()
    var chatYou = String()
    var chatMessage = String()
    var chatSticker: String?
    var chatSent = String()
    var chatEntry = String()
    var chatImage = String()
    var chatDate: Date?
    var serverDate = String()
    
    static let df = DateFormatter()
    
    init()
    {}
    
    init(ChatListJSON: [String:Any]){
        
        if let chat_id = ChatListJSON["id"] as? String{
            self.chatId = chat_id
        }
        if let chat_me = ChatListJSON["me"] as? String{
            self.chatMe = chat_me
        }
        if let chat_you = ChatListJSON["you"] as? String{
            self.chatYou = chat_you
        }
        if let chat_message = ChatListJSON["message"] as? String{
            self.chatMessage = chat_message
        }
        
        if let chat_sent = ChatListJSON["sent"] as? String{
            self.chatSent = chat_sent
        }
        
        if let chat_entry = ChatListJSON["entry"] as? String{
            self.chatEntry = chat_entry
            ChatModel.df.dateFormat = "yyyy-MM-dd hh:mm:ss"
            ChatModel.df.timeZone = NSTimeZone(name: "UTC")! as TimeZone
           // serverDate = ChatModel.df.string(for: chat_entry) as! String
            self.chatDate = ChatModel.df.date(from: chat_entry)
            
//            ChatModel.df.dateFormat = "yyyy-MM-dd hh:mm:ss"
//            //ChatModel.df.timeZone = TimeZone.current
//            self.serverDate = ChatModel.df.string(from: self.chatEntry)
//
//            ChatModel.df.dateFormat = "hh:mm a"
//            self.chatDate = ChatModel.df.string(from: serverDate!)
        }
        
        if let chat_image = ChatListJSON["img"] as? String{
            self.chatImage = chat_image
            
        }
        
        if let chat_sticker = ChatListJSON["sticker"] as? String{
            self.chatSticker = chat_sticker
        }
        
    }
    
    func isMessageFromCurrentUser() -> Bool {
        return chatMe == chatYou
    }
    
    func isSticketMessage() -> Bool {
        if let sticker = chatSticker,
            sticker != "null"
        {
            return true
        }
        return false
    }
    
    func getStickerImage() -> UIImage? {
        if let sticker = chatSticker,
            sticker != "null"
        {
            if sticker == "1" {
                return #imageLiteral(resourceName: "Sticker1")
            } else if sticker == "2" {
                return #imageLiteral(resourceName: "Sticker2")
            }
            
        }
        
        return nil
    }
}
