//
//  LogIn_2_VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 29/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//test repo
//test_repo

import UIKit
import Alamofire
import QuartzCore

class LogIn_2_VC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txt_Email: UITextField!
    
    @IBOutlet weak var txt_PassWord: UITextField!
    var txtEmail = ""
    var userType = ""
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txt_Email.delegate = self
        self.txt_PassWord.delegate = self
        loginButton.layer.cornerRadius = 10
        loginButton.clipsToBounds = true
        print("user name \(txtEmail)")
        txt_Email.text = txtEmail
        
        print("Email: \(String(describing: txt_Email.text))")
        
        
    }
    
    
    @IBAction func forgotPasswordBtn_action(_ sender: Any) {
        
        let forgotPasswordVC = storyboard?.instantiateViewController(withIdentifier: "ForgotPass_1VC")
        
        present(forgotPasswordVC!, animated: true, completion: nil)
        
    }
    

    
    @IBAction func LogInBtn_action(_ sender: Any) {
        
        if self.txt_Email.text! == "" && self.txt_PassWord.text == "" {
            
            let alert = UIAlertController(title: "Error", message: "Please enter email and password", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
            
        else{
            let URL_String = "http://www.nitevibe.in/webservices/login.php?username=\(txt_Email.text!)&password=\(txt_PassWord.text!)"
            
            print("Login Url String: \(URL_String)")
            
            
            do {
                //disable cache using reloadIgnoringCacheData Not updating backend update in app
                var urlRequest = try URLRequest(url: URL_String, method:.get)
                urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
                
                Alamofire.request(urlRequest).responseJSON{
                    response in
                    
                    print(" response :\(String(describing: response.request))")
                    print(" response :\(String(describing: response.request))")
                    
                    guard response.result.isSuccess  else {
                        print("Block 1 ")
                        print(" response :\(response)")
                        print("Error with response: \(String(describing: response.result.error))")
                        
                        return
                        
                    }
                    guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                        
                        print(" response :\(response)")
                        
                        print("Error with dictionary: \(String(describing: response.result.error))")
                        
                        return
                        
                    }
                    
                    print("dict respo \(dict["response"]!)")
                    //response 200 : Login Success
                    if(dict["response"] as! Int == 200){
                        
                        guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                            
                            print("Error with dictionary data: \(String(describing: response.result.error))")
                            
                            return
                        }
                        
                        print(" dict data\(dictData)")
                        
                        for data in dictData{
                            
                            //get usertype from response and store in userdefault
                            self.userType = data["user_type"] as! String
                            print("User Type:\(self.userType)")
                            let setUserType:UserDefaults = UserDefaults.standard
                            setUserType.set(self.userType, forKey: "usertype")
                            //get userid from response and store in userdefault
                            let userId = data["user_id"] as! String
                            print("User Id:\(userId)")
                            let setUserId:UserDefaults = UserDefaults.standard
                            setUserId.set(userId, forKey: "userid")
                            print("userid :\(String(describing: "userid"))!)")
                            
                            
                            if self.userType == "sub_admin"{
                                
                                
                                print("Goes to admin Home VC ")
                                let defaults = UserDefaults.standard
                                defaults.removeObject(forKey: "loginflag")
                                //defaults.removeObject(forKey: "ovelayflag")
                                defaults.set("1", forKey: "loginflag")
                                
                                defaults.set("1", forKey: "ovelayflag")
                                defaults.set("1", forKey: "subadmin")
                                
                                
                                let userProfileStoryBoard : UIStoryboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
                                
                                let clubSideHomeVC = userProfileStoryBoard.instantiateViewController(withIdentifier: "PieChartViewController") as! PieChartViewController
                                
                                self.present(clubSideHomeVC, animated: false, completion: nil)
                                
                                
                                
                            }else{
                                
                                
                                //self.getUserDeviceID()
                                let defaults = UserDefaults.standard
                                
                                defaults.removeObject(forKey: "loginflag")
                                //defaults.removeObject(forKey: "ovelayflag")
                                defaults.set("1", forKey: "loginflag")
                                
                                defaults.set("1", forKey: "ovelayflag")
                                
                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                
                                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController")
                                appDelegate.window?.rootViewController = initialViewController
                                appDelegate.window?.makeKeyAndVisible()
                                
                                
                            }
                        
                        
                        }  //response 201 : invalid username
                    }else if(dict["response"] as! Int == 201){
                        let alert = UIAlertController(title: "", message:"Please enter valid username", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        //response 202 : invalid password
                    }else if(dict["response"] as! Int == 202){
                        let alert = UIAlertController(title: "", message:"Please enter valid password", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        //response 203 : inactive user
                    }else if(dict["response"] as! Int == 203){
                        
                        
                        //call active_user web service and activate user diect
                        let URL_String = "http://www.nitevibe.in/webservices/active_user.php?user_name=\(self.txt_Email.text!)"
                        
                        print("Active User String URL\(URL_String)")
                        Alamofire.request(URL_String, method: .get).responseJSON{
                            
                            (response) in
                            
                            print(" response :\(String(describing: response.request))")
                            
                            guard response.result.isSuccess  else {
                                
                                print("Block 1 ")
                                print(" response :\(response)")
                                
                                print("Error with response: \(String(describing: response.result.error))")
                                
                                return
                                
                            }
                            guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                                
                                print(" response :\(response)")
                                
                                print("Error with dictionary: \(String(describing: response.result.error))")
                                
                                return
                                
                            }
                            print("message\(dict)")
                            print("dict respo: \(dict["response"]!)")
                            print("dict message:\(dict["message"]!)")
                            
                            var respo = Int()
                            respo = dict["response"]! as! Int
                            if(respo == 200){
                                
                                print("Goes to Home VC ")
                                
                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate

                                let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController")
                                appDelegate.window?.rootViewController = initialViewController
                                appDelegate.window?.makeKeyAndVisible()

                                print("End")
                                
                                
                            }
                            
                            
                        }
                        
                        
                        //response 408: request method not accpeted
                    }else if(dict["response"] as! Int == 408){
                        
                        let alert = UIAlertController(title: "", message:"Somthing went wrong", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
            } catch {
                // TODO: find a better way to handle error
                print(error)
                
            }
            
        }
        
    }
    
    
    
    @IBAction func backBtn_action(_ sender: Any) {
        
        let LoginVC:LogIn_1_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_1_VC") as! LogIn_1_VC
        
        present(LoginVC, animated: true, completion: nil)
        //        let FBVC:FBLogin_VC = storyboard?.instantiateViewController(withIdentifier: "FBLogin_VC") as! FBLogin_VC
        //
        //        present(FBVC, animated: true, completion: nil)
    }
    
    
    @IBAction func forgetButtonTapped(_ sender: Any) {
        
        
        //        Auth.auth().sendPasswordReset(withEmail: self.userNameTextfield.text!) { error in
        //
        //            if error != nil
        //
        //            {
        //
        //                let alert = UIAlertController(title: "Alert!", message: "Unidentified email", preferredStyle: .alert)
        //
        //                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        //
        //                alert.addAction(alertAction)
        //
        //                self.present(alert, animated: true, completion: nil)
        //
        //            }
        //
        //            else{
        //
        //
        //                let alert = UIAlertController(title: "Alert!", message: "Sent Recovery Mail ", preferredStyle: .alert)
        //
        //                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        //
        //                alert.addAction(alertAction)
        //
        //                self.present(alert, animated: true, completion: nil)
        //
        //            }
        //
        //        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}

