//
//  ParentTableSecondCellTableViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 21/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ParentTableSecondCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lastMessage: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func menuBtnAction(_ sender: UIButton) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
