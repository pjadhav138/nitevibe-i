//
//  ClubDetailFeatureImageViewCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 14/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class ClubDetailFeatureImageViewCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var clubFeatureImageView: UIImageView!
    
    
    @IBOutlet weak var backButtonTapped: UIButton!
    
    
    @IBOutlet weak var shareButtonTapped: UIButton!
    @IBOutlet weak var favButtontTapped: UIButton!
}
