//
//  SecondTableViewCell.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 21/07/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class SecondTableViewCell: UITableViewCell {

    @IBOutlet weak var chatTableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
