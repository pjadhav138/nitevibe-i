//
//  AddImageAlertViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 17/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
class AddImageAlertViewController: UIViewController {

 
    @IBOutlet weak var removePhotoHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!
    
    var photoNameStringUrl = String()
    @IBOutlet weak var removeAsProfileButtonOutlet: UIButton!
    
    @IBOutlet weak var removePhotoPermanentlyButtonOutlet: UIButton!
    
    
    @IBOutlet weak var setAsProfileButtonOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        profileImage.layer.masksToBounds = true
        
        profileImage.layer.cornerRadius = 12
        
        
        //print("image profile\(imageProfile)")
        
        
        //profileImage.image = imageProfile
        //profileImage.kf.setImage(with: URL(string: image))
        removeAsProfileButtonOutlet.layer.masksToBounds = true
        removeAsProfileButtonOutlet.layer.cornerRadius = 12
        
        removePhotoPermanentlyButtonOutlet.layer.masksToBounds = true
        removePhotoPermanentlyButtonOutlet.layer.cornerRadius = 12
        
        setAsProfileButtonOutlet.layer.masksToBounds = true
        
        setAsProfileButtonOutlet.layer.cornerRadius = 12
        
        
        removeAsProfileButtonOutlet.addTarget(self, action: #selector(removeAsProfileAction), for:.touchUpInside)
        
        setAsProfileButtonOutlet.addTarget(self, action: #selector(setAsProfileAction), for: .touchUpInside)
        
        removePhotoPermanentlyButtonOutlet.addTarget(self, action: #selector(removePhotoPermanentlyAction), for: .touchUpInside)
        
     //self.profileImage.image = image
        
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let createProfileVC  = storyboard.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
        
        present(createProfileVC, animated: false, completion: nil)
       
    }
    
    @objc func removeAsProfileAction(){
        
        print("removeAsProfileAction")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id!))")
        //pass status 2 as set Profile
        let URL_String = "http://www.nitevibe.in/webservices/remove_photo.php?user_id=\(User_Id!)&photo_name=\(photoNameStringUrl)&status=2"
        print(" removeAsProfileAction URL String\(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
        
        do{
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(escapeUrl!, method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                //Your Profile Image is Updated Successfully!
                
                let alert = UIAlertController(title:nil, message: "Your Profile Image is Updated Successfully!", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: {(action:UIAlertAction!) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let createProfileVC  = storyboard.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
                    
                    self.present(createProfileVC, animated: false, completion: nil)
                    
                })
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }catch{
            
            print("error")
        }
        
    
    }
    @objc func removePhotoPermanentlyAction(){
        print("removePhotoPermanentlyAction")
        
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id!))")
        //pass status 3 as set Profile
        let URL_String = "http://www.nitevibe.in/webservices/remove_photo.php?user_id=\(User_Id!)&photo_name=\(photoNameStringUrl)&status=3"
        print(" removePhotoPermanentlyAction URL String\(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
        
        do{
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(escapeUrl!, method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                //Your Profile Image is Updated Successfully!
                
                let alert = UIAlertController(title:nil, message: "Your Profile Image is Updated Successfully!", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: {(action:UIAlertAction!) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let createProfileVC  = storyboard.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
                    
                    self.present(createProfileVC, animated: false, completion: nil)
                    
                })
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }catch{
            
            print("error")
        }
        
    }
    @objc func setAsProfileAction(){
        print("setAsProfileAction")
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id!))")
        //pass status 1 as set Profile
        let URL_String = "http://www.nitevibe.in/webservices/remove_photo.php?user_id=\(User_Id!)&photo_name=\(photoNameStringUrl)&status=1"
        print("setAsProfileAction URL String\(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        do{
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(escapeUrl!, method: .get).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                //Your Profile Image is Updated Successfully!
                
                let alert = UIAlertController(title:nil, message: "Your Profile Image is Updated Successfully!", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: {(action:UIAlertAction!) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let createProfileVC  = storyboard.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
                    
                    self.present(createProfileVC, animated: false, completion: nil)
                    
                })
                
                alert.addAction(alertAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }catch{
            
            print("error")
        }
        
        
    }
}
