//
//  BlockUserCustomAlertViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 10/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
class BlockUserCustomAlertViewController: UIViewController {
    
    
    @IBOutlet weak var alertSubView: UIView!
    var selectedChatUserId = ""
    var selectedMode = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertSubView.layer.masksToBounds = true
        self.alertSubView.layer.cornerRadius = 10
        self.view.addSubview(alertSubView)
        print("selected chat user id for block user :\(selectedChatUserId)")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func blockButtonTapped(_ sender: Any) {
        
        requestButtonTapped()
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    @IBAction func noButtonTapped(_ sender: Any) {
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    func requestButtonTapped(){
        
        //need to get user id and block_id
        var respo = Int()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        //let User_Location = getUserData.value(forKey: "selectedlocationcity")
        print("userid :\(String(describing: User_Id))!)")
        
        //mode 0 for block 1 for unblock
        let URL_String = "http://www.nitevibe.in/webservices/block.php?user_id=\(User_Id!)&block_id=\(selectedChatUserId)&mode=0"
        
        print("block user url string : \(URL_String)")
        
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData

            Alamofire.request(URL_String, method: .get).responseJSON{

                (response) in

                print(" response :\(String(describing: response.request))")

                guard response.result.isSuccess  else {

                    print("Block 1 ")
                    print(" response :\(response)")

                    print("Error with response: \(String(describing: response.result.error))")

                    return

                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {

                    print(" response :\(response)")

                    print("Error with dictionary: \(String(describing: response.result.error))")

                    return

                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")


                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]


                print(type(of:responseMessage))

                if (respo == 200){

                    let alert = UIAlertController(title: ":)", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }
                else if(respo == 201){

                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)

                }else if(respo == 408){


                    let alert = UIAlertController(title: ":(", message:"Somthing went wrong" , preferredStyle: .alert)

                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)

                    alert.addAction(alertAction)

                    self.present(alert, animated: true, completion: nil)
                }


            }


        }catch {
            // TODO: find a better way to handle error
            print(error)

        }
        
    }
    
    
    
    
}

