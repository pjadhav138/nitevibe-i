//
//  CustomAlertViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 06/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire


class CustomAlertViewController: UIViewController {
var reason = String()
    @IBOutlet weak var dontLikeButtonOutlet: UIButton!
    
    
    @IBOutlet weak var breakButtonOutlet: UIButton!
    
    @IBOutlet weak var reasonButtonOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func dontLikeVibeButtonTapped(_ sender: UIButton) {
        print("Don't like")
        self.reason = "1"
        self.requestToDeleteAccount()
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    @IBAction func needABreakButtonTapped(_ sender: UIButton) {
        print("Need a break")
        self.reason = "2"
        self.requestToDeleteAccount()
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    
    @IBAction func myReasoneButtonTapped(_ sender: UIButton) {
        print("I have my reasons")
        self.reason = "3"
        self.requestToDeleteAccount()
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    func requestToDeleteAccount(){
        var respo = Int()
        let getUserId:UserDefaults = UserDefaults.standard
        
        let userId = getUserId.value(forKey: "userid")
        let URL_String = "http://www.nitevibe.in/webservices/delete_account.php?user_id=\(userId!)&reason=\(reason)"
        //let URL_String = "http://www.nitevibe.in/webservices/delete_account.php?user_id=23&reason=2"
        
        
        
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(URL_String, method: .get).responseJSON{
                
                (response) in
                
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 200){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 201){
                    
                    let alert = UIAlertController(title: ":(", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 408){
                    
                    
                    let alert = UIAlertController(title: "Yes", message:"Somthing went wrong" , preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
      
    }
}
