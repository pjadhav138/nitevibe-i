//
//  CreateProfileViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 11/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import RangeSeekSlider
import SwiftyJSON
import Kingfisher


open class CreateProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var drinksTextviewHeight: NSLayoutConstraint!
    @IBOutlet weak var drinksTextView: UITextView!
    @IBOutlet weak var aboutWordCount: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var AddImageCollectionView: UICollectionView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userAgeTextField: UITextField!
    @IBOutlet weak var userOccupationTextField: UITextField!
    @IBOutlet weak var userCityLocationTextField: UITextField!
    @IBOutlet weak var aboutUserTextView: UITextView!
    @IBOutlet weak var genderSegmentOutlet: UISegmentedControl!
    @IBOutlet weak var sexualPreferenceSegmentOutlet: UISegmentedControl!
    @IBOutlet fileprivate weak var agePreferenceRangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var drinkPreferenceTextField: UITextView!
    @IBOutlet weak var userAgeStatusSwitchOutlet: UISwitch!
    @IBOutlet weak var userAgeStatusLabelOutlet: UILabel!
    @IBOutlet weak var userLoactionStatusLabelOutlet: UILabel!
    @IBOutlet weak var userLoactionStatusSwitchOutlet: UISwitch!
    
    @IBOutlet weak var userDrinkPrefStatusLabelOutLet: UILabel!
    @IBOutlet weak var userDrinkStatusSwitchOutlet: UISwitch!
    var numberArray = ["1","2","3","4","5"]
    var selectedPhoto = UIImage()
    var ageStatus = String()
    var cityStatus = String()
    var drinkPreferenceStatus = String()
    var selectedGender = String()
    var selectedSexualPreference = String()
    //var storeSubDrink = [SaveDrinkModel]()
    var storeSubDrink = [String]()
    var subDrinkArray = [String]()
    var profileImageArray = [String]()
    var emptyImageString = ""
    var stringURL = ""
    var repeatTimer: Timer!
    var prefAgeRangeString = ""
    var drinkPrefFlag = ""
    var displayProfile = [DisplayProfileModel]()
    var drinksInCreateProfile = [DrinkModel]()
    var prefDrinkArray = [String]()
    var prefSubDrinkIdArray = [String]()
    var userName = String()
    
    
    
    //Capture image:
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 60, height: 60)
    var localImage = UIImage()
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    var testCameraToken = String()
    override open func viewDidLoad() {
        super.viewDidLoad()
       
       // drinksTextView.isUserInteractionEnabled = false
        
        DispatchQueue.main.async {
            
            self.getSwiftyDisplayProfileData()
            
            self.AddImageCollectionView.reloadData()
            
        }
        
        if (FBSDKAccessToken.current() != nil) {

            //if already login from facebook
            self.userNameTextField.isUserInteractionEnabled = false
            

        }else{

            //If not login via facebook
            self.userNameTextField.isUserInteractionEnabled = true
           

        }
        
        fetchswiftyDrinkData()
        
        
        self.userNameTextField.delegate = self
        self.userAgeTextField.delegate = self
        self.userOccupationTextField.delegate = self
        self.userCityLocationTextField.delegate = self
        self.aboutUserTextView.delegate = self
        self.AddImageCollectionView.delegate = self
        self.AddImageCollectionView.dataSource = self
        self.drinksTextView.delegate = self
        
        self.aboutUserTextView.tag = 10
        
        userNameTextField.attributedPlaceholder = NSAttributedString(string: "Your Name",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        userAgeTextField.attributedPlaceholder = NSAttributedString(string: "Your Age",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        userOccupationTextField.attributedPlaceholder = NSAttributedString(string: "Your Occupation",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        userCityLocationTextField.attributedPlaceholder = NSAttributedString(string: "Your Location",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        
        self.aboutUserTextView.text = ""
        
        
        
        // setup()
        // Do any additional setup after loading the view.
        
        DispatchQueue.main.async {
            
            
            print("17")
            self.AddImageCollectionView.reloadData()
            
        }
        
        print("check flag croppingEnabled :\(croppingEnabled)")
        
        print("check flag allowResizing :\(allowResizing)")
        print("check flag allowMoving : \(allowMoving)")
        print("check flag minimumSize \(minimumSize)")
        //print("print image size \(String(describing: imageView.image))")
        
        print("test Camera Token \(testCameraToken)")
        //
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        self.aboutUserTextView.sizeToFit()
//        self.drinksTextView.sizeToFit()
        self.contentView.sizeToFit()
        
        
        let fixedWidth = aboutUserTextView.frame.size.width
        let newSize = aboutUserTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        aboutUserTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        aboutUserTextView.isScrollEnabled = true
        
        drinksTextView.isScrollEnabled = false
        drinksTextviewHeight.constant = drinksTextView.contentSize.height
       
        
        
        //let getUserData :UserDefaults = UserDefaults.standard
        //let User_Name = getUserData.value(forKey: "first_name")
        //print("User_Name :\(String(describing: User_Name!))")
        //let User_Gender = getUserData.value(forKey: "gender")
        //print("User_Gender :\(String(describing: User_Gender!))")
        
        //        self.userNameTextField.text = User_Name as? String
        //
        //        print("userNameTextField : \(self.userNameTextField!)")
        
    }
    
//    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == userNameTextField {
//            return false; //do not show keyboard nor cursor
//        }else if textField == userAgeTextField {
//            return false;
//        }
//        return true
//    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    func updateCharacterCount() {
        //let summaryCount = self.ticketSummaryTV.text.characters.count
        //let descriptionCount = self.aboutUserTextView.text.characters.count
        let descriptionCount = self.aboutUserTextView.text.count
        
       // self.summaryCountLbl.text = "\((0) + summaryCount)/50"
        
        self.aboutWordCount.text = "\((0) + descriptionCount)/500"
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        self.updateCharacterCount()
    }
    
    func getSwiftyDisplayProfileData(){
        
        URLCache.shared.removeAllCachedResponses()
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        
        print("userid :\(String(describing: User_Id))!)")
        let urlString = "http://www.nitevibe.in/webservices/display_profile.php?user_id=\(User_Id!))"
        
        print("display profile string \(urlString)")
        let url = NSURL(string: urlString)
        var urlRequest = URLRequest(url: url! as URL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        Alamofire.request(urlRequest).validate().responseJSON {(response) -> Void in
            
            if let value = response.data {
                do {
                    
                    
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                
                                if arr.count == 0{
                                    
                                    print("No Display Data Found")
                                }else{
                                    self.displayProfile.append(DisplayProfileModel(json: arr))
                                    self.prefDrinkArray = self.displayProfile[0].userDrink
                                    let defaults = UserDefaults.standard
                                    defaults.set(self.prefDrinkArray, forKey: "jsondrinkarray")
                                    DispatchQueue.main.async {
                                        self.AddImageCollectionView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                    // set json data on textfields
                    //user name
                    if self.displayProfile[0].userName == ""{
                        //self.userName = self.userNameTextField.text!
                        self.userNameTextField.text = ""
                    }else{
                        self.userNameTextField.text = "\(self.displayProfile[0].userName)"
                    }
                    //age
                    if self.displayProfile[0].userAge == ""{
                        
                        self.userAgeTextField.text = ""
                    }else{
                        self.userAgeTextField.text = "\(self.displayProfile[0].userAge)"
                    }
                    //location
                    if self.displayProfile[0].userLoaction == ""{
                        
                        self.userCityLocationTextField.text = ""
                    }else{
                        self.userCityLocationTextField.text = "\(self.displayProfile[0].userLoaction)"
                    }
                    //occupation
                    if self.displayProfile[0].userOccupation == ""{
                        self.userOccupationTextField.text = ""
                    }else{
                        self.userOccupationTextField.text = "\(self.displayProfile[0].userOccupation)"
                    }
                    //user bio
                    if self.displayProfile[0].userBio == ""{
                        self.aboutUserTextView.text = ""
                    }else{
                        self.aboutUserTextView.text = "\(self.displayProfile[0].userBio)"
                    }
                    
                    
                    //user drinks
                    //if data come from json set flag 0 and load json data and if data come from drink pref set flag 1
                    
                    let defaults = UserDefaults.standard
                    
                    defaults.set("0", forKey: "jsondrinkprefdata")
                    
                    let jsonDrinkPrefDataFlag = defaults.value(forKey: "jsondrinkprefdata")
                    print("DrinksfromDrink : \(String(describing: jsonDrinkPrefDataFlag))")
                    
                    if jsonDrinkPrefDataFlag as! String == "0"{
                        
                        if self.displayProfile[0].userDrink.count <= 0 {
                            self.drinksTextView.text = ""
                        } else {
                            self.storeSubDrink = self.displayProfile[0].userDrink
                            self.drinksTextView.text = (self.storeSubDrink.joined(separator: ","))
                        }
                    }
                    //if data come from drink pref set flag 1
                    if self.drinkPrefFlag == "1"{
                        if self.storeSubDrink.count == 0{
                            self.drinksTextView.text = self.displayProfile[0].userDrink.joined(separator: ",")
                        } else {
                            self.subDrinkArray = self.storeSubDrink
                            self.subDrinkArray.append(contentsOf: self.storeSubDrink)
                            self.drinksTextView.text = "\(self.subDrinkArray.joined(separator: ","))"
                        }
                    }
                    
                    //set gender segment
                    //let getIndex = genderSegmentOutlet.selectedSegmentIndex
                    //print("Selected Gender Index \(getIndex)")
                    
                    if self.displayProfile[0].userGender == ""{
                        
                        self.genderSegmentOutlet.selectedSegmentIndex = UISegmentedControlNoSegment
                        
                    }else if self.displayProfile[0].userGender == "Male"{
                        self.genderSegmentOutlet.selectedSegmentIndex = 0
                        self.selectedGender = "Male"
                        
                    }else if self.displayProfile[0].userGender == "Female"{
                        self.genderSegmentOutlet.selectedSegmentIndex = 1
                        self.selectedGender = "Female"
                    }
                    
                    //Age status
                    if self.displayProfile[0].userAgeStatus == "Hide"{
                        
                        self.userAgeStatusSwitchOutlet.isOn = false
                        
                        self.userAgeStatusLabelOutlet.text! = "HIDE"
                        
                        self.ageStatus = "\(self.userAgeStatusLabelOutlet.text!)"
                        
                        print("age status 1\(self.ageStatus)")
                        
                    }else if self.displayProfile[0].userAgeStatus == "Show"{
                        
                        self.userAgeStatusSwitchOutlet.isOn = true
                        
                        self.userAgeStatusLabelOutlet.text! = "SHOW"
                        
                        self.ageStatus = "\(self.userAgeStatusLabelOutlet.text!)"
                        
                        print("age status 2\(self.ageStatus)")
                    }
                    
                    //Location status
                    
                    if self.displayProfile[0].userLoactionSatus == "Hide"{
                        
                        self.userLoactionStatusSwitchOutlet.isOn = false
                        
                        self.userLoactionStatusLabelOutlet.text! = "HIDE"
                        
                        
                        
                    }else if self.displayProfile[0].userLoactionSatus == "Show"{
                        
                        self.userLoactionStatusSwitchOutlet.isOn = true
                        
                        self.userLoactionStatusLabelOutlet.text! = "SHOW"
                    }
                    
                    
                    //Pref Gender
                    
                    if self.displayProfile[0].userPrefGender == ""{
                        
                        self.sexualPreferenceSegmentOutlet.selectedSegmentIndex = UISegmentedControlNoSegment
                        
                        self.selectedSexualPreference = ""
                        
                    }else if self.displayProfile[0].userPrefGender == "Male"{
                        
                        print("preff Male ")
                        
                        self.sexualPreferenceSegmentOutlet.selectedSegmentIndex = 0
                        
                        self.selectedSexualPreference = "Male"
                        
                    }else if self.displayProfile[0].userPrefGender == "Male,Female"{
                        
                        print("preff Both ")
                        
                        self.sexualPreferenceSegmentOutlet.selectedSegmentIndex = 1
                        
                        self.selectedSexualPreference = "Male,Female"
                    }else if self.displayProfile[0].userPrefGender == "Female"{
                        
                        print("preff Female ")
                        
                        self.sexualPreferenceSegmentOutlet.selectedSegmentIndex = 2
                        self.selectedSexualPreference = "Female"
                    }
                    
                    
                    
                    //user drink status
                    
                    
                    if self.displayProfile[0].userDrinkStatus == "Hide"{
                        
                        self.userDrinkStatusSwitchOutlet.isOn = false
                        
                        self.userDrinkPrefStatusLabelOutLet.text! = "HIDE"
                        
                        
                    }else if self.displayProfile[0].userDrinkStatus == "Show"{
                        
                        self.userDrinkStatusSwitchOutlet.isOn = true
                        
                        self.userDrinkPrefStatusLabelOutLet.text! = "SHOW"
                    }
                    
                    //print("location status \(self.displayProfile[0].userLoactionSatus)")
                    
                    //range seek slider
                    
                    
                    if self.displayProfile[0].userPrefAge == ""{
                        
                        self.prefAgeRangeString = "18-18"
                        
                        self.agePreferenceRangeSlider.selectedMinValue = 18
                        self.agePreferenceRangeSlider.selectedMaxValue = 18
                        
                        
                        
                        let selectedMinValue  = "18"
                        let selectedMaxValue = "18"
                        
                        print("print min \(CGFloat(Int(selectedMinValue)!))")
                        print("print max \(CGFloat(Int(selectedMaxValue)!))")
                        
                        self.setup(selectedMinValue: CGFloat(Int(selectedMinValue)!), selectedMaxValue: CGFloat(Int(selectedMaxValue)!))
                        
                        //print("print age range \()")
                        
                        //rangeSeekSlider(_ slider: self, stringForMinValue: 0.18)
                        //rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue: 0.18)
                        //rangeSeekSlider(_, slider: RangeSeekSlider, didChange minValue: 0.18 , maxValue: 0.18)
                        //rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue: <#T##CGFloat#>)
                        
                    }else{
                        
                        print("json data pref age range \(self.displayProfile[0].userPrefAge)")
                        
                        self.prefAgeRangeString = "\(self.displayProfile[0].userPrefAge)"
                        
                        
                        //split string and store value in min-max variable to pass range seek slider
                        let origanlString    = "\(self.displayProfile[0].userPrefAge)"
                        let splitedString = origanlString.components(separatedBy: "-")
                        
                        let selectedMinValue  = splitedString[0]
                        let selectedMaxValue = splitedString[1]
                        
                        
                        print("min Value -\(selectedMinValue)")
                        print("max value -\(selectedMaxValue)")
                        
                        //pass data to range seek slider
                        
                        //self.agePreferenceRangeSlider.selectedMinValue = CGFloat(Int(selectedMinValue)!)
                        //self.agePreferenceRangeSlider.selectedMaxValue = CGFloat(Int(selectedMaxValue)!)
                        
                        
                        print("print min \(CGFloat(Int(selectedMinValue)!))")
                        print("print max \(CGFloat(Int(selectedMaxValue)!))")
                        
                        print(" type of min \(type(of: CGFloat(Int(selectedMinValue)!)))")
                        
                        self.setup(selectedMinValue: CGFloat(Int(selectedMinValue)!), selectedMaxValue: CGFloat(Int(selectedMaxValue)!))
                        
                    }
                    
                    
                    if self.displayProfile[0].profileImageStack.count == 0{
                        
                        print("No Image Found")
                        
                    }else{
                        self.profileImageArray.removeAll()
                        // 1st append available json image data
                        print("print number array count \(self.numberArray.count)")
                        
                        print("user profile stack images data \(self.displayProfile[0].profileImageStack[0].images)")
                        print("profile profile image count \(self.displayProfile[0].profileImageStack.count)")
                        
                        for i in 0..<self.displayProfile[0].profileImageStack.count{
                            
                            self.profileImageArray.append(self.displayProfile[0].profileImageStack[i].images)
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                print("22")
                                self.AddImageCollectionView.reloadData()
                                
                            }
                        }
                        var remainImageCount = (self.numberArray.count - self.displayProfile[0].profileImageStack.count)
                        print("print remain array count \(remainImageCount)")
                        
                        
                        
                        if remainImageCount == 0{
                            
                            print("profile Image Array \(self.profileImageArray)")
                            
                        }else{
                            for j in 1...remainImageCount{
                                self.profileImageArray.append(self.emptyImageString)
                                DispatchQueue.main.async {
                                    self.AddImageCollectionView.reloadData()
                                }
                            }
                            
                        }
                        
                    }
                }catch {
                    print("cannot convert to Json")
                }
            }
            
        }
    }
    
    func setup(selectedMinValue
        :CGFloat,selectedMaxValue:CGFloat){
        
        agePreferenceRangeSlider.delegate = self
        
        print("pass min value \(selectedMinValue)")
        print("pass max value \(selectedMaxValue)")
        
        agePreferenceRangeSlider.minValue = 18
        agePreferenceRangeSlider.maxValue = 60
        
        agePreferenceRangeSlider.selectedMinValue = selectedMinValue
        agePreferenceRangeSlider.selectedMaxValue = selectedMaxValue
        
        print("selected min value 3\(agePreferenceRangeSlider.selectedMinValue)")
        
    }
    
    
    
    func fetchswiftyDrinkData(){
        let urlString = "http://www.nitevibe.in/webservices/drinks.php"
        
        Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                
                                self.drinksInCreateProfile.append(DrinkModel(drinkJson: arr))
                                
                                print("print drink count array zoom \(self.drinksInCreateProfile.count)")
                                print("print drink count array zoom \(self.drinksInCreateProfile)")
                                
                                
                            }
                            
                        }
                        
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
            
            //some of the reason preDrinkArray data come here the store that array in user default and access here
            
            let defaults = UserDefaults.standard
            let jsonDrinkArray = defaults.stringArray(forKey: "jsondrinkarray") ?? [String]()
            
            
            print("pref drink array inside swifty drink \(jsonDrinkArray.count)")
            
            //pass this jsonDrinkArray to getting id of sub drink from drink json
            for k in 0..<jsonDrinkArray.count{
                
                print("drink at \(k) and drink name \(jsonDrinkArray[k])")
                
                
                
                for i in 0..<self.drinksInCreateProfile.count{
                    
                    for j in 0..<self.drinksInCreateProfile[i].subDrinkDictionary.count{
                        
                        if self.drinksInCreateProfile[i].subDrinkDictionary[j].sub_drink_name == "\(jsonDrinkArray[k])"{
                            
                            //access provided sub drink name id ...Whoo :)
                            print("selected drink id \(self.drinksInCreateProfile[i].subDrinkDictionary[j].sub_drink_id)")
                            
                            
                            //append data to prefSubDrinkIdArray
                            self.prefSubDrinkIdArray.append(self.drinksInCreateProfile[i].subDrinkDictionary[j].sub_drink_id)
                            
                            
                            
                            print("print prefSubDrinkIdArray\(self.prefSubDrinkIdArray)")
                            
                            let defaults = UserDefaults.standard
                            defaults.set(self.prefSubDrinkIdArray, forKey: "prefsubdrinkidarray")
                            
                            
                        }
                        
                    }
                    
                }
                
            }
            
            //
            //            for i in 0..<self.drinksInCreateProfile.count{
            //
            //                for j in 0..<self.drinksInCreateProfile[i].subDrinkDictionary.count{
            //
            //                    if self.drinksInCreateProfile[i].subDrinkDictionary[j].sub_drink_name == "Kingfisher"{
            //
            //
            //                        print("selected drink id \(self.drinksInCreateProfile[i].subDrinkDictionary[j].sub_drink_id)")
            //
            //                    }
            //
            //                }
            //
            //            }
            
        }
    }
    
    
    
    @IBAction func userAgeSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == false){
            
            
            self.userAgeStatusLabelOutlet.text! = "HIDE"
            
            //ageStatus = "\(self.userAgeStatusLabelOutlet.text!)"
            
        }else{
            
            
            self.userAgeStatusLabelOutlet.text! = "SHOW"
            //ageStatus = "\(self.userAgeStatusLabelOutlet.text!)"
            
        }
    }
    
    
    @IBAction func userCitySwitch(_ sender: UISwitch) {
        
        
        if (sender.isOn == false){
            
            self.userLoactionStatusLabelOutlet.text! = "HIDE"
            
        }else{
            self.userLoactionStatusLabelOutlet.text! = "SHOW"
        }
        
    }
    
    
    @IBAction func userDrinkSwitch(_ sender: UISwitch) {
        if (sender.isOn == false){
            
            drinkPreferenceStatus = "Hide"
            self.userDrinkPrefStatusLabelOutLet.text = "HIDE"
            //self.userDrinkPrefHideShowLbl.text! = "HIDE"
            print("Drink Preference Status: \(drinkPreferenceStatus)")
            
        }else{
            
            drinkPreferenceStatus = "Show"
            self.userDrinkPrefStatusLabelOutLet.text = "SHOW"
            //self.userDrinkPrefHideShowLbl.text! = "SHOW"
            print("Drink Preference  Status: \(drinkPreferenceStatus)")
        }
        
    }
    
    
    
    @IBAction func genderSegmentTapped(_ sender: Any) {
        
        let getIndex = genderSegmentOutlet.selectedSegmentIndex
        print("Selected Gender Index \(getIndex)")
        
        
        
        switch (getIndex) {
        case 0:
            
            selectedGender = "Male"
            print("Selected Male")
        case 1:
            selectedGender = "Female"
            print("Selected Female")
        default:
            selectedGender = ""
            print("No Select")
            
        }
    }
    
    
    @IBAction func sexualPreferenceTapped(_ sender: Any) {
        
        let getIndex = sexualPreferenceSegmentOutlet.selectedSegmentIndex
        print("Selected Sexual Preferences Index \(getIndex)")
        switch (getIndex) {
        case 0:
            
            selectedSexualPreference = "Male"
            print("Selected Male")
        case 1:
            selectedSexualPreference = "Male,Female"
            print("Selected Both")
        case 2:
            selectedSexualPreference = "Female"
            print("Selected Female")
            
        default:
            selectedSexualPreference = ""
            print("No Select")
            
        }
        
        
    }
    
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //return profileImages.count
        return numberArray.count
        
        if displayProfile[0].profileImageStack.count == 0{
            
            return numberArray.count
        }
        
        //return displayProfile[0].profileImageStack.count
        
        
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell:AddImagesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionViewCell", for: indexPath) as! AddImagesCollectionViewCell
        
        print("print index \(numberArray[indexPath.row])")
        cell.numberOfImagesLabel.text = numberArray[indexPath.row]
        
        //cell.addImageButtonTapped.addTarget(self, action: #selector(addImageButtonAction), for: .touchUpInside)
        // cell.whosgoingImageView.kf.setImage(with: URL(string:events[eventIndexNumber].eventUserGoing[indexPath.row].userIamge))
        
        // cell.profileImage.kf.setImage(with: URL(string:displayProfile[0].profileImageStack[indexPath.row].images))
        
        if profileImageArray.count == 0{
            
            print("empty profile image array ")
            
           // cell.profileImage.image = UIImage(named: "Add photo")
            
            
        }else {
            
            
            stringURL = profileImageArray[indexPath.row]
            print("index  \(indexPath.row)th  at position image name \(profileImageArray[indexPath.row])")
            //ImageCache.default.removeImage(forKey:stringURL, fromDisk: true)
            cell.profileImage.kf.setImage(with: URL(string: stringURL))
            //ImageCache.default.removeImage(forKey:stringURL, fromDisk: true)
            
            
            if profileImageArray[indexPath.row] == ""{
                
                //cell.profileImage.image = UIImage(named: "Add photo")
            }
            
        }
        
        
        return cell
        
        
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("print selected index path \(indexPath.row)")
        
        if profileImageArray.count == 0{
            
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingParameters) { [weak self] image, asset in
                // self?.imageView.image = image
                
                if(image != nil) {
                    self?.localImage = image!
                    print("original size \(String(describing: image?.size))")
                    self?.dismiss(animated: true, completion: nil)
                }
                
            }
            
            present(libraryViewController, animated: true, completion: nil)
            
            // let myViewController = AddProfileImageViewController(nibName: "AddProfileImageViewController", bundle: nil)
            // self.present(myViewController, animated: true, completion: nil)
            
        }
        else if profileImageArray[indexPath.row] == ""{
            
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingParameters) { [weak self] image, asset in
                //self?.imageView.image = image
                
                
                //self?.localImage = image!
                
                
                if self?.localImage == nil{
                    
                    print("Do nothing")
                }else{
                    print("original size \(image?.size)")
                    self?.dismiss(animated: true, completion: nil)
                }
                
            }
            
            present(libraryViewController, animated: true, completion: nil)
            // let myViewController = AddProfileImageViewController(nibName: "AddProfileImageViewController", bundle: nil)
            // self.present(myViewController, animated: true, completion: nil)
            
            
        }else{
            
            print("instantiate AddImageAlertViewController view")
            let showAddImageAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "AddImageAlertViewController") as! AddImageAlertViewController
            showAddImageAlertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
            
            
            let selectedPhotoNameString = profileImageArray[indexPath.row]
            
            
            print("profile image  2 \(self.displayProfile[0].profileImage )")
            
            let x = "\(profileImageArray[indexPath.row])"
            let y = "\(self.displayProfile[0].profileImage)"
            
            
            if  x == y{
                
                print("true")
                
                print("print string \(selectedPhotoNameString)")
                
                let imageURL = URL(string: profileImageArray[indexPath.row])
                let imageData = try! Data(contentsOf: imageURL! as URL)
                print("image data \(imageData)")
                showAddImageAlertVC.setAsProfileButtonOutlet.isHidden = true
                showAddImageAlertVC.removeAsProfileButtonOutlet.isHidden = false
                showAddImageAlertVC.removePhotoPermanentlyButtonOutlet.isHidden = false
                showAddImageAlertVC.profileImage.image = UIImage(data: imageData)!
                showAddImageAlertVC.photoNameStringUrl = selectedPhotoNameString
                //present(showAddImageAlertVC, animated: true, completion: nil)
                self.addChildViewController(showAddImageAlertVC)
                self.view.addSubview(showAddImageAlertVC.view)
            }else{
                print("false")
                let imageURL = URL(string: profileImageArray[indexPath.row])
                let imageData = try! Data(contentsOf: imageURL! as URL)
                print("image data \(imageData)")
                showAddImageAlertVC.setAsProfileButtonOutlet.isHidden = false
                showAddImageAlertVC.removeAsProfileButtonOutlet.isHidden = true
                showAddImageAlertVC.removePhotoHeight.constant = 0
                showAddImageAlertVC.removePhotoPermanentlyButtonOutlet.isHidden = false
                showAddImageAlertVC.profileImage.image = UIImage(data: imageData)!
                showAddImageAlertVC.photoNameStringUrl = selectedPhotoNameString
                showAddImageAlertVC.removeAsProfileButtonOutlet.isHidden = true
                //present(showAddImageAlertVC, animated: true, completion: nil)
                self.addChildViewController(showAddImageAlertVC)
                self.view.addSubview(showAddImageAlertVC.view)
            }
            
            
        }
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        print("user Name: \(String(describing: userNameTextField.text!))")
        print("user Age: \(String(describing: userAgeTextField.text!))")
        
        //if user not selcted get default status Hide for Age
        
        if self.userAgeStatusLabelOutlet.text! == "HIDE"{
            
            ageStatus = "Hide"
            print("user Age Status: \(ageStatus)")
        }else if self.userAgeStatusLabelOutlet.text! == "SHOW"{
            
            ageStatus = "Show"
            print("user Age Status: \(ageStatus)")
            
        }
        
        //if user not selected get default status Hide for Locaion
        
        if self.userLoactionStatusLabelOutlet.text! == "HIDE"{
            
            cityStatus = "Hide"
            print("user city status: \(cityStatus)")
            
        }else if self.userLoactionStatusLabelOutlet.text! == "SHOW"{
            cityStatus = "Show"
            print("user city status: \(cityStatus)")
        }
        
        print("user ocuupation: \(String(describing: userOccupationTextField.text!))")
        print("user Location: \(String(describing: userCityLocationTextField.text!))")
        print("user City Status: \(cityStatus)")
        print("About User: \(aboutUserTextView.text!)")
        
        print("Drink Preference Status: \(drinkPreferenceStatus)")
        print("Selected Gender: \(selectedGender)")
        print("Selected Sexual Preference: \(selectedSexualPreference)")
        
        //if user not selected get default status Hide for drink
        if self.userDrinkPrefStatusLabelOutLet.text! == "HIDE"{
            
            drinkPreferenceStatus = "Hide"
            print("user drink status: \(drinkPreferenceStatus)")
            
        }else if self.userDrinkPrefStatusLabelOutLet.text! == "SHOW"{
            drinkPreferenceStatus = "Show"
            print("user drink status: \(drinkPreferenceStatus)")
        }
        
        if self.userAgeTextField.text == ""{
            
            print("please enter your age")
            
            let alert = UIAlertController(title: "", message: "Please enter your age", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            
            updateUserProfile()
            
            let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
            
            
            BaseController.selectedIndex = 0
            self.present(BaseController, animated: true, completion: nil)
            
        }
        
        
        //
        //        let ProfileView:ProfileViewController = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        //
        //
        //        navigationController?.pushViewController(ProfileView, animated: true)
        
        // self.present(BaseController, animated: true, completion: nil)
        
        //        let ProfileView:ProfileViewController = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        //
        //        present(ProfileView, animated: true, completion: nil)
        
    }
    
    func updateUserProfile(){
        
        URLCache.shared.removeAllCachedResponses()
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        var respo = Int()
        //let URL_String = "http://www.nitevibe.in/webservices/test_update_profile.php?user_id=14&name=riya&age=22&age_status=Hide&gender=Female&occupation=HRatCBS&city=Mumbai&city_status=Show&about=I am from mumbai&age_pref=22-28&gender_pref=Male&drink_pref=1,2,3&drink_status=Show&photos=&pic_name="
        
        let URL_String = "http://www.nitevibe.in/webservices/test_update_profile.php?user_id=\(User_Id!)&name=\(userNameTextField.text!)&age=\(userAgeTextField.text!)&age_status=\(ageStatus)&gender=\(selectedGender)&occupation=\(userOccupationTextField.text!)&city=\(userCityLocationTextField.text!)&city_status=\(cityStatus)&about=\(aboutUserTextView.text!)&age_pref=\(prefAgeRangeString)&gender_pref=\(selectedSexualPreference)&drink_pref=\(drinksTextView.text!)&drink_status=\(drinkPreferenceStatus)"
        
        
        
        print("test update profile URL String\(URL_String)")
        let escapeUrl = URL_String.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        do {
            //disable cache using reloadIgnoringCacheData Not updating backend update in app
            var urlRequest = try URLRequest(url: escapeUrl!, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            Alamofire.request(urlRequest).responseJSON{
                
                (response) in
                print(" response :\(String(describing: response.request))")
                
                guard response.result.isSuccess  else {
                    
                    print("Block 1 ")
                    print(" response :\(response)")
                    
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                print("message\(dict)")
                print("dict respo: \(dict["response"]!)")
                print("dict message:\(dict["message"]!)")
                
                
                respo = dict["response"]! as! Int
                let responseMessage = dict["message"]
                
                
                print(type(of:responseMessage))
                
                if (respo == 201){
                    
                    let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if(respo == 202){
                    
                    let alert = UIAlertController(title: "Oops!", message: responseMessage as! String, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else if(respo == 200){
                    
                    print("Profile Updated Successfully")
                }
            }
            
        }catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            textView.resignFirstResponder()
            return false
        }
        if(textView == aboutUserTextView){
            return textView.text.count + (text.count - range.length) <= 300
        }
        
        return false
    }
    
    @IBAction func addDrinksButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "DrinkPreference", bundle: nil)
        let drinkPrefVC = storyboard.instantiateViewController(withIdentifier: "DrinkPreferenceViewController") as! DrinkPreferenceViewController
        drinkPrefVC.delegate = self
        var tempSubDrinkArray = Array<SaveDrinkModel>()
        for sdrink in drinksInCreateProfile {
            for drinkname in storeSubDrink {
                if let subdrink = sdrink.subDrinkDictionary.filter({$0.sub_drink_name == drinkname}).first {
                    tempSubDrinkArray.append(SaveDrinkModel.init(name: subdrink.sub_drink_name, id: subdrink.sub_drink_id))
                }
            }
        }
        drinkPrefVC.storeSubDrink = tempSubDrinkArray
        present(drinkPrefVC, animated: true, completion: nil)
    }
    
}

// MARK: - RangeSeekSliderDelegate

extension CreateProfileViewController: RangeSeekSliderDelegate{
    
    public func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === agePreferenceRangeSlider {
            prefAgeRangeString = "\(Int(minValue))-\(Int(maxValue))"
        } else{
            
        }
    }
    
    public func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    public func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}

extension CreateProfileViewController: DrinkPreferenceVCDelegate {
    
    func getDataFromDrinkPrefVC(drinks: [SaveDrinkModel]) {
        storeSubDrink = drinks.map({$0.sDrinkanme})
        self.drinksTextView.text = storeSubDrink.joined(separator: "," + " ")
    }
}

extension UIScrollView {
    
    func resizeScrollViewContentSizeCreateP() {
        var contentRect = CGRect.zero
        for view in self.subviews {
            contentRect = contentRect.union(view.frame)
        }
        self.contentSize = contentRect.size
    }
}
