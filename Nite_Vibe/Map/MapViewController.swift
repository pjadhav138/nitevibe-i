//
//  MapViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 19/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import UIKit
import Foundation
import CoreLocation
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON


class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    private let locationManager = CLLocationManager()
    private var mapView = GMSMapView()
    private var marker = GMSMarker()
    private var marker2 = GMSMarker()
    private var path = GMSMutablePath()
    private let locationStart = CLLocation()
    private let locationEnd = CLLocation()
    var clublatitude:String = String()
    var clublongitude:String = String()
    var clubname:String = String()
    var clubLatFloat:Double = Double()
    var clubLongFloat:Double = Double()
    var clubLat:String = String()
    var clubLong:String = String()
    var clubName:String = String()
    var clubData = [ClubModel]()
    private let searchRadius: Int = 10
    
    var selectedClubid = ""
    var userID = ""
    var backButton = UIButton()
    var buttonConstraints:[NSLayoutConstraint] = []
    var myLatitude = Double()
    var myLongitude = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        self.fetchswiftyMapData()
        print("Lat : \(myLatitude)")
        print("Long : \(myLongitude)")
        let camera = GMSCameraPosition.camera(withLatitude:
            (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 12.0)
        
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        view = mapView
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        }
        
        let image = UIImage(named: "BackButton") as UIImage?
        backButton = UIButton(type: UIButtonType.custom) as UIButton
        backButton.setImage(image, for: .normal)
        backButton.addTarget(self, action: #selector(self.backButtonClicked), for: .touchUpInside)
        self.view.addSubview(backButton)
        
        
        let buttonHeight = backButton.heightAnchor.constraint(equalToConstant: 36)
        let buttonWidth = backButton.widthAnchor.constraint(equalToConstant: 36)
        let topConstraint = backButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25)
        let bottomConstraint = backButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -53)
        let leftConstraint = backButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 17)
        let rightConstraint = backButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -322)
        buttonConstraints = [buttonHeight,buttonWidth,topConstraint,bottomConstraint,leftConstraint,rightConstraint]
        NSLayoutConstraint.activate(buttonConstraints)
    }
    
    @IBAction func backbtn_action(_ sender: UIBarButtonItem) {
        
        
        let ClubDetailScreenViewController:ClubDetailScreenViewController = storyboard?.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
        
        ClubDetailScreenViewController.selectedClubId = selectedClubid
        ClubDetailScreenViewController.SendUSerIDToMap = userID
        navigationController?.isNavigationBarHidden = true
        
        self.navigationController?.pushViewController(ClubDetailScreenViewController, animated: true)
    }
    
    @objc func backButtonClicked() {
        
        
        
        //        let ClubDetailScreenViewController:ClubDetailScreenViewController = storyboard?.instantiateViewController(withIdentifier: "ClubDetailScreenViewController") as! ClubDetailScreenViewController
        //
        //        //present(ClubDetailScreenViewController, animated: true, completion: nil)
        
        
        
    }
    
    func fetchswiftyMapData(){
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id))!)")
        
        let urlString = "http://www.nitevibe.in/webservices/club.php?user_id=\(User_Id!)&club_id=\(selectedClubid)"
        
        Alamofire.request(urlString, method: .get).validate().responseJSON { (response) -> Void in
            if let value = response.data {
                do {
                    let json = try JSON(data: value)
                    if let dictionnary = json.dictionaryObject {
                        if let messageArray = dictionnary["message"] as? [[String: Any]] {
                            for arr in messageArray {
                                
                                
                                
                                self.clubData.append(ClubModel(clubJson: arr))
                                
                                print("print clubData count array  \(self.clubData.count)")
                                print("ClubName : \(self.clubData[0].club_name)")
                                self.clubname = self.clubData[0].club_name
                                
                            }
                            
                            self.getMapData()
                            
                        }
                        
                    }
                    
                }catch {
                    print("cannot convert to Json")
                }
            }
            
            
        }
    }
    
    func getMapData()
    {
        
        print("club_Name: \(clubData[0].club_name)")
        print("club_Lat: \(clubData[0].clubLat)")
        print("club_Long: \(clubData[0].clubLong)")
        
        self.clubLatFloat = (clubData[0].clubLat as NSString).doubleValue
        print("club lat :\(self.clubLatFloat)")
        self.clubLongFloat = (clubData[0].clubLong as NSString).doubleValue
        print("club lang :\(self.clubLongFloat)")
        
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
        marker.title = "Your Location"
        marker.snippet = "Mumbai"
        marker.map = mapView
        
        marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2D(latitude: clubLatFloat, longitude: clubLongFloat)
        
        marker2.title = clubData[0].club_name
        marker2.snippet = "Mumbai"
        marker2.map = mapView
        
        self.drawPath()
        
        
        //   path.add(CLLocationCoordinate2DMake((self.locationManager.location?.coordinate.latitude)!, (self.locationManager.location?.coordinate.longitude)!))
        //        path.add(CLLocationCoordinate2DMake(clubLatFloat, clubLongFloat))
        //
        //    let rectangle = GMSPolyline(path: path)
        //    rectangle.strokeWidth = 5.0
        //    rectangle.geodesic = true
        //    rectangle.strokeColor = .blue
        //    rectangle.map = mapView
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error to get Location : \(error)")
    }
    
    //    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //        locationManager.startUpdatingLocation()
    //
    //    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        
        /* you can use these values*/
        let myLat = location.coordinate.latitude
        let myLong = location.coordinate.longitude
        
        print("Latitude : \(myLat)")
        print("Longitude : \(myLong)")
        
//        locationManager.stopUpdatingLocation()
    }
    
    func  drawPath(){
        
        //        print("start loaction \(locationStart)")
        //        print("end location \(locationEnd)")
        
        let origin = "\((locationManager.location?.coordinate.latitude)!),\((locationManager.location?.coordinate.longitude)!)"
        print("origin:  \(origin)")
        
        let destination = "\(clubLatFloat),\(clubLongFloat)"
        print("destination:  \(destination)")
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
        
        do {
            
            var urlRequest = try URLRequest(url: url, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            print("map url ..\(urlRequest)")
            Alamofire.request(url).responseJSON { response in
                
                print(response.request as Any)
                print(response.response as Any)
                print(response.data as Any)
                print(response.result as Any)
                
                
                let json = try! JSON(data: response.data!)
                //print("json : \(json)")
                let routes = json["routes"].arrayValue
                
                
                print("\(routes)")
                for route in routes
                {
                    let routeOverviewPloyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPloyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    
                    polyline.strokeWidth = 4
                    polyline.strokeColor = UIColor.red
                    polyline.map = self.mapView
                }
            }
        }catch{
            
            print(error)
        }
    }
    
}
