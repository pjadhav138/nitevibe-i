//
//  ClubModel.swift
//  GMap
//
//  Created by Shraddha Mahajan on 19/06/18.
//  Copyright © 2018 Shraddha Mahajan. All rights reserved.
//

import Foundation
import SwiftyJSON


struct ClubModel{
    
    var club_name = String()
    var clubLat = String()
    var clubLong = String()
    
    init()
    {
        
    }
    
    init(clubJson:[String:Any]){
        
        if let club_NAME = clubJson["club_name"] as? String{
            
            self.club_name = club_NAME
        }
        
        if let club_Lat = clubJson["lat"] as? String{
            self.clubLat = club_Lat
        }
        
        if let club_Long = clubJson["lon"] as? String{
            self.clubLong = club_Long
        }
        
        
        
    }
    
}
