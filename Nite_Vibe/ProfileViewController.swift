//
//  ProfileViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 10/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore
import CTShowcase

class ProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var settingBtn: UIButton!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userEventsTableView: UITableView!
    @IBOutlet weak var userNameAndAgeLbl: UILabel!
    @IBOutlet weak var userOccupationAndComapnyNameLbl: UILabel!
    @IBOutlet weak var userLocationLbl: UILabel!
   
    @IBOutlet weak var noEventImageView: UIImageView!
    
    @IBOutlet weak var youdontHaveAnyEventsLabel: UILabel!
    
    
    @IBOutlet weak var findEventNearMeButtonTapped: UIButton!
    
    //var eventArray = ["ABC Night Event","Khar Social Krunk Live ft Your Chin","ABC Night Event"]
    var eventArray = [String]()
    var eventDateArray = [String]()
    var eventIdArray = [String]()
    var clubIdArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
      self.noEventImageView.isHidden = true
      self.youdontHaveAnyEventsLabel.isHidden = true
      self.findEventNearMeButtonTapped.isHidden = true
        self.userEventsTableView.isHidden = true
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.layer.borderColor = UIColor.blue.cgColor
        self.profileImageView.layer.borderWidth = 2
        self.profileImageView.clipsToBounds = true
        self.userEventsTableView.delegate = self
        self.userEventsTableView.dataSource = self
        userEventsTableView.reloadData()
        getProfileData()
        // Do any additional setup after loading the view.
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        let showcase = CTShowcaseView(title: "", message: "",image: #imageLiteral(resourceName: "CheckIn.png"), key: nil) { () -> Void in
//            print("dismissed")
//        }
//        
//        let highlighter = CTDynamicGlowHighlighter()
//        highlighter.highlightColor = UIColor.white
//        highlighter.animDuration = 0.5
//        highlighter.glowSize = 5
//        highlighter.maxOffset = 10
//        
//        showcase.highlighter = highlighter
//        
//        showcase.setup(for: self.settingBtn, offset: CGPoint.zero, margin: 0)
//        showcase.show()
//    }
    
    @IBAction func settingButtonTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Swipe", bundle: nil)
        
      //  let eventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController
        
        let SettingsViewController:SettingsViewController = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        present(SettingsViewController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        
        
        let CreateProfileViewController:CreateProfileViewController = storyboard?.instantiateViewController(withIdentifier: "CreateProfileViewController") as! CreateProfileViewController
        
        present(CreateProfileViewController, animated: true, completion: nil)
        
        // CreateProfileViewController
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = userEventsTableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as!  ProfileTableViewCell
        cell.eventNameLbl.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        cell.eventNameLbl.text = "\("    " + eventArray[indexPath.row] + " \t")"
        cell.eventDateLbl.font = UIFont(name: "OpenSans-ExtraBold", size: 14)
        //cell.labelforFont = eventDateArray[indexPath.row]
        //print("LabelDate : \(cell.labelforFont)")
        cell.eventDateLbl.text = eventDateArray[indexPath.row]
        cell.eventDateLbl.layer.masksToBounds = true
        cell.eventDateLbl.layer.cornerRadius = 4
        
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
        let storyboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)

        let eventDetailVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsUpdatedViewController") as! EventDetailsUpdatedViewController

        eventDetailVC.selectedEventId = eventIdArray[indexPath.row]
        eventDetailVC.selectedClubId =  clubIdArray[indexPath.row]

        //print("Selected club Id \(eventDetailVC.selectedClubId)")
        //print("Selected event ID : \(eventIdArray[indexPath.row])")

        present(eventDetailVC, animated: true, completion: nil)
        
        
    }
    func getProfileData(){
        
        let getUserData :UserDefaults = UserDefaults.standard
        let User_Id = getUserData.value(forKey: "userid")
        print("userid :\(String(describing: User_Id!))")
        
        
        let URL_String = "http://www.nitevibe.in/webservices/profile.php?user_id=\(User_Id!)"
        print("user profile String: \(URL_String)")
        
        
        do {
    
            var urlRequest = try URLRequest(url: URL_String, method:.get)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            
            Alamofire.request(urlRequest).responseJSON{
                response in
                
                print(" response :\(response.request!)")
                print(" response :\(response.request!)")
                
                guard response.result.isSuccess  else {
                    print("Block 1 ")
                    print(" response :\(response)")
                    print("Error with response: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                guard let dict = response.result.value as? Dictionary <String,AnyObject> else {
                    
                    print(" response :\(response)")
                    
                    print("Error with dictionary: \(String(describing: response.result.error))")
                    
                    return
                    
                }
                
                print("dict respo \(dict["response"]!)")
                //response 200 : Login Success
                if(dict["response"] as! Int == 200){
                    
                    guard let dictData = dict["message"] as? [Dictionary <String,AnyObject>] else {
                        
                        print("Error with dictionary data: \(String(describing: response.result.error))")
                        
                        return
                    }
                    
                    print(" dict data\(dictData)")
                    
                    
                    self.eventArray.removeAll()
                    self.eventDateArray.removeAll()
                    for data in dictData{
                        
                        let userDict = data["user"] as! [Dictionary <String,AnyObject>]
                        
                        for userData in userDict{
                            
                            let username = userData["name"] as! String
                            print("userName: \(username)")
                            let userage = userData["age"] as! String
                            print("userAge: \(userage)")
                            let userloction = userData["location"] as! String
                            print("userLocation: \(userloction)")
                            let useroccupation = userData["occupation"] as! String
                            print("userOccupation: \(useroccupation)")
                            let userblockcount = userData["block_count"] as! Int
                            print("userBlockCount: \(userblockcount)")
                            let userprofilepiclink = userData["profile"] as! String
                            print("user profile Link:\(userprofilepiclink)")
                            
                            if userprofilepiclink == ""{
                                
                                print("Do nothing")
                            }else{
                                
                                let imageURL = URL(string: userprofilepiclink)
                                
                                do{
                                 let imageData = try! Data(contentsOf: imageURL! as URL)
                                self.profileImageView.image = UIImage(data: imageData)
                                print("\(imageData)")
                                }catch{
                                    
                                    print("error")
                                }
                            }
                            
                            if userage == ""{
                                self.userNameAndAgeLbl.text = "\(username)"
                            }else{
                                self.userNameAndAgeLbl.text = "\(username),"+" "+"\(userage)"
                            }
                            
                            
                            print("Name -> \(self.userNameAndAgeLbl)")
                            
                            self.userOccupationAndComapnyNameLbl.text = "\(useroccupation)"
                            print("Work -> \(self.userOccupationAndComapnyNameLbl)")
                           
                            self.userLocationLbl.text = "\(userloction)"
                            print("Location -> \(self.userLocationLbl)")
                            
                            
                        }
                        print("print user array \(userDict)")
                        
                        print("----------------------------------------------------------------------------------------")
                        
                        let eventData = data["event"] as? AnyObject
                        
                        
                        if eventData?.count == 0{
                            
                            print("No event Available")
                            self.noEventImageView.isHidden = false
                            self.youdontHaveAnyEventsLabel.isHidden = false
                            self.findEventNearMeButtonTapped.isHidden = false
                            
                            self.findEventNearMeButtonTapped.addTarget(self, action: #selector(self.findEventNearMe), for: .touchUpInside)
                            self.userEventsTableView.isHidden = true
                            
                        }else{
                            
                            self.noEventImageView.isHidden = true
                            self.youdontHaveAnyEventsLabel.isHidden = true
                            self.findEventNearMeButtonTapped.isHidden = true
                            self.userEventsTableView.isHidden = false
                            print("Event is Avilable")
                            
                            let eventDict = data["event"] as! [Dictionary <String,AnyObject>]
                            
                            print("user events : \(eventDict)")
                            self.eventArray.removeAll()
                            print("Event Array1\(self.eventArray)")
                            self.eventDateArray.removeAll()
                            self.eventIdArray.removeAll()
                            self.clubIdArray.removeAll()
                            for eventData in eventDict{
                                
                                let eventid = eventData["event_id"] as! String
                                print("eventid \(eventid)")
                                let eventname = eventData["event_name"] as! String
                                print("eventname \(eventname)")
                                let eventdate = eventData["event_date"] as! String
                                print("eventdate \(eventdate)")
                                let eventstart = eventData["event_start"] as! String
                                print("eventstart \(eventstart)")
                                let eventend = eventData["event_end"] as! String
                                print("eventend \(eventend)")
                                let clubId = eventData["club_id"] as! String
                                self.eventIdArray.append(eventid)
                                self.clubIdArray.append(clubId)
                                self.eventArray.append(eventname)
                                self.eventDateArray.append(eventdate)
                            }
                            
                            self.userEventsTableView.reloadData()
                            print("print user event array \(eventDict)")
                            print("Event Array2\(self.eventArray)")
                           
                            print("------------------Success")
                            
                        }
                        
                        
                    }  //response 201 : invalid username
                }else if(dict["response"] as! Int == 201){
                    let alert = UIAlertController(title: "Oops", message:"Please enter valid username", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    //response 202 : invalid password
                }else if(dict["response"] as! Int == 202){
                    let alert = UIAlertController(title: "Oops", message:"Please enter valid password", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    //response 203 : inactive user
                }else if(dict["response"] as! Int == 203){
                    
                    print("response 203")
                    
                    //response 408: request method not accpeted
                }else if(dict["response"] as! Int == 408){
                    
                    let alert = UIAlertController(title: "Oops", message:"Somthing went wrong", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            
        } catch {
            // TODO: find a better way to handle error
            print(error)
            
        }
        
    }
    
    
    
    @objc func findEventNearMe(){
        
        print("back button tapped to instatiate back view ")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let BaseController:BaseTabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
        
        BaseController.selectedIndex = 1
        self.present(BaseController, animated: true, completion: nil)
        
    }
    
    
    
}

