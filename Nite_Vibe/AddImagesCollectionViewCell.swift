//
//  AddImagesCollectionViewCell.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 17/04/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class AddImagesCollectionViewCell: UICollectionViewCell,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
  
    @IBOutlet weak var squareView: UIView!
    
    @IBOutlet weak var numberOfImagesLabel: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = 12
        squareView.layer.masksToBounds = true
        squareView.layer.cornerRadius = 12
        
        
    }
    
    
}
