//
//  LogIn_1_VC.swift
//  Nite_Vibe
//
//  Created by Appmonks on 28/03/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import Alamofire
enum DataType:Int{
    case Other = 0
    case Number = 1
    case Email = 2
}
class LogIn_1_VC: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var txt_MobEmail: UITextField!
    
    @IBOutlet weak var imageViewCheck: UIImageView!
    
    @IBOutlet weak var checkButton: UIButton!
    
    var selectedUserName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.txt_MobEmail.delegate = self
        
        txt_MobEmail.attributedPlaceholder = NSAttributedString(string: "Enter Your Email or Mobile",attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 123/255, green: 152/255, blue: 255/255, alpha: 1)])
        
        let image = UIImage(named:"SubmitBtnDisable") as! UIImage
        checkButton.setBackgroundImage(image, for: UIControlState.normal)
        checkButton.isEnabled = false
        //default user name
        
        //imageViewCheck.image = UIImage(named:"SubmitBtnDisable")
        
        
        if selectedUserName.isEmpty{
            
            print("do nothing")
        }else{
            
            let image2 = UIImage(named:"SubmitBtnEnable") as! UIImage
            self.txt_MobEmail.text = "\(selectedUserName)"
            checkButton.setBackgroundImage(image2, for: UIControlState.normal)
            checkButton.isEnabled = true
        }
        
    }
    
    @IBAction func textFieldEditingDidChanged(_ sender: Any) {
        
        print("TextFieldDidChanged:\(String(describing: txt_MobEmail.text))")
        var dataType = checkDataType(text: txt_MobEmail.text!)
        
        print("DataType = \(dataType)")
        
        if dataType == DataType.Email{
            print("true")
            
            let image = UIImage(named:"SubmitBtnEnable") as! UIImage
            checkButton.setBackgroundImage(image, for: UIControlState.normal)
            checkButton.isEnabled = true
            // imageViewCheck.image = UIImage(named:"SubmitBtnEnable")
            
            // let Login2VC:LogIn_2_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_2_VC") as! LogIn_2_VC
            
            
            // present(Login2VC, animated: true, completion: nil)
            //self.navigationController?.pushViewController(RegVC, animated: true)
            
        }
        else if dataType == DataType.Number{
            print("true")
            let image = UIImage(named:"SubmitBtnEnable") as! UIImage
            checkButton.setBackgroundImage(image, for: UIControlState.normal)
            checkButton.isEnabled = true
            //imageViewCheck.image = UIImage(named:"SubmitBtnEnable")
            //let Login2VC:LogIn_2_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_2_VC") as! LogIn_2_VC
            
            
            //present(Login2VC, animated: true, completion: nil)
        }else if dataType == DataType.Other{
            print("false")
            let image = UIImage(named:"SubmitBtnDisable") as! UIImage
            checkButton.setBackgroundImage(image, for: UIControlState.normal)
            checkButton.isEnabled = false
            
            //imageViewCheck.image = UIImage(named:"SubmitBtnDisable")
            // imageViewCheck.image = UIImage(named:"SubmitBtnDisable")
        }
        //        if validatePassword(text: txt_MobEmail.text!){
        //            print("true")
        //        }else{
        //            print("false")
        //        }
        
    }
    
    @IBAction func checkButtonTapped(_ sender: Any) {
        let Login2VC:LogIn_2_VC = storyboard?.instantiateViewController(withIdentifier: "LogIn_2_VC") as! LogIn_2_VC
        
        
        Login2VC.txtEmail = txt_MobEmail.text!
        present(Login2VC, animated: true, completion: nil)
        let setUsername:UserDefaults = UserDefaults.standard
        
        setUsername.removeObject(forKey: "username")
        print("Username:\(txt_MobEmail.text!)")
        setUsername.set(txt_MobEmail.text, forKey: "username")
        
        
        
    }
    @IBAction func createAccBtn_action(_ sender: Any) {
        let RegVC:Register_1_VC = storyboard?.instantiateViewController(withIdentifier: "Register_1_VC") as! Register_1_VC
        //  self.navigationController?.pushViewController(RegVC, animated: true)
        
        present(RegVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func backBtn_action(_ sender: Any) {
        let UserGuideWithFBLogIn:UserGuideWithFBLogIn = storyboard?.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
        
        present(UserGuideWithFBLogIn, animated: true, completion: nil)
        // self.navigationController?.pushViewController(FBVC, animated: true)
    }
    
    
    
    func isValidEmail(email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: txt_MobEmail.text!)
        return result
    }
    
    func isValidPhone(phone: String) -> Bool {
        
        let PHONE_REGEX = "^[7-9][0-9]{9}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: txt_MobEmail.text!)
        return result
        
    }
    
    func checkDataType(text:String)->DataType{
        
        //if let intVal = Int(text){
        if isValidPhone(phone: text){
            
            return DataType.Number
        }else if isValidEmail(email: text){
            return DataType.Email
            
        }else{
            
            return DataType.Other
        }
        
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}



