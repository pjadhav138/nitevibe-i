//
//  LaunchVideoViewController.swift
//  Nite_Vibe
//
//  Created by Shraddha Mahajan on 21/06/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
class LaunchVideoViewController: UIViewController {
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        
        let checkLoginflag =  defaults.value(forKey: "loginflag") as? String
        
        print("checkLoginflag 121 :\(checkLoginflag)")
        
        
        if checkLoginflag == "1"{
            print("checkLoginflag if already login:\(checkLoginflag)")
            
            let defaults = UserDefaults.standard
            
            let subAdminFlag  = defaults.value(forKey: "subadmin") as? String
            
            if subAdminFlag == "1"{
                let userProfileStoryBoard : UIStoryboard = UIStoryboard(name: "UserProfileInfo", bundle: nil)
                
                let clubSideHomeVC = userProfileStoryBoard.instantiateViewController(withIdentifier: "PieChartViewController") as! PieChartViewController
                
                self.present(clubSideHomeVC, animated: false, completion: nil)
            }else{
                let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
                
                self.present(BaseController, animated: true, completion: nil)
            }
            
            
        }else if checkLoginflag == "0"{
            print("checkLoginflag if not already login:\(checkLoginflag)")
            
            try? VideoBackground.shared.play(view: view, videoName: "720", videoType: "mp4")
            
            navigationController?.isNavigationBarHidden = true
            
            timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.goToMainScreen), userInfo: nil, repeats: true)
            
            
            
        }else{
            
            
            print("current facebook token\(FBSDKAccessToken.current())")
            
            //Checks for facebook login
            if (FBSDKAccessToken.current() != nil) {
                
                //if already login from facebook
                
                let BaseController:BaseTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! BaseTabBarController
                
                
                self.present(BaseController, animated: true, completion: nil)
                
            }else{
                
                //If not login via facebook
//                let size = CGSize(width:, height: )
//                let View = UIView(frame: CGRect(x: 0, y: 150, width:  self.view.frame.width, height: self.view.frame.height/2))
                try? VideoBackground.shared.play(view: view, videoName: "720", videoType: "mp4")
                
                navigationController?.isNavigationBarHidden = true
                
                timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.goToMainScreen), userInfo: nil, repeats: true)
                
            }
            
        }
        
        
        
    }
    
    @objc func goToMainScreen() {
        
        timer.invalidate()
        
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "UserGuideWithFBLogIn") as! UserGuideWithFBLogIn
        
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    

    

}
