//
//  SupportViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 04/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func liveitTonightButtonTapped(_ sender: Any) {
        
        self.sendEmailToliveitTonight()
        
        
    }
    
    @IBAction func supportMailButtonTapped(_ sender: Any) {
        
      self.sendMailToSupport()
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
        
        let settingVC = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        present(settingVC, animated: true, completion: nil)
    }
    
    func sendEmailToliveitTonight() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as! MFMailComposeViewControllerDelegate as! MFMailComposeViewControllerDelegate
        mail.setToRecipients(["liveittonight@nitevibe.in"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }

        
    }
    
    func sendMailToSupport(){
        
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as! MFMailComposeViewControllerDelegate
            mail.setToRecipients(["support@nitevibe.in"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
        
    }

    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
