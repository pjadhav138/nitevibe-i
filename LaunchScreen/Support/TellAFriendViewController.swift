//
//  TellAFriendViewController.swift
//  Nite_Vibe
//
//  Created by Vinayak Bhor on 04/08/18.
//  Copyright © 2018 Appmonks. All rights reserved.
//

import UIKit

class TellAFriendViewController: UIViewController {

    
    @IBOutlet weak var shareButtonTapped: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shareButtonTapped.layer.cornerRadius = 8
        shareButtonTapped.clipsToBounds = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Swipe", bundle: nil)
        
        let settingVC = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        present(settingVC, animated: true, completion: nil)
        
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
